
var _setMessage = function (type, content) {
    var template = '<div class="alert alert-' + type + ' alert-dismissable">';
    template += '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
    template += content;
    template += '</div>';
    $('.box-message').html('');

    $('.box-message').prepend(template);
    $('html, body').animate({
        scrollTop: $(".wrapper-content").offset().top
    }, 300);

    if ($('#loadingModal').length)
        $('#loadingModal').modal('hide');
}

var _setToast = function (positionClass) {
    if(positionClass=='') {
        positionClass = "toast-top-center";
    }
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "progressBar": true,
        "preventDuplicates": false,
        "positionClass": positionClass,
        "onclick": null,
        "showDuration": "400",
        "hideDuration": "500",
        "timeOut": "1000",
        "extendedTimeOut": "500",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };
}

$( "#mydiv" ).hasClass( "foo" )

$(document).ready(function () {
    
    if($('#second-level li').hasClass('active')){
        console.log('meron');
        $( this ).parent().addClass('active');
        $("#second-level").addClass('in')
    }


});
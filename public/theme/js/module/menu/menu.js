$(document).ready(function () {

	var menuForm = $('form[name=menu-form]');

	var valConfig = {
        message: 'This value is not valid',
        fields: {
            menu_id: {
                validators: {
                        stringLength: {
                        min: 1,
                    },
                        notEmpty: {
                        message: 'Id is required'
                    }
                }
            },
            menu_name: {
                validators: {
                     stringLength: {
                        min: 2,
                    },
                    notEmpty: {
                        message: 'Menu Name is required'
                    }
                }
            } ,
            menu_name: {
                validators: {
                     stringLength: {
                        min: 2,
                    },
                    notEmpty: {
                        message: 'Menu Name is required'
                    }
                }
            } 
        }
    };
        
    menuForm.bootstrapValidator(valConfig);

    if(document.getElementById("menu") != null) {
        var table = $('#menu').DataTable({
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            processing: true,
            serverSide: false,
            "order": [[ 4, "asc" ]],
            rowReorder: true,
            buttons: [
                
            ],
            "ajax": {
                "url": baseUrl + "settings/getMenuList",
                "type": "POST"
            },
            "aoColumns": [
                {"sName": "menu_id","sClass": "", "bVisible": false},
                {"sName": "menu_name","sClass": "", "bVisible": true},
                {"sName": "icon","sClass": "", "bVisible": true,
                    "mRender": function ( data, type, row, meta )
                    {
                        return "<i class='fa "+ data +"'></i> " + data;
                
                    }
                },
                {"sName": "is_enabled","sClass": "text-center", "bVisible": true,
                    "mRender": function ( data, type, row, meta )
                    {
                        var checked = '';
                        if(data != null) 
                        {
                            if(data == 1) 
                                checked = 'Yes';
                            else 
                                checked = 'No';
                            
                            return checked;
                        }
                        return "";
                
                    }
                },
                {"sName": "sort","sClass": "text-center", "bVisible": true},
                {"sName": "id","sClass": "text-center", "bSortable": false, "bVisible": true,
                    "mRender": function ( data, type, row, meta )
                    {
                        var btn = ' <button type="button" class="btn btn-sm btn-primary btn-menu-edit" data-id="' + data + '"><i class="fa fa-pencil" aria-hidden="true"></i> Edit</button>';
                        return btn;
                    }
                }
            ]
        });
    }

    if(document.getElementById("submenu") != null) {

        var tableSubmenu = $('#submenu').DataTable({
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            processing: true,
            serverSide: false,
            "order": [[ 1, "asc" ]],
            rowReorder: true,
            buttons: [
               
            ],
            "ajax": {
                "url": baseUrl + "settings/getSubMenuList",
                "type": "POST"
            },
            "aoColumns": [
                {"sName": "id","sClass": "", "bVisible": false},
                {"sName": "menu_name","sClass": "", "bVisible": true},
                {"sName": "menu","sClass": "", "bVisible": true},
                {"sName": "url","sClass": "", "bVisible": true},
                {"sName": "icon","sClass": "", "bVisible": true,
                    "mRender": function ( data, type, row, meta )
                    {
                        return "<i class='fa "+ data +"'></i> " + data;
                
                    }
                },
                {"sName": "is_enabled","sClass": "text-center", "bVisible": true,
                    "mRender": function ( data, type, row, meta )
                    {
                        var checked = '';
                        if(data != null) 
                        {
                            if(data == 1) 
                                checked = 'Yes';
                            else 
                                checked = 'No';
                            
                            return checked;
                        }
                        return "";
                
                    }
                },
                {"sName": "sort","sClass": "text-center", "bVisible": true},
                {"sName": "idx","sClass": "text-center", "bSortable": false, "bVisible": true,
                    "mRender": function ( data, type, row, meta )
                    {
                        var btn = ' <button type="button" class="btn btn-sm btn-primary btn-submenu-edit" data-id="' + data + '"><i class="fa fa-pencil" aria-hidden="true"></i> Edit</button>';
                        return btn;
                    }
                }
            ]
        });
    }

    
});

$(document).on('click', '.btn-menu-edit', function(){
    var btn = $(this);
    $('#loadingModal').modal('show');
    btn.button('loading');
    var id = btn.data('id');
    window.location.href = baseUrl+"settings/menu/edit/"+id;
   });

$(document).on('click', '.btn-submenu-edit', function(){
    var btn = $(this);
    $('#loadingModal').modal('show');
    btn.button('loading');
    var id = btn.data('id');
    window.location.href = baseUrl+"settings/submenu/edit/"+id;
});

   $(document).on('click', '.btn-menu-back', function(){
    var l = Ladda.create(this);
    l.start();
    $('#loadingModal').modal('show');
    window.location.href = baseUrl+"settings/menu/";
   });

$(document).on('click', '.btn-submenu-cancel', function(){
    var btn = $(this);
    $('#loadingModal').modal('show');
    btn.button('loading');
    window.location.href = baseUrl+"settings/submenu/";
});



$(document).on('click', '.btn-add-main-menu', function(){
    var btn = $(this);
    $('#loadingModal').modal('show');
    btn.button('loading');
    window.location.href = baseUrl+"settings/menu/create";
   });

$(document).on('click', '.btn-add-sub-menu', function(){
    var btn = $(this);
    $('#loadingModal').modal('show');
    btn.button('loading');
    window.location.href = baseUrl+"settings/submenu/create";
});


   $(document).on('click', '.btn-save', function() {

     var l = Ladda.create(this);

    l.start();

    var form = $('form[name=menu-form]');
    var validator = form.data('bootstrapValidator');
    validator.validate();
    if (validator.isValid()) {
        $.ajax({
            type: 'POST',
            url: baseUrl+'settings/menu_store',
            data:  form.serialize(),
            dataType: 'json',
            success:function(data){
                _setToast();

                if(data.success) {
                    toastr.options.onHidden = function() { 
                        window.location.href = data.url;
                    };
                    toastr.success('Please wait, redirect the page','Successful');
                   
                } else {
                    toastr.error('Unable to submit your request','Oooooppss');
                    l.stop();
                }
            }
        });
    }  else {
        l.stop();
    }
});

$(document).on('click', '.btn-submenu-save', function() {
    var btn = $(this);
    var form = $('form[name=menu-form]');
    btn.button('loading');
    var validator = form.data('bootstrapValidator');
    validator.validate();
    if (validator.isValid()) {
        $.ajax({
            type: 'POST',
            url: baseUrl+'settings/submenu_store',
            data:  form.serialize(),
            dataType: 'json',
            success:function(data){
                toastr.options = {
                  "closeButton": true,
                  "debug": true,
                  "progressBar": true,
                  "preventDuplicates": false,
                  "positionClass": "toast-top-right",
                  "onclick": null,
                  "showDuration": "400",
                  "hideDuration": "1000",
                  "timeOut": "1500",
                  "extendedTimeOut": "1000",
                  "showEasing": "swing",
                  "hideEasing": "linear",
                  "showMethod": "fadeIn",
                  "hideMethod": "fadeOut"
                };
                
                if(data.success) {
                    toastr.options.onHidden = function() { 
                        window.location.href = data.url;
                    };
                    toastr.success('Please wait, redirect the page','Successful');
                    //btn.button('reset');
                } else {
                    toastr.error('Unable to submit your request','Oooooppss');
                    btn.button('reset');
                }

            }
        });
    }  else {
        btn.button('reset');
    }
});
$(document).ready(function() {
    
    if ($("#form_upload").length > 0) {
        Dropzone.autoDiscover = false;
        $(".upload-file-btn").dropzone({ 
            url: "/transaction/upload",
            addRemoveLinks: true,
            clickable: ".upload-file-btn",
            init: function() {
                this.on("error", function(errorMessage) { 
                    $(".dz-error-mark svg").css("background", "red");
                    $(".dz-success-mark svg").css("border-radius", "50%");
                    $(".dz-success-mark").css("display", "none");
                });
                this.on("success", function(serverResponse) { 
                    $(".dz-success-mark svg").css("background", "green");
                    $(".dz-success-mark svg").css("border-radius", "50%");
                    $(".dz-error-mark").css("display", "none");
                  // do whatever you need to do...
                });
                this.on("addedfile", function(file) {
                    let dis = this;
                    file.previewElement.addEventListener("click", function() {
                      dis.removeFile(file);
                    });
                });
            },
            accept: function (file, done) {
                $('.upload-file-btn').html('<i class="fa fa-refresh fa-spin"></i> Uploading file');
                done();
            },
            success: function(file, responseText) {
                var response = JSON.parse(responseText);
                var html = response.filename + '  <a class="remove-file-btn" href="javascript:void(0);" ><i class="fa fa-trash-o" aria-hidden="true"></i> Remove</a>';
                $('input[name=update-status-attached-file]').val(response.filename);
                $('.upload-container').html(html);
            }
        });    
    }
    
    if ($('.form-select-multiple').length > 0) {
        $('.form-select-multiple').select2();
    }

    if ($("#edit_form_upload").length > 0) {
        Dropzone.autoDiscover = false;
        $("#edit_form_upload").dropzone({ 
            url: "/transaction/upload",
            addRemoveLinks: true,
            init: function() {
                var filename = $("input[name=trans-document]").val();
                var size = $("input[name=trans-document-size]").val();
                var type = $("input[name=trans-document-type]").val();
                // var path = $("input[name=trans-document-path]").val();
                var mockFile = { name: filename, size: size, type: type };       
                var path = "uploads/test/"+filename;
                console.log(path);
                this.options.addedfile.call(this, mockFile);
                // this.options.thumbnail.call(this, mockFile, path);
                $(".dz-success-mark svg").css("background", "green");
                $(".dz-success-mark svg").css("border-radius", "50%");
                $(".dz-error-mark").css("display", "none");
                
                this.on("error", function(errorMessage) { 
                    $(".dz-error-mark svg").css("background", "red");
                    $(".dz-success-mark svg").css("border-radius", "50%");
                    $(".dz-success-mark").css("display", "none");
                });
                this.on("success", function(serverResponse) { 
                    $(".dz-success-mark svg").css("background", "green");
                    $(".dz-success-mark svg").css("border-radius", "50%");
                    $(".dz-error-mark").css("display", "none");
                  // do whatever you need to do...
                });
                this.on("addedfile", function(file) {
                    let dis = this;
                    file.previewElement.addEventListener("click", function() {
                      dis.removeFile(file);
                    });
                });
            },
            success: function(file, responseText) {
                var response = JSON.parse(responseText);
                console.log(response);
                $("input[name=trans-document]").val(response.filename);
            },
        });   
    }

    if ($('.selet-tags').length > 0) {
        $('.selet-tags').select2({
            tags: true
        });
    }
    

    if ($("form[name=edit-entry-form]").length > 0) {
        if ($('.select-shipper').hasClass("select2-hidden-accessible")) {
            $('.select-shipper').select2().trigger('change');
        }
        if ($('.select-consignee').hasClass("select2-hidden-accessible")) {
            $('.select-consignee').select2().trigger('change');
        }
        if ($('.select-agent').hasClass("select2-hidden-accessible")) {
            $('.select-agent').select2().trigger('change');
        }
        // if ($('.select-agent1-notify').hasClass("select2-hidden-accessible")) {
        //     $('.select-agent1-notify').select2().trigger('change');
        // }
        // if ($('.select-agent2-notify').hasClass("select2-hidden-accessible")) {
        //     $('.select-agent2-notify').select2().trigger('change');
        // }
    }

  

    if ($('.date').length > 0) {
        $('.date').daterangepicker({
                singleDatePicker: true,
                showDropdowns: true,
                minYear: 1901,
                maxYear: 2100
            });
    }

    if ($('.status-date').length > 0) {
        $('.status-date').daterangepicker({
            locale: {
                format: 'MM/DD/YYYY h:mm A'
            },
            singleDatePicker: true,
            showDropdowns: true,
            timePicker: true,
            minYear: 1901,
            maxYear: 2100
        });
    }


    if ($('#transaction_list').length > 0) {
        $('#transaction_list').DataTable({
            "bFilter": true,
            "sAjaxSource": '/transaction/list',
            "sPaginationType": "full_numbers",
            "iDisplayLength": 25,
            "responsive": true,
            "aLengthMenu": [
                [1,10, 15, 25, 35, 50, 100, -1],
                [1,10, 15, 25, 35, 50, 100, "All"]
            ],
            "autoWidth": false,

            "oLanguage": {
                "sProcessing": "loading..."
            },
            'columnDefs': [
                // {
                //     'targets': 2,
                //     'searchable': false,
                //     'orderable': false,
                //     'className': 'dt-body-center',
                //     'render': function(data, type, full, meta) {
                //         return '<i class="fa '+data+'"></i> '+data;
                //     }
                // },
                {
                    'targets': 3,
                    'searchable': false,
                    'className': 'dt-body-center',
                    'render': function(data, type, full, meta) {
                        if(data == "P") {
                            return '<span class="badge badge-pill badge-success">Posted</span>';
                        } else {
                            return '<span class="badge badge-pill badge-warning">Saved</span>'
                        }
                    }
                },
                {
                    'targets': 4,
                    'className': 'dt-body-center',
                    'render': function(data, type, full, meta) {
                        if(data == "IS") {
                            return 'Import Sea';
                        } else if (data == "ES") {
                            return 'Export Sea';
                        } else if (data == "IA") {
                            return 'Import Air';
                        } else if (data == "EA") {
                            return 'Export Air';
                        } else {
                            return ''
                        }
                    }
                },
                {
                    'targets': 5,
                    'className': 'dt-body-center',
                    'render': function(data, type, full, meta) {
                        return data;
                    }
                },
                {
                    'targets': 6,
                    'className': 'dt-body-center',
                    'render': function(data, type, full, meta) {
                        return data;
                    }
                },
                {
                    'targets': 7,
                    'className': 'dt-body-center',
                    'render': function(data, type, full, meta) {
                        return data;
                    }
                },
                {
                    'targets': 8,
                    'className': 'dt-body-center',
                    'render': function(data, type, full, meta) {
                        return data;
                    }
                },
                {
                    'targets': 9,
                    'className': 'dt-body-center',
                    'render': function(data, type, full, meta) {
                        console.log(full)
                        let html = "<a href='/transaction/"+data+"/view' class='m-1 btn btn-primary btn-sm'><i class='fa fa-eye'></i> </a>";
                        if (full[3] == "S")
                            html += "<a href='/transaction/"+data+"/edit' class='m-1 btn btn-warning btn-sm'><i class='fa fa-pencil'></i> </a>";
                        html += "<a href='javascript:void(0)' data-trxn='"+data+"' class='btn-delete-transaction m-1 btn btn-danger btn-sm'><i class='fa fa-trash'></i> </a>";
                        return html;
                    }
                }
            ],
            "fnServerParams": function(aoData) {
                // aoData.push({
                //         "name": "batch_no",
                //         "value": $('#batch_no').val()
                //     }, {
                //         "name": "store_name",
                //         "value": $('#store_name').val()
                //     }, {
                //         "name": "status",
                //         "value": $('#status').val()
                //     }

                // );
            },

            'fnServerData': function(sSource, aoData, fnCallback) {
                $.ajax({
                    'dataType': 'json',
                    'type': 'POST',
                    'url': sSource,
                    'data': aoData,
                    'success': fnCallback
                });
            },

        });
    }
});

$(document).on('change', '.select-shipper', function(){
    var id = $(this).val();
    var address = [];
    var obj = $('input[name=shippers_obj]').val();
    var shippers = JSON.parse(obj);
    var partner = shippers.find((o) => o.partner_code === id);
    var sAddress = partner.address1 + partner.address2 + partner.address3
    $('textarea[name="shipper-address"]').val(sAddress);
});

$(document).on('change', '.select-consignee', function(){
    var id = $(this).val();
    var address = [];
    var obj = $('input[name=consignee_obj]').val();
    var consignees = JSON.parse(obj);
    var partner = consignees.find((o) => o.partner_code === id);
    var sAddress = partner.address1 + partner.address2 + partner.address3
    $('textarea[name="consignee-address"]').val(sAddress);
});

$(document).on('change', '.select-agent', function(){
    var id = $(this).val();
    var obj = $('input[name=agent_obj]').val();
    var agents = JSON.parse(obj);
    var partner = agents.find((o) => o.partner_code === id);
    var sAddress = partner.address1 + partner.address2 + partner.address3
    $('textarea[name="agent-address"]').val(sAddress);
});
$(document).on('change', '.select-agent-notify', function(){
    var id = $(this).val();
    var obj = $('input[name=agent_obj]').val();
    var agents = JSON.parse(obj);
    var partner = agents.find((o) => o.partner_code === id);
    var sAddress = partner.address1 + partner.address2 + partner.address3
    $('textarea[name="notify-address"]').val(sAddress);
});
$(document).on('change', '.select-agent-third', function(){
    var id = $(this).val();
    var obj = $('input[name=agent_obj]').val();
    var agents = JSON.parse(obj);
    var partner = agents.find((o) => o.partner_code === id);
    var sAddress = partner.address1 + partner.address2 + partner.address3
    $('textarea[name="third-party-address"]').val(sAddress);
});

function getPartner(id, obj) {
    return obj.filter(function(item){
        return item.partner_code == id
    });
}

$(document).on('click', '.btn-add-item', function(){
    var element_length = $('.item-row').length;
    var clone = $('.item-row').first().clone();

    clone.find('input').each(function() {
        $(this).val("");
        this.id = this.id + "-" + element_length
    });
    clone.find('textarea').each(function() { 
        $(this).val("");
    });
    clone.find('.attr-num').html(element_length + 1)
    clone.appendTo(".attr-container");
});

$(document).on('click', '.btn-remove-item', function(){ 
    var element_length = $('.item-row').length;
    if (element_length > 1) {
        $('.item-row').last().remove();
    }
});

$(document).on('change', 'input[name=direct]', function(){
    if ($(this).prop('checked')) {
        $('input[name=house-bill-no]').val("");
        $('input[name=house-bill-no]').prop('disabled', true);
    } else {
        $('input[name=house-bill-no]').prop('disabled', false);
    }
});

$(document).on('click', '.btn-post-entry', function(){
    var l = Ladda.create(this);
    l.start();
    var form = $('form[name=entry-form]');
    var config = {
        url: '/transaction/store',
        type: 'post',
        dataType: 'json',
        data: form.serialize() + '&posted=true'
   }
   do_submit(config, l)
});
$(document).on('click', '.btn-save-entry', function(){
    var l = Ladda.create(this);
    l.start();
    var form = $('form[name=entry-form]');
    var config = {
        url: '/transaction/store',
        type: 'post',
        dataType: 'json',
        data: form.serialize() + '&posted=false'
   }
   do_submit(config, l)
});

function do_submit(config, l) {
    var ajax = $.ajax(config);
    ajax.done(function(response) {
        if (response.success) {
            toastr.options.onHidden = function() { 
                l.stop();
                window.location = "/transaction/index";
            };
            toastr.success(response.msg, "Successful")
        }
    })
}

$(document).on('click', '.btn-edit-post-entry', function(){
    var l = Ladda.create(this);
    l.start();
    var form = $('form[name=edit-entry-form]');
    var config = {
        url: '/transaction/update',
        type: 'post',
        dataType: 'json',
        data: form.serialize() + '&posted=true'
   }
   do_submit(config, l)
});
$(document).on('click', '.btn-edit-save-entry', function(){
    var l = Ladda.create(this);
    l.start();
    var form = $('form[name=edit-entry-form]');
    var config = {
        url: '/transaction/update',
        type: 'post',
        dataType: 'json',
        data: form.serialize() + '&posted=false'
   }
   do_submit(config, l)
});

$(document).on('click', '.btn-update-status', function(){
    $("#trans-status-modal").modal("show");
});

$(document).on('click', '.btn-save-trans-status', function(){
    var l = Ladda.create(this);
    l.start();
    var trans_id = $('input[name="update-trans-status-code"]').val();
    var status = $('select[name=update-trans-status]').val();
    var comment = $('textarea[name=update-trans-comment]').val();
    var datetime = $('input[name="status-update-datetime"]').val();

    var config = {
        url: '/transaction/update_status',
        type: 'post',
        dataType: 'json',
        data: {
            trxn_id: trans_id, 
            status: status, 
            comment: comment,
            datetime: datetime
        }
   }

   var ajax = $.ajax(config);

   ajax.done(function(response) {
        if (response.success) {
            toastr.options.onHidden = function() { 
                l.stop();
                location.reload();
            };         
        }
        toastr.success(response.msg, "Successful")
    })

});

$('#trans-status-modal').on('shown.bs.modal', function () {
    $('select[name="update-trans-status"]').select2({
        dropdownCssClass: 'increasezindex',
        dropdownParent: $('#trans-status-modal')
    });

    $('.modal-date').daterangepicker({
        locale: {
            format: 'MM/DD/YYYY h:mm A'
        },
        singleDatePicker: true,
        showDropdowns: true,
        timePicker: true,
        minYear: 1901,
        maxYear: 2100
    });
});

$(document).on('click', '.btn-reset-entry', function() {
    $('form[name=entry-form]')[0].reset();
});


$(document).on("change", "select[name=origin-destination]", function(){
    var select_country = $(this);
    var select_port = $("select[name=port-of-loading]");
    
    show_loading(select_port);

    var config = {
        url: '/transaction/get_port',
        type: 'post',
        dataType: 'json',
        data: {country: select_country.val()}
   }

   update_port_options(select_port, config);

});

$(document).on("change", "select[name=final-destination]", function(){
    var select_country = $(this);
    var select_port = $("select[name=port-of-discharge");
    
    show_loading(select_port);

    var config = {
        url: '/transaction/get_port',
        type: 'post',
        dataType: 'json',
        data: {country: select_country.val()}
   }

   update_port_options(select_port, config);

});

var update_port_options = (element, config) => {
    var ajax = $.ajax(config);

    ajax.done(function(response) {
        element.html('');
        if (response.success) {
            element.select2({data: response.ports});
            element.select2().trigger('change');
        }
        element.prop("disabled", false)
    })
}

var show_loading = (element) => {
    element.prop("disabled", true);
    element.select2({data: [{id: "", text: "Loading..."}]});
}

$(document).on("click", ".btn-add-attr", function() {
    $("#trans_attr_modal").modal("show"); 
});

$(document).on("click", ".btn-temp-add", function() {

    var form_values = $("form[name=temp-form-attr]").serializeArray();

    var temp_row = "";

    temp_row += "<tr>";
    $.each(form_values, function(k, v) {
        var input_name = v.name.split("_");
        temp_row += "<td>";    
        temp_row += v.value;
        temp_row += "<input type='hidden' name='"+input_name[1]+"[]' value='"+v.value+"' />";
        temp_row += "</td>";    
    });

    temp_row += "<td><button type='button' class='btn btn-sm btn-danger btn-remove-row'><i class='fa fa-trash'></i> Delete</button></td>";
    temp_row += "</tr>";

    $('table[id=transaction_attr] > tbody').append(temp_row);

    $("form[name=temp-form-attr]")[0].reset();
    $("#trans_attr_modal").modal("hide"); 
});

$(document).on("click", ".btn-add-chrg", function() {
    $("#trans_chrg_modal").modal("show"); 
});

$(document).on("click", ".btn-temp-add-chrg", function() {

    var form_values = $("form[name=temp-form-chrg]").serializeArray();

    var temp_row = "";

    temp_row += "<tr>";
    $.each(form_values, function(k, v) {
        var input_name = v.name.split("_");
        temp_row += "<td>";    
        temp_row += v.value;
        temp_row += "<input type='hidden' name='"+input_name[1]+"[]' value='"+v.value+"' />";
        temp_row += "</td>";    
    });

    temp_row += "<td><button type='button' class='mr-1 btn btn-sm btn-warning btn-edit-charge-row'><i class='fa fa-pencil'></i> Edit</button><button type='button' class='btn btn-sm btn-danger btn-remove-row'><i class='fa fa-trash'></i> Delete</button></td>";
    temp_row += "</tr>";

    $('table[id=trxn_chrg] > tbody').append(temp_row);

    $("form[name=temp-form-chrg]")[0].reset();
    $("#trans_chrg_modal").modal("hide"); 
});

$(document).on("click", ".btn-add-mtrl", function() {
    $("#trans_mtrl_modal").modal("show"); 
});

$(document).on("click", ".btn-temp-add-mtrl", function() {
    var form_values = $("form[name=temp-form-mtrl]").serializeArray();

    var temp_row = "";

    temp_row += "<tr>";
    $.each(form_values, function(k, v) {
        var input_name = v.name.split("_");
        temp_row += "<td>";    
        temp_row += v.value;
        temp_row += "<input type='hidden' name='"+input_name[1]+"[]' value='"+v.value+"' />";
        temp_row += "</td>";    
    });

    temp_row += "<td>";
    temp_row += '<button type="button" class="mr-1 btn btn-sm btn-warning btn-edit-mtrl-row"><i class="fa fa-pencil"></i> Edit</button>';
    temp_row += "<button type='button' class='btn btn-sm btn-danger btn-remove-row'><i class='fa fa-trash'></i> Delete</button></td>";
    temp_row += "</tr>";

    $('table[id=trxn_mtrl] > tbody').append(temp_row);

    $("form[name=temp-form-mtrl]")[0].reset();
    $("#trans_mtrl_modal").modal("hide"); 
});
$(document).on("click", ".btn-add-cargo", function() {
    $("#trans_cargo_modal").modal("show"); 
});

$("#trans_cargo_modal").on('shown.bs.modal', function () {
    // $('select[name="temp_cargo-truck-type"]').select2({
    //     dropdownCssClass: 'increasezindex',
    //     dropdownParent: $('#trans_cargo_modal')
    // });
});

$(document).on("click", ".btn-temp-add-cargo", function() {
    var form_values = $("form[name=temp-form-cargo]").serializeArray();

    var temp_row = "";

    temp_row += "<tr>";
    $.each(form_values, function(k, v) {
        var input_name = v.name.split("_");
        temp_row += "<td>";    
        temp_row += v.value;
        temp_row += "<input type='hidden' name='"+input_name[1]+"[]' value='"+v.value+"' />";
        temp_row += "</td>";    
    });

    temp_row += "<td>";
    temp_row += '<button type="button" class="mr-1 btn btn-sm btn-warning btn-edit-cargo-row"><i class="fa fa-pencil"></i> Edit</button>';
    temp_row += "<button type='button' class='btn btn-sm btn-danger btn-remove-row'><i class='fa fa-trash'></i> Delete</button></td>";
    temp_row += "</tr>";

    $('table[id=trxn_cargo] > tbody').append(temp_row);

    $("form[name=temp-form-cargo]")[0].reset();
    $("#trans_cargo_modal").modal("hide"); 
});
$(document).on("click", ".btn-add-cont", function() {
    $("#trans_cont_modal").modal("show"); 
});

$(document).on("click", ".btn-temp-add-cont", function() {      
    var form_values = $("form[name=temp-form-cont]").serializeArray();

    console.log(form_values)
    var temp_row = "";

    temp_row += "<tr>";

    $.each(form_values, function(k, v) {
        var input_name = v.name.split("_");
        temp_row += "<td>";    
        temp_row += v.value;
        temp_row += "<input type='hidden' name='"+input_name[1]+"[]' value='"+v.value+"' />";
        temp_row += "</td>";    
    });

    temp_row += "<td>";
    temp_row += '<button type="button" class="mr-1 btn btn-sm btn-warning btn-edit-cont-row"><i class="fa fa-pencil"></i> Edit</button>';
    temp_row += "<button type='button' class='btn btn-sm btn-danger btn-remove-row'><i class='fa fa-trash'></i> Delete</button></td>";
    temp_row += "</tr>";

    $('table[id=trxn_cont] > tbody').append(temp_row);

    $("form[name=temp-form-cont]")[0].reset();
    $("#trans_cont_modal").modal("hide"); 
});

$(document).on('click', '.btn-remove-row', function(){
    $(this).parent().parent().remove();
});

$(document).on('click', '.remove-file-btn', function(){
    var html = '<a class="upload-file-btn" href="javascript:void(0);" ><i class="fa fa-paperclip" aria-hidden="true"></i> Attach a file</a>';
    $('input[name=update-status-attached-file]').val("");
    $('.upload-container').html(html);
});

$(document).on('click', '.btn-submit-update-status', function(){
    var l = Ladda.create(this);
    l.start();

    var trans_id = $('input[name="transaction-id"]').val();
    var status = $('select[name=update-status]').val();

    if (status == "" || status == null) {
        toastr.options.onHidden = function() { 
            l.stop();
        }; 
        toastr.error("Status is required.", "Error",{timeOut: 1000});
        return;
    }
    var datetime = $('input[name=update-status-date]').val();
    var comment = $('textarea[name="update-status-description"]').val();
    var file = $('input[name="update-status-attached-file"]').val();

    var config = {
        url: '/transaction/update_status',
        type: 'post',
        dataType: 'json',
        data: {
            trxn_id: trans_id, 
            status: status, 
            comment: comment,
            datetime: datetime,
            file: file
        }
   }

   var ajax = $.ajax(config);

   ajax.done(function(response) {
        if (response.success) {
            toastr.options.onHidden = function() { 
                l.stop();
                location.reload();
            };         
        }
        toastr.success(response.msg, "Successful")
    })
});

$('#transaction_tabs a').on('click', function (e) {
    e.preventDefault()
    $(this).tab('show')
})

$(document).on('click', '.btn-delete-transaction', function() {
    var value = $(this).data('trxn');
    $("input[name=del-trans-data]").val(value);
    $("#trans_del_modal").modal('show');
});

$(document).on('click', '.btn-del-traxn', function() {
    var l = Ladda.create(this);
    l.start();
    var value = $("input[name=del-trans-data]").val();
    var config = {
        url: '/transaction/delete_trans',
        type: 'post',
        dataType: 'json',
        data: {
            trxn: value
        }
    }

    var ajax = $.ajax(config);

    ajax.done(function(response) {
        if (response.success) {
            toastr.options.onHidden = function() { 
                l.stop();
                location.reload();
            };         
        }
        toastr.success(response.msg, "Successful")
    })
});


$(document).on('click', '.btn-edit-charge-row', function(){
    $(this).closest("tr").addClass('edit-row');

    var code = $(this).closest("tr").find('td:nth-child(1)').find('input').val();
    var desc = $(this).closest("tr").find('td:nth-child(2)').find('input').val();
    var amnt = $(this).closest("tr").find('td:nth-child(3)').find('input').val();
    var curr = $(this).closest("tr").find('td:nth-child(4)').find('input').val();
    var pval = $(this).closest("tr").find('td:nth-child(5)').find('input').val();
   
    $('input[name=edit_charge-code]').val(code);
    $('textarea[name=edit_charge-desc]').val(desc);
    $('input[name=edit_charge-amount]').val(amnt);
    $('input[name=edit_charge-currency]').val(curr);
    $('input[name=edit_php-value]').val(pval);

    $("#edit_trans_chrg_modal").modal('show');
});
$(document).on('click', '.btn-edit-mtrl-row', function(){
    $(this).closest("tr").addClass('edit-row');

    var pono = $(this).closest("tr").find('td:nth-child(1)').find('input').val();
    var mlid = $(this).closest("tr").find('td:nth-child(2)').find('input').val();
    var imdc = $(this).closest("tr").find('td:nth-child(3)').find('input').val();
    var oqty = $(this).closest("tr").find('td:nth-child(4)').find('input').val();
    var acts = $(this).closest("tr").find('td:nth-child(5)').find('input').val();
    var orbl = $(this).closest("tr").find('td:nth-child(6)').find('input').val();
    var unme = $(this).closest("tr").find('td:nth-child(7)').find('input').val();
   
    $('input[name=edit_ml-po-num]').val(pono);
    $('input[name=edit_ml-material-id]').val(mlid);
    $('input[name=edit_ml-item-declaration').val(imdc);
    $('input[name=edit_ml-order-qty]').val(oqty);
    $('input[name=edit_ml-actual-ship]').val(acts);
    $('input[name=edit_ml-order-balance]').val(orbl);
    $('input[name=edit_ml-unit-of-measure]').val(unme);

    $("#trans_mtrl_modal_edit").modal('show');
});

$(document).on('click', '.btn-temp-save-chrg', function(){
  
    var form_values = $("form[name=edit-form-chrg]").serializeArray();

    var temp_row = "";

    $.each(form_values, function(k, v) {
        var input_name = v.name.split("_");
        temp_row += "<td>";    
        temp_row += v.value;
        temp_row += "<input type='hidden' name='"+input_name[1]+"[]' value='"+v.value+"' />";
        temp_row += "</td>";    
    });

    temp_row += "<td>";
    temp_row += '<button type="button" class="mr-1 btn btn-sm btn-warning btn-edit-charge-row"><i class="fa fa-pencil"></i> Edit</button>';
    temp_row += '<button type="button" class="btn btn-sm btn-danger btn-remove-row"><i class="fa fa-trash"></i> Delete</button>';
    temp_row += "</td>";

    $('table[id=trxn_chrg] tr.edit-row').html(temp_row);
    $('table[id=trxn_chrg] tr.edit-row').removeClass('edit-row');
    $("form[name=edit-form-chrg]")[0].reset();
    $("#edit_trans_chrg_modal").modal("hide"); 
});

$(document).on('click', '.btn-temp-save-mtrl', function(){
  
    var form_values = $("form[name=edit-form-mtrl]").serializeArray();

    var temp_row = "";

    $.each(form_values, function(k, v) {
        var input_name = v.name.split("_");
        temp_row += "<td>";    
        temp_row += v.value;
        temp_row += "<input type='hidden' name='"+input_name[1]+"[]' value='"+v.value+"' />";
        temp_row += "</td>";    
    });

    temp_row += "<td>";
    temp_row += '<button type="button" class="mr-1 btn btn-sm btn-warning btn-edit-mtrl-row"><i class="fa fa-pencil"></i> Edit</button>';
    temp_row += '<button type="button" class="btn btn-sm btn-danger btn-remove-row"><i class="fa fa-trash"></i> Delete</button>';
    temp_row += "</td>";

    $('table[id=trxn_mtrl] tr.edit-row').html(temp_row);
    $('table[id=trxn_mtrl] tr.edit-row').removeClass('edit-row');
    $("form[name=edit-form-mtrl]")[0].reset();
    $("#trans_mtrl_modal_edit").modal("hide"); 
});

$(document).on('click', '.btn-edit-cargo-row', function() {
    console.log("click");
    var tr_elem = $(this).closest("tr");
    tr_elem.addClass('edit-row');

    var nopk = tr_elem.find('td:nth-child(1)').find('input').val();
    var grsw = tr_elem.find('td:nth-child(2)').find('input').val();
    var chgw = tr_elem.find('td:nth-child(3)').find('input').val();
    var wdth = tr_elem.find('td:nth-child(4)').find('input').val();
    var lngt = tr_elem.find('td:nth-child(5)').find('input').val(); 
    var hegt = tr_elem.find('td:nth-child(6)').find('input').val();
    var cbm = tr_elem.find('td:nth-child(7)').find('input').val();
    var trtp = tr_elem.find('td:nth-child(8)').find('input').val();
    var notr = tr_elem.find('td:nth-child(9)').find('input').val();

    $('input[name=edit_cargo-no-of-pkgs]').val(nopk);
    $('input[name=edit_cargo-gross-wt]').val(grsw);
    $('input[name=edit_cargo-chargeable-wt]').val(chgw);
    $('input[name=edit_cargo-width]').val(wdth);
    $('input[name=edit_cargo-length]').val(lngt);
    $('input[name=edit_cargo-height]').val(hegt);
    $('input[name=edit_cargo-cbm]').val(cbm);
    $('select[name=edit_cargo-truck-type]').val(trtp);
    $('input[name=edit_cargo-no-of-trucks]').val(notr);

    $("#trans_cargo_modal_edit").modal('show');
});

$(document).on('click', '.btn-temp-save-cargo', function() {
    var form_values = $("form[name=edit-form-cargo]").serializeArray();

    var temp_row = "";

    $.each(form_values, function(k, v) {
        var input_name = v.name.split("_");
        temp_row += "<td>";    
        temp_row += v.value;
        temp_row += "<input type='hidden' name='"+input_name[1]+"[]' value='"+v.value+"' />";
        temp_row += "</td>";    
    });

    temp_row += "<td>";
    temp_row += '<button type="button" class="mr-1 btn btn-sm btn-warning btn-edit-cargo-row"><i class="fa fa-pencil"></i> Edit</button>';
    temp_row += '<button type="button" class="btn btn-sm btn-danger btn-remove-row"><i class="fa fa-trash"></i> Delete</button>';
    temp_row += "</td>";

    $('table[id=trxn_cargo] tr.edit-row').html(temp_row);
    $('table[id=trxn_cargo] tr.edit-row').removeClass('edit-row');
    $("form[name=edit-form-cargo]")[0].reset();
    $("#trans_cargo_modal_edit").modal("hide");   
});

$(document).on('click', '.btn-edit-cont-row', function(){
    $(this).closest("tr").addClass('edit-row');

    var cono = $(this).closest("tr").find('td:nth-child(1)').find('input').val();
    var slno = $(this).closest("tr").find('td:nth-child(2)').find('input').val();
    var size = $(this).closest("tr").find('td:nth-child(3)').find('input').val();
    var type = $(this).closest("tr").find('td:nth-child(4)').find('input').val();
   
    $('input[name=edit_cl-container-no]').val(cono);
    $('input[name=edit_cl-seal-no]').val(slno);
    $('input[name=edit_cl-size]').val(size);
    $('input[name=edit_cl-type]').val(type);

    $("#trans_cont_modal_edit").modal('show');
});


$(document).on('click', '.btn-temp-save-cont', function() {
    var form_values = $("form[name=edit-form-cont]").serializeArray();

    var temp_row = "";

    $.each(form_values, function(k, v) {
        var input_name = v.name.split("_");
        temp_row += "<td>";    
        temp_row += v.value;
        temp_row += "<input type='hidden' name='"+input_name[1]+"[]' value='"+v.value+"' />";
        temp_row += "</td>";    
    });

    temp_row += "<td>";
    temp_row += '<button type="button" class="mr-1 btn btn-sm btn-warning btn-edit-cont-row"><i class="fa fa-pencil"></i> Edit</button>';
    temp_row += '<button type="button" class="btn btn-sm btn-danger btn-remove-row"><i class="fa fa-trash"></i> Delete</button>';
    temp_row += "</td>";

    $('table[id=trxn_cont] tr.edit-row').html(temp_row);
    $('table[id=trxn_cont] tr.edit-row').removeClass('edit-row');
    $("form[name=edit-form-cont]")[0].reset();
    $("#trans_cont_modal_edit").modal("hide");   
});
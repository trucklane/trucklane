$(function() {
    Morris.Line({
        element: 'morris-one-line-chart',
            data: [
                { year: '2008', value: 5 },
                { year: '2009', value: 10 },
                { year: '2010', value: 8 },
                { year: '2011', value: 22 },
                { year: '2012', value: 8 },
                { year: '2014', value: 10 },
                { year: '2015', value: 5 }
            ],
        xkey: 'year',
        ykeys: ['value'],
        resize: true,
        lineWidth:4,
        labels: ['Value'],
        lineColors: ['#1ab394'],
        pointSize:5,
    });

    // Morris.Area({
    //     element: 'morris-area-chart',
    //     data: [{ period: '2010 Q1', iphone: 2666, ipad: null, itouch: 2647 },
    //         { period: '2010 Q2', iphone: 2778, ipad: 2294, itouch: 2441 },
    //         { period: '2010 Q3', iphone: 4912, ipad: 1969, itouch: 2501 },
    //         { period: '2010 Q4', iphone: 3767, ipad: 3597, itouch: 5689 },
    //         { period: '2011 Q1', iphone: 6810, ipad: 1914, itouch: 2293 },
    //         { period: '2011 Q2', iphone: 5670, ipad: 4293, itouch: 1881 },
    //         { period: '2011 Q3', iphone: 4820, ipad: 3795, itouch: 1588 },
    //         { period: '2011 Q4', iphone: 15073, ipad: 5967, itouch: 5175 },
    //         { period: '2012 Q1', iphone: 10687, ipad: 4460, itouch: 2028 },
    //         { period: '2012 Q2', iphone: 8432, ipad: 5713, itouch: 1791 } ],
    //     xkey: 'period',
    //     ykeys: ['iphone', 'ipad', 'itouch'],
    //     labels: ['iPhone', 'iPad', 'iPod Touch'],
    //     pointSize: 2,
    //     hideHover: 'auto',
    //     resize: true,
    //     lineColors: ['#87d6c6', '#54cdb4','#1ab394'],
    //     lineWidth:2,
    //     pointSize:1,
    // });

    // Morris.Donut({
    //     element: 'morris-donut-chart',
    //     data: [{ label: "Download Sales", value: 12 },
    //         { label: "In-Store Sales", value: 30 },
    //         { label: "Mail-Order Sales", value: 20 } ],
    //     resize: true,
    //     colors: ['#87d6c6', '#54cdb4','#1ab394'],
    // });

    Morris.Bar({
        element: 'morris-bar-chart',
        data: [
          {services: 'IS', count: 1361},
          {services: 'ES', count: 137},
          {services: 'IA', count: 275},
          {services: 'EA', count: 380},
          {services: 'IB', count: 655},
          {services: 'EB', count: 1571},
          {services: 'AD', count: 151},
          {services: 'SD', count: 571},
        ],
        xkey: 'services',
        ykeys: ['count'],
        labels: ['Total'],
        barRatio: 0.4,
        xLabelAngle: 0,
        hideHover: 'auto',
        resize: true,
        barColors: ['#1ab394'],
      });

    Morris.Line({
        element: 'morris-line-chart',
        data: [{ y: '2006', a: 100, b: 90 },
            { y: '2007', a: 75, b: 65 },
            { y: '2008', a: 50, b: 40 },
            { y: '2009', a: 75, b: 65 },
            { y: '2010', a: 50, b: 40 },
            { y: '2011', a: 75, b: 65 },
            { y: '2012', a: 100, b: 90 } ],
        xkey: 'y',
        ykeys: ['a', 'b'],
        labels: ['Series A', 'Series B'],
        hideHover: 'auto',
        resize: true,
        lineColors: ['#54cdb4','#1ab394'],
    });
    

    var barData = {
        labels: ["IS", "ES", "IA", "EA", "IB", "EB", "AD", "SD"],
        datasets: [
            {
                label: "TONG01",
                backgroundColor: '#17a2b8',
                pointBorderColor: "#fff",
                data: [65, 59, 80, 81, 56, 55, 40, 20]
            },
            {
                label: "CPPH01",
                backgroundColor: '#1ab394',
               
                pointBackgroundColor: "rgba(26,179,148,1)",
                pointBorderColor: "#fff",
                data: [28, 48, 40, 19, 86, 27, 90,50]
            },
            {
                label: "PAPT01",
                backgroundColor: '#fd7e14',
                pointBorderColor: "#fff",
                data: [38, 30, 60, 20, 49, 38, 30,52]
            },
            {
                label: "FONG01",
                backgroundColor: '#ffc107',
                pointBorderColor: "#fff",
                data: [76, 45, 29, 10, 38, 40, 80, 35]
            },
            {
                label: "GALV01",
                backgroundColor: '#007bff',
                pointBorderColor: "#fff",
                data: [30, 70, 29, 30, 78, 69, 39,35]
            }
        ]
    };

    var barOptions = {
        responsive: true
    };


    var ctx2 = document.getElementById("barChart").getContext("2d");
    new Chart(ctx2, {type: 'bar', data: barData, options:barOptions});


    c3.generate({
        bindto: '#pie',
        data:{
            columns: [
                ['TONG01',	63],
                ['CPPH01',	48],
                ['FONG01',	53],
                ['PAPT01',	54],
                ['GALV01',	38],
                ['CTPI01',	35],
                ['AERO01',	38],
                ['STMI01',	45],
                ['HONG01',	44],
                ['JTEK01',	26],
                ['EATO01',	27],
                ['MAXI01',	19]

            ],
            // colors:{
            //     data1: '#1ab394',
            //     data2: '#BABABA'
            // },
            type : 'pie'
        }
    });


});
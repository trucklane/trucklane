$(function() {
    
    $('.datepicker').datepicker();

});

$(document).ready(function() {
    console.log("ready table"); 
    getData();
});

function getData() 
{
    $('#reports_table').DataTable({
        "bFilter": true,
        "sAjaxSource": '/reports/trxn',
        "sPaginationType": "full_numbers",
        "iDisplayLength": 25,
        "responsive": true,
        "aLengthMenu": [
            [1,10, 15, 25, 35, 50, 100, -1],
            [1,10, 15, 25, 35, 50, 100, "All"]
        ],
        dom: 'Bfrtip',
        buttons: [
            'csv'
        ],
        "autoWidth": false,
        "oLanguage": {
            "sProcessing": "loading..."
        },
        'columnDefs': [
            {
                'targets': 0,
                'className': 'dt-body-center',
                'render': function(data, type, full, meta) {
                    return data;
                }
            },
            {
                'targets': 1,
                'className': 'dt-body-center',
                'render': function(data, type, full, meta) {
                    return data;
                }
            },
            {
                'targets': 2,
                'className': 'dt-body-center',
                'render': function(data, type, full, meta) {
                    return data;
                }
            },
            {
                'targets': 2,
                'className': 'dt-body-center',
                'render': function(data, type, full, meta) {
                    return data;
                }
            },
            {
                'targets': 3,
                'className': 'dt-body-center',
                'render': function(data, type, full, meta) {
                    return data;
                }
            },
            {
                'targets': 4,
                'className': 'dt-body-center',
                'render': function(data, type, full, meta) {
                    return data;
                }
            },
            {
                'targets': 5,
                'className': 'dt-body-center',
                'render': function(data, type, full, meta) {
                    return data;
                }
            },
            {
                'targets': 6,
                'className': 'dt-body-center',
                'render': function(data, type, full, meta) {
                    return data;
                }
            },
            {
                'targets': 7,
                'className': 'dt-body-center',
                'render': function(data, type, full, meta) {
                    return data;
                }
            },
        ],
        "fnServerParams": function(aoData) {
            aoData.push(
                {
                    "name": "filter_by_type",
                    "value": $('input[name=filter_type]').val()
                },
                {
                    "name": "filter_by_value",
                    "value": $('select[name=filter_value]').val()
                },
                {
                    "name": "filter_start_date",
                    "value": moment($('input[name=filter-start-date]').val()).format('YYYY-MM-DD')
                },
                {
                    "name": "filter_end_date",
                    "value": moment($('input[name=filter-end-date]').val()).format('YYYY-MM-DD')
                }
            );
        },
        'fnServerData': function(sSource, aoData, fnCallback) {
            $.ajax({
                'dataType': 'json',
                'type': 'POST',
                'url': sSource,
                'data': aoData,
                'success': fnCallback
            });
        }
    });
}

$(document).on('click','.btn-apply-filter', function(e){
    e.preventDefault();
    $('#reports_table').DataTable().clear().destroy();
    getData();
    return false;
});
$(document).ready(function () {
	
	load_user_access();

	function load_user_access() {
        $('#user_access_list').dataTable().fnDestroy();
        var table = $('#user_access_list').DataTable({
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            processing: true,
            serverSide: false,
            "order": [[ 4, "asc" ]],
            rowReorder: true,
            buttons: [
                { extend: 'copy'},
                {extend: 'csv'},
                {extend: 'excel', title: 'ExampleFile'},
                {extend: 'pdf', title: 'ExampleFile'},
    
                {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');
    
                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                }
            ],
            "ajax": {
                "url": baseUrl + "supplier/getSupplierList",
                "type": "POST"
             },
            "aoColumns": [
                {"sName": "uid","sClass": "", "bVisible": false},
                {"sName": "display_name","sClass": "", "bVisible": true},

                {"sName": "username","sClass": "", "bVisible": true},
                {"sName": "last_name","sClass": "", "bVisible": true},
                {"sName": "first_name","sClass": "", "bVisible": true},
                {"sName": "viewx","sClass": "", "bVisible": true},
                {"sName": "addx","sClass": "", "bVisible": true},
                {"sName": "cancelx","sClass": "", "bVisible": true},
                {"sName": "deletex","sClass": "", "bVisible": true},
                {"sName": "postx","sClass": "", "bVisible": true},
                
                {"sName": "unpostx","sClass": "text-center", "bVisible": true,
                    "mRender": function ( data, type, row, meta )
                    {
                        var checked = '';
                        if(data != null) 
                        {
                            if(data == 1) 
                                checked = 'Yes';
                            else 
                                checked = 'No';
                            
                            return checked;
                        }
                        return "";
                
                    }
                },
            ]
        });
    }
});
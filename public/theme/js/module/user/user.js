$(document).ready(function () {
    var userForm = $('form[name=user-form]');
    
    var userProfileForm = $('form[name=user-profile-form]');

    var valConfig = {
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'fa fa-check-circle',
            //invalid: 'fa fa-times',
            validating: 'fa fa-refresh'
        }, 
        fields: {
            username: {
                message: 'The username is not valid',
                validators: {
                    notEmpty: {
                        message: 'The username is required and can\'t be empty'
                    },
                    stringLength: {
                        min: 4,
                        max: 30,
                        message: 'The username must be more than 6 and less than 30 characters long'
                    },
                    regexp: {
                        regexp: /^[a-zA-Z0-9_\.]+$/,
                        message: 'The username can only consist of alphabetical, number, dot and underscore'
                    }
                }
            },
            first_name: {
                validators: {
                    notEmpty: {
                        message: 'First Name is required'
                    }
                }
            },
            last_name: {
                validators: {
                    notEmpty: {
                        message: 'Last Name is required'
                    }
                }
            },
            email: {
                validators: {
                    notEmpty: {
                        message: 'The email address is required and can\'t be empty'
                    },
                    emailAddress: {
                        message: 'The input is not a valid email address'
                    }
                }
            },
            company: {
                validators: {
                    notEmpty: {
                         message: 'Company is required'
                    }
                }
            },
            phone: {
                validators: {
                    phone: {
                        message: 'The input is not a valid phone number'
                    }
                }
            },
            password: {
                validators: {
                    notEmpty: {
                        message: 'The password is required and can\'t be empty'
                    },
                    identical: {
                        field: 'confirmPassword',
                        message: 'The password and its confirm are not the same'
                    }
                }
            },
            confirmPassword: {
                validators: {
                    notEmpty: {
                        message: 'The confirm password is required and can\'t be empty'
                    },
                    identical: {
                        field: 'password',
                        message: 'The password and its confirm are not the same'
                    }
                }
            }
        }
    };
        
    userForm.bootstrapValidator(valConfig);

    var valProfileConfig = {
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'fa fa-check-circle',
            //invalid: 'fa fa-times',
            validating: 'fa fa-refresh'
        }, 
        fields: {
            username: {
                message: 'The username is not valid',
                validators: {
                    stringLength: {
                        min: 4,
                        max: 30,
                        message: 'The username must be more than 6 and less than 30 characters long'
                    },
                    regexp: {
                        regexp: /^[a-zA-Z0-9_\.]+$/,
                        message: 'The username can only consist of alphabetical, number, dot and underscore'
                    }
                }
            },
            password: {
                validators: {
                    identical: {
                        field: 'confirmPassword',
                        message: 'The password and its confirm are not the same'
                    }
                }
            },
            confirmPassword: {
                validators: {
                    identical: {
                        field: 'password',
                        message: 'The password and its confirm are not the same'
                    }
                }
            }
        }
    };
        
    userProfileForm.bootstrapValidator(valProfileConfig);

    $('#add-user-form').on('hidden.bs.modal', function() {
        document.getElementById("user-form").reset();
    });



   


    $(document).on('change','#editPassword', function() {
        var checked = $(this).is(':checked');
        if (checked) {
            $('#update-password').removeClass('hide');
        } else {
            $('#update-password').addClass('hide');
            $('#password').val('');
            $('#confirmPassword').val('');
        }
    });



    $(document).on('click', '.btn-user-save', function() {

        var l = Ladda.create(this);
    // l.start();
        
        var form = $('form[name=user-form]');
        var validator = form.data('bootstrapValidator');
        validator.validate();
        if (validator.isValid()) {
            $.ajax({
                type: 'POST',
                url: baseUrl+'users/user_store',
                data:  form.serialize(),
                dataType: 'json',
                success:function(data){
                    _setToast();
                    
                    if(data.success) {
                        toastr.options.onHidden = function() { 
                            window.location.href = data.url;
                        };
                        toastr.success('Please wait, redirect the page','Successful');
                    } else {
                        //toastr.error('Unable to submit your request','Oooooppss');
                        _setMessage('danger', data.msg);
                        l.stop();
                    }
                }
            });
        } else {
            l.stop();
        }
    });


   
    if(typeof page === 'undefined'){
        load_users();
    }

    $('.i-checks').iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square-green',
    });


    var options3 = {};
    options3.ui = {
        container: "#pwd-container2",
        showVerdictsInsideProgressBar: true,
        viewports: {
            progress: ".pwstrength_viewport_progress2"
        }
    };
    options3.common = {
        debug: true,
        usernameField: "#username"
    };
    $('#password').pwstrength(options3);
    
    

});

$(document).on('click','.add-user', function() {
    var btn = $(this);
    $('#loadingModal').modal('show');
    btn.button('loading');
    window.location.href = baseUrl+"users/create/";
});

$(document).on('click','.btn-user-back', function() {
    var btn = $(this);
    $('#loadingModal').modal('show');
    btn.button('loading');
    window.location.href = baseUrl+"users";
});

$(document).on('click', '.btn-user-edit', function () {
    var btn = $(this);
    $('#loadingModal').modal('show');
    btn.button('loading');
    var id = btn.data('id');
    window.location.href = baseUrl+"users/edit/"+id;
});


$(document).on('click', '.btn-profile-save', function() {
    var l = Ladda.create(this);

    l.start();

    var form = $('form[name=user-profile-form]');
    var validator = form.data('bootstrapValidator');
    validator.validate();
    if (validator.isValid()) {
        $.ajax({
            type: 'POST',
            url: baseUrl+'users/update_profile_store',
            data:  form.serialize(),
            dataType: 'json',
            success:function(data){
                _setToast();
                
                if(data.success) {
                    toastr.options.onHidden = function() { 
                        window.location.href = data.url;
                    };
                    toastr.success('Please wait, redirect the page','Successful');
                } else {
                    toastr.error('Unable to submit your request','Oooooppss');
                    //_setMessage('danger', data.msg);
                    l.stop();
                }
            }
        });
    } else {
    l.stop();
    }
});


function load_users() {
   
    var table = $('#users').DataTable({
        responsive: true,
        dom: '<"html5buttons"B>lTfgitp',
        processing: true,
        serverSide: false,
        "order": [[ 4, "asc" ]],
        rowReorder: true,
        buttons: [
            // { extend: 'copy'},
            // {extend: 'csv'},
            {extend: 'excel', title: 'ExampleFile'},
            // {extend: 'pdf', title: 'ExampleFile'},

            // {extend: 'print',
	        //      customize: function (win){
	        //             $(win.document.body).addClass('white-bg');
	        //             $(win.document.body).css('font-size', '10px');

	        //             $(win.document.body).find('table')
	        //                     .addClass('compact')
	        //                     .css('font-size', 'inherit');
	        //     }
            // }
        ],
        "ajax": {
            "url": baseUrl + "users/getUserList",
            "type": "POST",
            dataSrc:""
        },
        "columns": [
            {"data": "id","sClass": "", "bVisible": false},
            {"data": "username","sClass": "", "bVisible": true},
            {"data": "first_name","sClass": "", "bVisible": true},
            {"data": "last_name","sClass": "", "bVisible": true},
            {"data": "email","sClass": "", "bVisible": true},
            {"data": "active","sClass": "text-center", "bVisible": true,
				"mRender": function ( data, type, row, meta )
				{
					var checked = '';
					if(data != null) 
					{
						if(data == 1) 
							checked = 'Yes';
						else 
							checked = 'No';
						
						return checked;
					}
					return "";
			
				}
			},
            {"data": "id","sClass": "text-center", "bSortable": false, "bVisible": true,
                "mRender": function ( data, type, row, meta )
                {
                    var btn = ' <button type="button" class="btn btn-sm btn-primary btn-user-edit" data-id="' + data + '"><i class="fa fa-pencil" aria-hidden="true"></i> Edit</button>';
                    return btn;
                }
            }
        ]
    });

}
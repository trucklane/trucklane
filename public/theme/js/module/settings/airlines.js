$(document).ready(function () {

    if ($('#carrier_list').length > 0) {
       $('#carrier_list').DataTable();
    }

});

$(document).on('click', '.btn-add-carrier', function(){
    $('input[name=carrier-code]').val("");
    $('input[name=carrier-name]').val("");
    $('#carrier-form-modal').modal('show');
});

$(document).on('click', '.btn-save-carrier', function(){
    var l = Ladda.create(this);
    l.start();

    var form = $('form[name=carrier-form]');

    var config = {
        type: 'POST',
        url: '/settings/store_carrier',
        data:  form.serialize(),
        dataType: 'json',
    }

    var ajax = $.ajax(config);

    ajax.done(function(response){
        _setToast();
        if (response.success) {
            toastr.options.onHidden = function() { 
                $('#carrier-form-modal').modal('hide');
                l.stop();
                window.location = "/settings/carriers";
            };
            toastr.success('Please wait, redirect the page','Successful');
        } else {
            toastr.error('Unable to submit your request','Oooooppss');
            l.stop();
        }
    });
});

$(document).on('click', '.btn-edit-carrier', function () {
    id = $(this).data('id');

    $.ajax({
        type: 'GET',
        url: '/settings/carrier_data',
        data:  {'carrier_id' :  id},
        dataType: 'json',
        success:function(data){
            if(data.success) {

                $('input[name=edit-carrier-code]').val(data.result.carrier_code);
                $('input[name=edit-carrier-name]').val(data.result.carrier_name);
                $('select[name=edit-carrier-type]').val(data.result.carrier_type);
                
                $('input[name=edit-carrier-id]').val(data.result.id);

                $('#edit-carrier-form-modal').modal('show');
                
            } 
        }
    });

});

$(document).on('click', '.btn-save-edit-carrier', function(){
    var l = Ladda.create(this);
    l.start(); 
    var form = $('form[name="edit-carrier-form"]');
    $.ajax({
        type: 'POST',
        url: '/settings/carrier_save_data',
        data:  form.serialize(),
        dataType: 'json',
        success:function(data){
            if(data.success) {
                toastr.options.onHidden = function() { 
                    $('#edit-carrier-form-modal').modal('hide');
                    l.stop();
                    window.location = "/settings/carriers";
                };
                toastr.success('Please wait, redirect the page','Successful');
            } else {
                toastr.error('Unable to submit your request','Oooooppss');
                l.stop();
            }
        }
    });
});

$(document).on('click', '.btn-del-carrier', function() {
    var l = Ladda.create(this);
    

    id = $(this).data('id');
    
    var r = confirm("Are you sure you want to delete this carrier?",'Delete');

    if(r) {
        l.start();
        $.ajax({
            type: 'POST',
            url: '/settings/remove_carrier',
            data:  {'id' :  id},
            dataType: 'json',
            success:function(data){
                _setToast();
                if(data.success) {
                    toastr.options.onHidden = function() { 
                        l.stop();
                        window.location = "/settings/carriers";
                    };
                    toastr.success('Please wait, redirect the page','Successful');
                } else {
                    toastr.error('Unable to submit your request','Oooooppss');
                    l.stop();
                }
            }
        });
    } else {
        return false;
    }
});

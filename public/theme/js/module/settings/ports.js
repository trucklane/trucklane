
$(document).ready(function () {

    if(document.getElementById("port-list") != null) {
        var table = $('#port-list').DataTable();
    }
});

$("#port-form").validate({
    rules: {
        port_code:   { required: true },
        port_id:   { required: true },
        port_name:   { required: true },
        port_type:   { required: true },
        port_country:   { required: true }
    },
});

$(document).on('click', '#btn-add-port', function () {
    $('#port_code').val('');
    $('#port_id').val(0);
    $('#port_name').val('');

    $('#port_type').val('');
    $('#is_enabled').val('');
    $('#port_country').val('');
    $('#iata_area').val('');

    $('#port-form-modal').modal('show');
});


$(document).on('click', '.del-port', function() {
    var l = Ladda.create(this);
    l.start();

    port_id = $(this).data('id');
    
    var r = confirm("Are you sure you want to delete this port?",'Delete');

    if(r) {
        $.ajax({
            type: 'POST',
            url: '/settings/port_remove',
            data:  {'port_id' :  port_id},
            dataType: 'json',
            success:function(data){
                _setToast();
                if(data.success) {
                    toastr.options.onHidden = function() { 
                        l.stop();
                        window.location = "/settings/ports";
                    };
                    toastr.success('Please wait, redirect the page','Successful');
                } else {
                    toastr.error('Unable to submit your request','Oooooppss');
                    l.stop();
                }
            }
        });
    } else {
        return false;
    }
});


$(document).on('click', '.btn-save-port', function(){
    var l = Ladda.create(this);
    l.start();
    var form = $('form[name=port-form]');

    if (!$('#port-form').valid()) {
        l.stop();
        return false;
    } else {
        $.ajax({
            type: 'POST',
            url: '/settings/port_store',
            data:  form.serialize(),
            dataType: 'json',
            success:function(data){
                _setToast();

                if(data.success) {
                    toastr.options.onHidden = function() { 
                        $('#country-form-modal').modal('hide');
                        l.stop();
                        window.location = "/settings/country";
                    };
                    toastr.success('Please wait, redirect the page','Successful');
                } else {
                    toastr.error('Unable to submit your request','Oooooppss');
                    l.stop();
                }
            }
        });
    } 
});

$(document).on('click', '.edit-port', function () {
    port_id = $(this).data('id');

    $.ajax({
        type: 'GET',
        url: '/settings/port_data',
        data:  {'port_id' :  port_id},
        dataType: 'json',
        success:function(data){
            if(data.success) {

                $('#port_code').val(data.result.port_code);
                $('#port_id').val(data.result.port_id);
                $('#port_name').val(data.result.port_name);

                $('#port_type').val(data.result.port_type);
                
                $("#port_country option[value='"+data.result.port_country+"']").prop('selected', true);
                $("#is_enabled option[value='"+data.result.is_enabled+"']").prop('selected', true);

                $('#iata_area').val(data.result.iata_area);

                $('#port-form-modal').modal('show');
                
            } 
        }
    });

});
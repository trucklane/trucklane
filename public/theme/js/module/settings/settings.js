$(document).ready(function () {

    if ($('#unit-list').length > 0) {
       $('#unit-list').DataTable();
    }

});


$(document).on('click', '.btn-add-unit', function(){
    $('input[name=unit-code]').val("");
    $('textarea[name=unit-desc]').val("");
    $('#unit-form-modal').modal('show');
});

$(document).on('click', '.btn-save-unit', function(){
    var l = Ladda.create(this);
    l.start();

    var form = $('form[name=unit-form]');

    var config = {
        type: 'POST',
        url: '/settings/store_unit_of_measure',
        data:  form.serialize(),
        dataType: 'json',
    }

    var ajax = $.ajax(config);

    ajax.done(function(response){
        _setToast();
        if (response.success) {
            toastr.options.onHidden = function() { 
                $('#status-form-modal').modal('hide');
                l.stop();
                window.location = "/settings/unit_of_measure";
            };
            toastr.success('Please wait, redirect the page','Successful');
        } else {
            toastr.error('Unable to submit your request','Oooooppss');
            l.stop();
        }
    });
});

$(document).on('click', '.btn-edit-unit', function () {
    id = $(this).data('id');

    $.ajax({
        type: 'GET',
        url: '/settings/unit_data',
        data:  {'unit_id' :  id},
        dataType: 'json',
        success:function(data){
            if(data.success) {

                $('input[name=edit-unit-code]').val(data.result.unit_code);
                $('textarea[name=edit-unit-desc]').val(data.result.unit_description);
                

                if (data.result.is_active == 1) 
                    $('input[name="edit-unit-enabled"]').prop("checked", true);
                else 
                    $('input[name="edit-status-enabled"]').prop("checked", false);
                
                $('input[name=edit-unit-id]').val(data.result.id);

                $('#edit-unit-form-modal').modal('show');
                
            } 
        }
    });

});

$(document).on('click', '.btn-save-edit-unit', function(){
    var l = Ladda.create(this);
    l.start(); 
    var form = $('form[name="edit-unit-form"]');
    $.ajax({
        type: 'POST',
        url: '/settings/unit_save_data',
        data:  form.serialize(),
        dataType: 'json',
        success:function(data){
            if(data.success) {
                toastr.options.onHidden = function() { 
                    $('#edit-unit-form-modal').modal('hide');
                    l.stop();
                    window.location = "/settings/unit_of_measure";
                };
                toastr.success('Please wait, redirect the page','Successful');
            } else {
                toastr.error('Unable to submit your request','Oooooppss');
                l.stop();
            }
        }
    });
});

$(document).on('click', '.btn-del-unit', function() {
    var l = Ladda.create(this);
   

    id = $(this).data('id');
    
    var r = confirm("Are you sure you want to delete this unit of measure?",'Delete');

    if(r) {
        l.start();
        $.ajax({
            type: 'POST',
            url: '/settings/unit_remove',
            data:  {'id' :  id},
            dataType: 'json',
            success:function(data){
                _setToast();
                if(data.success) {
                    toastr.options.onHidden = function() { 
                        l.stop();
                        window.location = "/settings/unit_of_measure";
                    };
                    toastr.success('Please wait, redirect the page','Successful');
                } else {
                    toastr.error('Unable to submit your request','Oooooppss');
                    l.stop();
                }
            }
        });
    } else {
        return false;
    }
});
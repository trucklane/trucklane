
$(document).ready(function () {
    $('.select2').select2();

    $('#partner-form').validate({
        rules: {
            partner_code: {  required: true,},
            partner_name: { required: true },
            partner_type: { required: true },
            address1: { required: true },
            address2: { required: true },
            address3: { required: true },
            country_code: { required: true },
            
        },
        // messages: {
        //     partner_code: "This field is required",
        // },
        errorElement: 'span',
        errorPlacement: function (error, element) {
          error.addClass('invalid-feedback');
          element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
          $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
          $(element).removeClass('is-invalid');
        }
      });

    if(document.getElementById("partner-list") != null) {
        $('#partner-list').DataTable({
            "bFilter": true,
            "sAjaxSource": 'settings/partner_list',
            "sPaginationType": "full_numbers",
            "iDisplayLength": 25,
            "responsive": true,
            "aLengthMenu": [
                [1,10, 15, 25, 35, 50, 100, -1],
                [1,10, 15, 25, 35, 50, 100, "All"]
            ],
            "autoWidth": false,

            "oLanguage": {
                "sProcessing": "loading..."
            },
            'columnDefs': [
                {
                    'targets': 2,
                    'searchable': false,
                    'orderable': false,
                    'className': 'dt-body-center',
                    'render': function(data, type, full, meta) {
                        return '<i class="fa '+data+'"></i> '+data;
                    }
                },
                {
                    'targets': 3,
                    'searchable': false,
                    'orderable': false,
                    'className': 'dt-body-center',
                    'render': function(data, type, full, meta) {
                        if(data == 'A')
                            return 'Agent';
                        else if(data == 'C')
                            return 'Consignee';
                        else if(data == 'S')
                            return 'Shipper';
                        else if(data == 'X')
                            return 'Customer'
                        else {
                            return ''
                        }
                    }
                },
                {
                    'targets': 4,
                    'searchable': false,
                    'orderable': false,
                    'className': 'dt-body-center',
                    'render': function(data, type, full, meta) {
                        return ' <button type="button" data-id="'+data+'" class="btn btn-primary btn-sm" id="partner-edit"><i class="fa fa-eye"></i> Edit</button>';
                    }
                },
            ],
            "fnServerParams": function(aoData) {
                // aoData.push({
                //         "name": "batch_no",
                //         "value": $('#batch_no').val()
                //     }, {
                //         "name": "store_name",
                //         "value": $('#store_name').val()
                //     }, {
                //         "name": "status",
                //         "value": $('#status').val()
                //     }

                // );
            },

            'fnServerData': function(sSource, aoData, fnCallback) {
                $.ajax({
                    'dataType': 'json',
                    'type': 'POST',
                    'url': sSource,
                    'data': aoData,
                    'success': fnCallback
                });
            },

        });
    }
});

$(document).on('click', '#partner-edit', function() {
    var id = $(this).data('id');
    window.location.href = baseUrl +'/settings/partner_view/'+id;
});

var x = 0; //Initial field counter
var list_maxField = 10; //Input fields increment limitation

$(document).on('click', '.list_add_button', function() {
    //Check maximum number of input fields
    if(x < list_maxField){ 
        x++; //Increment field counter
        var list_fieldHTML = '<div class="row"><div class="col-xs-6 col-sm-6 col-md-3"><div class="form-group"><input name="contact_name[]" type="text" placeholder="Contact Name" class="form-control"/></div></div><div class="col-xs-6 col-sm-6 col-md-3"><div class="form-group"><input name="email_address[]" type="text" placeholder="Email Address" class="form-control"/></div></div><div class="col-xs-6 col-sm-6 col-md-3"><div class="form-group"><input name="contact_no[]" type="text" placeholder="Contact No" class="form-control"/></div></div><div class="col-xs-6 col-sm-6 col-md-2"><div class="form-group"><input name="position[]" type="text" placeholder="Position" class="form-control"/></div></div><div class="col-xs-1 col-sm-5 col-md-1"><a href="javascript:void(0);" class="list_remove_button btn btn-danger">-</a></div></div>'; //New input field html 
        $('.list_wrapper').append(list_fieldHTML); //Add field html
    }
});

//Once remove button is clicked
$(document).on('click', '.list_remove_button', function() {
    $(this).closest('div.row').remove(); //Remove field html
    x--; //Decrement field counter
});
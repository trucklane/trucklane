$(document).ready(function () {

    if ($('#charges_list').length > 0) {
       $('#charges_list').DataTable();
    }

});

$(document).on('click', '.btn-add-charges', function(){
    $('input[name=charges-code]').val("");
    $('textarea[name=charges-description]').val("");
    $('#charges-form-modal').modal('show');
});

$(document).on('click', '.btn-save-charges', function(){
    var l = Ladda.create(this);
    l.start();

    var form = $('form[name=charges-form]');

    var config = {
        type: 'POST',
        url: '/settings/store_charges',
        data:  form.serialize(),
        dataType: 'json',
    }

    var ajax = $.ajax(config);

    ajax.done(function(response){
        _setToast();
        if (response.success) {
            toastr.options.onHidden = function() { 
                $('#carrier-form-modal').modal('hide');
                l.stop();
                window.location = "/settings/charges";
            };
            toastr.success('Please wait, redirect the page','Successful');
        } else {
            toastr.error('Unable to submit your request','Oooooppss');
            l.stop();
        }
    });
});

$(document).on('click', '.btn-edit-charges', function () {
    id = $(this).data('id');

    $.ajax({
        type: 'GET',
        url: '/settings/charge_data',
        data:  {'charge_id' :  id},
        dataType: 'json',
        success:function(data){
            if(data.success) {

                $('input[name=edit-charges-code]').val(data.result.charge_code);
                $('textarea[name=edit-charges-description]').val(data.result.charge_description);
                $('select[name=edit-charges-group]').val(data.result.report_group);
                if (data.result.is_active == 1) 
                    $('input[name="edit-charges-enabled"]').prop("checked", true);
                else 
                    $('input[name="edit-charges-enabled"]').prop("checked", false);

                $('input[name=edit-charges-id]').val(data.result.id);

                $('#edit-charges-form-modal').modal('show');
                
            } 
        }
    });

});

$(document).on('click', '.btn-save-edit-charges', function(){
    var l = Ladda.create(this);
    l.start(); 
    var form = $('form[name="edit-charges-form"]');
    $.ajax({
        type: 'POST',
        url: '/settings/charge_save_data',
        data:  form.serialize(),
        dataType: 'json',
        success:function(data){
            if(data.success) {
                toastr.options.onHidden = function() { 
                    $('#edit-charges-form-modal').modal('hide');
                    l.stop();
                    window.location = "/settings/charges";
                };
                toastr.success('Please wait, redirect the page','Successful');
            } else {
                toastr.error('Unable to submit your request','Oooooppss');
                l.stop();
            }
        }
    });
});

$(document).on('click', '.btn-del-charges', function() {
    var l = Ladda.create(this);
    

    id = $(this).data('id');
    
    var r = confirm("Are you sure you want to delete this charge?",'Delete');

    if(r) {
        l.start();
        $.ajax({
            type: 'POST',
            url: '/settings/remove_charge',
            data:  {'id' :  id},
            dataType: 'json',
            success:function(data){
                _setToast();
                if(data.success) {
                    toastr.options.onHidden = function() { 
                        l.stop();
                        window.location = "/settings/charges";
                    };
                    toastr.success('Please wait, redirect the page','Successful');
                } else {
                    toastr.error('Unable to submit your request','Oooooppss');
                    l.stop();
                }
            }
        });
    } else {
        return false;
    }
});
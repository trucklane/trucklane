
$(document).on('click', '.btn-add-status', function(){
    $('input[name=status_name]').val("");
    $('textarea[name=status_desc]').val("");
    $('#status-form-modal').modal('show');
});


$(document).on('click', '.btn-save-status', function(){
    var l = Ladda.create(this);
    l.start();

    var form = $('form[name=status-form]');

    var config = {
        type: 'POST',
        url: '/settings/store_status',
        data:  form.serialize(),
        dataType: 'json',
    }

    var ajax = $.ajax(config);

    ajax.done(function(response){
        _setToast();
        if (response.success) {
            toastr.options.onHidden = function() { 
                $('#status-form-modal').modal('hide');
                l.stop();
                window.location = "/settings/status";
            };
            toastr.success('Please wait, redirect the page','Successful');
        } else {
            toastr.error('Unable to submit your request','Oooooppss');
            l.stop();
        }
    });
});

$(document).on('click', '.btn-edit-status', function () {
    status = $(this).data('id');

    $.ajax({
        type: 'GET',
        url: '/settings/status_data',
        data:  {'status_id' :  status},
        dataType: 'json',
        success:function(data){
            if(data.success) {

                $('input[name=edit_status_name]').val(data.result.status_name);
                $('textarea[name=edit_status_desc]').val(data.result.status_description);
                
                if (data.result.status_visibility == 1) 
                    $('input[name="edit_status_visibility"]').prop("checked", true);
                else 
                    $('input[name="edit_status_visibility"]').prop("checked", false);

                if (data.result.is_enabled == 1) 
                    $('input[name="edit_status_enabled"]').prop("checked", true);
                else 
                    $('input[name="edit_status_enabled"]').prop("checked", false);
                
                $('input[name=edit_status_id]').val(data.result.id);

                $('#edit-status-form-modal').modal('show');
                
            } 
        }
    });

});


$(document).on('click', '.btn-del-status', function() {
    var l = Ladda.create(this);
    l.start();

    status_id = $(this).data('id');
    
    var r = confirm("Are you sure you want to delete this status?",'Delete');

    if(r) {
        $.ajax({
            type: 'POST',
            url: '/settings/status_remove',
            data:  {'status_id' :  status_id},
            dataType: 'json',
            success:function(data){
                _setToast();
                if(data.success) {
                    toastr.options.onHidden = function() { 
                        l.stop();
                        window.location = "/settings/status";
                    };
                    toastr.success('Please wait, redirect the page','Successful');
                } else {
                    toastr.error('Unable to submit your request','Oooooppss');
                    l.stop();
                }
            }
        });
    } else {
        return false;
    }
});


$(document).on('click', '.btn-save-edit-status', function(){
    var l = Ladda.create(this);
    l.start(); 
    var form = $('form[name="edit-status-form"]');
    $.ajax({
        type: 'POST',
        url: '/settings/status_save_data',
        data:  form.serialize(),
        dataType: 'json',
        success:function(data){
            if(data.success) {
                toastr.options.onHidden = function() { 
                    $('#edit-status-form-modal').modal('hide');
                    l.stop();
                    window.location = "/settings/status";
                };
                toastr.success('Please wait, redirect the page','Successful');
            } else {
                toastr.error('Unable to submit your request','Oooooppss');
                l.stop();
            }
        }
    });
});
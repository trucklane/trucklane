$(document).ready(function () {

    if(document.getElementById("country-list") != null) {
        var table = $('#country-list').DataTable({
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            processing: true,
            serverSide: false,
            "order": [[ 1, "asc" ]],
            rowReorder: true,
            buttons: [
                
            ],
            "ajax": {
                "url": baseUrl + "settings/getCountryList",
                "type": "POST"
            },
            "aoColumns": [
                {"sName": "country_code","sClass": "", "bVisible": false},
                {"sName": "country_name","sClass": "", "bVisible": true},
                {"sName": "country_code","sClass": "text-center", "bSortable": false, "bVisible": true,
                    "mRender": function ( data, type, row, meta )
                    {
                        var btn = ' <button type="button" class="btn btn-sm btn-primary edit-country" data-name="' + row[1] + '" data-id="' + data + '"><i class="fa fa-pencil" aria-hidden="true"></i> </button>';
                         btn += ' <button type="button" class="btn btn-sm btn-danger del-country"  data-id="' + data + '"><i class="fa fa-trash" aria-hidden="true"></i> </button>';
                        return btn;
                    }
                }
            ]
        });
    }
});


$(document).on('click', '#btn-add-country', function () {
    $('#country_code').val('');
    $('#country_name').val('');
    $('#country_code').attr('readonly',false);
    $('#country-form-modal').modal('show');
});

$(document).on('click', '.edit-country', function () {
    country_code = $(this).data('id');
    country_name = $(this).data('name');

    $('#country_code').val(country_code);
    $('#country_code').attr('readonly',true);
    $('#country_name').val(country_name);
    $('#country-form-modal').modal('show');
});

$("#country-form").validate({
    rules: {
        country_code:   { required: true },
        country_name:   { required: true },
    },
});

$(document).on('click', '.btn-save-country', function(){
    var l = Ladda.create(this);
    l.start();
    var form = $('form[name=country-form]');

    if (!$('#country-form').valid()) {
        l.stop();
        return false;
    } else {
        $.ajax({
            type: 'POST',
            url: '/settings/country_store',
            data:  form.serialize(),
            dataType: 'json',
            success:function(data){
                _setToast();

                if(data.success) {
                    toastr.options.onHidden = function() { 
                        $('#country-form-modal').modal('hide');
                        l.stop();
                        window.location = "/settings/ports";
                    };
                    toastr.success('Please wait, redirect the page','Successful');
                } else {
                    toastr.error('Unable to submit your request','Oooooppss');
                    l.stop();
                }
            }
        });
    } 
});

$(document).on('click', '.del-country', function() {
    var l = Ladda.create(this);
    l.start();

    country_code = $(this).data('id');
    
    var r = confirm("Are you sure you want to delete this country?",'Delete');

    if(r) {
        $.ajax({
            type: 'POST',
            url: '/settings/country_remove',
            data:  {'country_code' :  country_code},
            dataType: 'json',
            success:function(data){
                _setToast();
                if(data.success) {
                    toastr.options.onHidden = function() { 
                        $('#country-form-modal').modal('hide');
                        l.stop();
                        window.location = "/settings/country";
                    };
                    toastr.success('Please wait, redirect the page','Successful');
                } else {
                    toastr.error('Unable to submit your request','Oooooppss');
                    l.stop();
                }
            }
        });
    } else {
        return false;
    }
});
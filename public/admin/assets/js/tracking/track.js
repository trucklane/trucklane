$(document).on('click', '.btn-search-trans', function(){
    var l = Ladda.create(this);
    l.start();

    var search_input = $('input[name="search-freight"]').val();
    var search_service = $('input[name="trnxn_service"]:checked').val();
    var config = {
        url: '/tracking/search',
        type: 'post',
        dataType: 'json',
        data: {search: search_input, service: search_service}
   }
   var ajax = $.ajax(config);
   ajax.done(function(response) {
        if (response.success) {
            $('.timeline-container').html(response.html);
        }
        l.stop();
   });
});
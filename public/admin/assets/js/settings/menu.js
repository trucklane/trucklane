
$(document).ready(function () {
    $('#menu-list').DataTable({
        // "bProcessing": true,
        // "bServerSide": true,
        "bFilter": true,
        "sAjaxSource": 'menu_list',
        "sPaginationType": "full_numbers",
        "iDisplayLength": 25,
        "responsive": true,
        "aLengthMenu": [
            [1,10, 15, 25, 35, 50, 100, -1],
            [1,10, 15, 25, 35, 50, 100, "All"]
        ],
        "autoWidth": false,

        "oLanguage": {
            "sProcessing": "loading..."
        },
        'columnDefs': [
            {
                'targets': 2,
                'searchable': false,
                'orderable': false,
                'className': 'dt-body-center',
                'render': function(data, type, full, meta) {
                    return '<i class="fa '+data+'"></i> '+data;
                }
            },

            {
                'targets': 5,
                'searchable': false,
                'orderable': false,
                'className': 'dt-body-center',
                'render': function(data, type, full, meta) {
                    return ' <button type="button" data-id="'+data+'" class="btn btn-primary btn-sm" id="menu-edit"><i class="fa fa-eye"></i> Edit</button>';
                }
            },
            {
                'targets': 3,
                'className': 'dt-body-center',
                'render': function(data, type, full, meta) {
                    return (data == 1 ) ? "Yes" : 'No';
                }
            },


        ],
        "fnServerParams": function(aoData) {
            // aoData.push({
            //         "name": "batch_no",
            //         "value": $('#batch_no').val()
            //     }, {
            //         "name": "store_name",
            //         "value": $('#store_name').val()
            //     }, {
            //         "name": "status",
            //         "value": $('#status').val()
            //     }

            // );
        },

        'fnServerData': function(sSource, aoData, fnCallback) {
            $.ajax({
                'dataType': 'json',
                'type': 'POST',
                'url': sSource,
                'data': aoData,
                'success': fnCallback
            });
        },

    });
});

$(document).on('click', '#menu-edit', function() {
    var id = $(this).data('id');
    window.location.href = baseUrl +'/settings/menu_view/'+id;
})

$(document).ready(function () {
    $('#country-list').DataTable();
    $('#port-list').DataTable();
    $('#unit-list').DataTable();
    $('#status-list').DataTable(
        {
            "columnDefs": [
              { "width": "30%", "targets": 2 },
            ]
        } 
    );
});


$(document).on('click', '#btn-add-country', function () {
   
    $('#country_code').val('');
    $('#country_name').val('');
    $('#country_code').attr('readonly',false);
    $('#country-form-modal').modal('show');
});

$(document).on('click', '.edit-country', function () {
    country_code = $(this).data('id');
    country_name = $(this).data('name');

    $('#country_code').val(country_code);
    $('#country_code').attr('readonly',true);
    $('#country_name').val(country_name);
    $('#country-form-modal').modal('show');
});

$("#country-form").validate({
    rules: {
        country_code:   { required: true },
        country_name:   { required: true },
    },
});

$(document).on('click', '.btn-save-country', function(){
    var l = Ladda.create(this);
    l.start();
    var form = $('form[name=country-form]');

    if (!$('#country-form').valid()) {
        l.stop();
        return false;
    } else {
        $.ajax({
            type: 'POST',
            url: '/settings/country_store',
            data:  form.serialize(),
            dataType: 'json',
            success:function(data){
                _setToast();

                if(data.success) {
                    toastr.options.onHidden = function() { 
                        $('#country-form-modal').modal('hide');
                        l.stop();
                        window.location = "/settings/country";
                    };
                    toastr.success('Please wait, redirect the page','Successful');
                } else {
                    toastr.error('Unable to submit your request','Oooooppss');
                    l.stop();
                }
            }
        });
    } 
});

$(document).on('click', '.del-country', function() {
    var l = Ladda.create(this);
    l.start();

    country_code = $(this).data('id');
    
    var r = confirm("Are you sure you want to delete this country?",'Delete');

    if(r) {
        $.ajax({
            type: 'POST',
            url: '/settings/country_remove',
            data:  {'country_code' :  country_code},
            dataType: 'json',
            success:function(data){
                _setToast();
                if(data.success) {
                    toastr.options.onHidden = function() { 
                        $('#country-form-modal').modal('hide');
                        l.stop();
                        window.location = "/settings/country";
                    };
                    toastr.success('Please wait, redirect the page','Successful');
                } else {
                    toastr.error('Unable to submit your request','Oooooppss');
                    l.stop();
                }
            }
        });
    } else {
        return false;
    }
});

$(document).on('click', '#btn-add-port', function () {
    $('#port_code').val('');
    $('#port_id').val(0);
    $('#port_name').val('');

    $('#port_type').val('');
    $('#is_enabled').val('');
    $('#port_country').val('');
    $('#iata_area').val('');

    $('#port-form-modal').modal('show');
});

$(document).on('click', '.edit-port', function () {
    port_id = $(this).data('id');

    $.ajax({
        type: 'GET',
        url: '/settings/port_data',
        data:  {'port_id' :  port_id},
        dataType: 'json',
        success:function(data){
            if(data.success) {

                $('#port_code').val(data.result.port_code);
                $('#port_id').val(data.result.port_id);
                $('#port_name').val(data.result.port_name);

                $('#port_type').val(data.result.port_type);
                
                $("#port_country option[value='"+data.result.port_country+"']").prop('selected', true);
                $("#is_enabled option[value='"+data.result.is_enabled+"']").prop('selected', true);

                ;
                $('#iata_area').val(data.result.iata_area);


                $('#port-form-modal').modal('show');
                
            } 
        }
    });

});

$(document).on('click', '.btn-add-status', function(){
    $('input[name=status_name]').val("");
    $('textarea[name=status_desc]').val("");
    $('#status-form-modal').modal('show');
});


$(document).on('click', '.btn-save-status', function(){
    var l = Ladda.create(this);
    l.start();

    var form = $('form[name=status-form]');

    var config = {
        type: 'POST',
        url: '/settings/store_status',
        data:  form.serialize(),
        dataType: 'json',
    }

    var ajax = $.ajax(config);

    ajax.done(function(response){
        _setToast();
        if (response.success) {
            toastr.options.onHidden = function() { 
                $('#status-form-modal').modal('hide');
                l.stop();
                window.location = "/settings/status";
            };
            toastr.success('Please wait, redirect the page','Successful');
        } else {
            toastr.error('Unable to submit your request','Oooooppss');
            l.stop();
        }
    });
});

$(document).on('click', '.btn-edit-status', function () {
    status = $(this).data('id');

    $.ajax({
        type: 'GET',
        url: '/settings/status_data',
        data:  {'status_id' :  status},
        dataType: 'json',
        success:function(data){
            if(data.success) {

                $('input[name=edit_status_name]').val(data.result.status_name);
                $('textarea[name=edit_status_desc]').val(data.result.status_description);
                
                if (data.result.status_visibility == 1) 
                    $('input[name="edit_status_visibility"]').prop("checked", true);
                else 
                    $('input[name="edit_status_visibility"]').prop("checked", false);

                if (data.result.is_enabled == 1) 
                    $('input[name="edit_status_enabled"]').prop("checked", true);
                else 
                    $('input[name="edit_status_enabled"]').prop("checked", false);
                
                $('input[name=edit_status_id]').val(data.result.id);

                $('#edit-status-form-modal').modal('show');
                
            } 
        }
    });

});


$(document).on('click', '.btn-save-edit-status', function(){
    var l = Ladda.create(this);
    l.start(); 
    var form = $('form[name="edit-status-form"]');
    $.ajax({
        type: 'POST',
        url: '/settings/status_save_data',
        data:  form.serialize(),
        dataType: 'json',
        success:function(data){
            if(data.success) {
                toastr.options.onHidden = function() { 
                    $('#edit-status-form-modal').modal('hide');
                    l.stop();
                    window.location = "/settings/status";
                };
                toastr.success('Please wait, redirect the page','Successful');
            } else {
                toastr.error('Unable to submit your request','Oooooppss');
                l.stop();
            }
        }
    });
});
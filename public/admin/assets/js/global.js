var _setToast = function (positionClass) {
    if(positionClass=='') {
        positionClass = "toast-top-center";
    }
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "progressBar": true,
        "preventDuplicates": false,
        "positionClass": positionClass,
        "onclick": null,
        "showDuration": "400",
        "hideDuration": "500",
        "timeOut": "1000",
        "extendedTimeOut": "500",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };
}
<?php namespace App\Controllers;

use App\Libraries\Crud;
use CodeIgniter\Controller;
use App\Models\MainModel;

class Bill extends BaseController
{

	var $data;
	protected $crud;

	function __construct()
	{
		$params = [
			'table' => 'master_bills',
			'dev' => false,
			'fields' => $this->field_options(),
			'form_title_add' => 'Master bill entry',
			'form_title_update' => 'Edit User',
			'form_submit' => 'Add',
			'table_title' => 'Master Bill',
			'form_submit_update' => 'Update',
			'base' => '',

		];
		$this->crud = new Crud($params, service('request'));
		$this->data['load_css'][] = 'plugins/datatables-bs4/css/dataTables.bootstrap4.min.css';
        $this->data['load_css'][] = 'plugins/datatables-responsive/css/responsive.bootstrap4.min.css';
     
        $this->data['load_js'][] = 'plugins/datatables/jquery.dataTables.min.js';
        $this->data['load_js'][] = 'plugins/datatables-bs4/js/dataTables.bootstrap4.min.js';
        $this->data['load_js'][] = 'plugins/datatables-responsive/js/dataTables.responsive.min.js';
		$this->data['load_js'][] = 'plugins/datatables-responsive/js/responsive.bootstrap4.min.js';
		
	}

	public function index() 
	{
		$this->data['load_js'][] = 'js/bills/bill.js'; 
		
		return view('admin/bills/list', $this->data);
	}

	public function list()
	{
		
		$mainModel = new MainModel();
        $iTotal = $mainModel->countAll('transactions');
        $result = $mainModel->getAnyItems('transactions');

        $iFilteredTotal = count($result);

        if ($iFilteredTotal == 1) {
            $record[] = array(
                $result['id'],
                $result['master_ref'],
                $result['house_ref'],
                $result['direct'],
				$result['service_type'],
				$result['etd'],
				$result['eta'],
				$result['atd'],
				$result['ata']
            );
        } else {
            if($result) {
                foreach ($result as $idx=> $row) {
                    $record[] = array(
                        $row->id,
                        $row->master_ref,
                        $row->house_ref,
                        $row->direct,
                        $row->service_type,
                        date("m/d/Y",strtotime($row->etd)),
                        date("m/d/Y",strtotime($row->eta)),
                        date("m/d/Y",strtotime($row->atd)),
                        date("m/d/Y",strtotime($row->ata))
                    );
                }
            }
        }

        if (!isset($_POST['sEcho'])) {
            $num = 1;
        } else {
            $num = $_POST['sEcho'];
        }
        $output = array(
            "sEcho" => intval($num),
            "iTotalRecords" => $iFilteredTotal,
            "iTotalDisplayRecords" => $iTotal,
            "aaData" => $record
        );
        echo json_encode($output);
	}

	public function entry() 
	{
		$this->data['load_js'][] = 'plugins/moment/moment.min.js'; 
		$this->data['load_js'][] = 'plugins/daterangepicker/daterangepicker.js'; 
		$this->data['load_js'][] = 'js/bills/bill.js'; 
		$this->data['load_css'][] = 'plugins/daterangepicker/daterangepicker.css'; 
		$this->data['load_css'][] = 'css/main.css'; 
		
		$mainModel = new MainModel();

		$agents = $mainModel->getAnyItems('partners', ["partner_type" => "A"]);
		$consignees = $mainModel->getAnyItems('partners', ["partner_type" => "C"]);
		$shippers = $mainModel->getAnyItems('partners', ["partner_type" => "S"]);
		$countries = $mainModel->getAnyItems('countries');
		$ports = $mainModel->getAnyItems('ports');

		$this->data["params"] = [
			"agents" => $agents,
			"consignees" => $consignees,
			"shippers" => $shippers,
			"countries" => $countries,
			"ports" => $ports
		];

		return view('admin/bills/master', $this->data);
	}

	public function submit() 
	{
		if ($this->request->isAJAX()) {
			
			$mainModel = new MainModel();
			
			$data = $this->request->getRawInput();

			// echo "<pre>";
			// print_r($data);
			// echo "</pre>";

			$direct = $this->request->getVar('direct');
			$master_ref = $this->request->getVar('master-bill-no');
			$house_ref = $this->request->getVar('house-bill-no');
			$loading_port = $this->request->getVar('port-of-loading');
			$discharge_port = $this->request->getVar('port-of-discharge');
			$origin_destination = $this->request->getVar('origin-destination');
			$final_destination = $this->request->getVar('final-destination');
			$agent = $this->request->getVar('agent');
			$agent1 = $this->request->getVar('agent1');
			$agent2 = $this->request->getVar('agent2');
			$status = $this->request->getVar('trans-status');
			$vessel = $this->request->getVar('vessel-voyage');
			$posted = $this->request->getVar('posted');

			$trans_data = [
				"master_ref" => $master_ref,
				"house_ref" => $house_ref,
				"sub_ref" => "",
				"posted" => $posted == 'false' ? 0 : 1,
				"direct" => ($direct == "on") ? 1 : 0,
				"service_type" => $data['service-type'],
				"vessel_voyage" => $vessel,
				"loading_port" => $loading_port,
				"discharge_port" => $discharge_port,
				"origin_destination" => $origin_destination,
				"final_destination" => $final_destination,
				"agent_code" => $agent,
				"notify_agent_code_1" => $agent1,
				"notify_agent_code_2" => $agent2,
				"etd" => date("Y-m-d H:i:s", strtotime($data['etd'])),
				"eta" => date("Y-m-d H:i:s", strtotime($data['eta'])),
				"atd" => date("Y-m-d H:i:s", strtotime($data['atd'])),
				"ata" => date("Y-m-d H:i:s", strtotime($data['ata'])),
				"status" => $status
			];

			$trans_id = $mainModel->saveData("transactions", $trans_data);
			
			$shippers = $this->request->getVar('shipper');
			$consignees = $this->request->getVar('consignee');

			if (is_array($shippers)) {
				$aShippers = [];
				foreach ($shippers as $shipper) {
					$tempShipper = [
						"transaction_id" => $trans_id,
						"partner_code" => $shipper,
						"partner_type" => "S"
					];
					array_push($aShippers, $tempShipper);
				}
				$mainModel->batchInsert("transaction_shippers", $aShippers);
			}
			if (is_array($consignees)) {
				$aConsignees = [];
				foreach ($consignees as $consignee) {
					$tempConsignees = [
						"transaction_id" => $trans_id,
						"partner_code" => $consignee,
						"partner_type" => "C"
					];
					array_push($aConsignees, $tempConsignees);
				}
				$mainModel->batchInsert("transaction_shippers", $aConsignees);
			}


			$loop_counter = count($data['container-num']);
			$x = 0;
			$attributes = [];

			while($x < $loop_counter) {
				
				$attrib = [
					"transaction_id" => $trans_id,
					"container_no" => $data["container-num"][$x],
					"seal_no" => $data["seal-num"][$x],
					"no_of_pkgs" => $data["pkgs"][$x],
					"gross_weight" => $data["weight"][$x],
					"cbm" => $data["cbm"][$x],
					"size_type" => $data["size-type"][$x],
					"fcl_lcl" => $data["fcl-lcl"][$x],
					"description" => $data["description"][$x]
				];
				
				array_push($attributes, $attrib);
				$x++;
			}
			
			$mainModel->batchInsert("transaction_attributes", $attributes);

			$msg = ($posted == "false") ? "Successfully saved." : "Successfully posted.";
			echo json_encode(array('success'=>true, 'msg'=> $msg));  
		}
	}

	public function house() 
	{
		$this->data['load_js'][] = 'js/bills/bill.js'; 
		$this->data['load_css'][] = 'css/main.css'; 
		
		$mainModel = new MainModel();

		$agents = $mainModel->getAnyItems('partners', ["partner_type" => "A"]);
		$consignees = $mainModel->getAnyItems('partners', ["partner_type" => "C"]);
		$shippers = $mainModel->getAnyItems('partners', ["partner_type" => "S"]);
		$countries = $mainModel->getAnyItems('countries');
		$ports = $mainModel->getAnyItems('ports');

		$this->data["params"] = [
			"agents" => $agents,
			"consignees" => $consignees,
			"shippers" => $shippers,
			"countries" => $countries,
			"ports" => $ports
		];


		return view('admin/bills/house', $this->data);
	}

	protected function field_options()
	{
		$fields = [];
		$field['id'] = ['label' => 'ID'];
		
		$field['master_bill_no'] = ['label' => 'Master bill no.', 'required' => true, 'helper' => '', 'class' => 'col-md-6 col-sm-6'];
		$field['reference_no'] = ['label' => 'Reference no.', 'required' => true, 'helper' => '', 'class' => 'col-md-6 col-sm-6'];

		return $fields;
	}

	//--------------------------------------------------------------------

}

<?php

namespace App\Controllers;

use App\Libraries\Crud;
use App\Models\MainModel;
use App\Models\UserModel;

class Users extends BaseController
{
	protected $crud;

	protected $helpers = ['auth_helper'];

	function __construct()
	{
		$params = [
			'table' => 'users',
			'dev' => false,
			'fields' => $this->field_options(),
			'form_title_add' => 'Add User',
			'form_title_update' => 'Edit User',
			'form_submit' => 'Add',
			'table_title' => 'Users',
			'form_submit_update' => 'Update',
			'base' => '',

		];
		$this->crud = new Crud($params, service('request'));

		$this->data['load_js'][] = 'module/user/user.js'; 
		$this->data['load_js'][] = 'plugins/dataTables/datatables.min.js';
		$this->data['load_js'][] = 'plugins/dataTables/dataTables.bootstrap4.min.js';
		//$this->data['load_js'][] = 'plugins/ag-grid/ag-grid-community.min.noStyle.js';
		

		$this->data['load_css'][] = 'plugins/dataTables/datatables.min.css';
		// $this->data['load_css'][] = 'plugins/ag-grid/ag-grid.css';
		// $this->data['load_css'][] = 'plugins/ag-grid/ag-theme-balham.css';
        
		$this->data['load_js'][] = 'plugins/pwstrength/pwstrength-bootstrap.min.js';
		$this->data['load_js'][] = 'plugins/pwstrength/zxcvbn.js';

		helper(['url', 'form', 'template']);
	}

	public function index() {
		echo inspinia_theme('admin/users/table', $this->data);
	}

	function getUserList() {
		$user = new UserModel();
		$data = $user->getUserList();
		echo $data;
	}

	public function add() {
		$this->User = new UserModel();
	
		$this->data['user']= array();

		$this->data['modules'] = $this->User->getModules();

		$this->data['useraccess'] = array();

		$this->data['user_permissions'] = $this->User->getPermission();
		$this->data['user_access_permissions'] = array();

		echo inspinia_theme('admin/users/create', $this->data);
		
	}

	public function edit($user_id) {
		$this->User = new UserModel();
		
		$this->data['user']=$this->User->get_data($user_id);

		$this->data['modules'] = $this->User->getModules($user_id);

		$this->data['useraccess'] = $this->User->getUserAccess($user_id);

		$this->data['user_permissions'] = $this->User->getPermission();

		$this->data['user_access_permissions'] = $this->User->getUserPermission($user_id);

		echo inspinia_theme('admin/users/create', $this->data);
	}

	function user_store() {
		$this->data = [];

		helper(['form', 'url']);
		$main = new MainModel();
		$user = new UserModel();

		if ($this->request->getMethod() == 'post') {
			
			//save
			$user_id = $this->request->getVar('user_id');
			$first_name = $this->request->getVar('first_name');

			$last_name = $this->request->getVar('last_name');
			$email = $this->request->getVar('email');
			$position = $this->request->getVar('position');
			$password = $this->request->getVar('password');
			$username = $this->request->getVar('username');
			$is_enabled = $this->request->getVar('is_enabled');
			$company = $this->request->getVar('company');
			$department = $this->request->getVar('department');

			$editPassword = $this->request->getVar('editPassword');

			$user_access =  getPrefixedItemsFromArray($_POST, "attr_");
			$user_permissions =  getPrefixedItemsFromArray($_POST, "permission_");

			$additional_data = array(
				'first_name' => $first_name,
				'last_name' => $last_name,
				'company' => $company,
				'department'=>$department,
				'position'=>$position,
				'active'=>$is_enabled
			);
			
			$data_variable = array(
				'first_name' =>  $first_name,
				'last_name' =>  $last_name,
				'username' =>  $username,
				'email' =>  $email,
				'password' =>  $password,
				'department' =>  $department,
				'position' =>  $position,
				'active' =>  $is_enabled,
				'company' =>  $company,
				
			);

			$session = \Config\Services::session();

			if($user_id) {
				
				$user->update_user($user_id, $data_variable);

				$main->deleteItems('useraccess', array('user_id'=>$user_id));

				$useraccess_data = array();

				foreach ($user_access as $access_key => $value) {

					$user_access_permission = explode("_", $access_key);

					$useraccess_data = array(
						'module_id' => $user_access_permission[1],
						'user_id' => $user_id,
						$user_access_permission[2] => 1
					);
					
					$main->saveData('useraccess', $useraccess_data);
					
				}

				/*Update User Permission*/
				$main->deleteItems('user_permissions', array('user_id'=>$user_id));

				foreach ($user_permissions as $permission_key => $perm_val) {

					$user_permission_selected = explode("_", $permission_key);
					
					$user_permission_data = array(
						'main_menu_id' => $user_permission_selected[1],
						'main_sub_id' => $user_permission_selected[2],
						'date_created' => date('Y-m-d H:i:s'),
						'created_by' => '',
						'user_id' => $user_id,
					);

					$main->saveData('user_permissions', $user_permission_data);
					
				}

			} else {

				$ret = $user->register($username, $password, $email, $additional_data, $groups = array());

				$useraccess_data = array();

				/*Insert User Access*/
				$main->deleteItems('useraccess', array('user_id'=>$ret));

				foreach ($user_access as $access_key => $value) {

					$user_access_permission = explode("_", $access_key);
					
					$useraccess_data = array(
						'module_id' => $user_access_permission[1],
						'user_id' => $ret,
						$user_access_permission[2] => 1
					);
					$main->saveData('useraccess', $useraccess_data);
											
				}

				/*Insert User Permission*/
				$main->deleteItems('user_permissions', array('user_id'=>$ret));

				foreach ($user_permissions as $permission_key => $perm_val) {

					$user_permission_selected = explode("_", $permission_key);
					
					$user_permission_data = array(
						'main_menu_id' => $user_permission_selected[1],
						'main_sub_id' => $user_permission_selected[2],
						'date_created' => date('Y-m-d H:i:s'),
						'created_by' => '',
						'user_id' => $ret,
					);

					$main->saveData('user_permissions', $user_permission_data);
				
				}

				$session->setFlashdata('success','User created');
			}

			//return redirect()->to('users');
			echo json_encode(array('success'=>true,'msg'=>'Added a new user','url'=>base_url('users')));
            
		}
		
	}

	protected function field_options() {
		$fields = [];
		$field['id'] = ['label' => 'ID'];
		$fields['first_name'] = ['label' => 'First Name', 'required' => true, 'helper' => 'Type your First name', 'class' => 'col-12 col-sm-6'];
		$fields['last_name'] = ['label' =>'Last Name', 'required' => true, 'helper' => 'Type your Last name', 'class' => 'col-12 col-sm-6'];
		$fields['email'] = ['label' => 'Email','required' => true, 'unique' => [true, 'email']];
		$fields['active'] = ['label' => 'Status','required' => true,];
		$fields['created_at'] = ['label' => 'Created at', 'only_edit' => true];
		$fields['ip_address'] = ['label' => 'IP address', 'only_edit' => true];
		$fields['salt'] = ['only_edit' => true];
		$fields['activate_hash'] = ['only_edit' => true];
		$fields['reset_hash'] = ['only_edit' => true];
		$fields['reset_expires'] = ['only_edit' => true];
		$fields['last_login'] = ['only_edit' => true];

		$fields['password'] = ['label' => 'Password',
		 'required' => true, 
		 'only_add' => true,
		 'type' => 'password',
			'class' => 'col-12 col-sm-6',
		 'confirm' => true, 
		 'password' => true];

		return $fields;
	}
}

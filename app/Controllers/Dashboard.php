<?php namespace App\Controllers;

use App\Library\Template;

class Dashboard extends BaseController
{
	function __construct()
	{
        $this->data['title'] = 'Dasboard';

        $this->data['load_js'][] = 'plugins/morris/raphael-2.1.0.min.js';
		$this->data['load_js'][] = 'plugins/morris/morris.js';

		$this->data['load_js'][] =  'plugins/chartJs/Chart.min.js';

		$this->data['load_js'][] =  'plugins/d3/d3.min.js';
		$this->data['load_js'][] =  'plugins/c3/c3.min.js';

		$this->data['load_js'][] = 'module/dashboard/home.js';

       
		
		$this->data['load_css'][] = 'plugins/morris/morris-0.4.3.min.css';
		$this->data['load_css'][] = 'plugins/c3/c3.min.css';
        
       

        helper('template');
    }

	
	public function index()
	{
		$isLoggedIn =  session()->get("isLoggedIn");

		if($isLoggedIn) {
			echo inspinia_theme('dashboard/index', $this->data);

		} else {
			return redirect()->to('/login');
		}
		
	}

	//--------------------------------------------------------------------

}

<?php
namespace App\Controllers;


use App\Models\MainModel;
use App\Models\TransactionModel;

class Service extends BaseController {
    
    var $data;
    var $db;
    var $mainModel;

    protected $helpers = ['auth_helper', 'form', 'url'];

    function __construct()
	{
        $this->data['title'] = 'test';

        $this->data['load_js'][] = 'plugins/dataTables/datatables.min.js';
		$this->data['load_js'][] = 'plugins/dataTables/dataTables.bootstrap4.min.js';
        $this->data['load_css'][] = 'plugins/dataTables/datatables.min.css';
        
        $this->db = \Config\Database::connect();

        $this->mainModel = new MainModel();
        

        helper('template');
    }

    function sea_import() 
    {
        $this->data['load_js'][] = 'module/transaction/service.js'; 
        echo inspinia_theme('admin/v2/sea_import_list', $this->data);
    }

    function sea_export() 
    {
        $this->data['load_js'][] = 'module/transaction/service.js'; 
        echo inspinia_theme('admin/v2/sea_export_list', $this->data);
    }

    function air_import() 
    {
        $this->data['load_js'][] = 'module/transaction/service.js'; 
        echo inspinia_theme('admin/v2/air_import_list', $this->data);
    }

    function air_export() 
    {
        $this->data['load_js'][] = 'module/transaction/service.js'; 
        echo inspinia_theme('admin/v2/air_export_list', $this->data);
    }

    function air_domestic() 
    {
        $this->data['load_js'][] = 'module/transaction/service.js'; 
        echo inspinia_theme('admin/v2/air_domestic_list', $this->data);
    }

    function sea_domestic() 
    {
        $this->data['load_js'][] = 'module/transaction/service.js'; 
        echo inspinia_theme('admin/v2/sea_domestic_list', $this->data);
    }

    function import_brokerage() 
    {
        $this->data['load_js'][] = 'module/transaction/service.js'; 
        echo inspinia_theme('admin/v2/import_brokerage_list', $this->data);
    }

    function export_brokerage() 
    {
        $this->data['load_js'][] = 'module/transaction/service.js'; 
        echo inspinia_theme('admin/v2/export_brokerage_list', $this->data);
    }

    function get_data()
    {
        $transModel = new TransactionModel();
        $this->data['load_js'][] = 'plugins/moment/moment.min.js'; 
		$this->data['load_js'][] = 'plugins/daterangepicker/daterangepicker.js'; 
		$this->data['load_js'][] = 'plugins/dropzone/dropzone.js'; 
        $this->data['load_js'][] = 'module/transaction/service.js'; 
		$this->data['load_css'][] = 'plugins/daterangepicker/daterangepicker.css'; 

        $this->data["agents"] = $this->mainModel->getAnyItems('partners', ["partner_type" => "A"], ["partner_type" => "X"]);
        $this->data["consignees"] = $this->mainModel->getAnyItems('partners', ["partner_type" => "C",], ["partner_type" => "X"]);
        $this->data["shippers"] = $this->mainModel->getAnyItems('partners', ["partner_type" => "S"], ["partner_type" => "X"]);
        $this->data["countries"] = $this->mainModel->getAnyItems('countries');
        $this->data["ports"] = $this->mainModel->getAnyItems('ports');
        $this->data["incoterms"] = $this->mainModel->getAnyItems('incoterms', ["is_enabled" => 1]);
        $this->data["load_types"] = $this->mainModel->getAnyItems('load_types');
        $this->data["vehicles"] = $this->mainModel->getAnyItems('vehicles', ["is_enabled" => 1]);
        $this->data["unit_of_measures"] = $this->mainModel->getAnyItems('unit_of_measures', ["is_active" => 1]);
        $this->data["list_of_charges"] = $this->mainModel->getAnyItems('list_of_charges', ["is_active" => 1]);
        $this->data["status"] = $this->mainModel->getAnyItems('status', ["is_enabled" => 1]);
        $this->data["master_refs"] = $transModel->get_master_refs();
    }

    function sea_import_entry() 
    {
        $this->get_data();
        $this->data["carriers"] = $this->mainModel->getAnyItems('carriers', ["carrier_type" => "sea"]);
        echo inspinia_theme('admin/v2/sea_import_entry', $this->data);
    }

    function sea_export_entry() 
    {
        $this->get_data();
        $this->data["carriers"] = $this->mainModel->getAnyItems('carriers', ["carrier_type" => "sea"]);
        echo inspinia_theme('admin/v2/sea_export_entry', $this->data);
    }

    function air_import_entry() 
    {
        $this->get_data();
        $this->data["carriers"] = $this->mainModel->getAnyItems('carriers', ["carrier_type" => "air"]);
        echo inspinia_theme('admin/v2/air_import_entry', $this->data);
    }

    function air_export_entry() 
    {
        $this->get_data();
        $this->data["carriers"] = $this->mainModel->getAnyItems('carriers', ["carrier_type" => "air"]);
        echo inspinia_theme('admin/v2/air_export_entry', $this->data);
    }

    function air_domestic_entry() 
    {
        $this->get_data();
        $this->data["carriers"] = $this->mainModel->getAnyItems('carriers', ["carrier_type" => "air"]);
        echo inspinia_theme('admin/v2/air_domestic_entry', $this->data);
    }
    function sea_domestic_entry() 
    {
        $this->get_data();
        $this->data["carriers"] = $this->mainModel->getAnyItems('carriers', ["carrier_type" => "sea"]);
        echo inspinia_theme('admin/v2/sea_domestic_entry', $this->data);
    }
    function import_brokerage_entry() 
    {
        $this->get_data();
        $this->data["carriers"] = $this->mainModel->getAnyItems('carriers');
        echo inspinia_theme('admin/v2/import_brokerage_entry', $this->data);
    }
    function export_brokerage_entry() 
    {
        $this->get_data();
        $this->data["carriers"] = $this->mainModel->getAnyItems('carriers');
        echo inspinia_theme('admin/v2/export_brokerage_entry', $this->data);
    }

    function get_list()
    {
        if (!isset($_POST['service_type'])) {
            $service_type = "is";
        } else {
            $service_type = $_POST['service_type'];
        }
        
        $trxn = new TransactionModel();
        $iTotal = $trxn->get_count_transaction_data(["service_type" => $service_type]);
        $result = $trxn->get_transaction_data(["service_type" => $service_type]);

        

        $iFilteredTotal = count($result);

        if ($iFilteredTotal == 1) {
            $record[] = array(
                $result[0]->id,
                $result[0]->master_ref,
                $result[0]->house_ref,
                $result[0]->status,
				$result[0]->etd,
				$result[0]->eta,
				$result[0]->atd,
				$result[0]->ata,
				$result[0]->id,
            );
        } else {
            if($result) {
                foreach ($result as $idx=> $row) {
                    $record[] = array(
                        $row->id,
                        $row->master_ref,
                        $row->house_ref,
                        $row->status,
                        date("m/d/Y",strtotime($row->etd)),
                        date("m/d/Y",strtotime($row->eta)),
                        date("m/d/Y",strtotime($row->atd)),
                        date("m/d/Y",strtotime($row->ata)),
                        $row->id
                    );
                }
            } else {
                $record = [];
            }
        }

        if (!isset($_POST['sEcho'])) {
            $num = 1;
        } else {
            $num = $_POST['sEcho'];
        }
        $output = array(
            "sEcho" => intval($num),
            "iTotalRecords" => $iFilteredTotal,
            "iTotalDisplayRecords" => $iTotal,
            "aaData" => $record
        );
        echo json_encode($output);
    }

    function store_entry() 
    {
        if ($this->request->isAJAX()) {

            $raw_data  = $this->request->getRawInput();

            $transaction_datetime = $this->request->getVar('transaction-datetime');
            $service_type = $this->request->getVar('service-type');
            $posted = $this->request->getVar('posted');
            $is_direct = $this->request->getVar('direct');

            $master_ref = $this->request->getVar('master-ref');
            $vessel_voyage = $this->request->getVar('vessel-voyage');
            $carriers = $this->request->getVar('carriers');
            $master_gross_wt = $this->request->getVar('master-gross-wt');
            $master_chargeable_wt = $this->request->getVar('master-chargeable-weight');
            $etd = $this->request->getVar('etd');
            $eta = $this->request->getVar('eta');
            $atd = $this->request->getVar('atd');
            $ata = $this->request->getVar('ata');
            $country_of_origin = $this->request->getVar('origin-country');
            $port_of_origin = $this->request->getVar('origin-port');
            $country_of_destination = $this->request->getVar('destination-country');
			$port_of_destination = $this->request->getVar('destination-port');

            $house_ref  = $this->request->getVar('house-ref');
            $consignee  = $this->request->getVar('consignee');
            $shipper = $this->request->getVar('shipper');
            $agent = $this->request->getVar('agent');
            $agent_1 = $this->request->getVar('agent_notify');
            $agent_2 = $this->request->getVar('agent_third_party');
            $inco_term             = $this->request->getVar('inco-term');
            $mode_of_shipment      = $this->request->getVar('mode-of-shipment');
            $gross_wt              = $this->request->getVar('gross-wt');
            $chargeable_weight     = $this->request->getVar('chargeable-weight');
            $uom                   = $this->request->getVar('uom');
            $forwarder             = $this->request->getVar('forwarder');
            $supp_invoice_number   = $this->request->getVar('supp-invoice-number');
            $invoice_value         = $this->request->getVar('invoice-value');
            $invoice_currency      = $this->request->getVar('invoice-currency');
            $import_entry_no       = $this->request->getVar('import-entry-no');
            $load_type             = $this->request->getVar('load-type');
            $nta_icc_no            = $this->request->getVar('nta-icc-no');
            $peza_permit           = $this->request->getVar('peza-permit');
            $custom_value          = $this->request->getVar('custom-value');
            $custom_duties         = $this->request->getVar('custom-duties');
            $vat_value             = $this->request->getVar('vat-value');
            $date_customs_cleared  = $this->request->getVar('date-customs-cleared');
            $no_of_containers      = $this->request->getVar('no-of-containers');
            $warehouse_rcv_date    = $this->request->getVar('warehouse-rcv-date');
            $general_description   = $this->request->getVar('general-description');

            $charge_code         = $this->request->getVar('charge-code');
            $cargo_tbl           = $this->request->getVar('cargo-no-of-pkgs');
            $container_tbl       = $this->request->getVar('cl-container-no');
            $material_tbl        = $this->request->getVar('ml-po-num');

            $t_status            = $this->request->getVar('update-status');
            $t_status_date       = $this->request->getVar('update-status-date');
            $t_status_desc       = $this->request->getVar('update-status-description');
            $t_status_file       = $this->request->getVar('update-status-attached-file');

            $trxn_data = [
                "transaction_datetime" => date("Y-m-d H:i:s", strtotime($transaction_datetime)),
                "master_ref" => $master_ref,
                "is_direct" => ($is_direct == "on") ? 1 : 0,
                "status" => ($posted == 'false') ? "S" : "P",
                "service_type" => $service_type,
                "port_of_origin" => $port_of_origin,
                "port_of_destination" => $port_of_destination,
                "country_of_origin" => $country_of_origin,
                "country_of_destination" => $country_of_destination,
                "etd" => date("Y-m-d H:i:s", strtotime($etd)),
                "eta" => date("Y-m-d H:i:s", strtotime($eta)),
                "atd" => date("Y-m-d H:i:s", strtotime($atd)),
                "ata" => date("Y-m-d H:i:s", strtotime($ata)),
                "total_gross_wt" => $master_gross_wt,
                "total_chargeable_wt" => $master_chargeable_wt,
                "vessel" => $vessel_voyage,
                "carrier" => $carriers,
                "current_status" => $t_status,
                "current_status_datetime" => date("Y-m-d H:i:s", strtotime($t_status_date)),
                "created_by" => session()->get("id"),
                "created_at" => date("Y-m-d H:i:s"),
                "updated_at" => date("Y-m-d H:i:s")
            ];

            $trans_id = $this->mainModel->saveData("transactions", $trxn_data);

            $house_data = [
                "transaction_id" => $trans_id,
                "house_ref" => $house_ref,
                "shipper_code" => $shipper,
                "consignee_code" => $consignee,
                "agent_code" => $agent,
                "agent_notify_1" => $agent_1,
                "agent_notify_2" => $agent_2,
                "mode_of_shpt" => $mode_of_shipment,
                "general_desc" => trim($general_description),
                "inco_term" => $inco_term,
                "forwarder" => $forwarder,
                "gross_wt" => $gross_wt,
                "chargeable_wt" => $chargeable_weight,
                "uom" => $uom,
                "load_type" => $load_type, 
                "nta_icc_number" => $nta_icc_no,
                "peza_permit" => $peza_permit,
                "custom_value" => $custom_value,
                "custom_duties" => $custom_duties,
                "vat_value" => $vat_value,
                "date_customs_cleared" => date("Y-m-d H:i:s", strtotime($date_customs_cleared)),
                "no_of_containers" => $no_of_containers,
                "warehouse_rcv_date" => date("Y-m-d H:i:s", strtotime($warehouse_rcv_date)),
                "supp_invoice_number" => $supp_invoice_number,
                "invoice_value" => $invoice_value,
                "invoice_currency" => $invoice_currency,
                "import_entry_no" => $import_entry_no,
                "created_at" => date("Y-m-d H:i:s"),
                "updated_at" => date("Y-m-d H:i:s")
            ];

            $this->mainModel->saveData("transaction_house", $house_data);

            $status_history_data = [
                "transaction_id" => $trans_id,
                "status_code" => $t_status,
                "status_comment" => $t_status_desc,
                "status_file" =>  $t_status_file,
                "updated_by" => session()->get("id"),
                "created_at" => date("Y-m-d H:i:s", strtotime($t_status_date))
            ];

            $this->mainModel->saveData("transactions_history", $status_history_data);

            $charge_loop_counter = $charge_code ? count($charge_code) : 0;
            if ($charge_loop_counter > 0) {
                $this->insert_charges($charge_loop_counter, $trans_id, $raw_data);
            }
            $cargo_tbl_ctr = $cargo_tbl ? count($cargo_tbl) : 0;
            if ($cargo_tbl_ctr > 0) {
                $this->insert_cargo($cargo_tbl_ctr, $trans_id, $raw_data);
            }
            $cl_tbl_ctr = $container_tbl ? count($container_tbl) : 0;
            if ($cl_tbl_ctr > 0) {
                $this->insert_container($cl_tbl_ctr, $trans_id, $raw_data);
            }
            $ml_tbl_ctr = $material_tbl ? count($material_tbl) : 0;
            if ($ml_tbl_ctr > 0) {
                $this->insert_material($ml_tbl_ctr, $trans_id, $raw_data);
            }

            $msg = ($posted == "false") ? "Successfully saved." : "Successfully posted.";
            $url = $this->what_url($service_type);
			echo json_encode(array('success'=>true, 'msg'=> $msg, 'url' => $url));  
            return;
        }
        echo json_encode(array('success'=>false, 'msg'=> "Something went wrong!."));  
        return;
    }

    function what_url($service_type = "is")
    {
        $url = "";
        if (strtolower($service_type) == "ia") {
           $url = "/import_air";
        } else if (strtolower($service_type) == "ea") {
            $url = "/export_air";
        } elseif (strtolower($service_type) == "is") {
            $url = "/import_sea";
        } elseif (strtolower($service_type) == "es") {
            $url = "/export_sea";
        } elseif (strtolower($service_type) == "ib") {
            $url = "/import_brokerage";
        } elseif (strtolower($service_type) == "eb") {
            $url = "/export_brokerage";
        } elseif (strtolower($service_type) == "ad") {
            $url = "/air_domestic";
        } elseif (strtolower($service_type) == "sd") {
            $url = "/sea_domestic";
        } else {
            $url = "/import_sea";
        }
        return $url;
    }

    function insert_charges($ctr, $trans_id, $raw_data) 
    {
        $i = 0;
        $array_chrg = [];
        while($i < $ctr) {
            $charge_data = [
                "transaction_id" => $trans_id,
                "chg_code" => $raw_data["charge-code"][$i],
                "chg_desc" => $raw_data["charge-desc"][$i],
                "chg_amount" => $raw_data["charge-amount"][$i],
                "currency" => $raw_data["charge-currency"][$i],
                "php_value" => $raw_data["php-value"][$i],
                "receipted" => "",
                "doc_ref" => "",
                "doc_ref_type" => "",
                "supplier_code" => "",
                "remarks" => ""
            ];
            array_push($array_chrg, $charge_data);
            $i++;
        }
        $this->mainModel->batchInsert("transaction_charges", $array_chrg);
    }

    function insert_cargo($ctr, $trans_id, $raw_data) 
    {
        $i = 0;
        $batch_data = [];
        while($i < $ctr) {
            $data = [
                "transaction_id" => $trans_id,
                "no_of_package" => $raw_data["cargo-no-of-pkgs"][$i],
                "gross_wt" => $raw_data["cargo-gross-wt"][$i],
                "chargeable_wt" => $raw_data["cargo-chargeable-wt"][$i],
                "width" => $raw_data["cargo-width"][$i],
                "length" => $raw_data["cargo-length"][$i],
                "height" => $raw_data["cargo-height"][$i],
                "cbm" => $raw_data["cargo-cbm"][$i],
                "truck_type" => $raw_data["cargo-truck-type"][$i],
                "no_of_trucks" => $raw_data["cargo-no-of-trucks"][$i]
            ];
            array_push($batch_data, $data);
            $i++;
        }
        $this->mainModel->batchInsert("transaction_for_loose_cargo", $batch_data);
    }

    function insert_container($ctr, $trans_id, $raw_data) 
    {
        $i = 0;
        $batch_data = [];
        while($i < $ctr) {
            $data = [
                "transaction_id" => $trans_id,
                "container_number" => $raw_data["cl-container-no"][$i],
                "seal_num" => $raw_data["cl-seal-no"][$i],
                "size" => $raw_data["cl-size"][$i],
                "type" => $raw_data["cl-type"][$i]
            ];
            array_push($batch_data, $data);
            $i++;
        }
        $this->mainModel->batchInsert("transaction_container_list", $batch_data);
    }

    function insert_material($ctr, $trans_id, $raw_data) 
    {
        $i = 0;
        $batch_data = [];
        while($i < $ctr) {
            $data = [
                "transaction_id" => $trans_id,
                "po_number" => $raw_data["ml-po-num"][$i],
                "material_id" => $raw_data["ml-material-id"][$i],
                "item_declaration" => $raw_data["ml-item-declaration"][$i],
                "order_qty" => $raw_data["ml-order-qty"][$i],
                "actual_ship" => $raw_data["ml-actual-ship"][$i],
                "order_balance" => $raw_data["ml-order-balance"][$i],
                "unit_of_measure" => $raw_data["ml-unit-of-measure"][$i]
            ];
            array_push($batch_data, $data);
            $i++;
        }
        $this->mainModel->batchInsert("transaction_po_materials", $batch_data);
    }

    function view($trxn_id) 
    {
        $this->data['load_js'][] = 'plugins/moment/moment.min.js'; 
		$this->data['load_js'][] = 'plugins/daterangepicker/daterangepicker.js'; 
		$this->data['load_js'][] = 'plugins/dropzone/dropzone.js'; 
        $this->data['load_js'][] = 'module/transaction/service.js'; 
		$this->data['load_css'][] = 'plugins/daterangepicker/daterangepicker.css'; 

        $transModel = new TransactionModel();
        $transaction = $this->mainModel->getItem('transactions', ["id" => $trxn_id]);
        
        if ($transaction) {
            $this->data['transaction'] = $transaction;
            $house = $this->mainModel->getItem('transaction_house', ["transaction_id" => $transaction->id]);
            $this->data['house'] = $house;
            if (isset($house->shipper_code)) {
                $shipper = $this->mainModel->getItem('partners', ["partner_code" => $house->shipper_code]);
                $this->data["shipper"] = "{$shipper->partner_code} - {$shipper->partner_name}";
                // $this->data["shipper_address"] = "{$shipper->address1} {$shipper->address2} {$shipper->address3} {$shipper->zipcode} {$shipper->country_code}";
                $this->data["shipper_address"] = "{$shipper->address1}";
            } 
            if (isset($house->consignee_code)) {
                $consignee = $this->mainModel->getItem('partners', ["partner_code" => $house->consignee_code]);
                $this->data["consignee"] = "{$consignee->partner_code} - {$consignee->partner_name}";
                // $this->data["consignee_address"] = "{$consignee->address1} {$consignee->address2} {$consignee->address3} {$consignee->zipcode} {$consignee->country_code}";
                $this->data["consignee_address"] = "{$consignee->address1}";
            }
            if (isset($house->agent_code)) {
                $agent = $this->mainModel->getItem('partners', ["partner_code" => $house->agent_code]);
                $this->data["agent"] = "{$agent->partner_code} - {$agent->partner_name}";
                // $this->data["agent_address"] = "{$agent->address1} {$agent->address2} {$agent->address3} {$agent->zipcode} {$agent->country_code}";
                $this->data["agent_address"] = "{$agent->address1}";
            }
            if (isset($house->agent_notify_1)) {
                $agent_1 = $this->mainModel->getItem('partners', ["partner_code" => $house->agent_notify_1]);
                $this->data["agent_1"] = "{$agent_1->partner_code} - {$agent_1->partner_name}";
                // $this->data["agent_1_address"] = "{$agent_1->address1} {$agent_1->address2} {$agent_1->address3} {$agent_1->zipcode} {$agent_1->country_code}";
                $this->data["agent_1_address"] = "{$agent_1->address1}";
            }
            if (isset($house->agent_notify_2)) {
                $agent_2 = $this->mainModel->getItem('partners', ["partner_code" => $house->agent_notify_2]);
                $this->data["agent_2"] = "{$agent_2->partner_code} - {$agent_2->partner_name}";
                // $this->data["agent_2_address"] = "{$agent_2->address1} {$agent_2->address2} {$agent_2->address3} {$agent_2->zipcode} {$agent_2->country_code}";
                $this->data["agent_2_address"] = "{$agent_2->address1}";
            }
            $this->data["created_by"]       = $this->mainModel->getItem('users', ["id" => $transaction->created_by]); 
            $this->data["current_status"]   = $this->mainModel->getItem('status', ["status_code" => $transaction->current_status]); 
            $this->data["status"]           = $this->mainModel->getAnyItems('status', ["is_enabled" => 1]);

            if (isset($transaction->inco_term)) {
                $this->data["inco_term"] = $this->mainModel->getItem('incoterms', ["incoterm_code" => $transaction->inco_term]);
            }
            if (isset($transaction->carrier)) {
                $this->data["carrier"] = $this->mainModel->getItem('carriers', ["carrier_code" => $transaction->carrier]); 
            }
            if (isset($transaction->country_of_origin)) {
                $this->data["country_of_origin"] = $this->mainModel->getItem('countries', ["country_code" => $transaction->country_of_origin]); 
            }
            if (isset($transaction->port_of_origin)) {
                $this->data["port_of_origin"] = $this->mainModel->getItem('ports', ["port_code" => $transaction->port_of_origin, "port_country" => $transaction->country_of_origin]); 
            }
            if (isset($transaction->country_of_destination)) {
                $this->data["country_of_destination"] = $this->mainModel->getItem('countries', ["country_code" => $transaction->country_of_destination]); 
            }
            if (isset($transaction->port_of_destination)) {
                $this->data["port_of_destination"]  = $this->mainModel->getItem('ports', ["port_code" => $transaction->port_of_destination, "port_country" => $transaction->country_of_destination]); 
            }
            if (isset($house->load_type)) {
                $this->data["load_type"] = $this->mainModel->getItem('load_types', ["load_type_code" => $house->load_type]);
            }
            $this->data["po_materials"]     = $this->mainModel->getAnyItems('transaction_po_materials', ["transaction_id" => $transaction->id]);
            $this->data["container_list"]   = $this->mainModel->getAnyItems('transaction_container_list', ["transaction_id" => $transaction->id]);
            $this->data["charges"]          = $this->mainModel->getAnyItems('transaction_charges', ["transaction_id" => $transaction->id]);
            $this->data["loose_cargo"]      = $this->mainModel->getAnyItems('transaction_for_loose_cargo', ["transaction_id" => $transaction->id]);
            $this->data["history"]          = $transModel->get_history_items($transaction->id);
            
            $view = "";
            if ($transaction->service_type == "is") {
                $view = "admin/v2/sea_import_view";
            } elseif ($transaction->service_type == "es") {
                $view = "admin/v2/sea_export_view";
            } elseif ($transaction->service_type == "ia") {
                $view = "admin/v2/air_import_view";
            } elseif ($transaction->service_type == "ea") {
                $view = "admin/v2/air_export_view";
            } elseif ($transaction->service_type == "sd") {
                $view = "admin/v2/sea_domestic_view";
            } elseif ($transaction->service_type == "ad") {
                $view = "admin/v2/air_domestic_view";
            } elseif ($transaction->service_type == "ib") {
                $view = "admin/v2/import_brokerage_view";
            } elseif ($transaction->service_type == "eb") {
                $view = "admin/v2/export_brokerage_view";
            } else {
                $view = "admin/v2/sea_import_view";
            }

            echo inspinia_theme($view, $this->data);
            return;
        }
        return redirect()->to('/'); 
       
    }

    function edit($trxn_id)
    {
        $this->get_data();
        $transModel = new TransactionModel();
        $transaction = $this->mainModel->getItem('transactions', ["id" => $trxn_id]);
        
        if ($transaction) {

            if ($transaction->status == "P") {
                return redirect()->back();
            }

            $this->data['transaction'] = $transaction;
            $house = $this->mainModel->getItem('transaction_house', ["transaction_id" => $transaction->id]);
            $this->data['house'] = $house;
            if (isset($house->shipper_code)) {
                $shipper = $this->mainModel->getItem('partners', ["partner_code" => $house->shipper_code]);
                $this->data["shipper"] = "{$shipper->partner_code} - {$shipper->partner_name}";
                // $this->data["shipper_address"] = "{$shipper->address1} {$shipper->address2} {$shipper->address3} {$shipper->zipcode} {$shipper->country_code}";
                $this->data["shipper_address"] = "{$shipper->address1}";
            } 
            if (isset($house->consignee_code)) {
                $consignee = $this->mainModel->getItem('partners', ["partner_code" => $house->consignee_code]);
                $this->data["consignee"] = "{$consignee->partner_code} - {$consignee->partner_name}";
                // $this->data["consignee_address"] = "{$consignee->address1} {$consignee->address2} {$consignee->address3} {$consignee->zipcode} {$consignee->country_code}";
                $this->data["consignee_address"] = "{$consignee->address1}";
            }
            if (isset($house->agent_code)) {
                $agent = $this->mainModel->getItem('partners', ["partner_code" => $house->agent_code]);
                $this->data["agent"] = "{$agent->partner_code} - {$agent->partner_name}";
                // $this->data["agent_address"] = "{$agent->address1} {$agent->address2} {$agent->address3} {$agent->zipcode} {$agent->country_code}";
                $this->data["agent_address"] = "{$agent->address1}";
            }
            if (isset($house->agent_notify_1)) {
                $agent_1 = $this->mainModel->getItem('partners', ["partner_code" => $house->agent_notify_1]);
                $this->data["agent_1"] = "{$agent_1->partner_code} - {$agent_1->partner_name}";
                // $this->data["agent_1_address"] = "{$agent_1->address1} {$agent_1->address2} {$agent_1->address3} {$agent_1->zipcode} {$agent_1->country_code}";
                $this->data["agent_1_address"] = "{$agent_1->address1}";
            }
            if (isset($house->agent_notify_2)) {
                $agent_2 = $this->mainModel->getItem('partners', ["partner_code" => $house->agent_notify_2]);
                $this->data["agent_2"] = "{$agent_2->partner_code} - {$agent_2->partner_name}";
                // $this->data["agent_2_address"] = "{$agent_2->address1} {$agent_2->address2} {$agent_2->address3} {$agent_2->zipcode} {$agent_2->country_code}";
                $this->data["agent_2_address"] = "{$agent_2->address1}";
            }
            $this->data["created_by"]       = $this->mainModel->getItem('users', ["id" => $transaction->created_by]); 
            $this->data["current_status"]   = $this->mainModel->getItem('status', ["status_code" => $transaction->current_status]); 
            $this->data["status"]           = $this->mainModel->getAnyItems('status', ["is_enabled" => 1]);
            if (isset($transaction->inco_term)) {
                $this->data["inco_term"] = $this->mainModel->getItem('incoterms', ["incoterm_code" => $transaction->inco_term]);
            }
            if (isset($transaction->carrier)) {
                $this->data["carrier"] = $this->mainModel->getItem('carriers', ["carrier_code" => $transaction->carrier]); 
            }
            
            $this->data["ports_of_origin"] = $this->mainModel->getAnyItems('ports', ["port_country" => $transaction->country_of_origin]);
            $this->data["ports_of_destination"] = $this->mainModel->getAnyItems('ports', ["port_country" => $transaction->country_of_destination]);

           
            if (isset($house->load_type)) {
                $this->data["load_type"] = $this->mainModel->getItem('load_types', ["load_type_code" => $house->load_type]);
            }
            $this->data["po_materials"]     = $this->mainModel->getAnyItems('transaction_po_materials', ["transaction_id" => $transaction->id]);
            $this->data["container_list"]   = $this->mainModel->getAnyItems('transaction_container_list', ["transaction_id" => $transaction->id]);
            $this->data["charges"]          = $this->mainModel->getAnyItems('transaction_charges', ["transaction_id" => $transaction->id]);
            $this->data["loose_cargo"]      = $this->mainModel->getAnyItems('transaction_for_loose_cargo', ["transaction_id" => $transaction->id]);
            $this->data["history"]          = $transModel->get_history_items($transaction->id);
            $view = "";
            if ($transaction->service_type == "is") {
                $this->data["carriers"] = $this->mainModel->getAnyItems('carriers', ["carrier_type" => "sea"]);
                $view = "admin/v2/sea_import_edit";
            } elseif ($transaction->service_type == "es") {
                $this->data["carriers"] = $this->mainModel->getAnyItems('carriers', ["carrier_type" => "sea"]);
                $view = "admin/v2/sea_export_edit";
            } elseif ($transaction->service_type == "ia") {
                $this->data["carriers"] = $this->mainModel->getAnyItems('carriers', ["carrier_type" => "air"]);
                $view = "admin/v2/air_import_edit";
            } elseif ($transaction->service_type == "ea") {
                $this->data["carriers"] = $this->mainModel->getAnyItems('carriers', ["carrier_type" => "air"]);
                $view = "admin/v2/air_export_edit";
            } elseif ($transaction->service_type == "sd") {
                $this->data["carriers"] = $this->mainModel->getAnyItems('carriers', ["carrier_type" => "sea"]);
                $view = "admin/v2/sea_domestic_edit";
            } elseif ($transaction->service_type == "ad") {
                $this->data["carriers"] = $this->mainModel->getAnyItems('carriers', ["carrier_type" => "air"]);
                $view = "admin/v2/air_domestic_edit";
            } elseif ($transaction->service_type == "ib") {
                $this->data["carriers"] = $this->mainModel->getAnyItems('carriers');
                $view = "admin/v2/import_brokerage_edit";
            } elseif ($transaction->service_type == "eb") {
                $this->data["carriers"] = $this->mainModel->getAnyItems('carriers');
                $view = "admin/v2/export_brokerage_edit";
            } else {
                $view = "admin/v2/sea_import_edit";
            }

            echo inspinia_theme($view, $this->data);
            return;
        }
        return redirect()->to('/'); 
    }

    function update_entry()
    {
        if ($this->request->isAJAX()) {

            $raw_data  = $this->request->getRawInput();

            $transaction_datetime = $this->request->getVar('transaction-datetime');
            $service_type = $this->request->getVar('service-type');
            $posted = $this->request->getVar('posted');
            $is_direct = $this->request->getVar('direct');

            $master_ref = $this->request->getVar('master-ref');
            $vessel_voyage = $this->request->getVar('vessel-voyage');
            $carriers = $this->request->getVar('carriers');
            $master_gross_wt = $this->request->getVar('master-gross-wt');
            $master_chargeable_wt = $this->request->getVar('master-chargeable-weight');
            $etd = $this->request->getVar('etd');
            $eta = $this->request->getVar('eta');
            $atd = $this->request->getVar('atd');
            $ata = $this->request->getVar('ata');
            $country_of_origin = $this->request->getVar('origin-country');
            $port_of_origin = $this->request->getVar('origin-port');
            $country_of_destination = $this->request->getVar('destination-country');
			$port_of_destination = $this->request->getVar('destination-port');

            $house_ref  = $this->request->getVar('house-ref');
            $consignee  = $this->request->getVar('consignee');
            $shipper = $this->request->getVar('shipper');
            $agent = $this->request->getVar('agent');
            $agent_1 = $this->request->getVar('agent_notify');
            $agent_2 = $this->request->getVar('agent_third_party');
            $inco_term             = $this->request->getVar('inco-term');
            $mode_of_shipment      = $this->request->getVar('mode-of-shipment');
            $gross_wt              = $this->request->getVar('gross-wt');
            $chargeable_weight     = $this->request->getVar('chargeable-weight');
            $uom                   = $this->request->getVar('uom');
            $forwarder             = $this->request->getVar('forwarder');
            $supp_invoice_number   = $this->request->getVar('supp-invoice-number');
            $invoice_value         = $this->request->getVar('invoice-value');
            $invoice_currency      = $this->request->getVar('invoice-currency');
            $import_entry_no       = $this->request->getVar('import-entry-no');
            $load_type             = $this->request->getVar('load-type');
            $nta_icc_no            = $this->request->getVar('nta-icc-no');
            $peza_permit           = $this->request->getVar('peza-permit');
            $custom_value          = $this->request->getVar('custom-value');
            $custom_duties         = $this->request->getVar('custom-duties');
            $vat_value             = $this->request->getVar('vat-value');
            $date_customs_cleared  = $this->request->getVar('date-customs-cleared');
            $no_of_containers      = $this->request->getVar('no-of-containers');
            $warehouse_rcv_date    = $this->request->getVar('warehouse-rcv-date');
            $general_description   = $this->request->getVar('general-description');

            $charge_code         = $this->request->getVar('charge-code');
            $cargo_tbl           = $this->request->getVar('cargo-no-of-pkgs');
            $container_tbl       = $this->request->getVar('cl-container-no');
            $material_tbl        = $this->request->getVar('ml-po-num');

            $t_status            = $this->request->getVar('update-status');
            $t_status_date       = $this->request->getVar('update-status-date');
            $t_status_desc       = $this->request->getVar('update-status-description');
            $t_status_file       = $this->request->getVar('update-status-attached-file');

            $trxn_data = [
                "transaction_datetime" => date("Y-m-d H:i:s", strtotime($transaction_datetime)),
                "master_ref" => $master_ref,
                "is_direct" => ($is_direct == "on") ? 1 : 0,
                "status" => ($posted == 'false') ? "S" : "P",
                "service_type" => $service_type,
                "port_of_origin" => $port_of_origin,
                "port_of_destination" => $port_of_destination,
                "country_of_origin" => $country_of_origin,
                "country_of_destination" => $country_of_destination,
                "etd" => date("Y-m-d H:i:s", strtotime($etd)),
                "eta" => date("Y-m-d H:i:s", strtotime($eta)),
                "atd" => date("Y-m-d H:i:s", strtotime($atd)),
                "ata" => date("Y-m-d H:i:s", strtotime($ata)),
                "total_gross_wt" => $master_gross_wt,
                "total_chargeable_wt" => $master_chargeable_wt,
                "vessel" => $vessel_voyage,
                "carrier" => $carriers,
                "current_status" => $t_status,
                "current_status_datetime" => date("Y-m-d H:i:s", strtotime($t_status_date)),
                "created_by" => session()->get("id"),
                "updated_at" => date("Y-m-d H:i:s")
            ];

            $trans_id = $this->request->getVar('transaction-id');

            $this->mainModel->updateData("transactions", ["id" => $trans_id],$trxn_data);

            $house_data = [
                "house_ref" => $house_ref,
                "shipper_code" => $shipper,
                "consignee_code" => $consignee,
                "agent_code" => $agent,
                "agent_notify_1" => $agent_1,
                "agent_notify_2" => $agent_2,
                "mode_of_shpt" => $mode_of_shipment,
                "general_desc" => trim($general_description),
                "inco_term" => $inco_term,
                "forwarder" => $forwarder,
                "gross_wt" => $gross_wt,
                "chargeable_wt" => $chargeable_weight,
                "uom" => $uom,
                "load_type" => $load_type, 
                "nta_icc_number" => $nta_icc_no,
                "peza_permit" => $peza_permit,
                "custom_value" => $custom_value,
                "custom_duties" => $custom_duties,
                "vat_value" => $vat_value,
                "date_customs_cleared" => date("Y-m-d H:i:s", strtotime($date_customs_cleared)),
                "no_of_containers" => $no_of_containers,
                "warehouse_rcv_date" => date("Y-m-d H:i:s", strtotime($warehouse_rcv_date)),
                "supp_invoice_number" => $supp_invoice_number,
                "invoice_value" => $invoice_value,
                "invoice_currency" => $invoice_currency,
                "import_entry_no" => $import_entry_no,
                "updated_at" => date("Y-m-d H:i:s")
            ];

            $this->mainModel->updateData("transaction_house", ["transaction_id" => $trans_id],$house_data);

            // $status_history_data = [
            //     "transaction_id" => $trans_id,
            //     "status_code" => $t_status,
            //     "status_comment" => $t_status_desc,
            //     "status_file" =>  $t_status_file,
            //     "updated_by" => session()->get("id"),
            //     "created_at" => date("Y-m-d H:i:s", strtotime($t_status_date))
            // ];

            // $this->mainModel->saveData("transactions_history", $status_history_data);

            $charge_loop_counter = $charge_code ? count($charge_code) : 0;
            if ($charge_loop_counter > 0) {
                $this->mainModel->deleteItems("transaction_charges", ["transaction_id" => $trans_id]);
                $this->insert_charges($charge_loop_counter, $trans_id, $raw_data);
            }
            $cargo_tbl_ctr = $cargo_tbl ? count($cargo_tbl) : 0;
            if ($cargo_tbl_ctr > 0) {
                $this->mainModel->deleteItems("transaction_for_loose_cargo", ["transaction_id" => $trans_id]);
                $this->insert_cargo($cargo_tbl_ctr, $trans_id, $raw_data);
            }
            $cl_tbl_ctr = $container_tbl ? count($container_tbl) : 0;
            if ($cl_tbl_ctr > 0) {
                $this->mainModel->deleteItems("transaction_container_list", ["transaction_id" => $trans_id]);
                $this->insert_container($cl_tbl_ctr, $trans_id, $raw_data);
            }
            $ml_tbl_ctr = $material_tbl ? count($material_tbl) : 0;
            if ($ml_tbl_ctr > 0) {
                $this->mainModel->deleteItems("transaction_po_materials", ["transaction_id" => $trans_id]);
                $this->insert_material($ml_tbl_ctr, $trans_id, $raw_data);
            }

            $msg = ($posted == "false") ? "Successfully saved." : "Successfully posted.";
            $url = $this->what_url($service_type);
			echo json_encode(array('success'=>true, 'msg'=> $msg, 'url' => $url));  
            return;
        }
        echo json_encode(array('success'=>false, 'msg'=> "Something went wrong!."));  
        return;
    }


}
<?php
namespace App\Controllers;


use App\Models\MainModel;
use App\Models\TransactionModel;

class Transaction extends BaseController {
    
    var $data;
    var $db;

    protected $helpers = ['auth_helper', 'form', 'url'];

    function __construct()
	{
        $this->data['title'] = 'test';

        $this->data['load_js'][] = 'plugins/dataTables/datatables.min.js';
		$this->data['load_js'][] = 'plugins/dataTables/dataTables.bootstrap4.min.js';
        $this->data['load_css'][] = 'plugins/dataTables/datatables.min.css';
        
        $this->db = \Config\Database::connect();

        helper('template');
    }

    function index() 
    {
        $this->data['load_js'][] = 'module/transaction/bill.js'; 
        
        echo inspinia_theme('admin/transaction/list', $this->data);
        
    }

    public function list() 
    {
       
        $mainModel = new MainModel();
        $iTotal = $mainModel->countAll('transactions');
        $result = $mainModel->getAnyItems('transactions');

        $iFilteredTotal = count($result);

        if ($iFilteredTotal == 1) {
            $record[] = array(
                $result[0]->id,
                $result[0]->master_ref,
                $result[0]->house_ref,
                $result[0]->status,
				$result[0]->service_type,
				$result[0]->etd,
				$result[0]->eta,
				$result[0]->atd,
				$result[0]->ata,
				$result[0]->id,
            );
        } else {
            if($result) {
                foreach ($result as $idx=> $row) {
                    $record[] = array(
                        $row->id,
                        $row->master_ref,
                        $row->house_ref,
                        $row->status,
                        $row->service_type,
                        date("m/d/Y",strtotime($row->etd)),
                        date("m/d/Y",strtotime($row->eta)),
                        date("m/d/Y",strtotime($row->atd)),
                        date("m/d/Y",strtotime($row->ata)),
                        $row->id
                    );
                }
            }
        }

        if (!isset($_POST['sEcho'])) {
            $num = 1;
        } else {
            $num = $_POST['sEcho'];
        }
        $output = array(
            "sEcho" => intval($num),
            "iTotalRecords" => $iFilteredTotal,
            "iTotalDisplayRecords" => $iTotal,
            "aaData" => $record
        );
        echo json_encode($output);
    }


    public function entry($service_type = "is") 
    {
        $this->data['load_js'][] = 'plugins/moment/moment.min.js'; 
		$this->data['load_js'][] = 'plugins/daterangepicker/daterangepicker.js'; 
		$this->data['load_js'][] = 'plugins/dropzone/dropzone.js'; 
        $this->data['load_js'][] = 'module/transaction/bill.js'; 
        
		$this->data['load_css'][] = 'plugins/daterangepicker/daterangepicker.css'; 
        
        $mainModel = new MainModel();
        $transModel = new TransactionModel();
        
        $view_form = "";

        if (strtolower($service_type) == "ia") {
            $view_form = "admin/transaction/air_import";
            $this->data["carriers"] = $mainModel->getAnyItems('carriers', ["carrier_type" => "air"]);
        } else if (strtolower($service_type) == "ea") {
            $view_form = "admin/transaction/air_export";
            $this->data["carriers"] = $mainModel->getAnyItems('carriers', ["carrier_type" => "air"]);
        } elseif (strtolower($service_type) == "is") {
            $view_form = "admin/transaction/sea_import";
            $this->data["carriers"] = $mainModel->getAnyItems('carriers', ["carrier_type" => "sea"]);
        } elseif (strtolower($service_type) == "es") {
            $view_form = "admin/transaction/sea_export";
            $this->data["carriers"] = $mainModel->getAnyItems('carriers', ["carrier_type" => "sea"]);
        } elseif (strtolower($service_type) == "ib") {
            $view_form = "admin/transaction/bro_import";
            $this->data["carriers"] = $mainModel->getAnyItems('carriers', ["carrier_type" => "sea"]);
        } elseif (strtolower($service_type) == "eb") {
            $view_form = "admin/transaction/bro_export";
            $this->data["carriers"] = $mainModel->getAnyItems('carriers', ["carrier_type" => "sea"]);
        } elseif (strtolower($service_type) == "ad") {
            $view_form = "admin/transaction/air_domestic";
            $this->data["carriers"] = $mainModel->getAnyItems('carriers', ["carrier_type" => "air"]);
        } elseif (strtolower($service_type) == "sd") {
            $view_form = "admin/transaction/sea_domestic";
            $this->data["carriers"] = $mainModel->getAnyItems('carriers', ["carrier_type" => "sea"]);
        } else {
            $view_form = "admin/transaction/air_import";
            $this->data["carriers"] = $mainModel->getAnyItems('carriers', ["carrier_type" => "air"]);
        }

        // $view_form = "admin/transaction/air_export";
        // $view_form = "admin/transaction/bro_export";

        $this->data["service_type"] = $service_type;
        // if (substr(strtolower($service_type), 0, 1) == "d") {
        //     $this->data["title"] = "DOMESTIC";
        // }else{
        //     $this->data["title"] = substr(strtolower($service_type), 0, 1) == "i" ? "IMPORT" : "EXPORT";
        // } 
        
        
        $title = $this->get_service_type($service_type);

        $this->data["title"] = $title;

        $this->data["incoterms"] = $mainModel->getAnyItems('incoterms', ["is_enabled" => 1]);
        $this->data["load_types"] = $mainModel->getAnyItems('load_types');

        $this->data["vehicles"] = $mainModel->getAnyItems('vehicles', ["is_enabled" => 1]);
        
        // echo "<pre>";
        // print_r($this->data);
        // echo "</pre>";

        $this->data["agents"] = $mainModel->getAnyItems('partners', ["partner_type" => "A"], ["partner_type" => "X"]);
        $this->data["consignees"] = $mainModel->getAnyItems('partners', ["partner_type" => "C",], ["partner_type" => "X"]);
        $this->data["shippers"] = $mainModel->getAnyItems('partners', ["partner_type" => "S"], ["partner_type" => "X"]);
        $this->data["countries"] = $mainModel->getAnyItems('countries');
        $this->data["ports"] = $mainModel->getAnyItems('ports');
        $this->data["status"] = $mainModel->getAnyItems('status', ["is_enabled" => 1]);
        $this->data["master_refs"] = $transModel->get_master_refs();
       
        echo inspinia_theme($view_form, $this->data);
    }


    public function store()
    {   
		if ($this->request->isAJAX()) {
			
			$mainModel = new MainModel();
            
            $raw_data  = $this->request->getRawInput();

            $transaction_datetime   = $this->request->getVar('transaction-datetime');

            $service_type   = $this->request->getVar('service-type');
            $master_ref     = $this->request->getVar('master-bill-no');
            $house_ref      = $this->request->getVar('house-bill-no');

            $country_of_origin      = $this->request->getVar('origin-destination');
            $port_of_origin         = $this->request->getVar('port-of-loading');
            $country_of_destination = $this->request->getVar('final-destination');
			$port_of_destination    = $this->request->getVar('port-of-discharge');

            $etd  = $this->request->getVar('etd');
            $eta  = $this->request->getVar('eta');
            $atd  = $this->request->getVar('atd');
            $ata  = $this->request->getVar('ata');

            $consignee  = $this->request->getVar('consignee');
            $shipper    = $this->request->getVar('shipper');
            $agent      = $this->request->getVar('agent');

            $vessel_voyage         = $this->request->getVar('vessel-voyage');
            $carriers              = $this->request->getVar('carriers');
            $inco_term             = $this->request->getVar('inco-term');
            $mode_of_shipment      = $this->request->getVar('mode-of-shipment');
            $gross_wt              = $this->request->getVar('gross-wt');
            $chargeable_weight     = $this->request->getVar('chargeable-weight');
            $uom                   = $this->request->getVar('uom');
            $forwarder             = $this->request->getVar('forwarder');
            $supp_invoice_number   = $this->request->getVar('supp-invoice-number');
            $invoice_value         = $this->request->getVar('invoice-value');
            $invoice_currency      = $this->request->getVar('invoice-currency');
            $import_entry_no       = $this->request->getVar('import-entry-no');
            $load_type             = $this->request->getVar('load-type');
            $nta_icc_no            = $this->request->getVar('nta-icc-no');
            $peza_permit           = $this->request->getVar('peza-permit');
            $custom_value          = $this->request->getVar('custom-value');
            $custom_duties         = $this->request->getVar('custom-duties');
            $vat_value             = $this->request->getVar('vat-value');
            $date_customs_cleared  = $this->request->getVar('date-customs-cleared');
            $no_of_containers      = $this->request->getVar('no-of-containers');
            $warehouse_rcv_date    = $this->request->getVar('warehouse-rcv-date');

            $general_description   = $this->request->getVar('general-description');
            
            // Charge TBL
            $charge_code         = $this->request->getVar('charge-code');
            $cargo_tbl           = $this->request->getVar('cargo-no-of-pkgs');
            $container_tbl       = $this->request->getVar('cl-container-no');
            $material_tbl        = $this->request->getVar('ml-po-num');
            $posted              = $this->request->getVar('posted');


            $t_status            = $this->request->getVar('update-status');
            $t_status_date       = $this->request->getVar('update-status-date');
            $t_status_desc       = $this->request->getVar('update-status-description');
            $t_status_file       = $this->request->getVar('update-status-attached-file');

            $trxn_data = [
                "transaction_datetime" => date("Y-m-d H:i:s", strtotime($transaction_datetime)),
                "master_ref" => $master_ref,
                "house_ref" => $house_ref,
                "sub_house_ref" => "",
                "status" => ($posted == 'false') ? "S" : "P",
                "service_type" => $service_type,
                "port_of_origin" => $port_of_origin,
                "port_of_destination" => $port_of_destination,
                "country_of_origin" => $country_of_origin,
                "country_of_destination" => $country_of_destination,
                "shipper_code" => $shipper,
                "consignee_code" => $consignee,
                "agent_code" => $agent,
                "agent_notify_1" => "",
                "agent_notify_2" => "",
                "etd" => date("Y-m-d H:i:s", strtotime($etd)),
                "eta" => date("Y-m-d H:i:s", strtotime($eta)),
                "atd" => date("Y-m-d H:i:s", strtotime($atd)),
                "ata" => date("Y-m-d H:i:s", strtotime($ata)),
                "mode_of_shpt" => $mode_of_shipment,
                "general_desc" => trim($general_description),
                "inco_term" => $inco_term,
                "carrier" => $carriers,
                "vessel" => $vessel_voyage,
                "forwarder" => $forwarder,
                "gross_wt" => $gross_wt,
                "chargeable_wt" => $chargeable_weight,
                "uom" => $uom,
                "load_type" => $load_type, 
                "nta_icc_number" => $nta_icc_no,
                "peza_permit" => $peza_permit,
                "custom_value" => $custom_value,
                "custom_duties" => $custom_duties,
                "vat_value" => $vat_value,
                "date_customs_cleared" => $date_customs_cleared,
                "no_of_containers" => $no_of_containers,
                "warehouse_rcv_date" => $warehouse_rcv_date,
                "supp_invoice_number" => $supp_invoice_number,
                "invoice_value" => $invoice_value,
                "invoice_currency" => $invoice_currency,
                "import_entry_no" => $import_entry_no,
                "current_status" => $t_status,
                "current_status_datetime" => date("Y-m-d H:i:s", strtotime($t_status_date)),
                "created_by" => session()->get("id"),
                "created_at" => date("Y-m-d H:i:s"),
                "updated_at" => date("Y-m-d H:i:s")
            ];

            $trans_id = $mainModel->saveData("transactions", $trxn_data);
            
			$status_history_data = [
                "transaction_id" => $trans_id,
                "status_code" => $t_status,
                "status_comment" => $t_status_desc,
                "status_file" =>  $t_status_file,
                "updated_by" => session()->get("id"),
                "created_at" => date("Y-m-d H:i:s", strtotime($t_status_date))
            ];

            $mainModel->saveData("transactions_history", $status_history_data);

            $charge_loop_counter = $charge_code ? count($charge_code) : 0;
            if ($charge_loop_counter > 0) {
                $this->insert_charges($charge_loop_counter, $trans_id, $raw_data);
            }
            $cargo_tbl_ctr = $cargo_tbl ? count($cargo_tbl) : 0;
            if ($cargo_tbl_ctr > 0) {
                $this->insert_cargo($cargo_tbl_ctr, $trans_id, $raw_data);
            }
            $cl_tbl_ctr = $container_tbl ? count($container_tbl) : 0;
            if ($cl_tbl_ctr > 0) {
                $this->insert_container($cl_tbl_ctr, $trans_id, $raw_data);
            }
            $ml_tbl_ctr = $material_tbl ? count($material_tbl) : 0;
            if ($ml_tbl_ctr > 0) {
                $this->insert_material($ml_tbl_ctr, $trans_id, $raw_data);
            }

			$msg = ($posted == "false") ? "Successfully saved." : "Successfully posted.";
			echo json_encode(array('success'=>true, 'msg'=> $msg));  
		}
    }

    function insert_charges($ctr, $trans_id, $raw_data) 
    {
        $mainModel = new MainModel();
        $i = 0;
        $array_chrg = [];
        while($i < $ctr) {
            $charge_data = [
                "transaction_id" => $trans_id,
                "chg_code" => $raw_data["charge-code"][$i],
                "chg_desc" => $raw_data["charge-desc"][$i],
                "chg_amount" => $raw_data["charge-amount"][$i],
                "currency" => $raw_data["charge-currency"][$i],
                "php_value" => $raw_data["php-value"][$i],
                "receipted" => "",
                "doc_ref" => "",
                "doc_ref_type" => "",
                "supplier_code" => "",
                "remarks" => ""
            ];
            array_push($array_chrg, $charge_data);
            $i++;
        }
        $mainModel->batchInsert("transaction_charges", $array_chrg);
    }

    function insert_cargo($ctr, $trans_id, $raw_data) 
    {
        $mainModel = new MainModel();
        $i = 0;
        $batch_data = [];
        while($i < $ctr) {
            $data = [
                "transaction_id" => $trans_id,
                "no_of_package" => $raw_data["cargo-no-of-pkgs"][$i],
                "gross_wt" => $raw_data["cargo-gross-wt"][$i],
                "chargeable_wt" => $raw_data["cargo-chargeable-wt"][$i],
                "width" => $raw_data["cargo-width"][$i],
                "length" => $raw_data["cargo-length"][$i],
                "height" => $raw_data["cargo-height"][$i],
                "cbm" => $raw_data["cargo-cbm"][$i],
                "truck_type" => $raw_data["cargo-truck-type"][$i],
                "no_of_trucks" => $raw_data["cargo-no-of-trucks"][$i]
            ];
            array_push($batch_data, $data);
            $i++;
        }
        $mainModel->batchInsert("transaction_for_loose_cargo", $batch_data);
    }

    function insert_container($ctr, $trans_id, $raw_data) 
    {
        $mainModel = new MainModel();
        $i = 0;
        $batch_data = [];
        while($i < $ctr) {
            $data = [
                "transaction_id" => $trans_id,
                "container_number" => $raw_data["cl-container-no"][$i],
                "seal_num" => $raw_data["cl-seal-no"][$i],
                "size" => $raw_data["cl-size"][$i],
                "type" => $raw_data["cl-type"][$i]
            ];
            array_push($batch_data, $data);
            $i++;
        }
        $mainModel->batchInsert("transaction_container_list", $batch_data);
    }

    function insert_material($ctr, $trans_id, $raw_data) 
    {
        $mainModel = new MainModel();
        $i = 0;
        $batch_data = [];
        while($i < $ctr) {
            $data = [
                "transaction_id" => $trans_id,
                "po_number" => $raw_data["ml-po-num"][$i],
                "material_id" => $raw_data["ml-material-id"][$i],
                "item_declaration" => $raw_data["ml-item-declaration"][$i],
                "order_qty" => $raw_data["ml-order-qty"][$i],
                "actual_ship" => $raw_data["ml-actual-ship"][$i],
                "order_balance" => $raw_data["ml-order-balance"][$i],
                "unit_of_measure" => $raw_data["ml-unit-of-measure"][$i]
            ];
            array_push($batch_data, $data);
            $i++;
        }
        $mainModel->batchInsert("transaction_po_materials", $batch_data);
    }
    
    public function view($filenumber = NULL) 
    {
        $this->data['load_js'][] = 'plugins/moment/moment.min.js'; 
		$this->data['load_js'][] = 'plugins/daterangepicker/daterangepicker.js'; 
        $this->data['load_js'][] = 'plugins/daterangepicker/daterangepicker.js'; 
        $this->data['load_js'][] = 'plugins/dropzone/dropzone.js'; 
        $this->data['load_js'][] = 'module/transaction/bill.js'; 
        $this->data['load_css'][] = 'plugins/daterangepicker/daterangepicker.css'; 
       
        $mainModel = new MainModel();
        $transModel = new TransactionModel();
        $transaction = $transModel->get_transaction($filenumber);
        
        if ($transaction) {
            $this->data["transaction"] = $transaction;
            
            $shipper = $mainModel->getItem('partners', ["partner_code" => $transaction->shipper_code]);

            $this->data["shipper"] = $shipper;
            $this->data["shipper_address"] = "{$shipper->address1} {$shipper->address2} {$shipper->address3} {$shipper->zipcode} {$shipper->country_code}";

            $consignee = $mainModel->getItem('partners', ["partner_code" => $transaction->consignee_code]);

            $this->data["consignee"] = $consignee;
            $this->data["consignee_address"] = "{$consignee->address1} {$consignee->address2} {$consignee->address3} {$consignee->zipcode} {$consignee->country_code}";

            $agent = $mainModel->getItem('partners', ["partner_code" => $transaction->agent_code]);

            $this->data["agent"] = "";
            $this->data["agent_name"] = "";
            $this->data["agent_address"] = "";

            if ($agent) {
                $this->data["agent"] = $agent;
                $this->data["agent_name"] = "{$agent->partner_code} - {$agent->partner_name}";
                $this->data["agent_address"] = "{$agent->address1} {$agent->address2} {$agent->address3} {$agent->zipcode} {$agent->country_code}";
            } 
            
            $this->data["created_by"] = $mainModel->getItem('users', ["id" => $transaction->created_by]); 
            $this->data["current_status"] = $mainModel->getItem('status', ["status_code" => $transaction->current_status]); 

            $this->data["status"] = $mainModel->getAnyItems('status', ["is_enabled" => 1]);

            $this->data["inco_term"] = $mainModel->getItem('incoterms', ["incoterm_code" => $transaction->inco_term]);
            
            $this->data["history"] = $transModel->get_history_items($transaction->id);
            $this->data["carrier"] = $mainModel->getItem('carriers', ["carrier_code" => $transaction->carrier]); 

            $this->data["origin"] = $mainModel->getItem('countries', ["country_code" => $transaction->country_of_origin]); 
            $this->data["loading"] = $mainModel->getItem('ports', ["port_code" => $transaction->port_of_origin]); 

            $this->data["final"] = $mainModel->getItem('countries', ["country_code" => $transaction->country_of_destination]); 
            $this->data["discharge"] = $mainModel->getItem('ports', ["port_code" => $transaction->port_of_destination]); 

            $this->data["po_materials"] = $mainModel->getAnyItems('transaction_po_materials', ["transaction_id" => $transaction->id]);
            $this->data["container_list"] = $mainModel->getAnyItems('transaction_container_list', ["transaction_id" => $transaction->id]);
            $this->data["charges"] = $mainModel->getAnyItems('transaction_charges', ["transaction_id" => $transaction->id]);
            $this->data["loose_cargo"] = $mainModel->getAnyItems('transaction_for_loose_cargo', ["transaction_id" => $transaction->id]);
            
            $type = "Import Sea";
            if (strtoupper($transaction->service_type) == "ES") {
                $type = "Export Sea";
            } elseif (strtoupper($transaction->service_type) == "IA") {
                $type = "Import Air";
            } elseif (strtoupper($transaction->service_type) == "EA") {
                $type = "Export Air";
            } elseif (strtoupper($transaction->service_type) == "EB") {
                $type = "Export Brokerage";
            } elseif (strtoupper($transaction->service_type) == "IB") {
                $type = "Import Brokerage";
            } elseif (strtoupper($transaction->service_type) == "SD") {
                $type = "Sea Domestic";
            } elseif (strtoupper($transaction->service_type) == "AD") {
                $type = "Air Domestic";
            }

            $this->data["trxn_type"] = $type;
            echo inspinia_theme("admin/transaction/view", $this->data);
        } else {
            return redirect()->to('/transaction/index'); 
        }
    }
        

    public function update_status() 
    {
        if ($this->request->isAJAX()) {
            $mainModel = new MainModel();
            
            $trxn = $this->request->getVar('trxn_id');
            $status_code = $this->request->getVar('status');           
            $status_comment = $this->request->getVar('comment');           
            $status_datetime = $this->request->getVar('datetime');       
            $status_file = $this->request->getVar('file');  
            
            $mainModel->updateData("transactions", ["id" => $trxn], ["current_status" => $status_code, "current_status_datetime" => date("Y-m-d H:i:s", strtotime($status_datetime))]);

            $status_history_data = [
                "transaction_id" => $trxn,
                "status_code" => $status_code,
                "status_comment" => $status_comment,
                "status_file" =>  $status_file,
                "created_at" => date("Y-m-d H:i:s", strtotime($status_datetime)),
                "updated_by" => session()->get("id")
            ];

            $status_history = $mainModel->saveData("transactions_history", $status_history_data);           

            echo json_encode(array('success'=>true, 'msg'=> "Update Successful."));             
        }   
        return false;    
    }


    public function edit($trnx_id = "") 
    {
        $this->data['load_js'][] = 'plugins/moment/moment.min.js'; 
        $this->data['load_js'][] = 'plugins/daterangepicker/daterangepicker.js'; 
        $this->data['load_js'][] = 'plugins/dropzone/dropzone.js'; 
        $this->data['load_js'][] = 'module/transaction/bill.js'; 
        
		$this->data['load_css'][] = 'plugins/daterangepicker/daterangepicker.css'; 
        $mainModel = new MainModel();
        $transModel = new TransactionModel();
        $transaction = $transModel->get_transaction($trnx_id);
        
        if ($transaction) {

            $service_type = $transaction->service_type;

        
            $view_form = "";

            if (strtolower($service_type) == "ia") {
                $view_form = "admin/transaction/air_import_edit";
            } else if (strtolower($service_type) == "ea") {
                $view_form = "admin/transaction/air_export_edit";
            } elseif (strtolower($service_type) == "is") {
                $view_form = "admin/transaction/sea_import_edit";
            } elseif (strtolower($service_type) == "es") {
                $view_form = "admin/transaction/sea_export_edit";
            } elseif (strtolower($service_type) == "ib") {
                $view_form = "admin/transaction/bro_import_edit";
            } elseif (strtolower($service_type) == "eb") {
                $view_form = "admin/transaction/bro_export_edit";
            } elseif (strtolower($service_type) == "ad") {
                $view_form = "admin/transaction/air_domestic_edit";
            } elseif (strtolower($service_type) == "sd") {
                $view_form = "admin/transaction/sea_domestic_edit";
            } else {
                $view_form = "admin/transaction/air_import_edit";
            }
    
            $this->data["service_type"] = $service_type;

            $title = $this->get_service_type($service_type);

            $this->data["title"] = $title;
    
            $mainModel = new MainModel();
            $transModel = new TransactionModel();
    
            $this->data["incoterms"] = $mainModel->getAnyItems('incoterms', ["is_enabled" => 1]);
            $this->data["load_types"] = $mainModel->getAnyItems('load_types');
            $this->data["vehicles"] = $mainModel->getAnyItems('vehicles', ["is_enabled" => 1]);

            $this->data["agents"] = $mainModel->getAnyItems('partners', ["partner_type" => "A"], ["partner_type" => "X"]);
            $this->data["consignees"] = $mainModel->getAnyItems('partners', ["partner_type" => "C",], ["partner_type" => "X"]);
            $this->data["shippers"] = $mainModel->getAnyItems('partners', ["partner_type" => "S"], ["partner_type" => "X"]);
            $this->data["countries"] = $mainModel->getAnyItems('countries');
            $this->data["ports"] = $mainModel->getAnyItems('ports');
            $this->data["status"] = $mainModel->getAnyItems('status', ["is_enabled" => 1]);
            $this->data["carriers"] = $mainModel->getAnyItems('carriers');
            $this->data["master_refs"] = $transModel->get_master_refs();

            $this->data["ports_of_origin"] = $mainModel->getAnyItems('ports', ["port_country" => $transaction->country_of_origin]);
            $this->data["ports_of_destination"] = $mainModel->getAnyItems('ports', ["port_country" => $transaction->country_of_destination]);

            $this->data["transaction"] = $transaction;
                
            $this->data["history"] = $transModel->get_history_items($transaction->id);

            $this->data["po_materials"] = $mainModel->getAnyItems('transaction_po_materials', ["transaction_id" => $transaction->id]);
            $this->data["container_list"] = $mainModel->getAnyItems('transaction_container_list', ["transaction_id" => $transaction->id]);
            $this->data["charges"] = $mainModel->getAnyItems('transaction_charges', ["transaction_id" => $transaction->id]);
            $this->data["loose_cargo"] = $mainModel->getAnyItems('transaction_for_loose_cargo', ["transaction_id" => $transaction->id]);
            
            echo inspinia_theme($view_form, $this->data);
        } else {
            return redirect()->to('/transaction/index'); 
        }
    }

    public function update() 
    {
        if ($this->request->isAJAX()) {
			
			$mainModel = new MainModel();
			
			$data = $this->request->getRawInput();

			$raw_data  = $this->request->getRawInput();

            $transaction_datetime   = $this->request->getVar('transaction-datetime');

            $service_type   = $this->request->getVar('service-type');
            $master_ref     = $this->request->getVar('master-bill-no');
            $house_ref      = $this->request->getVar('house-bill-no');

            $country_of_origin      = $this->request->getVar('origin-destination');
            $port_of_origin         = $this->request->getVar('port-of-loading');
            $country_of_destination = $this->request->getVar('final-destination');
			$port_of_destination    = $this->request->getVar('port-of-discharge');

            $etd  = $this->request->getVar('etd');
            $eta  = $this->request->getVar('eta');
            $atd  = $this->request->getVar('atd');
            $ata  = $this->request->getVar('ata');

            $consignee  = $this->request->getVar('consignee');
            $shipper    = $this->request->getVar('shipper');
            $agent      = $this->request->getVar('agent');

            $vessel_voyage         = $this->request->getVar('vessel-voyage');
            $carriers              = $this->request->getVar('carriers');
            $inco_term             = $this->request->getVar('inco-term');
            $mode_of_shipment      = $this->request->getVar('mode-of-shipment');
            $gross_wt              = $this->request->getVar('gross-wt');
            $chargeable_weight     = $this->request->getVar('chargeable-weight');
            $uom                   = $this->request->getVar('uom');
            $forwarder             = $this->request->getVar('forwarder');
            $supp_invoice_number   = $this->request->getVar('supp-invoice-number');
            $invoice_value         = $this->request->getVar('invoice-value');
            $invoice_currency      = $this->request->getVar('invoice-currency');
            $import_entry_no       = $this->request->getVar('import-entry-no');
            $load_type             = $this->request->getVar('load-type');
            $nta_icc_no            = $this->request->getVar('nta-icc-no');
            $peza_permit           = $this->request->getVar('peza-permit');
            $custom_value          = $this->request->getVar('custom-value');
            $custom_duties         = $this->request->getVar('custom-duties');
            $vat_value             = $this->request->getVar('vat-value');
            $date_customs_cleared  = $this->request->getVar('date-customs-cleared');
            $no_of_containers      = $this->request->getVar('no-of-containers');
            $warehouse_rcv_date    = $this->request->getVar('warehouse-rcv-date');

            $general_description   = $this->request->getVar('general-description');
            
            // Charge TBL
            $charge_code         = $this->request->getVar('charge-code');
            $cargo_tbl           = $this->request->getVar('cargo-no-of-pkgs');
            $container_tbl       = $this->request->getVar('cl-container-no');
            $material_tbl        = $this->request->getVar('ml-po-num');
            $posted              = $this->request->getVar('posted');


            $t_status            = $this->request->getVar('update-status');
            $t_status_date       = $this->request->getVar('update-status-date');
            $t_status_desc       = $this->request->getVar('update-status-description');
            $t_status_file       = $this->request->getVar('update-status-attached-file');

			$tid = $this->request->getVar('transaction-id');

			$trxn_data = [
                "transaction_datetime" => date("Y-m-d H:i:s", strtotime($transaction_datetime)),
                "master_ref" => $master_ref,
                "house_ref" => $house_ref,
                "sub_house_ref" => "",
                "status" => ($posted == 'false') ? "S" : "P",
                "service_type" => $service_type,
                "port_of_origin" => $port_of_origin,
                "port_of_destination" => $port_of_destination,
                "country_of_origin" => $country_of_origin,
                "country_of_destination" => $country_of_destination,
                "shipper_code" => $shipper,
                "consignee_code" => $consignee,
                "agent_code" => $agent,
                "agent_notify_1" => "",
                "agent_notify_2" => "",
                "etd" => date("Y-m-d H:i:s", strtotime($etd)),
                "eta" => date("Y-m-d H:i:s", strtotime($eta)),
                "atd" => date("Y-m-d H:i:s", strtotime($atd)),
                "ata" => date("Y-m-d H:i:s", strtotime($ata)),
                "mode_of_shpt" => $mode_of_shipment,
                "general_desc" => trim($general_description),
                "inco_term" => $inco_term,
                "carrier" => $carriers,
                "vessel" => $vessel_voyage,
                "forwarder" => $forwarder,
                "gross_wt" => $gross_wt,
                "chargeable_wt" => $chargeable_weight,
                "uom" => $uom,
                "load_type" => $load_type, 
                "nta_icc_number" => $nta_icc_no,
                "peza_permit" => $peza_permit,
                "custom_value" => $custom_value,
                "custom_duties" => $custom_duties,
                "vat_value" => $vat_value,
                "date_customs_cleared" => $date_customs_cleared,
                "no_of_containers" => $no_of_containers,
                "warehouse_rcv_date" => $warehouse_rcv_date,
                "supp_invoice_number" => $supp_invoice_number,
                "invoice_value" => $invoice_value,
                "invoice_currency" => $invoice_currency,
                "import_entry_no" => $import_entry_no,
                "updated_at" => date("Y-m-d H:i:s")
            ];

            $mainModel->updateData("transactions", ["id" => $tid],$trxn_data);
            
            $charge_loop_counter = $charge_code ? count($charge_code) : 0;
            if ($charge_loop_counter > 0) {
                $mainModel->deleteItems("transaction_charges", ["transaction_id" => $tid]);
                $this->insert_charges($charge_loop_counter, $tid, $raw_data);
            }
            $cargo_tbl_ctr = $cargo_tbl ? count($cargo_tbl) : 0;
            if ($cargo_tbl_ctr > 0) {
                $mainModel->deleteItems("transaction_for_loose_cargo", ["transaction_id" => $tid]);
                $this->insert_cargo($cargo_tbl_ctr, $tid, $raw_data);
            }
            $cl_tbl_ctr = $container_tbl ? count($container_tbl) : 0;
            if ($cl_tbl_ctr > 0) {
                $mainModel->deleteItems("transaction_container_list", ["transaction_id" => $tid]);
                $this->insert_container($cl_tbl_ctr, $tid, $raw_data);
            }
            $ml_tbl_ctr = $material_tbl ? count($material_tbl) : 0;
            if ($ml_tbl_ctr > 0) {
                $mainModel->deleteItems("transaction_po_materials", ["transaction_id" => $tid]);
                $this->insert_material($ml_tbl_ctr, $tid, $raw_data);
            }

			$msg = ($posted == "false") ? "Successfully saved." : "Successfully posted.";
			echo json_encode(array('success'=>true, 'msg'=> $msg));  
		}
    }
    

    function upload() 
    {
        $file = $this->request->getFile('file');
        $newName = $file->getRandomName();
        $file->move(WRITEPATH.'uploads/test', $newName);
        
        $data = [
            'name' => $file->getName(),
            'type'  => $file->getClientMimeType()
        ];

        echo json_encode(array('success'=>true, 'filename'=> $file->getName()));  
        return;
    }

    function download() 
    {
        $uri = service('uri');
        $filename = $uri->getSegment(3);
        // $file = new \CodeIgniter\Files\File("/uploads/test/{$filename}");
        $file = WRITEPATH.'uploads/test/'.$filename;
        return $this->response->download($file, null);
    }

    function get_port() 
    {
        if ($this->request->isAJAX()) {
            
            $mainModel = new MainModel();
			
            $country_code = $this->request->getVar('country');           
            
            $ports = $mainModel->getAnyItems('ports', ["port_country" => $country_code, "is_enabled" => 1]);

            $array_ports = [];

            if ($ports) {
                foreach ($ports as $key => $value) {
                    $port_obj = new class{};
                    $port_obj->id = $value->port_code;
                    $port_obj->text = "{$value->port_code} - {$value->port_name}";
                    array_push($array_ports, $port_obj);
                }
            }

            echo json_encode(array('success'=>true, 'ports'=> $array_ports));  
            return;
            
        }       
    }


    function get_service_type($type = "is") 
    {
        switch (strtolower($type)) {
            case "es":
                return "Export Sea";
                break;
            case "ia":
                return "Import Air";
                break;
            case "ea":
                return "Export Air";
                break;
            case "ib":
                return "Import Brokerage";
                break;
            case "eb":
                return "Export Brokerage";
                break;
            case "ad":
                return "Air Domestic";
                break;
            case "sd":
                return "Sea Domestic";
                break;
            default:
                return "Import Sea";
        }
    }

    function delete_trans() 
    {
        if ($this->request->isAJAX()) {
            $mainModel = new MainModel();
            $trans_id = $this->request->getVar('trxn');           
            $mainModel->deleteItems("transaction_charges", ["transaction_id" => $trans_id]);
            $mainModel->deleteItems("transaction_for_loose_cargo", ["transaction_id" => $trans_id]);
            $mainModel->deleteItems("transaction_container_list", ["transaction_id" => $trans_id]);
            $mainModel->deleteItems("transaction_po_materials", ["transaction_id" => $trans_id]);
            $mainModel->deleteItems("transactions_history", ["transaction_id" => $trans_id]);
            $mainModel->deleteItems("transaction_house", ["transaction_id" => $trans_id]);
            $mainModel->deleteItems("transactions", ["id" => $trans_id]);
            echo json_encode(array('success'=>true, 'msg'=> "Delete Successful."));  
            return;
        }
        echo json_encode(array('success'=>false, "msg" => "Something went wrong."));  
        return;
    }

}
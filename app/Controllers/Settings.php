<?php 

namespace App\Controllers;
//use App\Libraries\Crud;

use App\Models\SettingsModel;
use App\Models\MainModel;

class Settings extends BaseController
{
    protected $main;

    var $data;
    var $db;

    function __construct()
	{
        $this->data['title'] = 'test';

        $this->data['load_js'][] = 'plugins/dataTables/datatables.min.js';
		$this->data['load_js'][] = 'plugins/dataTables/dataTables.bootstrap4.min.js';
        $this->data['load_css'][] = 'plugins/dataTables/datatables.min.css';
        
        $this->db = \Config\Database::connect();

        helper('template');
    }

    public function index() {
        $menu = new MainModel();
    
    }
    function menu($page='list', $menu_id = 0) {
        $this->data['load_js'][] = 'module/menu/menu.js'; 

        switch($page) {
            case 'list':
                echo inspinia_theme('settings/menu/list', $this->data);
                break;
            case 'create':
                $this->menu_create($menu_id);
                break;
            case 'edit':
                $this->menu_create($menu_id);
                break;
        } 
    }

    function menu_create($menu_id) {
        
        $this->data['menu_id'] = '';
        $this->data['menu_name'] = '';
        $this->data['icon'] = '';
        $this->data['is_enabled'] = '';
        $this->data['sort'] = '';

        if($menu_id) {
            $menu = new MainModel();
            $menu_data = $menu->getItem('menu_main', array('menu_id'=>$menu_id));
            $this->data['menu_id'] = $menu_data->menu_id;
            $this->data['menu_name'] = $menu_data->menu_name;
            $this->data['icon'] = $menu_data->icon;
            $this->data['is_enabled'] = $menu_data->is_enabled;
            $this->data['sort'] = $menu_data->sort;
            
        }

        echo inspinia_theme('settings/menu/create', $this->data);
    }

    function getMenuList()  {
        $menus = new SettingsModel();
        $iTotal = $menus->countAll();
      
        $menuList = new SettingsModel();
        
        $result = $menuList->orderBy('menu_main.sort', 'ASC')->findAll();

        $iFilteredTotal = count($result);
        
        if ($iFilteredTotal == 1) {
            $record[] = array(
                $result['menu_id'],
                $result['menu_name'],
                $result['icon'],
                $result['is_enabled'],
                $result['sort'],
                $result['menu_id'],
            );
        } else {
            if($result) {
                foreach ($result as $row) {
                    $record[] = array(
                        $row['menu_id'],
                        $row['menu_name'],
                        $row['icon'],
                        $row['is_enabled'],
                        $row['sort'],
                        $row['menu_id'],
                    );
                }
            }
        }

        if (!isset($_POST['sEcho'])) {
            $num = 1;
        } else {
            $num = $_POST['sEcho'];
        }
        $output = array(
            "sEcho" => intval($num),
            "iTotalRecords" => $iFilteredTotal,
            "iTotalDisplayRecords" => $iTotal,
            "aaData" => $record
        );
        echo json_encode($output);
    }

    function menu_store() {
        helper(['form', 'url']);

		if ($this->request->getMethod() == 'post') {
          	$rules = [
                'menu_name' => 'required',
                'icon' => 'required',
                'is_enabled' => 'required',
				'sort_num' => 'required',
            ];
            
            if (! $this->validate($rules)) {
                
				$this->data['validation'] = $this->validator;
			} else {

                $menu = new MainModel();
                //save
                $menu_id = $this->request->getVar('menu_id');
                $data_variable = array(
                    'menu_name' =>  $this->request->getVar('menu_name'),
                    'icon' =>  $this->request->getVar('icon'),
                    'is_enabled' =>  $this->request->getVar('is_enabled'),
                    'sort' =>  $this->request->getVar('sort_num'),
                );

                $session = \Config\Services::session();

                if($menu_id) {
                    $menu->updateData('menu_main', array('menu_id'=>$menu_id), $data_variable);
                    $session->setFlashdata('success','Menu updated');
                } else {
                    $menu->saveData('menu_main', $data_variable);
                    $session->setFlashdata('success','Menu created');
                }
                
                echo json_encode(array('success'=>true,'msg'=>'Added a new Menu', 'url'=> base_url('settings/menu')));
            }
        } else {
            echo json_encode(array('success'=>fasle,'msg'=>'Error on submitting', 'url'=> ""));
        }
    }

    function submenu($page='list', $submenu_id = 0) {
        $this->data['load_js'][] = 'module/menu/menu.js'; 

        switch($page) {
            case 'list':
                echo inspinia_theme('settings/submenu/list', $this->data);
                break;
            case 'create':
                $this->submenu_create($submenu_id);
                break;
            case 'edit':
                $this->submenu_create($submenu_id);
                break;
        }  
    }

    function submenu_create($submenu_id) {
        
        $this->data['id'] = 0;
		$this->data['menu_id'] = 0;
		$this->data['icon'] = '';
		$this->data['menu_name'] = '';
		$this->data['sort'] = '';
		$this->data['is_enabled'] = '';
		$this->data['url_link'] = '';
		$this->data['menu_list'] = $this->_parentMenu(0, true);

        if($submenu_id) {
            $submenu = new MainModel();
            $menu_data = $submenu->getItem('main_sub', array('id'=>$submenu_id));

            $this->data['id'] = $submenu_id;
            $this->data['url_link'] = $menu_data->url;
            $this->data['menu_list'] = $this->_parentMenu($menu_data->menu_id, true);
            
            $this->data['menu_id'] = $menu_data->menu_id;
            $this->data['menu_name'] = $menu_data->menu;
            $this->data['icon'] = $menu_data->icon;
            $this->data['is_enabled'] = $menu_data->is_enabled;
            $this->data['sort'] = $menu_data->sort;
            
        }

        echo inspinia_theme('settings/submenu/create', $this->data);
    }

    function submenu_store() {

        $id = $this->request->getVar('id');
        $menu_name = $this->request->getVar('menu_name');
        $icon = $this->request->getVar('icon');
        $is_enabled = $this->request->getVar('is_enabled');
        $sort = $this->request->getVar('sort_num');
        $menu_id = $this->request->getVar('menu_id');
        $url = $this->request->getVar('url_link');

        $main = new MainModel();

        $condition = array();
        
        $data = array(
            'menu' => $menu_name,
            'menu_id' => $menu_id,
            'url' => $url,
            'icon' => $icon,
            'sort' => $sort,
            'is_enabled' => $is_enabled,
        );

        if($id) {
            $condition = array('id' => $id);
            $main->updateData('main_sub', $condition, $data);
        } else {
            $main->saveData('main_sub', $data);
        }
        echo json_encode(array('success'=>true,'msg'=>'Added a new Sub Menu', 'url'=> base_url('settings/submenu')));
        
    }

    function getSubMenuList()  {
      
        $menuList = new MainModel();

        $select = "ms.id, mm.menu_name, ms.menu,  ms.url, ms.icon, ms.is_enabled, ms.sort, ms.id as idx";
        
        $result = $menuList->getRelationItemsJoin('main_sub ms', array(), 'menu_main as mm', 'mm.menu_id = ms.menu_id', $select);
      
        $iFilteredTotal = count($result);

        if ($iFilteredTotal == 1) {
            $record[] = array(
                $result[0]->id,
                $result[0]->menu_name,
                $result[0]->menu,
                $result[0]->url,
                $result[0]->icon,
                $result[0]->is_enabled,
                $result[0]->sort,
                $result[0]->idx,
            );
        } else {
            if($result) {

                foreach ($result as $row) {
                    $record[] = array(
                        $row->id,
                        $row->menu_name,
                        $row->menu,
                        $row->url,
                        $row->icon,
                        $row->is_enabled,
                        $row->sort,
                        $row->idx,
                    );
                }
            }
        }


        if (!isset($_POST['sEcho'])) {
            $num = 1;
        } else {
            $num = $_POST['sEcho'];
        }
        $output = array(
            "sEcho" => intval($num),
            "iTotalRecords" => $iFilteredTotal,
            "iTotalDisplayRecords" => $iFilteredTotal,
            "aaData" => $record
        );
        echo json_encode($output);
    }

    function _parentMenu($selected_id, $is_required=FALSE) {

		$required = "";

		if($is_required)
			$required = "required";

        $menu = new MainModel();
        $menu_data = $menu->getAnyItems('menu_main',array());

      		
		$option = "<select name='menu_id' id='menu_id' class='form-control' $required>";
		$option .= "<option value=''> -- Select Parent Menu --</option>";
		if($menu_data) {
            for($x=0; $x<count($menu_data); $x++) {
                $sel = ( $menu_data[$x]->menu_id == $selected_id) ? 'selected':'';
				$option .= "<option $sel value='". $menu_data[$x]->menu_id."'>". $menu_data[$x]->menu_name."</option>";
            }

          
		}
		$option .= "</select>";

		return $option;
	}

    function partners() {

        $this->data['load_js'][] = 'module/settings/partners.js'; 

        $Main = new SettingsModel();

        $mainModel = new MainModel();
        $data['partners'] = $mainModel->getAnyItems('partners');
    
        
        echo inspinia_theme('settings/partners/list', $this->data);
    }

    function partner_list()  {
        $mainModel = new MainModel();
        $iTotal = $mainModel->countAll('partners');
       
        $partnerList = new MainModel();
        
        $result = $partnerList->getRelationItems('partners');

        $iFilteredTotal = count($result);

        if ($iFilteredTotal == 1) {
            $record[] = array(
                $result['id'],
                $result['partner_code'],
                $result['partner_name'],
                $result['partner_type'],
                $result['id'],
            );
        } else {
            if($result) {
                foreach ($result as $idx=> $row) {
                    $record[] = array(
                        $row->id,
                        $row->partner_code,
                        $row->partner_name,
                        $row->partner_type,
                        $row->id,
                    );
                }
            }
        }

        if (!isset($_POST['sEcho'])) {
            $num = 1;
        } else {
            $num = $_POST['sEcho'];
        }
        $output = array(
            "sEcho" => intval($num),
            "iTotalRecords" => $iFilteredTotal,
            "iTotalDisplayRecords" => $iTotal,
            "aaData" => $record
        );
        echo json_encode($output);
    }

    function partner_add() {
        $this->data = [];
        $this->data['load_js'][] = 'module/settings/partners.js'; 

        helper(['form', 'url']);
        $partner = new MainModel();
    
		if ($this->request->getMethod() == 'post') {
          	$rules = [
                'partner_code' => 'required',
                'partner_name' => 'required',
                'address1' => 'required',
                // 'address2' => 'required',
                // 'address3' => 'required',
                'country_code' => 'required',
                'partner_type' => 'required',
            ];
            
            if (! $this->validate($rules)) {
				$this->data['validation'] = $this->validator;
			} else {
                //save
                $partner_id = $this->request->getVar('partner_id');
                $partner_code = $this->request->getVar('partner_code');

                $contact_name = $this->request->getVar('contact_name');
                $email_address = $this->request->getVar('email_address');
                $position = $this->request->getVar('position');
                $contact_no = $this->request->getVar('contact_no');

                
                $data_variable = array(
                    'partner_code' =>  $partner_code,
                    'partner_name' =>  $this->request->getVar('partner_name'),
                    'address1' =>  $this->request->getVar('address1'),
                    // 'address2' =>  $this->request->getVar('address2'),
                    // 'address3' =>  $this->request->getVar('address3'),
                    'country_code' =>  $this->request->getVar('country_code'),
                    'partner_type' =>  $this->request->getVar('partner_type'),
                    'updated_at'=>date('Y-m-d h:i:s')
                );

                $contact_data = array();
                for($x=0; $x<count($contact_name); $x++) {
                    $contact_data[] = array(
                        'partner_code'=>$partner_code,
                        'contact_name'=>$contact_name[$x],
                        'email_address'=>$email_address[$x],
                        'position'=>$position[$x],
                        'contact_number'=>$contact_no[$x],
                    );
                } 

              
                $session = \Config\Services::session();

                if($partner_id) {
                    $partner->updateData('partners', array('id'=>$partner_id), $data_variable);
                    $session->setFlashdata('success','Menu updated');
                } else {
                    $partner->saveData('partners', $data_variable);
                    $session->setFlashdata('success','Menu created');
                }

                //delete contacts
                if($partner->deleteItems('partner_contact', array('partner_code'=>$partner_code))) {
                    //batch insert
                    $partner->batchInsert('partner_contact', $contact_data);
                }


                return redirect()->to('partners');
            }
        } 

        $this->data['countries'] = $partner->getAnyItems('countries');
        $this->data['contact_list'] = '';

        echo inspinia_theme('settings/partners/create', $this->data);
    }

    function partner_view($id) {
        $data = [];

        
        $this->data['load_js'][] = 'module/settings/partners.js'; 

        $partner = new MainModel();
        $partner_data = $partner->getItem('partners', array('id'=>$id));
        $this->data['partner'] = $partner_data;

        //getCountryList
        $this->data['countries'] = $partner->getAnyItems('countries');

        //getContact
        $this->data['contact_list'] = $partner->getAnyItems('partner_contact', array('partner_code'=>$partner_data->partner_code));
        
        echo inspinia_theme('settings/partners/create', $this->data);
    
    }

    function country() {
        $this->data['load_js'][] = 'module/settings/country.js'; 

        $country = new MainModel();
        $this->data['countries'] = $country->getAnyItems('countries');
        echo inspinia_theme('settings/countries', $this->data);
    }

    function getCountryList() {
        $Main = new MainModel();
        $result = $Main->getAnyItems('countries');
      
        $iTotal = count($result);
      
        if ($iTotal == 1) {
            $record[] = array(
                $result->country_code,
                $result->country_name,
                $result->country_code
           
            );
        } else {
            if($result) {
                foreach ($result as $row) {
                    $record[] = array(
                        $row->country_code,
                        $row->country_name,
                        $row->country_code
                    );
                }
            }
        }

        if (!isset($_POST['sEcho'])) {
            $num = 1;
        } else {
            $num = $_POST['sEcho'];
        }
        $output = array(
            "sEcho" => intval($num),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iTotal,
            "aaData" => $record
        );
        echo json_encode($output);
    }

    function country_store() {

        $country = new MainModel();

        $this->db->transBegin();

        $country_code = $this->request->getVar('country_code');
        $country_name= $this->request->getVar('country_name');
        
        $has_record = count(($country->getAnyItems('countries', array('country_code' => $country_code ))));

        if($has_record > 0)  
            $country->updateData('countries', array('country_code'=>$country_code), array('country_name'=>$country_name));
        else
            $country->saveData('countries', array('country_name'=>$country_name, 'country_code'=>$country_code));
        
        if ($this->db->transStatus() === FALSE) {
            $this->db->transRollback();
            echo json_encode(array('success'=>false, 'msg'=>'Error in saving'));  
        } else {
            $this->db->transCommit();
            echo json_encode(array('success'=>true, 'msg'=>'Successfully Saved'));  
        }
        return false;
    }

    function country_remove() {
        $country = new MainModel();

        $this->db->transBegin();

        $country_code = $this->request->getVar('country_code');
       
        $country->deleteItems('countries', array('country_code'=>$country_code));
        
        if ($this->db->transStatus() === FALSE) {
            $this->db->transRollback();
            echo json_encode(array('success'=>false, 'msg'=>'Error in deletion'));  
        } else {
            $this->db->transCommit();
            echo json_encode(array('success'=>true, 'msg'=>'Successfully deleted'));  
        }
        return false;
    }

    function ports() {

        $this->data['load_js'][] = 'module/settings/ports.js'; 
        $port = new MainModel();
        
        $this->data['ports'] = $port->getAnyItems('ports');
        $this->data['countries'] = $port->getAnyItems('countries');

        echo inspinia_theme('settings/ports', $this->data);
    }

    function port_data() {
        $port = new MainModel();
        $port_id = $this->request->getVar('port_id');
        $data = $port->getItem('ports', array('port_id' => $port_id ));

        echo json_encode(array('success'=>true, 'result'=>$data));  

    }

    function port_store() {

        $country = new MainModel();

        $this->db->transBegin();


        $port_code = $this->request->getVar('port_code');
        $port_id= $this->request->getVar('port_id');
        $port_name= $this->request->getVar('port_name');
        $port_type= $this->request->getVar('port_type');
        $port_country= $this->request->getVar('port_country');
        $is_enabled= $this->request->getVar('is_enabled');
        $iata_area= $this->request->getVar('iata_area');

        $data = array(
            'port_code'=>$port_code,
            'port_name'=>$port_name,
            'port_type'=>$port_type,
            'port_country'=>$port_country,
            'is_enabled'=>$is_enabled,
            'iata_area'=>$iata_area
        );

        if($port_id) {
            $country->updateData('ports', array('port_id'=>$port_id), $data);
        } else {
            $country->saveData('ports', $data);
        }

        if ($this->db->transStatus() === FALSE) {
            $this->db->transRollback();
            echo json_encode(array('success'=>false, 'msg'=>'Error in saving'));  
        } else {
            $this->db->transCommit();
            echo json_encode(array('success'=>true, 'msg'=>'Successfully Saved'));  
        }
        return false;
    }

    function port_remove() {
        $Port = new MainModel();

        $this->db->transBegin();

        $port_id = $this->request->getVar('port_id');
       
        $Port->deleteItems('ports', array('port_id'=>$port_id));
        
        if ($this->db->transStatus() === FALSE) {
            $this->db->transRollback();
            echo json_encode(array('success'=>false, 'msg'=>'Error in deletion'));  
        } else {
            $this->db->transCommit();
            echo json_encode(array('success'=>true, 'msg'=>'Successfully deleted'));  
        }
        return false;
    }


    protected function field_options() {
		$fields = [];
		$field['menu_id'] = ['label' => 'ID'];
		// $fields['first_name'] = ['label' => 'First Name', 'required' => true, 'helper' => 'Type your First name', 'class' => 'col-12 col-sm-6'];
		// $fields['last_name'] = ['label' =>'Last Name', 'required' => true, 'helper' => 'Type your Last name', 'class' => 'col-12 col-sm-6'];
		// $fields['email'] = ['label' => 'Email','required' => true, 'unique' => [true, 'email']];
		// $fields['active'] = ['label' => 'Status','required' => true,];
		// $fields['created_at'] = ['label' => 'Created at', 'only_edit' => true];
		// $fields['ip_address'] = ['label' => 'IP address', 'only_edit' => true];
		// $fields['salt'] = ['only_edit' => true];
		// $fields['activate_hash'] = ['only_edit' => true];
		// $fields['reset_hash'] = ['only_edit' => true];
		// $fields['reset_expires'] = ['only_edit' => true];
		// $fields['last_login'] = ['only_edit' => true];

		// $fields['password'] = ['label' => 'Password',
		//  'required' => true, 
		//  'only_add' => true,
		//  'type' => 'password',
		// 	'class' => 'col-12 col-sm-6',
		//  'confirm' => true, 
		//  'password' => true];

		return $fields;
    }
    
    public function status() {
        $this->data['load_js'][] = 'module/settings/status.js'; 
       
        $mainModel = new MainModel();

        $this->data['status_list'] = $mainModel->getAnyItems('status');

        echo inspinia_theme('settings/status', $this->data);
    }

    public function store_status() {
        helper('url');
        $name = $this->request->getVar('status_name');
        $desc = $this->request->getVar('status_desc');
        $code = url_title($name, '_', TRUE);
        $visibility = $this->request->getVar('status_visibility');
        
        $data = [
            "status_code" => $code,
            "status_name"  => $name,
            "status_description" => $desc,
            "status_visibility" => ($visibility == "on") ? 1 : 0,
        ];

        $mainModel = new MainModel();
        
        $this->db->transBegin();

        $mainModel->saveData('status', $data);
        
        if ($this->db->transStatus() === FALSE) {
            $this->db->transRollback();
            echo json_encode(array('success'=>false, 'msg'=>'Error in saving'));  
        } else {
            $this->db->transCommit();
            echo json_encode(array('success'=>true, 'msg'=>'Successfully Saved'));  
        }
        return;
    }

    public function status_data() {
        $status = new MainModel();
        $status_id = $this->request->getVar('status_id');
        $data = $status->getItem('status', array('id' => $status_id ));

        echo json_encode(array('success'=>true, 'result'=>$data));  
        return;
    }

    public function status_save_data() {
        $name = $this->request->getVar('edit_status_name');
        $desc = $this->request->getVar('edit_status_desc');
        $visibility = $this->request->getVar('edit_status_visibility');
        $enabled = $this->request->getVar('edit_status_enabled'); 
        $id = $this->request->getVar("edit_status_id");
        $data = [
            "status_name"  => $name,
            "status_description" => $desc,
            "status_visibility" => ($visibility == "on") ? 1 : 0,
            "is_enabled" => ($enabled == "on") ? 1 : 0
        ];
        $mainModel = new MainModel();
        $flag = $mainModel->updateData('status', ["id" => $id], $data);
        if ($flag > 0) {
            echo json_encode(array('success'=>true, 'msg'=>'Successfully Saved')); 
            return;
        } else {
            echo json_encode(array('success'=>false, 'msg'=>'Failed')); 
            return;
        }
    }

    function status_remove() {
        $Status = new MainModel();

        $this->db->transBegin();

        $status_id = $this->request->getVar('status_id');
       
        $Status->deleteItems('status', array('id'=>$status_id));
        
        if ($this->db->transStatus() === FALSE) {
            $this->db->transRollback();
            echo json_encode(array('success'=>false, 'msg'=>'Error in deletion'));  
        } else {
            $this->db->transCommit();
            echo json_encode(array('success'=>true, 'msg'=>'Successfully deleted'));  
        }
        return false;
    }

    public function unit_of_measure()
    {
        $this->data['load_js'][] = 'module/settings/settings.js'; 
       
        $mainModel = new MainModel();

        $this->data['list'] = $mainModel->getAnyItems('unit_of_measures');

        echo inspinia_theme('settings/unit_of_measures', $this->data);
    }

    public function store_unit_of_measure() 
    {
        helper('url');
        $code = $this->request->getVar('unit-code');
        $desc = $this->request->getVar('unit-desc');
        
        $data = [
            "unit_code" => $code,
            "unit_description" => $desc
        ];

        $mainModel = new MainModel();
        
        $this->db->transBegin();

        $mainModel->saveData('unit_of_measures', $data);
        
        if ($this->db->transStatus() === FALSE) {
            $this->db->transRollback();
            echo json_encode(array('success'=>false, 'msg'=>'Error in saving'));  
        } else {
            $this->db->transCommit();
            echo json_encode(array('success'=>true, 'msg'=>'Successfully Saved'));  
        }
        return;
    }

    public function unit_data() {
        $mainModel = new MainModel();
        $unit_id = $this->request->getVar('unit_id');
        $data = $mainModel->getItem('unit_of_measures', array('id' => $unit_id));

        echo json_encode(array('success'=>true, 'result'=>$data));  
        return;
    }

    public function unit_save_data() {
        $code = $this->request->getVar('edit-unit-code');
        $desc = $this->request->getVar('edit-unit-desc');
        $enabled = $this->request->getVar('edit-unit-enabled'); 
        $id = $this->request->getVar("edit-unit-id");
        $data = [
            "unit_code" => $code,
            "unit_description" => $desc,
            "is_active" => ($enabled == "on") ? 1 : 0
        ];
        $mainModel = new MainModel();
        $flag = $mainModel->updateData('unit_of_measures', ["id" => $id], $data);
        if ($flag > 0) {
            echo json_encode(array('success'=>true, 'msg'=>'Successfully Saved')); 
            return;
        } else {
            echo json_encode(array('success'=>false, 'msg'=>'Failed')); 
            return;
        }
    }
    public function unit_remove() {
        $Status = new MainModel();

        $this->db->transBegin();

        $id = $this->request->getVar('id');
       
        $Status->deleteItems('unit_of_measures', array('id'=>$id));
        
        if ($this->db->transStatus() === FALSE) {
            $this->db->transRollback();
            echo json_encode(array('success'=>false, 'msg'=>'Error in deletion'));  
        } else {
            $this->db->transCommit();
            echo json_encode(array('success'=>true, 'msg'=>'Successfully deleted'));  
        }
        return false;
    }

    public function carriers()
    {
        $this->data['load_js'][] = 'module/settings/airlines.js'; 
       
        $mainModel = new MainModel();

        $this->data['list'] = $mainModel->getAnyItems('carriers');

        echo inspinia_theme('settings/carriers', $this->data);
    }

    public function store_carrier() 
    {
        helper('url');
        $code = $this->request->getVar('carrier-code');
        $name = $this->request->getVar('carrier-name');
        $type = $this->request->getVar('carrier-type');
        
        $data = [
            "carrier_code" => $code,
            "carrier_name" => $name,
            "carrier_type" => $type
        ];

        $mainModel = new MainModel();
        
        $this->db->transBegin();

        $mainModel->saveData('carriers', $data);
        
        if ($this->db->transStatus() === FALSE) {
            $this->db->transRollback();
            echo json_encode(array('success'=>false, 'msg'=>'Error in saving'));  
        } else {
            $this->db->transCommit();
            echo json_encode(array('success'=>true, 'msg'=>'Successfully Saved'));  
        }
        return;
    }

    public function carrier_data() {
        $mainModel = new MainModel();
        $carrier_id = $this->request->getVar('carrier_id');
        $data = $mainModel->getItem('carriers', array('id' => $carrier_id));

        echo json_encode(array('success'=>true, 'result'=>$data));  
        return;
    }

    public function carrier_save_data() {
        $code = $this->request->getVar('edit-carrier-code');
        $name = $this->request->getVar('edit-carrier-name');
        $type = $this->request->getVar('edit-carrier-type');
        $id = $this->request->getVar("edit-carrier-id");
        $data = [
            "carrier_code" => $code,
            "carrier_name" => $name,
            "carrier_type" => $type
        ];
        $mainModel = new MainModel();
        $flag = $mainModel->updateData('carriers', ["id" => $id], $data);
        if ($flag > 0) {
            echo json_encode(array('success'=>true, 'msg'=>'Successfully Saved')); 
            return;
        } else {
            echo json_encode(array('success'=>false, 'msg'=>'Failed')); 
            return;
        }
    }

    public function remove_carrier() {
        $mainModel = new MainModel();

        $this->db->transBegin();

        $id = $this->request->getVar('id');
       
        $mainModel->deleteItems('carriers', array('id'=>$id));
        
        if ($this->db->transStatus() === FALSE) {
            $this->db->transRollback();
            echo json_encode(array('success'=>false, 'msg'=>'Error in deletion'));  
        } else {
            $this->db->transCommit();
            echo json_encode(array('success'=>true, 'msg'=>'Successfully deleted'));  
        }
        return false;
    }

    public function charges()
    {
        $this->data['load_js'][] = 'module/settings/charges.js'; 
       
        $mainModel = new MainModel();

        $this->data['list'] = $mainModel->getAnyItems('list_of_charges');

        echo inspinia_theme('settings/charges', $this->data);
    }
    
    public function store_charges() 
    {
        helper('url');
        $code = $this->request->getVar('charges-code');
        $desc = $this->request->getVar('charges-description');
        $group = $this->request->getVar('charges-group');
        $enabled = $this->request->getVar('charges-enabled'); 
        
        $data = [
            "charge_code" => $code,
            "charge_description" => $desc,
            "report_group" => $group,
            "is_active" => ($enabled == "on") ? 1 : 0
        ];

        $mainModel = new MainModel();
        
        $this->db->transBegin();

        $mainModel->saveData('list_of_charges', $data);
        
        if ($this->db->transStatus() === FALSE) {
            $this->db->transRollback();
            echo json_encode(array('success'=>false, 'msg'=>'Error in saving'));  
        } else {
            $this->db->transCommit();
            echo json_encode(array('success'=>true, 'msg'=>'Successfully Saved'));  
        }
        return;
    }

    public function charge_data() {
        $mainModel = new MainModel();
        $charge_id = $this->request->getVar('charge_id');
        $data = $mainModel->getItem('list_of_charges', array('id' => $charge_id));

        echo json_encode(array('success'=>true, 'result'=>$data));  
        return;
    }

    public function charge_save_data() {
        $code = $this->request->getVar('edit-charges-code');
        $desc = $this->request->getVar('edit-charges-description');
        $group = $this->request->getVar('edit-charges-group');
        $enabled = $this->request->getVar('edit-charges-enabled');
        $id = $this->request->getVar("edit-charges-id");
        $data = [
            "charge_code" => $code,
            "charge_description" => $desc,
            "report_group" => $group,
            "is_active" => ($enabled == "on") ? 1 : 0
        ];
        $mainModel = new MainModel();
        $flag = $mainModel->updateData('list_of_charges', ["id" => $id], $data);
        if ($flag > 0) {
            echo json_encode(array('success'=>true, 'msg'=>'Successfully Saved')); 
            return;
        } else {
            echo json_encode(array('success'=>false, 'msg'=>'Failed')); 
            return;
        }
    }

    public function remove_charge() {
        $mainModel = new MainModel();

        $this->db->transBegin();

        $id = $this->request->getVar('id');
       
        $mainModel->deleteItems('list_of_charges', array('id'=>$id));
        
        if ($this->db->transStatus() === FALSE) {
            $this->db->transRollback();
            echo json_encode(array('success'=>false, 'msg'=>'Error in deletion'));  
        } else {
            $this->db->transCommit();
            echo json_encode(array('success'=>true, 'msg'=>'Successfully deleted'));  
        }
        return false;
    }
}
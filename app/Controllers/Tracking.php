<?php

namespace App\Controllers;
use App\Models\TransactionModel;
use App\Models\MainModel;

class Tracking extends BaseController {
    
    var $data;

    public function index() 
    {
        
       return view('tracking/index');
    }

    public function search() 
    {
        if ($this->request->isAJAX()) {
            $search = $this->request->getVar('search');
            $service = $this->request->getVar('service');


            $transModel = new TransactionModel();
            $mainModel = new MainModel();
     
            $service_type = [];
            $service_icon = "";

            if (strtolower($service) == "air") {
                $service_type = ["IA", "EA"];
                $service_icon = '<i class="fas fa-plane"></i>';
            } elseif (strtolower($service) == "brokerage") {
                $service_type = ["IB", "EB"];
                $service_icon = '<i class="fas fa-shipping-fast"></i>';
            } elseif (strtolower($service) == "domestic") {
                $service_type = ["AD", "SD"];
                $service_icon = '<i class="fas fa-shipping-fast"></i>';
            } else {
                $service_type = ["IS", "ES"];
                $service_icon = '<i class="fas fa-ship"></i>';
            }

            $transaction = $transModel->search_transation($search, $service_type);
           
            
            $this->data["service_type"] = $service_icon;
            $this->data["history"] = [];
            if ($transaction) {
                $this->data["history"] = $transModel->get_history_items($transaction->id);
            }
            // echo "<pre>";
            // print_r($this->data);
            // echo "</pre>";
            $html = view('tracking/timeline', $this->data);
            echo json_encode(array('success'=>true, 'html'=> $html)); 
            return;

        }
        return false;
    }


}
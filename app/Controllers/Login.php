<?php

namespace App\Controllers;

use App\Models\UserModel;

class Login extends BaseController
{
	public function index()
	{
		$data = [];
		helper(['form']);

		if ($this->request->getMethod() == 'post') {

			$rules = [
				'email' => 'required|min_length[5]|max_length[50]',
				'password' => 'required|min_length[5]|max_length[255]|validateUser[email,password]',
			];

			$errors = [
				'password' => [
					'validateUser' => 'Email or Password don\'t match'
				]
			];

			if (! $this->validate($rules, $errors)) {
				$data['validation'] = $this->validator;
			} else{
				
				$model = new UserModel();

				$user = $model
						->where('email', $this->request->getVar('email'))
						->orWhere('username', $this->request->getVar('email'))
						->first();

				$this->setUserSession($user);
				return redirect()->to('dashboard');

			}
		} 

		return view('login/index', $data);
	}

	private function setUserSession($user){
		$data = [
			'id' => $user['id'],
			'first_name' => $user['first_name'],
			'last_name' => $user['last_name'],
			'company' => $user['company'],
			'phone' => $user['phone'],
			'email' => $user['email'],
			'username' => $user['username'],
			'position' => $user['position'],
			'isLoggedIn' => true,
		];

		session()->set($data);
		return true;
	}

	public function logout(){
		session()->destroy();
		return redirect()->to('/login');
	}


}
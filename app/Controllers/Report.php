<?php
namespace App\Controllers;


use App\Models\MainModel;
use App\Models\TransactionModel;

class Report extends BaseController {
    var $data;
    var $db;
    var $mainModel;

    protected $helpers = ['auth_helper', 'form', 'url'];

    function __construct()
	{
        $this->data['title'] = 'test';

        $this->data['load_js'][] = 'plugins/dataTables/datatables.min.js';
		$this->data['load_js'][] = 'plugins/dataTables/dataTables.bootstrap4.min.js';
        $this->data['load_js'][] = 'plugins/moment/moment.min.js'; 
        $this->data['load_js'][] = 'plugins/daterangepicker/daterangepicker.js'; 
        $this->data['load_js'][] = 'module/reports/index.js'; 
        $this->data['load_css'][] = 'plugins/dataTables/datatables.min.css';
		$this->data['load_css'][] = 'plugins/daterangepicker/daterangepicker.css'; 
        $this->data['load_css'][] = 'custom.css'; 
        
        $this->db = \Config\Database::connect();

        $this->mainModel = new MainModel();
        

        helper('template');
    }

    function index() 
    {
        echo inspinia_theme('admin/reports/index', $this->data);
    }

    function index_consignee() 
    {
        $this->data["consignees"] = $this->mainModel->getAnyItems('partners', ["partner_type" => "C",], ["partner_type" => "X"]);
        echo inspinia_theme('admin/reports/consignee_per_dr', $this->data);
    }

    function index_shipper() 
    {
        $this->data["shippers"] = $this->mainModel->getAnyItems('partners', ["partner_type" => "S",], ["partner_type" => "X"]);
        echo inspinia_theme('admin/reports/shipper_per_dr', $this->data);
    }

    function index_agent() 
    {
        $this->data["agents"] = $this->mainModel->getAnyItems('partners', ["partner_type" => "A",], ["partner_type" => "X"]);
        echo inspinia_theme('admin/reports/agent_per_dr', $this->data);
    }

    function trxn()
    {
        $filter_by       = isset($_POST['filter_by_type']) ? $_POST['filter_by_type'] : "";
        $filter_value    = isset($_POST['filter_by_value']) ? $_POST['filter_by_value'] : "";
        $filter_sdate    = isset($_POST['filter_start_date']) ? $_POST['filter_start_date'] : "";
        $filter_edate    = isset($_POST['filter_end_date']) ? $_POST['filter_end_date'] : "";

        $trxn           = new TransactionModel();
        $mainModel      = new MainModel();

        
        $iTotal     = $trxn->get_count_of_report_data($filter_by, $filter_value, $filter_sdate, $filter_edate);
        $iResults   = $trxn->get_report_data($filter_by, $filter_value, $filter_sdate, $filter_edate);
        
        $iFilteredTotal = count($iResults);

        if ($iFilteredTotal == 1) {
            $record[] = array(
                $iResults[0]->id,
                $iResults[0]->master_ref,
                $iResults[0]->house_ref,
                $iResults[0]->status,
				$iResults[0]->etd,
				$iResults[0]->eta,
				$iResults[0]->atd,
				$iResults[0]->ata,
				$iResults[0]->id,
            );
        } else {
            if ($iResults) {
                foreach ($iResults as $idx => $row) {
                    $current_status = $mainModel->getItem('status', ['status_code' => $row->current_status]);
                    $service_type   = $mainModel->getItem('services', ['service_code' => $row->service_type]);
                    $record[] = array(
                        date("m/d/Y",strtotime($row->transaction_datetime)),
                        $row->master_ref,
                        $row->house_ref,
                        $service_type ? $service_type->service_name : "",
                        $current_status ? $current_status->status_name : "",
                        date("m/d/Y",strtotime($row->etd)),
                        date("m/d/Y",strtotime($row->eta)),
                        date("m/d/Y",strtotime($row->atd)),
                        date("m/d/Y",strtotime($row->ata)),
                    );
                }
            } else {
                $record = [];
            }
        }

        $num = isset($_POST['sEcho']) ? $_POST['sEcho'] : 1;
        
        $output = array(
            "sEcho" => intval($num),
            "iTotalRecords" => $iFilteredTotal,
            "iTotalDisplayRecords" => $iTotal,
            "aaData" => $record
        );
        
        echo json_encode($output);
    }

}
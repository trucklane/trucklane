<?php

use Config\Services;
use CodeIgniter\I18n\Time;

if (! function_exists('send_activation_email'))
{
    /**
    * Builds an account activation HTML email from views and sends it.
    */
    function send_activation_email($to, $activateHash)
    {
    	$htmlMessage = view('Auth\Views\emails\header');
    	$htmlMessage .= view('Auth\Views\emails\activation', ['hash' => $activateHash]);
    	$htmlMessage .= view('Auth\Views\emails\footer');

    	$email = \Config\Services::email();
		$email->initialize([
			'mailType' => 'html'
		]);

    	$email->setTo($to);
        $email->setSubject(lang('Auth.registration'));
		$email->setMessage($htmlMessage);

        return $email->send();
    }
}

if (! function_exists('send_confirmation_email'))
{
    /**
    * Builds an email confirmation HTML email from views and sends it.
    */
    function send_confirmation_email($to, $activateHash)
    {
        $htmlMessage = view('Auth\Views\emails\header');
        $htmlMessage .= view('Auth\Views\emails\confirmation', ['hash' => $activateHash]);
        $htmlMessage .= view('Auth\Views\emails\footer');

        $email = \Config\Services::email();
        $email->initialize([
            'mailType' => 'html'
        ]);

        $email->setTo($to);
        $email->setSubject(lang('Auth.confirmEmail'));
        $email->setMessage($htmlMessage);

        return $email->send();
    }
}


if (! function_exists('send_notification_email'))
{
    /**
    * Builds a notification HTML email about email address change from views and sends it.
    */
    function send_notification_email($to)
    {
        $htmlMessage = view('Auth\Views\emails\header');
        $htmlMessage .= view('Auth\Views\emails\notification');
        $htmlMessage .= view('Auth\Views\emails\footer');

        $email = \Config\Services::email();
        $email->initialize([
            'mailType' => 'html'
        ]);

        $email->setTo($to);
        $email->setSubject(lang('Auth.emailUpdateRequest'));
        $email->setMessage($htmlMessage);

        return $email->send();
    }
}


if (! function_exists('send_password_reset_email'))
{
    /**
    * Builds a password reset HTML email from views and sends it.
    */
    function send_password_reset_email($to, $resetHash)
    {
        $htmlMessage = view('Auth\Views\emails\header');
        $htmlMessage .= view('Auth\Views\emails\reset', ['hash' => $resetHash]);
        $htmlMessage .= view('Auth\Views\emails\footer');

        $email = \Config\Services::email();
        $email->initialize([
            'mailType' => 'html'
        ]);

        $email->setTo($to);
        $email->setSubject(lang('Auth.passwordResetRequest'));
        $email->setMessage($htmlMessage);

        return $email->send();
    }
}

if (! function_exists('check_access'))
{

    function check_access($useraccess, $module_name, $access) {                 
        if (isset($useraccess[$module_name])) {
            if (isset($useraccess[$module_name][$access])) {
                return $useraccess[$module_name][$access];
            }  else {
                return 0;
            }
            
        } else {
            return 0;
        }
    }
}

if (! function_exists('check_permission'))
    {
    function check_permission($user_permission, $main_menu_id, $sub_id) {                 
        if (isset($user_permission[$main_menu_id])) {
            if(in_array($sub_id, $user_permission[$main_menu_id])) {
                return 1;
            }  else {
                return 0;
            }
            
        } else {
            return 0;
        }
    }
}

if (! function_exists('ph_time'))
    {
    function ph_time() {
        $time = Time::parse('now', 'Asia/Manila');
        return $time->toDateTimeString();
    }
}

if (! function_exists('ph_date'))
    {
    function ph_date() {
        $time = Time::parse('now', 'Asia/Manila');
        return $time->toDateString();
    }
}



<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php')) {
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Dashboard');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);


/**
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

$routes->get('login', 'Login::index',['filter' => 'noauth']);
$routes->get('logout', 'Login::logout');
$routes->match(['get','post'],'profile', 'Users::profile',['filter' => 'auth']);
$routes->get('dashboard', 'Dashboard::index',['filter' => 'auth']);
$routes->get('users', 'Users::index', ['filter' => 'auth']);

$routes->get('menu', 'Settings::menu', ['filter' => 'auth']);
$routes->post('menu_list', 'Settings::menu_list', ['filter' => 'auth']);

$routes->get('submenu', 'Settings::menu', ['filter' => 'auth']);
$routes->get('countries', 'Settings::menu', ['filter' => 'auth']);
$routes->get('airports', 'Settings::menu', ['filter' => 'auth']);


$routes->add('transaction/(:num)/view', 'Service::view/$1',['filter' => 'auth']);
$routes->add('transaction/(:num)/edit', 'Service::edit/$1',['filter' => 'auth']);

$routes->add('transaction/', 'Transaction::index',['filter' => 'auth']);
$routes->add('transaction/index', 'Transaction::index',['filter' => 'auth']);

$routes->add('import_sea', 'Service::sea_import', ['filter' => 'auth']);
$routes->add('import_sea/entry', 'Service::sea_import_entry', ['filter' => 'auth']);
$routes->add('export_sea', 'Service::sea_export', ['filter' => 'auth']);
$routes->add('export_sea/entry', 'Service::sea_export_entry', ['filter' => 'auth']);
$routes->add('import_air', 'Service::air_import', ['filter' => 'auth']);
$routes->add('import_air/entry', 'Service::air_import_entry', ['filter' => 'auth']);
$routes->add('export_air', 'Service::air_export', ['filter' => 'auth']);
$routes->add('export_air/entry', 'Service::air_export_entry', ['filter' => 'auth']);
$routes->add('air_domestic', 'Service::air_domestic', ['filter' => 'auth']);
$routes->add('air_domestic/entry', 'Service::air_domestic_entry', ['filter' => 'auth']);
$routes->add('sea_domestic', 'Service::sea_domestic', ['filter' => 'auth']);
$routes->add('sea_domestic/entry', 'Service::sea_domestic_entry', ['filter' => 'auth']);
$routes->add('import_brokerage', 'Service::import_brokerage', ['filter' => 'auth']);
$routes->add('import_brokerage/entry', 'Service::import_brokerage_entry', ['filter' => 'auth']);
$routes->add('export_brokerage', 'Service::export_brokerage', ['filter' => 'auth']);
$routes->add('export_brokerage/entry', 'Service::export_brokerage_entry', ['filter' => 'auth']);

$routes->add('reports', 'Report::index', ['filter' => 'auth']);
$routes->add('reports/consignee_dr', 'Report::index_consignee', ['filter' => 'auth']);
$routes->add('reports/shipper_dr', 'Report::index_shipper', ['filter' => 'auth']);
$routes->add('reports/agent_dr', 'Report::index_agent', ['filter' => 'auth']);
$routes->add('reports/trxn', 'Report::trxn', ['filter' => 'auth']);






// We get a performance increase by specifying the default
// route since we don't have to scan directories.
//$routes->get('/', 'Home::index');

/**
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need to it be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}

<?php namespace Config;

class Site extends \CodeIgniter\Config\BaseConfig {

	public $login_url		 = 'login';
	public $logout_url 		 = 'login/logout';

	//Name of the sessions to convert to template variables
	public $sessions = array(
					'language' 	=> 'lang',
					'user' 		=> 'full_name',
					'group' 	=> 'groupname'
		);
	//Name of labels you can use lang helper
	public $labels = array(
		'logout'  => 'Logout',// You can use lang('lang_key')
		'profile' => 'Profile',// You can use lang('lang_key')
		'search'  => 'Search'
	);

	
	public $theme 		= 'inspinia';
	//mini-navbar fixed-sidebar fixed-nav fixed-nav-basic  boxed-layout top-navigation
	public $body_class 	= ' pace-done';
	public $title 			= 'Track and Track System';

	public $favicon 	    = 'theme/img/ci.png';
	public $company_name = 'Trucklane Group Of Companies';
	public $app_name 		= 'Track and Trace';
	public $app_version  = 'v1.0';

	public $app_year     = '2021';

	public $meta  	= array(
							'viewport'		=> 'width=device-width, initial-scale=1.0',
							'author'		=> 'Cris Baceta',
							'description'	=> 'Track And Trace',
							'resource-type'	=> 'document',
							'robots'		=> 'all, index, follow',
							'googlebot'		=> 'all, index, follow',
						);

	public $css_files = array(
							
							'theme/css/bootstrap4.min.css',
							'theme/css/animate.min.css',
							'theme/css/style.css',
							'theme/css/font-awesome/css/font-awesome.css',
							'theme/css/plugins/codemirror/codemirror.css',
							'theme/css/plugins/codemirror/ambiance.css',
							'theme/css/plugins/toastr/toastr.min.css',
							'theme/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css',
							'theme/css/plugins/select2/select2.min.css',
							'theme/css/plugins/iCheck/custom.css',
							'theme/css/plugins/ladda/ladda-themeless.min.css',
							'theme/css/plugins/sweetalert/sweetalert.css',
							'theme/css/plugins/datapicker/datepicker3.css',
							
						);

	public $js_files  = array(
							'theme/js/jquery.js',
							
							'theme/js/app.js',
							'theme/js/popper.min.js',
							'theme/js/bootstrap.min.js',
							'theme/js/plugins/metisMenu/jquery.metisMenu.js',
							'theme/js/plugins/slimscroll/jquery.slimscroll.min.js',
							'theme/js/plugins/validate/bootstrap-validator.min.js',
							'theme/js/plugins/toastr/toastr.min.js',
							'theme/js/plugins/iCheck/icheck.min.js',
							'theme/js/plugins/select2/select2.full.min.js',
							'theme/js/plugins/datapicker/bootstrap-datepicker.js',
							'theme/js/plugins/ladda/spin.min.js',
							'theme/js/plugins/ladda/ladda.min.js',
							'theme/js/plugins/jasny/jasny-bootstrap.min.js',
							'theme/js/plugins/sweetalert/sweetalert.min.js',
							'theme/js/plugins/validate/jquery.validate.min.js',
							'theme/js/main.js',
							
						);

	public $directories = array(
							'asset_path' 	=> 'theme/',
							'css_path' 		=> 'theme/css/',
							'js_path' 		=> 'theme/js/',
							'less_path' 	=> 'theme/less/',
							'img_path' 		=> 'theme/img/',
							'upload_path' 	=> 'upload/',
							'download_path' => 'download/'
						);

	public $multilingual = array(
								'default'		=> 'english',
								'available'		=> array(
									'en' => array(
										'label'	=> 'English',
										'value'	=> 'english',
									),
									'es' => array(
										'label'	=> 'Español',
										'value'	=> 'spanish',
									)
								),
								'autoload'		=> array('site')
								);

	public $google_analytics = '';

}












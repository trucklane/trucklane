<?php namespace Config;

class Template extends \CodeIgniter\Config\BaseConfig {
   
    public $inspinia = array(
        'enable_id' => false, 
        'list_open' 			=> '<li>',
        'list_close' 			=> '</li>',
        'link_open'  			=> '<a>',
        'link_close' 			=> '</a>',
        'label_open'  			=> '<span class="nav-label">',
        'label_close' 			=> '</span>',
        'icon_open' 	 		=> '<i>',
        'icon_close' 	 		=> '</i>',
        'expandable_icon' 		=> '<span class="fa arrow"></span>',
        'second_level_open'    => '<ul class="nav nav-second-level">',
        'second_level_close'	=> '</ul>',
        'third_level_open'  	=> '<ul class="nav nav-third-level">',
        'third_level_close' 	=> '</ul>',
        'active_class'   		=> 'active',
        'collapse_class' 		=> 'in',
        'icon_class'   		=> 'fa fa-',
    );

    /*
    | -------------------------------------------------------------------
    | INSPINIA ADMIN THEME - TOP BAR MENU TEMPLATE
    | -------------------------------------------------------------------
    */

    public $top_navigation = array (
        'enable_id' 			=> false,
        'list_open' 			=> '<li>',
        'list_close' 		=> '</li>',
        'link_open'  		=> '<a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" role="button">',
        'link_close' 		=> '</a>',
        'label_open'  		=> '',
        'label_close' 		=> '',
        'icon_open' 	 		=> '<i>',
        'icon_close' 		=> '</i>',
        'expandable_icon'	=> '<span class="caret"></span>',
        'second_level_open'  => '<ul role="menu" class="dropdown-menu">',
        'second_level_close'	=> '</ul>',
        'third_level_open'  	=> '<ul class="dropdown-menu">',
        'third_level_close' 	=> '</ul>',
        'active_class'   	=> 'active',
        'collapse_class' 	=> 'in',
        'icon_class'   		=> 'fa fa-',
    );

    /*
    | -------------------------------------------------------------------
    | LUNA ADMIN THEME - SIDE MENU TEMPLATE
    | -------------------------------------------------------------------
    */
    public $luna = array(
        'enable_id' 			=> true, //Assign id's to children and and conect parent via href="#{children_id}"
        'list_open' 			=> '<li>',
        'list_close' 			=> '</li>',
        'link_open'  			=> '<a data-toggle="collapse">',
        'link_close' 			=> '</a>',
        'label_open'  			=> '',
        'label_close' 			=> '',
        'icon_open' 	 		=> '<i>',
        'icon_close' 	 		=> '</i>',
        'expandable_icon' 		=> '<span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>',
        'second_level_open'    => '<ul class="nav nav-second collapse">',
        'second_level_close'	=> '</ul>',
        'third_level_open'  	=> '<ul class="nav nav-third">',
        'third_level_close' 	=> '</ul>',
        'active_class'   		=> 'active',
        'collapse_class' 		=> 'in',
        'icon_class'   		=> 'fa fa-'
    );

    /*
    | -------------------------------------------------------------------
    | BREADCRUMB TEMPLATE
    | -------------------------------------------------------------------
    */

    public $breadcrumb = array(
    'breadcrumb_open'   => '<ol class="breadcrumb">',
    'breadcrumb_close'  => '</ol>',
    'list_open'   	   => '<li>',
    'list_close'   	   => '</li>',
    'list_active'   	   => '<strong>',
    'list_active_close' => '</strong>',
    'link_open'  	   => '<a>',
    'link_close' 	   => '</a>',
    );

}

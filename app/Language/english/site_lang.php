<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * @name:  Site Lang- English
 *
 * @author: Ernest Hernandez
 * @description:  English language file for site template
 *
 */

return [
    'template_logout'   => 'Logout',
    'template_login'    => 'Login',
    'template_language' => 'Language',
    'return_label'      => 'Go Back',
    'main_home'         => 'Home',
    'welcome'           => 'Home',
    'github'            => 'Github',
    'groups'            => 'Groups',
    'users'             => 'Users',
    'log'               => 'Log',
    'log'               => 'Log',
    'config'            => 'Configuration',
    'interface'         => 'Interface',
    'interface_menu'    => 'Menus',
    'interface_theme'   => 'Themes',
    'manage'            => 'Manage',
    'projects'          => 'Projects',
    'levels'            => 'Menu Levels',
    'two'               => 'Secod Level',
    'three'             => 'Third Level',
    'three_item'        => 'Third Level Item',
    'user'              => 'Users',
    'notify'            => 'Notify',
    'dashboard'         => 'Dashboard'
];

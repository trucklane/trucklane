<?php 
namespace App\Models;

use CodeIgniter\Model;

class SettingsModel extends Model
{
    protected $table = 'menu_main';
    protected $primaryKey = 'menu_id';
    protected $allowedFields = ['menu_name', 'menu_id', 'icon'];

    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $useTimestamps = false;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    public function __construct() {
        parent::__construct();
        $db = \Config\Database::connect();
        $builder = $db->table($this->table);
    }

    function getMainMenu() {
    	$query = "SELECT * FROM menu_main where menu_main.is_enabled = 1 order by sort";
        $result = $this->db->query($query)->getResult();
        return $result;
    }
    
    function getUserPermission($user_id = '') {
    	
        $where = 'where 1=1 ';
        
        if(!empty($user_id)) {
			$where .= "and up.user_id = '".$user_id."'"; 
        }
        $query = "SELECT up.* FROM user_permissions up $where";

        $result = $this->db->query($query)->getResult();
        $permission = array();
        
        foreach ($result as $key => $value) {
        	$permission[$value->main_menu_id][] = $value->main_sub_id;
        }

        return $permission;
    }

    function getSubMenu($menu_id) {
        $sql = "SELECT * FROM main_sub where is_enabled=1 and menu_id ='".$menu_id."' order by sort";
        $result = $this->db->query($sql)->getResult();
		return $result;
	}
	function getMainMenuId($submenu_id) {
		$sql = "SELECT menu_main.menu_name FROM menu_main left join main_sub on menu_main.menu_id= main_sub.menu_id
        where menu_main.is_enabled = 1 and main_sub.id=".$submenu_id;
        
        $rec = $this->db->query($sql)->getRow();

		return strtolower($rec->menu_name);
		
	}

}
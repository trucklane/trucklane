<?php
 
namespace App\Models;
 
use CodeIgniter\Model;

class BillModel extends Model {
 
    protected $table      = 'partners';
    protected $primaryKey = 'id';

    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = ['menu_id', 'parent_id', 'menu_name', 'icon', 'is_enabled', 'sort'];

    protected $useTimestamps = false;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

     
    public function __construct() {
        parent::__construct();
        $db = \Config\Database::connect();
        $builder = $db->table($this->table);
    }

    public function get_partners_by_type($type = "A") 
    {
        $builder = $this->db->table($this->table);
        $builder->where('partner_type', $type);

        return $builder->get()->getResult();
    }
     
}
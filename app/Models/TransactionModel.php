<?php
 
namespace App\Models;
 
use CodeIgniter\Model;

class TransactionModel extends Model {
 
    var $table = "transactions";

    public function __construct() 
    {
        parent::__construct();
        $db = \Config\Database::connect();
        $builder = $db->table($this->table);
    }

    public function get_master_refs() 
    {
        $builder = $this->db->table($this->table);
        $builder->select('master_ref');
        $builder->where('master_ref !=', "");
        $builder->groupBy('master_ref');

        return $builder->get()->getResult();
    }

    public function get_transaction($id = "")
    {
        $builder = $this->db->table("transactions as t");
        $builder->where("t.id", $id);
        $builder->join('status as s', 's.status_code = t.status', 'left');
        $builder->select('t.*, s.status_name, s.status_code');
        return $builder->get()->getRow();
    }
     
    public function get_transaction_shippers($trans_id, $type = "") 
    {
        $builder = $this->db->table("transaction_shippers as t");
        $builder->where(["t.transaction_id" => $trans_id, "t.partner_type" => $type]);
        $builder->join('partners as p', 'p.partner_code = t.partner_code', 'left');
        return $builder->get()->getResult();
    }

    public function get_transaction_agent($partner_code = "") 
    {
        $builder = $this->db->table("partners");
        $builder->where(["partner_code" => $partner_code]);
        return $builder->get()->getRow();
    }

    public function search_transation($search = "", $service_type = []) 
    {
        $builder = $this->db->table('transactions as t');
        $builder->join('transaction_house as h', 't.id = h.transaction_id', 'left');
        $builder->groupStart();
        $builder->where("h.house_ref", $search);
        $builder->orWhere('t.master_ref', $search);
        $builder->groupEnd();
        // $builder->groupStart();
        // $builder->where('house_ref', $search);
        // $builder->orGroupStart();
        // $builder->where('master_ref', $search);
        // $builder->where('house_ref', '');
        // $builder->groupEnd();
        // $builder->groupEnd();
        $builder->whereIn("service_type", $service_type);
        // $builder->orderBy('house_ref', 'DESC');
        $builder->orderBy('master_ref', 'DESC');
        // $builder->get()->getRow();
        // echo ">>>".$this->db->getLastQuery();
        // exit();
        return $builder->get()->getRow();
    }

    public function get_history_items($trans_id = "") 
    {
        $builder = $this->db->table('transactions_history as t');
        $builder->where('t.transaction_id', $trans_id);
        $builder->where('s.status_visibility', 1);
        $builder->where('s.is_enabled', 1);
        $builder->join('status as s', 't.status_code = s.status_code', 'left');
        $builder->join('users as u', 'u.id = t.updated_by', 'left');
        $builder->select("t.*, s.status_name, s.status_description, u.first_name, u.last_name");
        $builder->orderBy("t.created_at", "DESC");
        return $builder->get()->getResult();
    }

    public function get_transaction_data($where = array())
    {
        $builder = $this->db->table("transactions as t");
        $builder->where($where);
        $builder->join('transaction_house as h', 't.id = h.transaction_id', 'left');
        return $builder->get()->getResult();
    }

    public function get_count_transaction_data($where = array())
    {
        $builder = $this->db->table("transactions as t");
        $builder->where($where);
        $builder->join('transaction_house as h', 't.id = h.transaction_id', 'left');
        return $builder->countAllResults();
    }

    public function get_report_data($filter_by, $filter_value, $sd, $ed) 
    {
        $builder = $this->db->table("transactions as t");
        $builder->join('transaction_house as h', 't.id = h.transaction_id', 'left');
        if (strtolower($filter_by) == "consignee") {
            $builder->where('h.consignee_code', $filter_value);    
        }

        $builder->where('t.created_at >=', $sd);
        $builder->where('t.created_at <=', $ed);
        return $builder->get()->getResult();
    }

    public function get_count_of_report_data($filter_by, $filter_value, $sd, $ed) 
    {
        $builder = $this->db->table("transactions as t");
        $builder->join('transaction_house as h', 't.id = h.transaction_id', 'left');
        if (strtolower($filter_by) == "consignee") {
            $builder->where('h.consignee_code', $filter_value);    
        }
        $builder->where('t.created_at >=', $sd);
        $builder->where('t.created_at <=', $ed);
        return $builder->countAllResults();
    }

    

}
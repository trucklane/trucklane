<?php
 
namespace App\Models;
 
use CodeIgniter\Model;

class MenuModel extends Model {
 
    protected $table      = 'menu_main';
    protected $primaryKey = 'menu_id';

    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = ['menu_id', 'parent_id', 'menu_name', 'icon', 'is_enabled', 'sort'];

    protected $useTimestamps = false;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

     
    public function __construct() {
        parent::__construct();
        $db = \Config\Database::connect();
        $builder = $db->table($this->table);
    }

 
    public function schema($table)
    {
        $query = "SHOW COLUMNS FROM $table";
        $result = $this->db->query($query)->getResult();
        return $result;
    }

 
}
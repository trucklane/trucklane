<?php namespace App\Models;

use CodeIgniter\Model;
use App\Libraries\Bcrypt;

class UserModel extends Model{
    protected $table = 'users';
    protected $allowedFields = ['firstname', 'lastname', 'email', 'password', 'updated_at'];
    protected $beforeInsert = ['beforeInsert'];
    protected $beforeUpdate = ['beforeUpdate'];

    public function __construct() 
    {
        parent::__construct();
        $db = \Config\Database::connect();
        $builder = $db->table($this->table);

        $this->store_salt = FALSE;
        $this->hash_method = 'bcrypt';
    }


    protected function beforeInsert(array $data){
        $data = $this->passwordHash($data);
        $data['data']['created_at'] = date('Y-m-d H:i:s');

        return $data;
    }

    protected function beforeUpdate(array $data){
        $data = $this->passwordHash($data);
        $data['data']['updated_at'] = date('Y-m-d H:i:s');
        return $data;
    }

    protected function passwordHash(array $data){
        if(isset($data['data']['password']))
            $data['data']['password'] = password_hash($data['data']['password'], PASSWORD_DEFAULT);

        return $data;
    }

    function get_data($id) {
        $builder = $this->db->table("users as u");
        $builder->where("u.id", $id);
        return $builder->get()->getRow();
    }
    
    function getModules($user_id = '') {
        $builder = $this->db->table("modules m");
        return $builder->get()->getResult();
    }

    function getUserAccess($user_id = 0) {

        $builder = $this->db->table("useraccess ua");
        $builder->select("m.module_name, ua.*");
        $builder->join('modules m','ua.module_id = m.module_id','right');
        $builder->join('users u','u.id = ua.user_id','left');
        
        if(!empty($user_id)) {
			$builder->where("u.id", $user_id);
        }
        
        $access = array();
        
        foreach ($builder->get()->getResult() as $key => $value) {
        	$access[$value->module_name] = array(
        		'add'=>$value->addx,
        		'delete'=>$value->deletex,
        		'view'=>$value->viewx,
        		'post'=>$value->postx,
        		'unpost'=>$value->unpostx,
                'cancel'=>$value->cancelx,
                'approve'=>$value->approvex,
                'backload'=>$value->backloadx,
        	);
        }

        return $access;
    }


    function getPermission() {
        
        $builder = $this->db->table("menu_main mm");
        $builder->select('mm.menu_id as mm_id, mm.menu_name, ms.id as ms_id, ms.menu, ms.url');
		$builder->join('main_sub ms','ms.menu_id = mm.menu_id','right');
		$builder->where('mm.is_enabled','1');
		$builder->where('ms.is_enabled','1');
		$builder->orderBy('mm.sort');
	
        $permission = array();
        foreach ($builder->get()->getResult() as $key => $value) {
        	$permission[$value->mm_id][$value->menu_name][] = array(
        		'mm_id'=>$value->mm_id,
        		'ms_id'=>$value->ms_id, 
        		'menu_name'=>$value->menu, 
        		'url'=>$value->url);
        }
        return $permission;
    }

    function getUserPermission($user_id = 0) {

        $builder = $this->db->table("user_permissions up");

    	$builder->select('up.*');
		
        if(!empty($user_id)) {
			$builder->where('up.user_id',$user_id);
		}
       
        $permission = array();
        
        foreach ($builder->get()->getResult() as $key => $value) {
        	$permission[$value->main_menu_id][] = $value->main_sub_id;
        	
        }

        return $permission;
    }

    function register($username, $password, $email, $additional_data = array(), $groups = array())
	{

		if ($this->identity_check($username))
		{
			$this->set_error('account_creation_duplicate_identity');
			return FALSE;
		}
		
		// IP Address
		$ip_address = ip_address();
		$salt       = FALSE;
        $password   = $this->hash_password($password, $salt);

		// Users table.
		$data = array(
		    'username'   => $username,
		    'password'   => $password,
		    'email'      => $email,
		    'ip_address' => $ip_address,
		    'created_at' => ph_time(),
		    'active'     => 1
        );
        
      
		if ($this->store_salt)
		{
			$data['salt'] = $salt;
		}

        $user_data = array_merge($additional_data, $data);

        $this->db->table('users')->insert($user_data);
        $id = $this->db->insertID();
		return (isset($id)) ? $id : FALSE;
    }
    
    function update_user($id, $data) {
		
        $user = $this->get_data($id);

        if (array_key_exists('username', $data) || array_key_exists('password', $data) || array_key_exists('email', $data))
		{
			if (array_key_exists('password', $data))
			{
				if( ! empty($data['password']))
				{
					$data['password'] = $this->hash_password($data['password'], $user->salt);
				}
				else
				{
					unset($data['password']);
				}
			}
        }

        $this->db->table('users')->where(array('id'=>$id))->update($data);
        return $this->db->affectedRows();
        
    }

    function getUserList() {
        $builder = $this->db->table("users u");
        return json_encode($builder->get()->getResult() );

    }
    public function identity_check($username = '')
	{
		if (empty($username)){
			return FALSE;
        }
        
        $builder = $this->db->table("users u");
        $builder->where("u.username", $username);
        return $builder->countAllResults() > 0;
    }
    
    public function salt()
	{

       $raw_salt_len = 16;

 		$buffer = '';
        $buffer_valid = false;

        if (function_exists('mcrypt_create_iv') && !defined('PHALANGER')) {
            $buffer = mcrypt_create_iv($raw_salt_len, MCRYPT_DEV_URANDOM);
            if ($buffer) {
                $buffer_valid = true;
            }
        }

        if (!$buffer_valid && function_exists('openssl_random_pseudo_bytes')) {
            $buffer = openssl_random_pseudo_bytes($raw_salt_len);
            if ($buffer) {
                $buffer_valid = true;
            }
        }

        if (!$buffer_valid && @is_readable('/dev/urandom')) {
            $f = fopen('/dev/urandom', 'r');
            $read = strlen($buffer);
            while ($read < $raw_salt_len) {
                $buffer .= fread($f, $raw_salt_len - $read);
                $read = strlen($buffer);
            }
            fclose($f);
            if ($read >= $raw_salt_len) {
                $buffer_valid = true;
            }
        }

        if (!$buffer_valid || strlen($buffer) < $raw_salt_len) {
            $bl = strlen($buffer);
            for ($i = 0; $i < $raw_salt_len; $i++) {
                if ($i < $bl) {
                    $buffer[$i] = $buffer[$i] ^ chr(mt_rand(0, 255));
                } else {
                    $buffer .= chr(mt_rand(0, 255));
                }
            }
        }

        $salt = $buffer;

        // encode string with the Base64 variant used by crypt
        $base64_digits   = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';
        $bcrypt64_digits = './ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        $base64_string   = base64_encode($salt);
        $salt = strtr(rtrim($base64_string, '='), $base64_digits, $bcrypt64_digits);

	    $salt = substr($salt, 0, $this->salt_length);


		return $salt;

    }
    
    public function hash_password($password, $salt=false, $use_sha1_override=FALSE)
	{
        $this->bcrypt = new Bcrypt();

        if (empty($password))
		{
			return FALSE;
		}

		// bcrypt
		if ($use_sha1_override === FALSE && $this->hash_method == 'bcrypt')
		{
			return $this->bcrypt->hash($password);
		}


		if ($this->store_salt && $salt)
		{
			return  sha1($password . $salt);
		}
		else
		{
			$salt = $this->salt();
			return  $salt . substr(sha1($salt . $password), 0, -$this->salt_length);
		}
    }
    


}

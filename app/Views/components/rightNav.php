<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="/" class="brand-link" style="background-color: #fff; padding-left: 25px;">
        <img src="<?=BASE?>admin/assets/img/trucklane_logo.png" alt="CI4 CRUD Logo" class="brand-image" style="">
        <span class="brand-text font-weight-light">&nbsp;</span>
    </a>
    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="<?=BASE?>admin/assets/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <a href="#" class="d-block">System Admin</a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <?= $this->include('components/nav') ?>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
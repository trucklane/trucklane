<footer class="main-footer">
    <strong>Copyright &copy; <?=date('Y')?></strong>-v3.0.5 
    Trucklane Group of Companies
    <div class="float-right d-none d-sm-inline-block">
        Power by : TEST
    </div>
</footer>
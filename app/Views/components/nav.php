<nav class="mt-2">
	<ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
		<?php
		$request = service('request');
        ?>
		<li class="nav-item">
			<a href="<?=BASE?>" class="nav-link <?#= !$request->uri->getSegment(1) ? 'active' : null; ?>">
				<i class="nav-icon fas fa-tachometer-alt"></i>
				<p>Dashboard</p>
			</a>
		</li>
		<li class="nav-item <?#= ($request->uri->getSegment(1) == 'transaction') ? 'menu-open' : null; ?>">
			<a href="#" class="nav-link <?#= ($request->uri->getSegment(1) == 'transaction') ? 'active' : null; ?>">
				<i class="nav-icon fa fa-users"></i>
				<p>
					Transacation
					<i class="fas fa-angle-left right"></i>
					<!-- <span class="badge badge-info right">6</span> -->
				</p>
			</a>
			<ul class="nav nav-treeview ">
				<li class="nav-item">
					<a href="/transaction/index" class="nav-link <?#= $request->uri->getSegment(2) == 'index' ? 'active' : null; ?>">
						<i class="fa fa-caret-right nav-icon"></i>
						<p>Listing</p>
					</a>
				</li>
				<li class="nav-item">
					<a href="/transaction/entry/is" class="nav-link <?#= $request->uri->getSegment(3) == 'is' ? 'active' : null; ?>">
						<i class="fa fa-caret-right nav-icon"></i>
						<p>Import Sea</p>
					</a>
				</li>
				<li class="nav-item">
					<a href="/transaction/entry/es" class="nav-link <?#= $request->uri->getSegment(3) == 'es' ? 'active' : null; ?>">
						<i class="fa fa-caret-right  nav-icon"></i>
						<p>Export Sea</p>
					</a>
				</li>
				<li class="nav-item">
					<a href="/transaction/entry/ia" class="nav-link <?#= $request->uri->getSegment(3) == 'ia' ? 'active' : null; ?>">
						<i class="fa fa-caret-right  nav-icon"></i>
						<p>Import Air</p>
					</a>
				</li>
				<li class="nav-item">
					<a href="/transaction/entry/ea" class="nav-link <?#= $request->uri->getSegment(3) == 'ea' ? 'active' : null; ?>">
						<i class="fa fa-caret-right  nav-icon"></i>
						<p>Export Air</p>
					</a>
				</li>
				<li class="nav-item">
					<a href="/transaction/entry/ib" class="nav-link <?#= $request->uri->getSegment(3) == 'ea' ? 'active' : null; ?>">
						<i class="far fa-circle nav-icon"></i>
						<p>Import Brokerage</p>
					</a>
				</li>
				<li class="nav-item">
					<a href="/transaction/entry/eb" class="nav-link <?#= $request->uri->getSegment(3) == 'ea' ? 'active' : null; ?>">
						<i class="far fa-circle nav-icon"></i>
						<p>Export Brokerage</p>
					</a>
				</li>
				<li class="nav-item">
					<a href="/transaction/entry/d" class="nav-link <?#= $request->uri->getSegment(3) == 'ea' ? 'active' : null; ?>">
						<i class="far fa-circle nav-icon"></i>
						<p>Domestic</p>
					</a>
				</li>
			</ul>
		</li>

		<li class="nav-item">
			<a href="#" class="nav-link">
				<i class="nav-icon fa fa-file"></i>
				<p>
					Reports
					<i class="fas fa-angle-left right"></i>
					<!-- <span class="badge badge-info right">6</span> -->
				</p>
			</a>
			<ul class="nav nav-treeview has-treeview">
				<li class="nav-item">
					<a href="<?=BASE?>users"
						class="nav-link <?#= $request->uri->getSegment(1) == 'users' ? 'active' : null; ?>">
						<i class="far fa-circle nav-icon"></i>
						<p>Production Per Cities</p>
					</a>
				</li>
				<li class="nav-item">
					<a href="<?=BASE?>users"
						class="nav-link <?#= $request->uri->getSegment(1) == 'users' ? 'active' : null; ?>">
						<i class="far fa-circle nav-icon"></i>
						<p>Production Per Origin</p>
					</a>
				</li>
			</ul>
		</li>
		<!-- <li class="nav-item">
            <a href="<?=BASE?>users" class="nav-link <?#= $request->uri->getSegment(1) == 'users' ? 'active' : null; ?>">
                <i class="nav-icon fas fa-users"></i>
                <p>Users</p>
            </a>
        </li> -->
		<li class="nav-item has-treeview <?#= ($request->uri->getSegment(1) == 'settings' ||  $request->uri->getSegment(1) == 'users') ? 'menu-open' : null; ?>">
			<a href="#" class="nav-link <?#= ($request->uri->getSegment(1) == 'settings' ||  $request->uri->getSegment(1) == 'users')  ? 'active' : null; ?>">
				<i class="nav-icon fas fa-crosshairs"></i>
				<p>
					Settings
					<i class="fas fa-angle-left right"></i>
					<!-- <span class="badge badge-info right">6</span> -->
				</p>
			</a>
			<ul class="nav nav-treeview">
				<li class="nav-item">
					<a href="<?=BASE?>users"
						class="nav-link <?#= $request->uri->getSegment(1) == 'users' ? 'active' : null; ?>">
						<i class="far fa-circle nav-icon"></i>
						<p>Users</p>
					</a>
				</li>
				<li class="nav-item">
					<a href="/menu" class="nav-link <?#= $request->uri->getSegment(1) == 'menu' ? 'active' : null; ?>">
						<i class="far fa-circle nav-icon"></i>
						<p>Main Menu</p>
					</a>
				</li>
				<li class="nav-item">
					<a href="/submenu" class="nav-link <?#= $request->uri->getSegment(1) == 'submenu' ? 'active' : null; ?>">
						<i class="far fa-circle nav-icon"></i>
						<p>Sub Menu</p>
					</a>
				</li>
				<li class="nav-item">
					<a href="/settings/country" class="nav-link <?#= $request->uri->getSegment(2) == 'country' ? 'active' : null; ?>">
						<i class="far fa-circle nav-icon"></i>
						<p>Countries</p>
					</a>
				</li>
				<li class="nav-item">
					<a href="/settings/ports" class="nav-link <?#= $request->uri->getSegment(2) == 'ports' ? 'active' : null; ?>">
						<i class="far fa-circle nav-icon"></i>
						<p>Ports</p>
					</a>
				</li>

				<li class="nav-item">
					<a href="/settings/partners" class="nav-link <?#= $request->uri->getSegment(2) == 'partners' ? 'active' : null; ?>">
						<i class="far fa-circle nav-icon"></i>
						<p>Partners</p>
					</a>
				</li>

				<li class="nav-item">
					<a href="#" class="nav-link">
						<i class="far fa-circle nav-icon"></i>
						<p>Carrier</p>
					</a>
				</li>

				<li class="nav-item">
					<a href="#" class="nav-link">
						<i class="far fa-circle nav-icon"></i>
						<p>Load Type</p>
					</a>
				</li>

				<li class="nav-item">
					<a href="#" class="nav-link">
						<i class="far fa-circle nav-icon"></i>
						<p>Services</p>
					</a>
				</li>
				<li class="nav-item">
					<a href="#" class="nav-link">
						<i class="far fa-circle nav-icon"></i>
						<p>Truck Type</p>
					</a>
				</li>
				<li class="nav-item">
					<a href="/settings/status" class="nav-link <?#= $request->uri->getSegment(2) == 'status' ? 'active' : null; ?>">
						<i class="far fa-circle nav-icon"></i>
						<p>Status</p>
					</a>
				</li>
			</ul>
		</li>
	</ul>
</nav>
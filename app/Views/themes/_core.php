<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <?php echo $meta; ?>
		<base href="<?php echo $base_url; ?>">
		<link href="<?php echo $favicon; ?>" rel="icon" type="image/png">
        <title><?php echo $app_title; ?></title>
        <?php echo $css_files; ?>
        
        <?php
            if(isset($load_css)) {
                foreach($load_css as $css_file) {
                    echo "\n\t\t";
                    if($css_file)
                        echo '<link rel="stylesheet" href="'. BASE . 'theme/css/'.$css_file.'?v='.VERSION.'"> ';
                }
            }
        ?>
        <script>var rules, addons, l = 'english'; baseUrl ='<?=BASE?>'</script>
    </head>
    <body id="app" class="<?php echo $body_class; ?>">
     

        <?php echo $body; ?>

         <!-- Start LOADING MODAL-->
        <div class="modal fade " id="loadingModal"  data-backdrop="static" data-keyboard="false" tabindex="-1"  aria-hidden="true" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-body">
                <div id='warning'>
                    <h4>Please wait . . .</h4>
                    <div id="progress" class="progress active">
                        <div style="width: 100%" aria-valuemax="100" aria-valuemin="0" role="progressbar" class="progress-bar progress-bar-primary progress-bar-striped">
                        <span class="loading"></span>
                        </div>
                    </div>
                </div>
                </div>
            </div>
            </div>
        </div>  

        <?php echo $js_files; ?>

        <?php
            if(isset($load_js)) {
                foreach($load_js as $js_file) {
                    echo "\n\t";
                    if($js_file)
                        echo '<script src="'. BASE . 'theme/js/'.$js_file.'?v='.VERSION.'"></script>';
                }
            }
        ?>
    </body>
    <!-- End LOADING MODAL-->

    <!--[if lt IE 9]> <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script> <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script><![endif]-->
</html>
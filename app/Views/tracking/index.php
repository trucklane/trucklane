<?= $this->extend('templates/layout/main') ?>

<?= $this->section('content') ?>
    <div class="container ">
        <div class="row justify-content-center text-center py-5">
            <h1 class="title text-primary">Track & <span class="text-info"> Trace Shipment</span></h1>
        </div>
    </div>
    <div class="container ">
        <div class="justify-content-center pr-4">
            <form class="form-horizontal" >
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="trnxn_service" id="inlineRadio1" value="sea" checked>
                    <label class="form-check-label" for="inlineRadio1">Sea Freight</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="trnxn_service" id="inlineRadio2" value="air">
                    <label class="form-check-label" for="inlineRadio2">Air Freight</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="trnxn_service" id="inlineRadio3" value="brokerage">
                    <label class="form-check-label" for="inlineRadio3">Brokerage</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="trnxn_service" id="inlineRadio4" value="domestic">
                    <label class="form-check-label" for="inlineRadio3">Domestic</label>
                </div>
                <div class="input-group mb-5 mt-3">
                    <input name="search-freight" type="text" class="form-control" placeholder="Tracking number" aria-label="Tracking number" aria-describedby="basic-addon2">
                    <div class="input-group-append">
                        <button class="btn btn-primary btn-search-trans" type="button">Track your shipment</button>
                    </div>
                </div>
            </form>
            <div class="border-top py-2 "></div>
            <!-- <div class="card">
                <div class="card-body ">
                    <p> No results found.</p>
                </div>
            </div> -->
        </div>
    </div>

    

    <div class="container">
        <div class="justify-content-center timeline-container">
      
            
        </div>
    </div>
<?= $this->endSection() ?>
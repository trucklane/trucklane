
<div class="container">
    <div class="row">
        <div class="col-md-10">
            <ul class="cbp_tmtimeline">
                <?php $date_storage = array(); ?>
                <?php if ($history): ?>
                    <?php foreach($history as $item):  ?>
                        <?php $date = date("Y-m-d", strtotime($item->created_at)); ?>
                        <li>
                            <time class="cbp_tmtime" datetime="2017-11-04T18:30">
                                <?php if (!in_array($date, $date_storage)):  ?>
                                    <span class="hidden"><?= date(" F d, Y", strtotime($item->created_at)); ?></span> 
                                    <span class="medium">
                                        <?= strtoupper(date("g:i a", strtotime($item->created_at))); ?>
                                    </span>
                                <?php else:?>
                                    <span >&nbsp;</span>
                                    <span class="medium">
                                        <?= strtoupper(date("g:i a", strtotime($item->created_at))); ?>
                                    </span>
                                <?php endif; ?>
                            </time>
                            <div class="cbp_tmicon"><?= $service_type; ?></div>
                            <div class="cbp_tmlabel">
                                <h2><a href="javascript:void(0);"><strong><?= $item->status_name ?></strong></a> - <?= $item->status_description; ?></h2>
                                <p><?= $item->status_comment; ?></p>
                                <?php if ($item->status_file): ?>
                                <div class="">
                                    <ul class="list-unstyled">
                                        <li><a href="<?= BASE."transaction/download/".$item->status_file; ?>"><i
                                                    class="fa fa-paperclip" aria-hidden="true"></i>
                                                <?= $item->status_file;?></a></li>
                                    </ul>
                                </div>
                                <?php endif; ?>
                            </div>
                           
                        </li>
                        <?php  $date_storage[] = $date; ?>
                    <?php endforeach; ?>
                <?php else: ?>
                    <li>
                        <time class="cbp_tmtime" >
                            <span class="hidden"></span> 
                            <span class="large"></span>
                        </time>
                        <div class="cbp_tmicon bg-white"><?= $service_type; ?></div>
                        <div class="cbp_tmlabel empty"> <span>No Activity</span> </div>
                    </li>
                <?php endif; ?>
                <li>
                    <div class="cbp_tmicon bg-gray"><i class="fas fa-clock"></i></div>
                </li>
            </ul>
        </div>
    </div>
</div>
    

   
<div class="timeline">
    <?php $date_storage = array(); ?>

    <?php if ($history): ?>
        <?php foreach($history as $item):  ?>
            <?php $date = date("Y-m-d", strtotime($item->created_at)); ?>
            <?php if (!in_array($date, $date_storage)):  ?>
                <div class="time-label">
                    <span class="bg-green"><?= date("F d, Y", strtotime($item->created_at)); ?></span>
                </div>
            <?php endif; ?>
            <div>
                <!-- Before each timeline item corresponds to one icon on the left scale -->
                <i class="fas fa-truck bg-blue"></i>
                <!-- Timeline item -->
                <div class="timeline-item">
                    <!-- Time -->
                    <span class="time"><i class="fas fa-clock"></i><?= date("H:i:s", strtotime($item->created_at)); ?></span>
                    <!-- Header. Optional -->
                    <h3 class="timeline-header"><a href="#"><?= $item->status_name ?></a> </h3>
                    <!-- Body -->
                    <div class="timeline-body">
                        <?= $item->status_description; ?>
                    </div>
                    <!-- Placement of additional controls. Optional -->
                    <div class="timeline-footer">

                    </div>
                </div>
            </div>
            <?php  $date_storage[] = $date; ?>
        <?php endforeach; ?>
    <?php else: ?>
        <div class="time-label">
            <span class="bg-green"><?= date("F d, Y"); ?></span>
        </div>
        <div>
                <!-- Before each timeline item corresponds to one icon on the left scale -->
                <i class="fas fa-truck bg-blue"></i>
                <!-- Timeline item -->
                <div class="timeline-item">
                    <!-- Time -->
                    <span class="time"><i class="fas fa-clock"></i><?= date("H:i:s"); ?></span>
                    <!-- Header. Optional -->
                    <h3 class="timeline-header"><a href="#">Unavailable</a> --</h3>
                    <!-- Body -->
                    <div class="timeline-body">
                        No transaction found.
                    </div>
                    <!-- Placement of additional controls. Optional -->
                    <div class="timeline-footer">

                    </div>
                </div>
            </div>
    <?php endif; ?>
    
    <!-- The last icon means the story is complete -->
    <div>
        <i class="fas fa-clock bg-gray"></i>
    </div>
</div>
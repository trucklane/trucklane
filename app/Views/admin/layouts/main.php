<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Track and Track System</title>

    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <link rel="icon" type="image/png" href="<?=BASE?>admin/assets/img/crud_logo.png" />

    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?=BASE?>admin/assets/plugins/fontawesome-free/css/all.min.css">

    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    
    <!-- Tempusdominus Bbootstrap 4 -->
    <link rel="stylesheet" href="<?=BASE?>admin/assets/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">

    <!-- ladda -->
    <link href="<?=BASE?>admin/assets/plugins/ladda/css/ladda-themeless.min.css" rel="stylesheet">
    
    <!-- iCheck -->
    <!-- <link rel="stylesheet" href="<?=BASE?>admin/assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css"> -->
    
    <!-- JQVMap -->
    <!-- <link rel="stylesheet" href="<?=BASE?>admin/assets/plugins/jqvmap/jqvmap.min.css"> -->
    
    <!-- Toast -->
    <link href="<?=BASE?>admin/assets/plugins/toastr/toastr.min.css" rel="stylesheet">
    
    <!-- Theme style -->
    <link rel="stylesheet" href="<?=BASE?>admin/assets/css/adminlte.min.css">
    
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="<?=BASE?>admin/assets/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
    
    <!-- Daterange picker -->
    <!-- <link rel="stylesheet" href="<?=BASE?>admin/assets/plugins/daterangepicker/daterangepicker.css"> -->
    
    <!-- summernote -->
    <!-- <link rel="stylesheet" href="<?=BASE?>admin/assets/plugins/summernote/summernote-bs4.css"> -->
    
    <!-- Google Font: Source Sans Pro -->
    <!-- <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet"> -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700&family=Roboto&display=swap" rel="stylesheet">
    
    <!-- Select2 css -->
    <link rel="stylesheet" href="<?=BASE?>admin/assets/plugins/select2/css/select2.min.css">
    <link rel="stylesheet" href="<?=BASE?>admin/assets/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
    <link rel="stylesheet" href="<?=BASE?>admin/assets/css/main.css">
    <?php
        if(isset($load_css)) {
            foreach($load_css as $css_file) {
                echo "\n\t\t";
                if($css_file)
                    echo '<link rel="stylesheet" href="'. BASE . 'admin/assets/'.$css_file.'?v='.VERSION.'"> ';
            }
        }
    ?>

   
    
</head>

<body class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">

        <!-- Navbar -->
        <?= $this->include('components/topNav') ?>
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        <?=$this->include('components/rightNav') ?>
        
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
           
            <section class="content py-3">
                <div class="container-fluid">
                    <?= $this->include('components/alerts') ?>

                    <?= $this->renderSection('content') ?>
                </div><!-- /.container-fluid -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        <?= $this->include('components/footer') ?>

        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Control sidebar content goes here -->
        </aside>
        <!-- /.control-sidebar -->
    </div>
    <!-- ./wrapper -->
</body>

    <script> var baseUrl  = '<?=base_url()?>'; </script>

    <!-- jQuery -->
    <script src="<?=BASE?>admin/assets/plugins/jquery/jquery.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="<?=BASE?>admin/assets/plugins/jquery-ui/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
        $.widget.bridge('uibutton', $.ui.button)
    </script>
    
    <!-- Bootstrap 4 -->
    <script src="<?=BASE?>admin/assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>


    <!-- jquery-validation -->
    <script src="<?=BASE?>admin/assets/plugins/jquery-validation/jquery.validate.min.js"></script>
    <script src="<?=BASE?>admin/assets/plugins/jquery-validation/additional-methods.min.js"></script>
    
    <!-- ChartJS -->
    <!-- <script src="<?=BASE?>admin/assets/plugins/chart.js/Chart.min.js"></script> -->

    <!-- Sparkline -->
    <!-- <script src="<?=BASE?>admin/assets/plugins/sparklines/sparkline.js"></script> -->

    <!-- daterangepicker -->
    <!-- <script src="<?=BASE?>admin/assets/plugins/moment/moment.min.js"></script> -->
    <!-- <script src="<?=BASE?>admin/assets/plugins/daterangepicker/daterangepicker.js"></script> -->
    
    <!-- Select2 js -->
    <script src="<?=BASE?>admin/assets/plugins/select2/js/select2.full.min.js"></script>

    <!-- Ladda-->
    <script src="<?=BASE?>admin/assets/plugins/ladda/js/spin.min.js"></script>
    <script src="<?=BASE?>admin/assets/plugins/ladda/js/ladda.min.js"></script>
    
    <!-- Tempusdominus Bootstrap 4 -->
    <!-- <script src="<?=BASE?>admin/assets/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script> -->
    
    <!-- Summernote -->
    <!-- <script src="<?=BASE?>admin/assets/plugins/summernote/summernote-bs4.min.js"></script> -->
    
    <!-- overlayScrollbars -->
    <script src="<?=BASE?>admin/assets/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>

    <!-- Toast -->
    <script src="<?=BASE?>admin/assets/plugins/toastr/toastr.min.js"></script>
    
    <!-- Custom BS4 File Input  -->
    <!-- <script src="<?=BASE?>admin/assets/plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script> -->
    

    <!-- AdminLTE App -->
    <script src="<?=BASE?>admin/assets/js/adminlte.js"></script>
    <script src="<?=BASE?>admin/assets/js/global.js"></script>

    <?php
        if(isset($load_js)) {
            foreach($load_js as $js_file) {
                echo "\n\t";
                if($js_file)
                    echo '<script src="'. BASE . 'admin/assets/'.$js_file.'?v='.VERSION.'"></script>';
            }
        }
    ?>

</html>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-6">
    <h2><i class="fa fa-list"></i> Import Sea</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">Home</a>
            </li>
            <li class="breadcrumb-item">
                <span>Import Sea</span>
            </li>
            <li class="breadcrumb-item active">
                <strong>Edit</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-6">
        <h2></h2>
        <div class="form-group pull-right">
            <div class="form-group pull-right">
                <a href="/import_sea" class="btn btn-light"> Back to list</a>
                <button class="btn-save-update btn btn-primary mx-1" ><i class="fa fa-save"></i> Save</button>
                <button class="btn-post-update btn btn-warning mx-1" ><i class="fa fa-lock"></i> Post</button>
            </div>
        </div>
    </div>
</div>


<div class="wrapper wrapper-content animated white-bg ecommerce pb-5">
    <div class="row pb-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">

                    <form name="edit-entry-form" accept-charset="utf-8">
                        <input type="hidden" name="service-type" value="is" />
                        <input type="hidden" name="transaction-id" value="<?= $transaction->id; ?>">
                        <div class="card mb-3">
                            <div class="card-header">
                                Master Information
                            </div>
                            <div class="card-body">
                                <div class="form-row">
                                    <div class="form-group col-md-3">
                                        <div class="">
                                            <label for="">Master reference</label>
                                            <select class="form-control select-tags" name="master-ref">
                                                <option value="" disabled="disabled" selected="selected" class="hidden"></option>
                                                <?php foreach ($master_refs as $ref): ?>
                                                <option <?= ($ref->master_ref == $transaction->master_ref) ? "selected" : "" ?> value="<?= $ref->master_ref ?>"><?= $ref->master_ref ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-1">
                                        <label class="form-check-label" for="">Direct</label>
                                        <div class="form-check mt-2">
                                            <input type="checkbox" class="form-check-input" name="direct" id="direct" <?= $transaction->is_direct == 1 ? 'checked' : ''; ?>>
                                           
                                        </div>
                                    </div>
                                    <div class="form-group offset-md-2 col-md-6">
                                        <div class="">
                                            <label for="">Transaction date</label>
                                            <input autocomplete="off" type="text" class="form-control date" id="transaction_datetime" name="transaction-datetime" placeholder="" value="<?= date('m/d/Y g:i A', strtotime($transaction->transaction_datetime))?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for=" ">Estimate date of departure</label>
                                            <input autocomplete="off" type="text" class="form-control date" id="etd" name="etd" value="<?= date("m/d/Y", strtotime($transaction->etd)); ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for=" ">Estimate date of arrival</label>
                                            <input autocomplete="off" type="text" class="form-control date" id="eta" name="eta" value="<?= date("m/d/Y", strtotime($transaction->eta)); ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for=" ">Actual date of departure</label>
                                            <input autocomplete="off" type="text" class="form-control date" id="atd" name="atd" value="<?= date("m/d/Y", strtotime($transaction->atd)); ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for=" ">Actual date of arrival</label>
                                            <input autocomplete="off" type="text" class="form-control date" id="ata" name="ata" value="<?= date("m/d/Y", strtotime($transaction->ata)); ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for=" ">Country of origin</label>
                                            <select class="form-control form-select" name="origin-country" >
                                                <option value="" disabled="disabled" selected="selected" class="hidden"></option>
                                                <?php foreach ($countries as $country): ?>
                                                    <option <?= ($country->country_code == $transaction->country_of_origin) ? "selected" : "" ?> value="<?= $country->country_code; ?>"><?= $country->country_code." - ".$country->country_name; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Port of origin</label>
                                            <select class="form-control form-select disabled" name="origin-port" <?= (!$ports_of_origin) ? 'disabled="disabled"' : '' ?>>
                                            <?php if ($ports_of_origin): ?>
                                                <?php foreach ($ports_of_origin as $port): ?>
                                                    <option <?= ($transaction->port_of_origin == $port->port_code) ? "selected" : "" ?>  value="<?= $port->port_code; ?>"><?= $port->port_code." - ".$port->port_name; ?></option>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group ">
                                            <label for="">Country of destination</label>
                                            <select class="form-control form-select" name="destination-country" >
                                                <option value="" disabled="disabled" selected="selected" class="hidden"></option>
                                                <?php foreach ($countries as $country): ?>
                                                    <option <?= ($country->country_code == $transaction->country_of_destination) ? "selected" : "" ?> value="<?= $country->country_code; ?>"><?= $country->country_code." - ".$country->country_name; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Port of destination</label>
                                            <select class="form-control form-select disabled" name="destination-port" <?= (!$ports_of_destination) ? 'disabled="disabled"' : '' ?>>
                                            <?php if ($ports_of_destination): ?>
                                            <?php foreach ($ports_of_destination as $port): ?>
                                                    <option <?= ($transaction->port_of_destination == $port->port_code) ? "selected" : "" ?>  value="<?= $port->port_code; ?>"><?= $port->port_code." - ".$port->port_name; ?></option>
                                            <?php endforeach; ?>
                                            <?php endif; ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Vessel</label>
                                            <input type="text" class="form-control " id="vessel_voyage" name="vessel-voyage" placeholder="" value="<?= $transaction->vessel ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Carriers</label>
                                            <select class="form-control form-select" name="carriers" >
                                                <option value="" disabled="disabled" selected="selected" class="hidden"></option>
                                                <?php foreach ($carriers as $carrier): ?>
                                                    <option <?= ($transaction->carrier == $carrier->carrier_code) ? "selected" : "" ?> value="<?= $carrier->carrier_code ?>"><?= "{$carrier->carrier_code} - {$carrier->carrier_name}" ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Total gross weight</label>
                                            <input type="number" class="form-control " name="master-gross-wt" placeholder="" value="<?= $transaction->total_gross_wt; ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Total chargeable weight</label>
                                            <input type="number" class="form-control " name="master-chargeable-weight" placeholder="" value="<?= $transaction->total_chargeable_wt; ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card my-3">
                            <div class="card-header">
                            House Information
                            </div>   
                            <div class="card-body">
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <div class="">
                                            <label for=" ">House reference</label>
                                            <input name="house-ref" type="text" class="form-control" id="house_ref" placeholder="" value="<?= $house->house_ref; ?>">
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for=" ">Shipper</label>
                                        <select class="select-shipper form-control form-select" name="shipper" >
                                            <option value="" disabled="disabled" selected="selected" class="hidden"></option>
                                            <?php foreach ($shippers as $shipper): ?>
                                                <option <?= ($house->shipper_code == $shipper->partner_code) ? "selected" : "" ?> value="<?= $shipper->partner_code ?>"><?= "{$shipper->partner_code} - {$shipper->partner_name}" ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                        <input disabled="disabled" name="shippers_obj" type="hidden" value='<?= json_encode($shippers, JSON_HEX_QUOT | JSON_HEX_APOS) ?>' />
                                        <textarea disabled="disabled" class="disabled form-control mt-3" name="shipper-address" id="shipper_address" rows="4"></textarea>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for=" ">Consignee</label>
                                        <select class="select-consignee form-control  form-select" name="consignee">
                                            <option value="" disabled="disabled" selected="selected" class="hidden"></option>
                                            <?php foreach ($consignees as $consignee): ?>
                                                <option <?= ($house->consignee_code == $consignee->partner_code) ? "selected" : "" ?> value="<?= $consignee->partner_code ?>"><?= "{$consignee->partner_code} - {$consignee->partner_name}" ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                        <input disabled="disabled" name="consignee_obj" type="hidden" value='<?= json_encode($consignees, JSON_HEX_QUOT | JSON_HEX_APOS) ?>' />
                                        <textarea disabled="disabled" class="disabled form-control mt-3" name="consignee-address" id="consignee"  rows="4"></textarea>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for=" ">Agent</label>
                                        <select class="select-agent form-control  form-select" name="agent" >
                                                <option value="" disabled="disabled" selected="selected" class="hidden"></option>
                                            <?php foreach ($agents as $agent): ?>
                                                <option  <?= ($house->agent_code == $agent->partner_code) ? "selected" : "" ?> value="<?= $agent->partner_code ?>"><?= "{$agent->partner_code} - {$agent->partner_name}" ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                        <input disabled="disabled" name="agent_obj" type="hidden" value='<?= json_encode($agents, JSON_HEX_QUOT | JSON_HEX_APOS) ?>' />
                                        <textarea disabled="disabled" class="disabled form-control mt-3" name="agent-address" id="agent_address"  rows="4"></textarea>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for=" ">Notify party</label>
                                        <select class="select-agent-notify form-control  form-select" name="agent_notify" >
                                            <option value="" disabled="disabled" selected="selected" class="hidden"></option>
                                            <?php foreach ($agents as $agent): ?>
                                                <option <?= ($house->agent_notify_1 == $agent->partner_code) ? "selected" : "" ?> value="<?= $agent->partner_code ?>"><?= "{$agent->partner_code} - {$agent->partner_name}" ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                        <textarea disabled="disabled" class="disabled form-control mt-3" name="notify-address" id="notify_consignee"  rows="4"></textarea>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for=" ">Third party</label>
                                        <select class="select-agent-third form-control  form-select" name="agent_third_party" >
                                            <option value="" disabled="disabled" selected="selected" class="hidden"></option>
                                            <?php foreach ($agents as $agent): ?>
                                                <option <?= ($house->agent_notify_2 == $agent->partner_code) ? "selected" : "" ?> value="<?= $agent->partner_code ?>"><?= "{$agent->partner_code} - {$agent->partner_name}" ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                        <textarea disabled="disabled" class="disabled form-control mt-3" name="third-party-address" id="third_party_address"  rows="4"></textarea>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-3">
                                        <label for="">Gross weight</label>
                                        <input type="number" class="form-control " name="house-gross-wt" placeholder="" value="<?= $house->gross_wt; ?>">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="">Chargeable weight</label>
                                        <input type="number" class="form-control "  name="house-chargeable-weight" placeholder="" value="<?= $house->chargeable_wt; ?>">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="">Uom</label>
                                        <input type="text" class="form-control " id="uom" name="uom" placeholder="" value="<?= $house->uom; ?>">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="">Inco term</label>
                                        <select class="form-control form-select" name="inco-term" >
                                            <option value="" disabled="disabled" selected="selected" class="hidden"></option>
                                            <?php foreach ($incoterms as $incoterm): ?>
                                                <option <?= ($house->inco_term == $incoterm->incoterm_code) ? "selected" : "" ?> value="<?= $incoterm->incoterm_code ?>"><?= "{$incoterm->incoterm_code} - {$incoterm->incoterm_desc}" ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="">Forwarder</label>
                                        <input type="text" class="form-control " id="forwarder" name="forwarder" placeholder="" value="<?= $house->uom; ?>">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="">Mode of shipment</label>
                                        <select class="form-control form-select disabled" name="mode-of-shipment-show" disabled="disabled">
                                            <option value="SEA" selected="selected">SEA</option>
                                        </select>
                                        <input type="hidden" class="form-control" name="mode-of-shipment" placeholder="" value="SEA">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="">Supp invoice number</label>
                                        <input type="text" class="form-control " id="supp_invoice_number" name="supp-invoice-number" placeholder="" value="<?= $house->supp_invoice_number; ?>">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="">Invoice value</label>
                                        <input type="text" class="form-control " id="invoice_value" name="invoice-value" placeholder="" value="<?= $house->invoice_value; ?>">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="">Invoice currency</label>
                                        <input type="text" class="form-control " id="invoice_currency" name="invoice-currency" placeholder="" value="<?= $house->invoice_currency; ?>">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="">Import entry no</label>
                                        <input type="text" class="form-control " id="import_entry_no" name="import-entry-no" placeholder="" value="<?= $house->import_entry_no; ?>">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="">Load type</label>
                                        <select class="form-control form-select" name="load-type" >
                                            <option value="" disabled="disabled" selected="selected" class="hidden"></option>
                                            <?php foreach ($load_types as $load_type): ?>
                                                <option <?= ($house->load_type == $load_type->load_type_code) ? "selected" : "" ?> value="<?= $load_type->load_type_code ?>"><?= "{$load_type->load_type_code} - {$load_type->load_type_desc}" ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col">
                                        <label for="">General description</label>
                                        <textarea class="form-control" id="general_description" name="general-description"  rows="4"><?= $house->general_desc; ?></textarea>
                                    </div>      
                                </div> 
                            </div>
                        </div>

                        <div class="card my-3">
                            <div class="card-header">
                                
                            </div>  
                            <div class="card-body">
                                <ul class="nav nav-tabs" id="transaction_tabs" role="tablist">
                                    <li class="nav-item" role="presentation">
                                        <a class="nav-link active" id="material_tab" data-toggle="tab" href="#material_content" role="tab" aria-controls="material_content" aria-selected="true">PO Material</a>
                                    </li>
                                    <li class="nav-item" role="presentation">
                                        <a class="nav-link" id="container_tab" data-toggle="tab" href="#container_content" role="tab" aria-controls="container_content" aria-selected="false">Container list</a>
                                    </li>
                                    <li class="nav-item" role="presentation">
                                        <a class="nav-link" id="cargo_tab" data-toggle="tab" href="#cargo_content" role="tab" aria-controls="cargo_content" aria-selected="false">Loose cargo</a>
                                    </li>
                                    <li class="nav-item" role="presentation">
                                        <a class="nav-link " id="charges_tab" data-toggle="tab" href="#charges_content" role="tab" aria-controls="charges_content" aria-selected="false">Charges</a>
                                    </li>
                                </ul>
                                <div class="tab-content" id="transaction_tab_content">
                                    <div class="tab-pane fade show active" id="material_content" role="tabpanel" aria-labelledby="material_tab">
                                        <?= $this->include('admin/transaction/material_tbl') ?>
                                    </div>
                                    <div class="tab-pane fade " id="container_content" role="tabpanel" aria-labelledby="container_tab">
                                        <?= $this->include('admin/transaction/container_tbl') ?>
                                    </div>
                                    <div class="tab-pane fade" id="cargo_content" role="tabpanel" aria-labelledby="cargo_tab">
                                        <?= $this->include('admin/transaction/cargo_tbl') ?>
                                    </div>
                                    <div class="tab-pane fade " id="charges_content" role="tabpanel" aria-labelledby="charges_tab">
                                        <?= $this->include('admin/transaction/charge_tbl') ?>
                                    </div>
                                </div>
                            </div>  
                        </div>  

                        <div class="card my-3">
                            <div class="card-header">
                                
                            </div> 
                            <div class="card-body">
                            <?= $this->include('admin/transaction/transaction_history') ?>
                            </div> 
                        </div> 
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<?= $this->include('admin/transaction/charge_mdl') ?>
<?= $this->include('admin/transaction/charge_mdl_edit') ?>
<?= $this->include('admin/transaction/material_mdl') ?>
<?= $this->include('admin/transaction/material_mdl_edit') ?>
<?= $this->include('admin/transaction/cargo_mdl') ?>
<?= $this->include('admin/transaction/cargo_mdl_edit') ?>
<?= $this->include('admin/transaction/container_mdl') ?>
<?= $this->include('admin/transaction/container_mdl_edit') ?>
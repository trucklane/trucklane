<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-6">
    <h2><i class="fa fa-list"></i> Export Air</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">Home</a>
            </li>
            <li class="breadcrumb-item">
                <span>Export Air</span>
            </li>
            <li class="breadcrumb-item active">
                <strong>List</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-6">
        <h2></h2>
        <div class="form-group pull-right">
            <a href="/export_air/entry" class="btn btn-primary"><i class="fa fa-plus"></i> Add entry</a>
        </div>
    </div>
</div>
<br/>
<div class="wrapper wrapper-content animated white-bg ecommerce">
    <div class="row">
        <div class="col-lg-12">
            <div class="table-responsive">
               
                <table style="width: 100% !important; " class="table table-bordered table-hover" id="ea_list">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Master Ref</th>
                            <th>House Ref</th>
                            <th>Status</th>
                            <th>ETD</th>
                            <th>ETA</th>
                            <th>ATD</th>
                            <th>ATA</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>        
        </div>
    </div>
</div>


<div class="modal fade" id="trans_del_modal" data-keyboard="false" data-backdrop="static" tabindex="-1"  aria-hidden="true" role="dialog" >
    <div class="modal-dialog ">
        <div class="modal-content animated fadeIn">
            <div class="modal-header">
                <div class="col-md-9 "><h3></h3></div>
                <div class="col-md-3 "> <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> </div>
            </div>
            <div class="modal-body">
                <p class="text-center">Are you sure you want to delete this transaction?</p>
            </div>
            <input type="hidden" name="del-trans-data" value="" />
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-danger btn-del-traxn">Delete</button>
            </div>
        </div>
    </div>
</div>

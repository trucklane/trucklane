<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-6">
    <h2><i class="fa fa-list"></i> View</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">Home</a>
            </li>
            <li class="breadcrumb-item">
                <span>Transaction</span>
            </li>
            <li class="breadcrumb-item active">
                <strong>View</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-6">
        <h2></h2>
        <div class="form-group pull-right">
            <div class="form-group pull-right">
                <a href="/export_brokerage" class="btn btn-light"> Back to list</a>
                <?php if ($transaction->status == "S"): ?>
                <a href="/transaction/<?= $transaction->id ?>/edit" class="btn btn-warning"><i class="fa fa-pencil"></i> Edit</a>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

<div class="wrapper wrapper-content animated white-bg ecommerce pb-5">
    <div class="row pb-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">

                    <form name="entry-form" accept-charset="utf-8">
                        <input type="hidden" name="service-type" value="is" />
                        <div class="card mb-3">
                            <div class="card-header">
                                Master Information
                            </div>
                            <div class="card-body">
                                <div class="form-row">
                                    <div class="form-group col-md-3">
                                        <div class="">
                                            <label for="">Master reference</label>
                                            <input disabled="disabled" type="text" readonly class="form-control-plaintext" id="static_master_ref" value="<?= $transaction->master_ref; ?>">
                                        </div>
                                    </div>
                                    <div class="form-group col-md-1">
                                        <label class="form-check-label" for="">Direct</label>
                                        <div class="form-check mt-2">
                                            <input type="checkbox" class="form-check-input" name="direct" id="direct" <?= $transaction->is_direct == 1 ? 'checked' : ''; ?>>
                                        </div>
                                    </div>
                                    <div class="form-group offset-md-2 col-md-6">
                                        <div class="">
                                            <label for="">Transaction date</label>
                                            <input autocomplete="off" type="text" class="form-control-plaintext" id="transaction_datetime" name="transaction-datetime" placeholder="" value="<?= date('m/d/Y g:i A', strtotime($transaction->transaction_datetime))?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for=" ">Estimate date of departure</label>
                                            <input disabled="disabled" type="text" readonly class="form-control-plaintext" id="etd" value="<?= date("F d, Y ", strtotime($transaction->etd)); ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for=" ">Estimate date of arrival</label>
                                            <input disabled="disabled" type="text" readonly class="form-control-plaintext" id="eta" value="<?= date("F d, Y ", strtotime($transaction->eta)); ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for=" ">Actual date of departure</label>
                                            <input disabled="disabled" type="text" readonly class="form-control-plaintext" id="atd" value="<?= date("F d, Y ", strtotime($transaction->atd)); ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for=" ">Actual date of arrival</label>
                                            <input disabled="disabled" type="text" readonly class="form-control-plaintext" id="ata" value="<?= date("F d, Y ", strtotime($transaction->ata)); ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for=" ">Country of origin</label>
                                            <?php 
                                                $coo = "";
                                                if (isset($country_of_origin)) {
                                                    $coo = $country_of_origin->country_code." - ".$country_of_origin->country_name; 
                                                } 
                                            ?>
                                            <input disabled="disabled" type="text" readonly class="form-control-plaintext" id="country_of_origin" value="<?= $coo; ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Port of origin</label>
                                            <?php 
                                                $poo = "";
                                                if (isset($port_of_origin)) {
                                                    $poo = $port_of_origin->port_code." - ".$port_of_origin->port_name; 
                                                } 
                                            ?>
                                            <input disabled="disabled" type="text" readonly class="form-control-plaintext" id="port_of_origin" value="<?= $poo; ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group ">
                                            <label for="">Country of destination</label>
                                            <?php 
                                                $cod = "";
                                                if (isset($country_of_destination)) {
                                                    $cod = $country_of_destination->country_code." - ".$country_of_destination->country_name; 
                                                } 
                                            ?>
                                            <input disabled="disabled" type="text" readonly class="form-control-plaintext" id="country_of_destination" value="<?= $cod; ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Port of destination</label>
                                            <?php 
                                                $pod = "";
                                                if (isset($port_of_destination)) {
                                                    $pod = $port_of_destination->port_code." - ".$port_of_destination->port_name; 
                                                } 
                                            ?>
                                            <input disabled="disabled" type="text" readonly class="form-control-plaintext" id="port_of_destination" value="<?= $pod; ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Vessel</label>
                                            <input disabled="disabled" type="text" readonly class="form-control-plaintext" id="vessel_voyage" value="<?= $transaction->vessel; ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Carriers</label>
                                            <?php 
                                                $t_carrier = "";
                                                if (isset($carrier)) {
                                                    $t_carrier = $carrier->carrier_code." - ".$carrier->carrier_name; 
                                                } 
                                            ?>
                                            <input disabled="disabled" type="text" readonly class="form-control-plaintext" id="carrier" value="<?= $t_carrier; ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Total gross weight</label>
                                            <input disabled="disabled" type="text" readonly class="form-control-plaintext" id="master_gross_wt" value="<?= $transaction->total_gross_wt; ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Total chargeable weight</label>
                                            <input disabled="disabled" type="text" readonly class="form-control-plaintext" id="master_chargeable_wt" value="<?= $transaction->total_chargeable_wt; ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card my-3">
                            <div class="card-header">
                            House Information
                            </div>   
                            <div class="card-body">
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <div class="">
                                            <label for=" ">House reference</label>
                                            <input disabled="disabled" type="text" readonly class="form-control-plaintext" id="static_house_ref" value="<?= $house->house_ref; ?>">
                                        </div>
                                        <div class="">
                                            <label for=" ">Current Status</label>
                                            <?php 
                                                $t_current_status = "";
                                                if (isset($current_status)) {
                                                    $t_current_status = "{$current_status->status_name} - {$current_status->status_description}";
                                                }
                                            ?>  
                                            <input disabled="disabled" type="text" readonly class="form-control-plaintext" id="static_current_status" value="<?= $t_current_status; ?>">
                                        </div>
                                        <div class="">
                                            <label for=" ">Created by</label>
                                            <?php if (isset($created_by->first_name) && isset($created_by->last_name)): ?>
                                            <input disabled="disabled" type="text" readonly class="form-control-plaintext" id="static_created_by" value="<?= $created_by->first_name . " " .$created_by->last_name ; ?>">
                                            <?php else: ?>
                                            <input disabled="disabled" type="text" readonly class="form-control-plaintext" id="static_created_by" value="">
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for=" ">Shipper</label>
                                        <input disabled="disabled" type="text" readonly class="form-control-plaintext" id="shipper" value="<?= isset($shipper) ? $shipper : "" ?>">
                                        <textarea disabled="disabled" class="disabled form-control mt-3" name="shipper-address" id="shipper_address" rows="4"><?= isset($shipper_address) ? $shipper_address : "" ?></textarea>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for=" ">Consignee</label>
                                        <input disabled="disabled" type="text" readonly class="form-control-plaintext" id="consignee" value="<?= isset($consignee) ? $consignee : "" ?>">
                                        <textarea disabled="disabled" class="disabled form-control mt-3" name="consignee-address" id="consignee" rows="4"><?= isset($consignee_address) ? $consignee_address : "" ?></textarea>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for=" ">Agent</label>
                                        <input disabled="disabled" type="text" readonly class="form-control-plaintext" id="agent" value="<?= isset($agent) ? $agent : "" ?>">
                                        <textarea disabled="disabled" class="disabled form-control mt-3" name="agent-address" id="agent_address"  rows="4"><?= isset($agent_address) ? $agent_address : "" ?></textarea>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for=" ">Notify party</label>
                                        <input disabled="disabled" type="text" readonly class="form-control-plaintext" id="agent_1" value="<?= isset($agent_1) ? $agent_1 : "" ?>">
                                        <textarea disabled="disabled" class="disabled form-control mt-3" name="notify-address" id="notify_consignee"  rows="4"><?= isset($agent_1_address) ? $agent_1_address : "" ?></textarea>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for=" ">Third party</label>
                                        <input disabled="disabled" type="text" readonly class="form-control-plaintext" id="agent_2" value="<?= isset($agent_2) ? $agent_2 : "" ?>">
                                        <textarea disabled="disabled" class="disabled form-control mt-3" name="third-party-address" id="third_party_address"  rows="4"><?= isset($agent_2_address) ? $agent_2_address : "" ?></textarea>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-3">
                                        <label for="">Gross weight</label>
                                        <input disabled="disabled" type="text" readonly class="form-control-plaintext" id="static_gross_wt" value="<?= $house->gross_wt; ?>">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="">Chargeable weight</label>
                                        <input disabled="disabled" type="text" readonly class="form-control-plaintext" id="static_chargeable_wt" value="<?= $house->chargeable_wt; ?>">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="">Uom</label>
                                        <input disabled="disabled" type="text" readonly class="form-control-plaintext" id="static_uom" value="<?= $house->uom; ?>">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="">Inco term</label>
                                        <?php 
                                            $t_inco_term = "";
                                            if (isset($inco_term)) {
                                                $t_inco_term = $inco_term->incoterm_code." - ".$inco_term->incoterm_desc; 
                                            } 
                                        ?>
                                        <input disabled="disabled" type="text" readonly class="form-control-plaintext" id="inco_term" value="<?= $t_inco_term; ?>">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="">Forwarder</label>
                                        <input disabled="disabled" type="text" readonly class="form-control-plaintext" id="static_forwarder" value="<?= $house->forwarder; ?>">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="">Mode of shipment</label>
                                        <input disabled="disabled" type="text" readonly class="form-control-plaintext" id="static_mode_of_shpt" value="<?= $house->mode_of_shpt; ?>">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="">Supp invoice number</label>
                                        <input disabled="disabled" type="text" readonly class="form-control-plaintext" id="static_supp_invoice_number" value="<?= $house->supp_invoice_number; ?>">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="">Invoice value</label>
                                        <input disabled="disabled" type="text" readonly class="form-control-plaintext" id="static_invoice_value" value="<?= $house->invoice_value; ?>">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="">Invoice currency</label>
                                        <input disabled="disabled" type="text" readonly class="form-control-plaintext" id="static_invoice_currency" value="<?= $house->invoice_currency; ?>">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="">Import entry no</label>
                                        <input disabled="disabled" type="text" readonly class="form-control-plaintext" id="static_import_entry_no" value="<?= $house->import_entry_no; ?>">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="">No of containers</label>
                                        <input type="text" class="form-control-plaintext " id="static_no_of_containers" name="no-of-containers" value="<?= $house->no_of_containers; ?>">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="">Load type</label>
                                        <?php 
                                            $t_load_type = "";
                                            if (isset($load_type)) {
                                                $t_load_type = $load_type->load_type_code." - ".$load_type->load_type_desc; 
                                            } 
                                        ?>
                                        <input disabled="disabled" type="text" readonly class="form-control-plaintext" id="static_load_type" value="<?= $t_load_type; ?>">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="">NTA ICC Number</label>
                                        <input type="text" class="form-control-plaintext " id="nta_icc_no" name="nta-icc-no" placeholder="" value="<?= $house->nta_icc_number; ?>">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="">PEZA Permit</label>
                                        <input type="text" class="form-control-plaintext " id="peza_permit" name="peza-permit" placeholder="" value="<?= $house->peza_permit; ?>">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="">Custom value</label>
                                        <input type="number" class="form-control-plaintext " id="custom_value" name="custom-value" placeholder="" value="<?= $house->custom_value; ?>">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="">Custom Duties</label>
                                        <input type="number" class="form-control-plaintext " id="custom_duties" name="custom-duties" placeholder="" value="<?= $house->custom_duties; ?>">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="">VAT value</label>
                                        <input type="number" class="form-control-plaintext " id="vat_value" name="vat-value" placeholder="" value="<?= $house->vat_value; ?>">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="">Date customs cleared</label>
                                        <input autocomplete="off" type="text" class="form-control-plaintext date" id="date_customs_cleared" name="date-customs-cleared" placeholder="" value="<?= date('m/d/Y g:i A', strtotime($house->date_customs_cleared)); ?>">
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col">
                                        <label for="">General description</label>
                                        <textarea class="form-control" id="general_description" name="general-description"  rows="4"><?= $house->general_desc; ?></textarea>
                                    </div>      
                                </div> 
                            </div>
                        </div>

                        <div class="card my-3">
                            <div class="card-header">
                                
                            </div>  
                            <div class="card-body">
                                <ul class="nav nav-tabs" id="transaction_tabs" role="tablist">
                                    <li class="nav-item" role="presentation">
                                        <a class="nav-link active" id="charges_tab" data-toggle="tab" href="#charges_content" role="tab" aria-controls="charges_content" aria-selected="false">Charges</a>
                                    </li>
                                </ul>
                                <div class="tab-content" id="transaction_tab_content">
                                    
                                    <div class="mt-3 tab-pane fade show active" id="charges_content" role="tabpanel" aria-labelledby="charges_tab">
                                        <table class="table table-bordered table-hover" id="trxn_chrg">
                                            <thead class="thead-light">
                                                <tr>
                                                    <th>Charge code</th>
                                                    <th>Charge description</th>
                                                    <th>Charge amount</th>
                                                    <th>Currency</th>
                                                    <th>Php value</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php if (isset($charges) && $charges): ?>
                                                <?php foreach ($charges as $charge):?>
                                                    <tr>
                                                        <td><?= $charge->chg_code; ?></td>
                                                        <td><?= $charge->chg_desc; ?></td>
                                                        <td><?= $charge->chg_amount; ?></td>
                                                        <td><?= $charge->currency; ?></td>
                                                        <td><?= $charge->php_value; ?></td>
                                                    </tr>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>  
                        </div>  

                        <div class="card my-3">
                            <div class="card-header">
                                
                            </div> 
                            <div class="card-body">
                                <?= $this->include('admin/transaction/transaction_history') ?>
                            </div> 
                        </div> 
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
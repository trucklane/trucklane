<?php
    $user_id = isset($user->id)?$user->id:'0';
    $first_name = isset($user->first_name)?$user->first_name:'';
    $last_name = isset($user->last_name)?$user->last_name:'';
    $company = isset($user->company)?$user->company:'';
    $active = isset($user->active)?$user->active:'0';
    $email = isset($user->email)?$user->email:'';
    $username = isset($user->username)?$user->username:'';
    $position = isset($user->position)?$user->position:'';
    $department = isset($user->department)?$user->department:'';



?>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-6">
    <h2><i class="fa fa-user"></i> <?=($user_id)?'Edit ':'Create New'?> User</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">Home</a>
            </li>
            <li class="breadcrumb-item">
                <span>Settings</span>
            </li>
            <li class="breadcrumb-item active">
                <strong>User</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-6">
        <h2></h2>
        <div class="form-group pull-right">
        <button class="btn btn-primary btn-user-save" type="button"><i class="fa fa-save"></i> Save</button>
            <button class="btn btn-default btn-user-back" type="button"><i class="fa fa-mail-reply"></i> Back</button>
        </div>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeIn">
    <form class="m-t" role="form" id="user-form" name="user-form" data-toggle="validator">  
    <div class="row">
        <div class="col-lg-12">
            <div class="tabs-container">
                <div class="tabs">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a class="nav-link" data-toggle="tab" href="#tab-1"> <i class="fa fa-user"></i> Personal Details</a></li>
                        <li><a class="nav-link" data-toggle="tab" href="#tab-2"><i class="fa fa-desktop"></i> Login Details</a></li>
                        <li><a class="nav-link" data-toggle="tab" href="#tab-3">User Access</a></li>
                        <li><a class="nav-link" data-toggle="tab" href="#tab-4">User Permission</a></li>
                    </ul>
                    <div class="tab-content">
                        <div role="tabpanel" id="tab-1" class="tab-pane active">
                            <div class="panel-body">
                                <fieldset class="form-horizontal">

                                    <div class="form-group row">
                                        <label class="col-sm-2 control-label">ID:</label>
                                        <div class="col-sm-10">
                                            <input type="text"  readonly class="form-control" id="user_id" name="user_id" placeholder="User Id" required="" value="<?=$user_id?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-2 control-label">First Name:<span class="asterisk">*</span></label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" placeholder="First Name" name="first_name" id="first_name" required="" value="<?=$first_name?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-2 control-label">Last Name:<span class="asterisk">*</span></label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" placeholder="Last Name" name="last_name" id="last_name" required="" value="<?=$last_name?>">
                                        </div>
                                    </div>
                                    
                                    <div class="form-group row">
                                        <label class="col-sm-2 control-label ">Email:<span class="asterisk">*</span></label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" placeholder="Email" name="email" id="email" required="" value="<?=$email?>">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-sm-2 control-label">Is Active:</label>
                                        <div class="col-sm-10">
                                            <select class="form-control" name="is_enabled" name="is_enabled" required="">
                                                <option <?=($active == 1)?'selected':''?> value="1">Yes</option>
                                                <option <?=($active == 0)?'selected':''?> value="0">No</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-2 control-label">Company:<span class="asterisk">*</span></label>
                                        <div class="col-sm-10">
                                            <input type="text" id="company"  name="company" required class="form-control" placeholder="Company" value="<?=$company?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-2 control-label">Department:<span class="asterisk">*</span></label>
                                        <div class="col-sm-10">
                                            <input type="text" id="department"  name="department" required class="form-control" placeholder="Department" value="<?=$department?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-2 control-label">Position<span class="asterisk">*</span></label>
                                        <div class="col-sm-10">
                                            <input type="text" id="position"  name="position" required class="form-control" placeholder="Position" value="<?=$position?>">
                                        </div>
                                    </div>
                                </fieldset>
                            
                            </div>
                        </div>
                        <div role="tabpanel" id="tab-2" class="tab-pane">
                            <div class="panel-body">
                                
                                <div class="form-group col-sm-12">
                                        <label class="control-label">Username<span class="asterisk">*</span></label>
                                        <input type="text" id="username" name="username" class="form-control" placeholder="Username" <?=($user_id)?'readOnly':''?> value="<?=$username?>">
                                </div>
                                <div class="form-group col-sm-12">
                                <label> <input type="checkbox" name="editPassword" id="editPassword" class=""> Edit Password </label>
                                    
                                
                                </div>
                                <div id="update-password" class="<?=($user_id) ? 'hide':'';?>">
                                    <div class="form-group col-sm-6 ">
                                        <label class="control-label">New password<span class="asterisk">*</span></label>
                                        <input type="password" name="password" id="password" class="form-control password" placeholder="Change Password">
                                    </div>
                                    

                                    <div class="form-group col-sm-6">
                                        <label class="control-label">Re-enter Password<span class="asterisk">*</span></label>
                                        <input type="password" name="confirmPassword" class="form-control"  id="confirmPassword" placeholder="Re-enter Password">
                                    </div>
                                    
                                    
                                    <div class="form-group col-sm-12">
                                        <div class="pwstrength_viewport_progress2"></div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div role="tabpanel" id="tab-3" class="tab-pane">
                            <div class="panel-body">
                                <fieldset class="form-horizontal">
                                    <table id="useraccess" class="table table-striped table-bordered table-hover" >
                                        <tr>
                                            <td>Module Name</td>
                                            <td align='center'>Add</td>
                                            <td align='center'>View</td>
                                            <td align='center'>Post</td>
                                            <td align='center'>Unpost</td>
                                            <td align='center'>Approve</td>
                                            <td align='center'>Delete</td>
                                            <td align='center'>Cancel</td>
                                            <td align='center'>Back Load</td>
                                        </tr>
                                  
                                       
                                    <?php if($modules) : 
                                            foreach ($modules as $key => $module) {
                                              
                                                echo "<tr>";
                                                echo "<td>".$module->display_name."</td>";

                                                $add = check_access($useraccess, $module->module_name, 'add');
                                                $add_selected = ($add == 1) ? "checked" : '';
                                                echo "<td align='center'> <div class='i-checks'><label> <input type='checkbox' name='attr_".$module->module_id."_addx' $add_selected ></label></div></td>";

                                                $view = check_access($useraccess, $module->module_name, 'view');
                                                $view_selected = ($view == 1) ? "checked" : '';

                                                echo "<td align='center'><div class='i-checks'><label><input type='checkbox' name='attr_".$module->module_id."_viewx' $view_selected ></label></div></td>";

                                                $post = check_access($useraccess, $module->module_name, 'post');
                                                $post_selected = ($post == 1) ? "checked" : '';

                                                echo "<td align='center'><div class='i-checks'><label><input type='checkbox' name='attr_".$module->module_id."_postx' $post_selected></label></div></td>";

                                                $unpost = check_access($useraccess, $module->module_name, 'unpost');
                                                $unpost_selected = ($unpost == 1) ? "checked" : '';
                                                echo "<td align='center'><div class='i-checks'><label><input type='checkbox' name='attr_".$module->module_id."_unpostx' $unpost_selected  ></label></div></td>";

                                                $approve = check_access($useraccess, $module->module_name, 'approve');
                                                $approve_selected = ($approve == 1) ? "checked" : '';
                                                echo "<td align='center'><div class='i-checks'><label><input type='checkbox' name='attr_".$module->module_id."_approvex' $approve_selected   ></label></div></td>";
                                                
                                                $delete = check_access($useraccess, $module->module_name, 'delete');
                                                $delete_selected = ($delete == 1) ? "checked" : '';
                                                echo "<td align='center'><div class='i-checks'><label><input type='checkbox' name='attr_".$module->module_id."_deletex' $delete_selected   ></label></div></td>";

                                                $cancel = check_access($useraccess, $module->module_name, 'cancel');
                                                $cancel_selected = ($cancel == 1) ? "checked" : '';
                                                echo "<td align='center'><div class='i-checks'><label><input type='checkbox' name='attr_".$module->module_id."_cancelx' $cancel_selected  ></label></div></td>";

                                                $back_load = check_access($useraccess, $module->module_name, 'backload');
                                                $back_load_selected = ($back_load == 1) ? "checked" : '';
                                                echo "<td align='center'><div class='i-checks'><label><input type='checkbox' name='attr_".$module->module_id."_backloadx' $back_load_selected  ></label></div></td>";

                                                echo "</tr>";
                                            }
                                        endif;
                                    ?>
                                    </table>
                                </fieldset>
                            </div>
                        </div>
                        
                        <div role="tabpanel" id="tab-4" class="tab-pane">
                            <div class="panel-body" id="pwd-container2">
                                <?php 
                                    if($user_permissions) :
                                        foreach ($user_permissions as $key => $permission_name) :
                                            # code...
                                            foreach ($permission_name as $name => $permission_list) :
                                            ?>
                                            <div class="col-sm-12">
                                                <div class="ibox ibox-border collapsed">
                                                    <div class="ibox-title collapse-link">
                                                        <h5><?=$name?></h5>
                                                        <div class="ibox-tools"><i class="fa fa-chevron-up"></i></div>
                                                    </div>
                                                    <div class="ibox-content" style="">
                                                        <ul>
                                                        <?php foreach ($permission_list as $key => $sub_memu) : ?>
                                                            <?php $user_permission = check_permission($user_access_permissions, $sub_memu['mm_id'], $sub_memu['ms_id']);
                                                                $permission_selected = ($user_permission == 1) ? "checked" : ''; 
                                                            ?>
                                                            <div class="i-checks"><label> <input type="checkbox" <?=$permission_selected?> name="permission_<?=$sub_memu['mm_id']?>_<?=$sub_memu['ms_id']?>" > <i></i> <?=$sub_memu['menu_name']?> </label></div>
                                                        <?php endforeach;  ?>
                                                    </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php endforeach; 
                                        endforeach; 
                                    endif; 
                                ?>
                            </div>
                        </div>

                    </div>
                </div>


            </div>
        </div>
    </div>
    </form>
</div>

<style>
    .ibox-border {
        border: 1px solid #e7eaec;
    }    
</style>
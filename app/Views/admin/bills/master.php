<?= $this->extend('admin/layouts/main') ?>
<?= $this->section('content') ?>

<div class="card">
    <div class="card-header bg-light">
        <h3>House Bill Lading Entry - <?= isset($_GET['type']) && $_GET['type'] == "export" ?  "Export Sea" : "Import Sea"; ?>
            <div class="float-right">
                <button class="btn-save-entry btn btn-warning">SAVE</button>
                <button class="btn-post-entry ladda-button btn btn-success expand-left"><span class="label">POST</span> <span class="spinner"></span></button>
            </div>
        </h3>
    </div>
    <div class="card-body">
        <form name="entry-form" accept-charset="utf-8">
            <input type="hidden" name="service-type" value="<?= isset($_GET['type']) && strtolower($_GET['type']) == "export" ? "ES" : "IS"; ?>">
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputEmail4">Shipper/Export</label>
                    <select class="select-shipper form-control form-select-multiple" name="shipper[]" multiple="multiple">
                        <?php foreach ($params['shippers'] as $shipper): ?>
                            <option value="<?= $shipper->partner_code ?>"><?= "{$shipper->partner_code} - {$shipper->partner_name}" ?>"</option>
                        <?php endforeach; ?>
                    </select>
                    <input disabled="disabled" name="shippers_obj" type="hidden" value='<?= json_encode($params['shippers']) ?>' />
                    <textarea disabled="disabled" class="disabled form-control mt-3" name="shipper-address" id="shipper_address" rows="4"></textarea>
                </div>
                <div class="form-group col-md-6">
                    <div>
                        <label for="inputEmail4">Bill of lading no.</label>
                        <select class="form-control selet-tags" name="master-bill-no">
                            <option value="" disabled="disabled" selected="selected" class="hidden"></option>
                        </select>
                    </div>
                    <div class="form-check my-3">
                        <input type="checkbox" class="form-check-input" name="direct" id="direct">
                        <label class="form-check-label" for="exampleCheck1">Direct</label>
                    </div>
                    <div>
                        <label for="inputPassword4">Export reference</label>
                        <input name="house-bill-no" type="text" class="form-control" id="house-bill-no" placeholder="House Bill No.">
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputEmail4">Consignee</label>
                    <select class="select-consignee form-control form-select-multiple" name="consignee[]" multiple="multiple">
                        <?php foreach ($params['consignees'] as $consignee): ?>
                            <option value="<?= $consignee->partner_code ?>"><?= "{$consignee->partner_code} - {$consignee->partner_name}" ?>"</option>
                        <?php endforeach; ?>
                    </select>
                    <input disabled="disabled" name="consignee_obj" type="hidden" value='<?= json_encode($params['consignees']) ?>' />
                    <textarea disabled="disabled" class="disabled form-control mt-3" name="consignee-address" id="consignee" rows="3"></textarea>
                </div>
                <div class="form-group col-md-6">
                    <label for="inputEmail4">To release cargo, please contact:</label>
                    <select class="select-agent form-control form-select-multiple" name="agent" >
                            <option value="" disabled="disabled" selected="selected" class="hidden"></option>
                        <?php foreach ($params['agents'] as $agent): ?>
                            <option value="<?= $agent->partner_code ?>"><?= "{$agent->partner_code} - {$agent->partner_name}" ?>"</option>
                        <?php endforeach; ?>
                    </select>
                    <textarea disabled="disabled" class="disabled form-control mt-3" name="agent-address" id="agent_address" rows="3"></textarea>
                </div>
            </div>
            <input disabled="disabled" name="agent_obj" type="hidden" value='<?= json_encode($params['agents']) ?>' />
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputEmail4">Notify party</label>
                    <select class="select-agent1-notify form-control form-select-multiple" name="agent1" >
                        <option value="" disabled="disabled" selected="selected" class="hidden"></option>
                        <?php foreach ($params['agents'] as $agent): ?>
                            <option value="<?= $agent->partner_code ?>"><?= "{$agent->partner_code} - {$agent->partner_name}" ?>"</option>
                        <?php endforeach; ?>
                    </select>
                    <textarea disabled="disabled" class="disabled form-control mt-3" name="agent1-address" id="agent1_address" rows="3"></textarea>
                </div>
                <div class="form-group col-md-6">
                    <label for="inputEmail4">Also notify</label>
                    <select class="select-agent2-notify form-control form-select-multiple" name="agent2" >
                        <option value="" disabled="disabled" selected="selected" class="hidden"></option>
                        <?php foreach ($params['agents'] as $agent): ?>
                            <option value="<?= $agent->partner_code ?>"><?= "{$agent->partner_code} - {$agent->partner_name}" ?>"</option>
                        <?php endforeach; ?>
                    </select>
                    <textarea disabled="disabled" class="disabled form-control mt-3" name="agent2-address" id="agent2_address" rows="3"></textarea>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputEmail4">Transaction status</label>
                    <select class="form-control form-select-multiple" name="trans-status" >
                        <option value="" disabled="disabled" selected="selected" class="hidden"></option>
                        <option value="pre-alert">Pre alert</option>
                        <option value="validated">Validated manifest</option>
                        <option value="billing">Billing</option>
                        <option value="endorsed">Endorsed</option>
                    </select>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputEmail4">Vessel/Voyage</label>
                    <input type="text" class="form-control " id="vessel_voyage" name="vessel-voyage" placeholder="">
                </div>
            </div>
            
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputEmail4">Estimate time of departure</label>
                    <input type="text" class="form-control date" id="etd" name="etd" placeholder="">
                </div>
                <div class="form-group col-md-6">
                    <label for="inputEmail4">Estimate time of arrival</label>
                    <input type="text" class="form-control date" id="eta" name="eta" placeholder="">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputEmail4">Actual time of departure</label>
                    <input type="text" class="form-control date" id="atd" name="atd" placeholder="">
                </div>
                <div class="form-group col-md-6">
                    <label for="inputEmail4">Actual time of arrival</label>
                    <input type="text" class="form-control date" id="ata" name="ata" placeholder="">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputEmail4">Place of receipt</label>
                    <select class="form-control form-select-multiple" name="origin-destination" >
                        <option value="" disabled="disabled" selected="selected" class="hidden"></option>
                        <?php foreach ($params['countries'] as $country): ?>
                            <option value="<?= $country->country_code; ?>"><?= $country->country_name; ?>"</option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-group col-md-6">
                    <label for="inputEmail4">Final destination</label>
                    <select class="form-control form-select-multiple" name="final-destination" >
                        <option value="" disabled="disabled" selected="selected" class="hidden"></option>
                        <?php foreach ($params['countries'] as $country): ?>
                            <option value="<?= $country->country_code; ?>"><?= $country->country_name; ?>"</option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputEmail4">Port of discharge</label>
                    <select class="form-control form-select-multiple" name="port-of-discharge" >
                        <option value="" disabled="disabled" selected="selected" class="hidden"></option>
                        <?php foreach ($params['ports'] as $port): ?>
                            <option value="<?= $port->port_code; ?>"><?= $port->port_name; ?>"</option>
                        <?php endforeach; ?>
                    </select>
                </div>
               
                <div class="form-group col-md-6">
                    <label for="inputEmail4">Port of loading</label>
                    <select class="form-control form-select-multiple" name="port-of-loading" >
                        <option value="" disabled="disabled" selected="selected" class="hidden"></option>
                        <?php foreach ($params['ports'] as $port): ?>
                            <option value="<?= $port->port_code; ?>"><?= $port->port_name; ?>"</option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <!-- <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputEmail4">Freight payable at</label>
                    <input type="text" class="form-control mb-2" name="payable" placeholder="">
                </div>
                <div class="form-group col-md-6">
                    <label for="inputEmail4">Numbers of origina</label>
                    <input type="text" class="form-control mb-2" name="origina" placeholder="">
                </div>
            </div> -->
            <div class="attr-container">
                <div class="item-row">
                    <lable>#<span class="attr-num">1</span></label>
                    <hr/>
                    <div class="form-row ">    
                        <div class="form-group col">
                            <label for="inputEmail4">Container no.</label>
                            <input type="text" class="form-control mb-2" id="container-num" name="container-num[]" placeholder="">
                        </div>      
                        <div class="form-group col">
                            <label for="inputEmail4">Seal no.</label>
                            <input type="text" class="form-control mb-2" id="seal-num" name="seal-num[]" placeholder="">
                        </div>      
                        <div class="form-group col">
                            <label for="inputEmail4">No. of pkgs</label>
                            <input type="text" class="form-control mb-2" id="pkgs" name="pkgs[]" placeholder="">
                        </div>      
                        <div class="form-group col">
                            <label for="inputEmail4">Gross weight</label>
                            <input type="text" class="form-control mb-2" id="weight" name="weight[]" placeholder="">
                        </div>      
                    </div>      
                    <div class="form-row">
                        <div class="form-group col">
                            <label for="inputEmail4">CBM</label>
                            <input type="text" class="form-control mb-2" id="cbm" name="cbm[]" placeholder="">
                        </div>      
                        <div class="form-group col">
                            <label for="inputEmail4">Size/Type</label>
                            <input type="text" class="form-control mb-2" id="size-type" name="size-type[]" placeholder="">
                        </div>      
                        <div class="form-group col">
                            <label for="inputEmail4">FCL/LCL</label>
                            <input type="text" class="form-control mb-2" id="fcl-lcl" name="fcl-lcl[]" placeholder="">
                        </div>      
                    </div>
                    <div class="form-row">
                        <div class="form-group col">
                            <label for="inputEmail4">Description</label>
                            <textarea class="form-control" id="description" name="description[]" id="description" rows="5"></textarea>
                        </div>      
                    </div>
                </div>
            </div>
            <div class="button-container">
                <button type="button" class="btn btn-primary btn-add-item">add item</button>
                <button type="button" class="btn btn-danger btn-remove-item">remove item</button>
            </div>
        </form>
    </div>
</div>




<?= $this->endSection() ?>
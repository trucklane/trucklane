<?= $this->extend('admin/layouts/main') ?>
<?= $this->section('content') ?>
<section class="content">
    <div class="row">
        <?php $session = \Config\Services::session(); ?>
        

        <div class="col-12 mt-3">
            <div class="card">
                
                <div class="card-header">
                    <h3 class="card-title">Transaction List</h3>

                    <!-- <div class="card-tools">
                        <a href="/settings/partner_add" class="btn btn-primary btn-sm" id="btn-add" >Add transaction</a>
                    </div> -->
                </div>
                <div class="card-body table-responsive">
                    <table class="table table-bordered table-hover dataTable" id="transaction_list">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Master Ref</th>
                                <th>House Ref</th>
                                <th>Direct</th>
                                <th>Service type</th>
                                <th>ETD</th>
                                <th>ETA</th>
                                <th>ATD</th>
                                <th>ATA</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<?= $this->endSection() ?>

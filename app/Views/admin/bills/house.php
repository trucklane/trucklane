<?= $this->extend('admin/layouts/main') ?>
<?= $this->section('content') ?>

<div class="card">
    <div class="card-header bg-light">
        <h3>Master Bill Lading Entry
            <div class="float-right">
                <button class="btn-save-bill btn btn-warning">SAVE</button>
                <button class="btn-post-bill btn btn-info">POST</button>
            </div>
        </h3>
    </div>
    <div class="card-body">
        <form action="#" method="post" accept-charset="utf-8">
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputEmail4">shipper / export</label>
                    <select class="select-shipper form-control form-select-multiple" name="shipper[]" multiple="multiple">
                        <?php foreach ($params['shippers'] as $shipper): ?>
                            <option value="<?= $shipper->id ?>"><?= "{$shipper->partner_code} - {$shipper->partner_name}" ?>"</option>
                        <?php endforeach; ?>
                    </select>
                    <input name="shippers_obj" type="hidden" value='<?= json_encode($params['shippers']) ?>' />
                    <textarea disabled="disabled" class="disabled form-control mt-3" name="shipper-address" id="shipper_address" rows="3"></textarea>
                </div>
                <div class="form-group col-md-6">
                    <label for="inputEmail4">bill of lading no.</label>
                    <input type="text" class="form-control mb-2" id="inputEmail4" placeholder="House Bill No.">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputEmail4">consignee</label>
                    <select class="select-consignee form-control form-select-multiple" name="consignee[]" multiple="multiple">
                        <?php foreach ($params['consignees'] as $consignee): ?>
                            <option value="<?= $consignee->id ?>"><?= "{$consignee->partner_code} - {$consignee->partner_name}" ?>"</option>
                        <?php endforeach; ?>
                    </select>
                    <input name="consignee_obj" type="hidden" value='<?= json_encode($params['consignees']) ?>' />
                    <textarea disabled="disabled" class="disabled form-control mt-3" name="consignee-address" id="consignee" rows="3"></textarea>
                </div>
                <div class="form-group col-md-6">
                    <label for="inputEmail4">to release cargo, please contact:</label>
                    <select class="select-agent form-control form-select-multiple" name="agent" >
                            <option value="" disabled="disabled" selected="selected" class="hidden"></option>
                        <?php foreach ($params['agents'] as $agent): ?>
                            <option value="<?= $agent->id ?>"><?= "{$agent->partner_code} - {$agent->partner_name}" ?>"</option>
                        <?php endforeach; ?>
                    </select>
                    <textarea disabled="disabled" class="disabled form-control mt-3" name="agent-address" id="agent_address" rows="3"></textarea>
                </div>
            </div>
            <input name="agent_obj" type="hidden" value='<?= json_encode($params['agents']) ?>' />
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputEmail4">notify party</label>
                    <select class="select-agent1-notify form-control form-select-multiple" name="agent1" >
                        <option value="" disabled="disabled" selected="selected" class="hidden"></option>
                        <?php foreach ($params['agents'] as $agent): ?>
                            <option value="<?= $agent->id ?>"><?= "{$agent->partner_code} - {$agent->partner_name}" ?>"</option>
                        <?php endforeach; ?>
                    </select>
                    <textarea disabled="disabled" class="disabled form-control mt-3" name="agent1-address" id="agent1_address" rows="3"></textarea>
                </div>
                <div class="form-group col-md-6">
                    <label for="inputEmail4">also notify</label>
                    <select class="select-agent2-notify form-control form-select-multiple" name="agent2" >
                        <option value="" disabled="disabled" selected="selected" class="hidden"></option>
                        <?php foreach ($params['agents'] as $agent): ?>
                            <option value="<?= $agent->id ?>"><?= "{$agent->partner_code} - {$agent->partner_name}" ?>"</option>
                        <?php endforeach; ?>
                    </select>
                    <input name="agents_obj" type="hidden" value='<?= json_encode($params['agents']) ?>' />
                    <textarea disabled="disabled" class="disabled form-control mt-3" name="agent2-address" id="agent2_address" rows="3"></textarea>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputEmail4">vessel/voyage</label>
                    <input type="text" class="form-control mb-2" id="vessel" name="voyage" placeholder="">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputEmail4">place of receipt</label>
                    <select class="form-control form-select-multiple" name="recepient_country" >
                        <option value="" disabled="disabled" selected="selected" class="hidden"></option>
                        <?php foreach ($params['countries'] as $country): ?>
                            <option value="<?= $country->country_code; ?>"><?= $country->country_name; ?>"</option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-group col-md-6">
                    <label for="inputEmail4">port of loading</label>
                    <select class="form-control form-select-multiple" name="port_of_loading" >
                        <option value="" disabled="disabled" selected="selected" class="hidden"></option>
                        <?php foreach ($params['countries'] as $country): ?>
                            <option value="<?= $country->country_code; ?>"><?= $country->country_name; ?>"</option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputEmail4">port of discharge</label>
                    <select class="form-control form-select-multiple" name="port_of_discharge" >
                        <option value="" disabled="disabled" selected="selected" class="hidden"></option>
                        <?php foreach ($params['ports'] as $port): ?>
                            <option value="<?= $port->port_id; ?>"><?= $port->port_name; ?>"</option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-group col-md-6">
                    <label for="inputEmail4">final destination</label>
                    <select class="form-control form-select-multiple" name="port_of_loading" >
                        <option value="" disabled="disabled" selected="selected" class="hidden"></option>
                        <?php foreach ($params['ports'] as $port): ?>
                            <option value="<?= $port->port_id; ?>"><?= $port->port_name; ?>"</option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputEmail4">freight payable at</label>
                    <input type="text" class="form-control mb-2" id="vessel" name="voyage" placeholder="">
                </div>
                <div class="form-group col-md-6">
                    <label for="inputEmail4">numbers of origina</label>
                    <input type="text" class="form-control mb-2" id="vessel" name="voyage" placeholder="">
                </div>
            </div>
            <hr/>
            <div class="attr-container">
                <div class="form-row item-row">    
                    <div class="form-group col">
                        <label for="inputEmail4">container no.</label>
                        <input type="text" class="form-control mb-2" id="vessel" name="container-num" placeholder="">
                    </div>      
                    <div class="form-group col">
                        <label for="inputEmail4">seal no.</label>
                        <input type="text" class="form-control mb-2" id="vessel" name="seal-num" placeholder="">
                    </div>      
                    <div class="form-group col">
                        <label for="inputEmail4">no. of pkgs</label>
                        <input type="text" class="form-control mb-2" id="vessel" name="pkgs" placeholder="">
                    </div>      
                    <div class="form-group col">
                        <label for="inputEmail4">gross weight</label>
                        <input type="text" class="form-control mb-2" id="vessel" name="weight" placeholder="">
                    </div>      
                    <div class="form-group col">
                        <label for="inputEmail4">cbm</label>
                        <input type="text" class="form-control mb-2" id="vessel" name="cbm" placeholder="">
                    </div>      
                    <div class="form-group col">
                        <label for="inputEmail4">size/type</label>
                        <input type="text" class="form-control mb-2" id="vessel" name="size-type" placeholder="">
                    </div>      
                    <div class="form-group col">
                        <label for="inputEmail4">fcl/lcl</label>
                        <input type="text" class="form-control mb-2" id="vessel" name="fcl-lcl" placeholder="">
                    </div>      
                    <div class="form-group col">
                        <label for="inputEmail4">description</label>
                        <input type="text" class="form-control mb-2" id="vessel" name="description" placeholder="">
                    </div>      
                </div>
            </div>
            <div class="button-container">
                <button type="button" class="btn btn-primary btn-add-item">add item</button>
                <button type="button" class="btn btn-danger btn-remove-item">remove item</button>
            </div>
        </form>
    </div>
</div>




<?= $this->endSection() ?>
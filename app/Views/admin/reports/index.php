<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-6">
    <h2><i class="fa fa-list"></i> Transactions by month</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">Home</a>
            </li>
            <li class="breadcrumb-item">
                <span>Reports</span>
            </li>
        </ol>
    </div>
    <div class="col-lg-6">
        <h2></h2>
        <div class="form-group pull-right">
            
        </div>
    </div>
</div>

<br/>

<div class="wrapper wrapper-content animated white-bg ecommerce">
    <div class="row">
        <div class="col-lg-12">
            <div class="form-row">
                <div class="form-group col-md-2">
                    <div class="">
                        <label for="">Filter by</label>
                        <select class="form-control form-select" name="filter_by" >
                            <option value="" class="">All</option>
                            <option value="C" class="">CONSIGNEE</option>
                            <option value="S" class="">SHIPPER</option>
                            <option value="A" class="">AGENT</option>
                            <option value="" class="">ORIGIN</option>
                            <option value="" class="">DESTINATION</option>
                        </select>
                    </div>
                </div>
                <div class="form-group col-md-2">
                    <div class="">
                        <label for="">Filter date</label>
                        <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 6px 10px; border: 1px solid #ccc; width: 100%">
                            <i class="fa fa-calendar"></i>&nbsp;
                            <span></span> <i class="fa fa-caret-down"></i>
                        </div>
                        <input type="hidden" name="filter-start-date" />
                        <input type="hidden" name="filter-end-date" />
                    </div>
                </div>
                <div class="form-group col-md-3">
                    <div class="">
                        <label for="">&nbsp;</label>
                        <div class="">
                            <button class="btn btn-primary btn-apply-filter">Apply filter</button>
                        </div>    
                    </div>
                </div>
            </div>
        </div>
    </div>   
    <div class="row">
        <div class="col-lg-12">
            <div class="table-responsive">
                <table style="width: 100% !important; " class="table table-bordered table-hover" id="reports_table">
                    <thead>
                        <tr>
                            <th>Transaction Date</th>
                            <th>Master Ref</th>
                            <th>House Ref</th>
                            <th>Current Status</th>
                            <th>ETD</th>
                            <th>ETA</th>
                            <th>ATD</th>
                            <th>ATA</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-6">
    <h2><i class="fa fa-list"></i>Transaction by consignee per date range</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">Home</a>
            </li>
            <li class="breadcrumb-item">
                <span>Reports</span>
            </li>
        </ol>
    </div>
    <div class="col-lg-6">
        <h2></h2>
        <div class="form-group pull-right">
            
        </div>
    </div>
</div>

<br/>

<div class="wrapper wrapper-content animated white-bg ecommerce">
    <div class="row">
        <div class="col-lg-12">
            <div class="form-row">
                <div class="form-group col-md-3">
                    <div class="">
                        <label for="">Consignee</label>
                        <select class="select-consignee form-control  form-select" name="filter_value">
                            <option value="" disabled="disabled" selected="selected" class="hidden"></option>
                            <?php foreach ($consignees as $consignee): ?>
                                <option value="<?= $consignee->partner_code ?>"><?= "{$consignee->partner_code} - {$consignee->partner_name}" ?></option>
                            <?php endforeach; ?>
                        </select>
                        <input type="hidden" name="filter_type" value="consignee" />
                    </div>
                </div>
                <div class="form-group col-md-2">
                    <div class="">
                        <label for="">Start date</label>
                        <div class="input-group date" data-provide="datepicker">
                            <input type="text" class="form-control" name="filter-start-date" value="<?php echo date("m/d/Y"); ?>">
                            <div class="input-group-addon">
                                <span class="glyphicon glyphicon-th"></span>
                            </div>
                        </div>
                        
                    </div>
                </div>
                <div class="form-group col-md-2">
                    <div class="">
                        <label for="">End date</label>
                        <div class="input-group date" data-provide="datepicker">
                            <input type="text" class="form-control" name="filter-end-date" value="<?php echo date("m/d/Y"); ?>">
                            <div class="input-group-addon">
                                <span class="glyphicon glyphicon-th"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group col-md-3">
                    <div class="">
                        <label for="">&nbsp;</label>
                        <div class="">
                            <button class="btn btn-primary btn-apply-filter">Apply filter</button>
                        </div>    
                    </div>
                </div>
            </div>
        </div>
    </div>   
    <div class="row">
        <div class="col-lg-12">
            <div class="table-responsive">
                <table style="width: 100% !important; " class="table table-bordered table-hover" id="reports_table">
                    <thead>
                        <tr>
                            <th>Transaction Date</th>
                            <th>Master Ref</th>
                            <th>House Ref</th>
                            <th>Service type</th>
                            <th>Current Status</th>
                            <th>ETD</th>
                            <th>ETA</th>
                            <th>ATD</th>
                            <th>ATA</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
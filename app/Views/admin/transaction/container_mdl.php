<div class="modal fade" id="trans_cont_modal" data-keyboard="false" data-backdrop="static" tabindex="-1"  aria-hidden="true" role="dialog" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated fadeIn">
            <div class="modal-header">
                <div class="col-md-9 "><h3></h3></div>
                <div class="col-md-3 "> <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> </div>
            </div>
            <div class="modal-body">
                <form name="temp-form-cont">
                    <div>
                        <div class="form-row ">    
                            <div class="form-group col-md-12">
                                <label for="inputEmail4">Container no.</label>
                                <input type="text" class="form-control mb-2" name="temp_cl-container-no" placeholder="">
                            </div>      
                        </div>
                        <div class="form-row ">    
                            <div class="form-group col-md-12">
                                <label for="inputEmail4">Seal no.</label>
                                <input type="text" class="form-control" name="temp_cl-seal-no" >
                            </div>      
                        </div>
                        <div class="form-row">    
                            <div class="form-group col-md-12">
                                <label for="inputEmail4">Size</label>
                                <input type="text" class="form-control mb-2" name="temp_cl-size" placeholder="">
                            </div>      
                        </div>
                        <div class="form-row">    
                            <div class="form-group col-md-12">
                                <label for="inputEmail4">Type</label>
                                <input type="text" class="form-control mb-2" name="temp_cl-type" placeholder="">
                            </div>      
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn-temp-add-cont">Add</button>
            </div>
        </div>
    </div>
</div>

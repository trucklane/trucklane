<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-6">
    <h2><i class="fa fa-list"></i> Transactions</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">Home</a>
            </li>
            <li class="breadcrumb-item">
                <span>Transaction</span>
            </li>
            <li class="breadcrumb-item active">
                <strong>Edit - <?= $title; ?></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-6">
        <h2></h2>
        <div class="form-group pull-right">
            <button class="btn-edit-reset-entry btn btn-info btn-sm mx-1" id="btn_reset_entry" >Reset</a>
            <button class="btn-edit-save-entry btn btn-primary btn-sm mx-1" id="btn_save_entry" >Save</a>
            <button class="btn-edit-post-entry btn btn-warning btn-sm mx-1" id="btn_post_entry">Post</button>
        </div>
    </div>
</div>
<br/>

<div class="wrapper wrapper-content animated white-bg ecommerce pb-5">
    <div class="row pb-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <form name="entry-form" accept-charset="utf-8">
                        <input type="hidden" name="service-type" value="<?= strtoupper($service_type); ?>">
                        <input type="hidden" name="transaction-id" value="<?= $transaction->id; ?>">
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="inputEmail4">Shipper/Export</label>
                                <select class="select-shipper form-control form-select-multiple" name="shipper" >
                                    <option value="" disabled="disabled" selected="selected" class="hidden"></option>
                                    <?php foreach ($shippers as $shipper): ?>
                                        <option <?= ($transaction->shipper_code == $shipper->partner_code) ? "selected" : "" ?> value="<?= $shipper->partner_code ?>"><?= "{$shipper->partner_code} - {$shipper->partner_name}" ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <input disabled="disabled" name="shippers_obj" type="hidden" value='<?= json_encode($shippers) ?>' />
                                <textarea disabled="disabled" class="disabled form-control mt-3" name="shipper-address" id="shipper_address" rows="5"></textarea>
                            </div>
                            <div class="form-group col-md-6">
                                <div class="">
                                    <label for="inputEmail4">Transaction date</label>
                                    <?php $tranxn_datetime = date('m/d/Y g:i a'); 
                                        if ($transaction->transaction_datetime != "" && $transaction->transaction_datetime != "0000-00-00 00:00:00") {
                                            $tranxn_datetime = date('m/d/Y g:i a', strtotime($transaction->transaction_datetime));
                                        }                           
                                    ?>
                                    <input autocomplete="off" type="text" class="form-control status-date" id="transaction_datetime" name="transaction-datetime" placeholder="" value="<?= $tranxn_datetime; ?>">
                                </div>
                                <div>
                                    <label for="inputEmail4">Master reference</label>
                                    <select class="form-control selet-tags" name="master-bill-no">
                                        <option value="" disabled="disabled" selected="selected" class="hidden"></option>
                                        <?php foreach ($master_refs as $ref): ?>
                                        <option <?= ($ref->master_ref == $transaction->master_ref) ? "selected" : "" ?> value="<?= $ref->master_ref ?>"><?= $ref->master_ref ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <!-- <div class="form-check my-3">
                                    <input type="checkbox" class="form-check-input" name="direct" id="direct">
                                    <label class="form-check-label" for="exampleCheck1">Direct</label>
                                </div> -->
                                <div class="mt-2">
                                    <label for="inputPassword4">House reference</label>
                                    <input name="house-bill-no" type="text" class="form-control" id="house-bill-no" placeholder="" value="<?= $transaction->house_ref ?>">
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="inputEmail4">Consignee</label>
                                <select class="select-consignee form-control form-select-multiple" name="consignee">
                                    <option value="" disabled="disabled" selected="selected" class="hidden"></option>
                                    <?php foreach ($consignees as $consignee): ?>
                                        <option <?= ($transaction->consignee_code == $consignee->partner_code) ? "selected" : "" ?> value="<?= $consignee->partner_code ?>"><?= "{$consignee->partner_code} - {$consignee->partner_name}" ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <input disabled="disabled" name="consignee_obj" type="hidden" value='<?= json_encode($consignees) ?>' />
                                <textarea disabled="disabled" class="disabled form-control mt-3" name="consignee-address" id="consignee" rows="3"></textarea>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputEmail4">Agent</label>
                                <select class="select-agent form-control form-select-multiple" name="agent" >
                                        <option value="" disabled="disabled" selected="selected" class="hidden"></option>
                                    <?php foreach ($agents as $agent): ?>
                                        <option <?= ($transaction->agent_code == $agent->partner_code) ? "selected" : "" ?> value="<?= $agent->partner_code ?>"><?= "{$agent->partner_code} - {$agent->partner_name}" ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <textarea disabled="disabled" class="disabled form-control mt-3" name="agent-address" id="agent_address" rows="3"></textarea>
                            </div>
                            
                        </div>
                        <input disabled="disabled" name="agent_obj" type="hidden" value='<?= json_encode($agents) ?>' />
                        
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="inputEmail4">Vessel</label>
                                <input type="text" class="form-control " id="vessel_voyage" name="vessel-voyage" placeholder="" value="<?= $transaction->vessel ?>">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputEmail4">Carriers</label>
                                <select class="form-control form-select-multiple" name="carriers" >
                                    <option value="" disabled="disabled" selected="selected" class="hidden"></option>
                                    <?php foreach ($carriers as $carrier): ?>
                                        <option <?= ($transaction->carrier == $carrier->carrier_code) ? "selected" : "" ?> value="<?= $carrier->carrier_code ?>"><?= "{$carrier->carrier_code} - {$carrier->carrier_name}" ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="inputEmail4">Inco term</label>
                                <select class="form-control form-select-multiple" name="inco-term" >
                                    <option value="" disabled="disabled" selected="selected" class="hidden"></option>
                                    <?php foreach ($incoterms as $incoterm): ?>
                                        <option <?= ($transaction->inco_term == $incoterm->incoterm_code) ? "selected" : "" ?> value="<?= $incoterm->incoterm_code ?>"><?= "{$incoterm->incoterm_code} - {$incoterm->incoterm_desc}" ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputEmail4">Mode of shipment</label>
                                <select class="form-control form-select-multiple" name="mode-of-shipment" >
                                    <option <?= ($transaction->mode_of_shpt == "AIR") ? "selected" : "" ?> value="AIR">AIR</option>
                                    <option <?= ($transaction->mode_of_shpt == "SEA") ? "selected" : "" ?> value="SEA">SEA</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="inputEmail4">Gross weight</label>
                                <input type="number" class="form-control " id="gross_wt" name="gross-wt" placeholder="" value="<?= $transaction->gross_wt ?>">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputEmail4">Chargeable weight</label>
                                <input type="number" class="form-control " id="chargeable_weight" name="chargeable-weight" placeholder="" value="<?= $transaction->chargeable_wt ?>">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="inputEmail4">Uom</label>
                                <input type="text" class="form-control " id="uom" name="uom" placeholder="" value="<?= $transaction->uom ?>">
                            </div>
                            
                        </div>
                        <div class="form-row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="inputEmail4">Estimate time of departure</label>
                                    <input autocomplete="off" type="text" class="form-control date" id="etd" name="etd" placeholder="" value="<?= date("m/d/Y", strtotime($transaction->etd)); ?>">
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail4">Estimate time of arrival</label>
                                    <input autocomplete="off" type="text" class="form-control date" id="eta" name="eta" placeholder="" value="<?= date("m/d/Y", strtotime($transaction->eta)); ?>">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="inputEmail4">Actual time of departure</label>
                                    <input autocomplete="off" type="text" class="form-control date" id="atd" name="atd" placeholder="" value="<?= date("m/d/Y", strtotime($transaction->atd)); ?>">
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail4">Actual time of arrival</label>
                                    <input autocomplete="off" type="text" class="form-control date" id="ata" name="ata" placeholder="" value="<?= date("m/d/Y", strtotime($transaction->ata)); ?>">
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col">
                                <label for="inputEmail4">General description</label>
                                <textarea class="form-control" id="general_description" name="general-description" rows="5"><?= trim($transaction->general_desc)?></textarea>
                            </div>      
                        </div>   
                        <ul class="nav nav-tabs" id="transaction_tabs" role="tablist">
                            <li class="nav-item" role="presentation">
                                <a class="nav-link active" id="charges_tab" data-toggle="tab" href="#charges_content" role="tab" aria-controls="charges_content" aria-selected="false">Charges</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="transaction_tab_content">
                            
                            <div class="tab-pane fade show active" id="charges_content" role="tabpanel" aria-labelledby="charges_tab">
                                <?= $this->include('admin/transaction/charge_tbl') ?>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <?= $this->include('admin/transaction/transaction_history') ?>
    </div>
</div>

<?= $this->include('admin/transaction/charge_mdl') ?>
<?= $this->include('admin/transaction/charge_mdl_edit') ?>


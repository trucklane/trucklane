<div class="modal fade" id="trans_mtrl_modal" data-keyboard="false" data-backdrop="static"  aria-hidden="true" role="dialog" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated fadeIn">
            <div class="modal-header">
                <div class="col-md-9 "><h3></h3></div>
                <div class="col-md-3 "> <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> </div>
            </div>
            <div class="modal-body">
                <form name="temp-form-mtrl">
                    <div>
                        <div class="form-row ">    
                            <div class="form-group col-md-12">
                                <label for="inputEmail4">PO no.</label>
                                <input type="text" class="form-control mb-2" name="temp_ml-po-num" placeholder="">
                            </div>      
                        </div>
                        <div class="form-row ">    
                            <div class="form-group col-md-12">
                                <label for="inputEmail4">Material id</label>
                                <input type="text" class="form-control mb-2" name="temp_ml-material-id" >
                            </div>      
                        </div>
                        <div class="form-row">    
                            <div class="form-group col-md-12">
                                <label for="inputEmail4">Item declaration</label>
                                <input type="text" class="form-control mb-2" name="temp_ml-item-declaration" placeholder="">
                            </div>      
                        </div>
                        <div class="form-row">    
                            <div class="form-group col-md-12">
                                <label for="inputEmail4">Order qty</label>
                                <input type="number" class="form-control mb-2" name="temp_ml-order-qty" placeholder="">
                            </div>      
                        </div>
                        <div class="form-row">    
                            <div class="form-group col-md-12">
                                <label for="inputEmail4">Actual ship</label>
                                <input type="number" class="form-control mb-2" name="temp_ml-actual-ship" placeholder="">
                            </div>      
                        </div>
                        <div class="form-row">    
                            <div class="form-group col-md-12">
                                <label for="inputEmail4">Order balance</label>
                                <input type="number" class="form-control mb-2" name="temp_ml-order-balance" placeholder="">
                            </div>      
                        </div>
                        <div class="form-row">    
                        
                            <div class="form-group col-md-12">
                                <label for="inputEmail4">Unit of measure</label>
                                <select class="modal-select form-control " name="temp_ml-unit-of-measure" >
                                    <option value="" disabled="disabled" selected="selected" class="hidden"></option>
                                    <?php if (isset($unit_of_measures)):?>
                                        <?php foreach ($unit_of_measures as $unit): ?>
                                            <option value="<?= $unit->unit_code; ?>"><?= $unit->unit_code." - ".$unit->unit_description; ?></option>
                                        <?php endforeach; ?>
                                    <?php endif ?>
                                </select>
                            </div>      
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn-temp-add-mtrl">Add</button>
            </div>
        </div>
    </div>
</div>

<div class="container mb-5">
    <div class="row">
        <div class="col-md-12 ">
            <div class="mb-3">
                <form name="form-update-status">
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="">Status</label>
                            <select class="form-control form-select" name="update-status">
                                <option value="" disabled="disabled" selected="selected" class="hidden"></option>
                                <?php foreach ($status as $st): ?>
                                <option value="<?= $st->status_code ?>">
                                    <?= "{$st->status_name} - {$st->status_description}" ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="">Status date & time</label>
                            <input autocomplete="off" type="text" class="form-control status-date"
                                id="update_status_date" name="update-status-date" placeholder="" value="<?= date('m/d/Y g:i a')?>">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col">
                            <textarea class="form-control" id="update_status_description"
                                name="update-status-description" rows="5"></textarea>
                        </div>
                    </div>
                    <input type="hidden" name="update-status-attached-file" />
                    <div class="button-container clearfix">
                        
                        <div class="dropzone hidden" id="form_upload" style="display: none;"></div>
                        <div class="upload-container pull-right">
                            <a class="upload-file-btn" href="javascript:void(0);"><i class="fa fa-paperclip"
                                    aria-hidden="true"></i> Attach a file</a>
                        </div>
                    </div>
                </div>
            </form>
            
        </div>
    </div>
</div>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-6">
    <h2><i class="fa fa-list"></i> Transactions</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">Home</a>
            </li>
            <li class="breadcrumb-item">
                <span>Transaction</span>
            </li>
            <li class="breadcrumb-item active">
                <strong>Entry - <?= $title; ?></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-6">
        <h2></h2>
        <div class="form-group pull-right">
            <button class="btn-reset-entry btn btn-info btn-sm mx-1" id="btn_reset_entry" >Reset</a>
            <button class="btn-save-entry btn btn-primary btn-sm mx-1" id="btn_save_entry" >Save</a>
            <button class="btn-post-entry btn btn-warning btn-sm mx-1" id="btn_post_entry">Post</button>
        </div>
    </div>
</div>
<br/>

<div class="wrapper wrapper-content animated white-bg ecommerce pb-5">
    <div class="row pb-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <form name="entry-form" accept-charset="utf-8">
                        <input type="hidden" name="service-type" value="<?= strtoupper($service_type); ?>">
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="inputEmail4">Shipper/Export</label>
                                <select class="select-shipper form-control form-select-multiple" name="shipper" >
                                    <option value="" disabled="disabled" selected="selected" class="hidden"></option>
                                    <?php foreach ($shippers as $shipper): ?>
                                        <option value="<?= $shipper->partner_code ?>"><?= "{$shipper->partner_code} - {$shipper->partner_name}" ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <input disabled="disabled" name="shippers_obj" type="hidden" value='<?= json_encode($shippers) ?>' />
                                <textarea disabled="disabled" class="disabled form-control mt-3" name="shipper-address" id="shipper_address" rows="5"></textarea>
                            </div>
                            <div class="form-group col-md-6">
                                <div class="">
                                    <label for="inputEmail4">Transaction date</label>
                                    <input autocomplete="off" type="text" class="form-control status-date" id="transaction_datetime" name="transaction-datetime" placeholder="" value="<?= date('m/d/Y g:i a')?>">
                                </div>
                                <div>
                                    <label for="inputEmail4">Master reference</label>
                                    <select class="form-control selet-tags" name="master-bill-no">
                                        <option value="" disabled="disabled" selected="selected" class="hidden"></option>
                                        <?php foreach ($master_refs as $ref): ?>
                                        <option value="<?= $ref->master_ref ?>"><?= $ref->master_ref ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <!-- <div class="form-check my-3">
                                    <input type="checkbox" class="form-check-input" name="direct" id="direct">
                                    <label class="form-check-label" for="exampleCheck1">Direct</label>
                                </div> -->
                                <div>
                                    <label for="inputPassword4">House reference</label>
                                    <input name="house-bill-no" type="text" class="form-control" id="house-bill-no" placeholder="">
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="inputEmail4">Consignee</label>
                                <select class="select-consignee form-control form-select-multiple" name="consignee">
                                    <option value="" disabled="disabled" selected="selected" class="hidden"></option>
                                    <?php foreach ($consignees as $consignee): ?>
                                        <option value="<?= $consignee->partner_code ?>"><?= "{$consignee->partner_code} - {$consignee->partner_name}" ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <input disabled="disabled" name="consignee_obj" type="hidden" value='<?= json_encode($consignees) ?>' />
                                <textarea disabled="disabled" class="disabled form-control mt-3" name="consignee-address" id="consignee" rows="3"></textarea>
                            </div>
                            
                            
                        </div>
                        <input disabled="disabled" name="agent_obj" type="hidden" value='<?= json_encode($agents) ?>' />
                        
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="inputEmail4">Vessel</label>
                                <input type="text" class="form-control " id="vessel_voyage" name="vessel-voyage" placeholder="">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputEmail4">Carriers</label>
                                <select class="form-control form-select-multiple" name="carriers" >
                                    <option value="" disabled="disabled" selected="selected" class="hidden"></option>
                                    <?php foreach ($carriers as $carrier): ?>
                                        <option value="<?= $carrier->carrier_code ?>"><?= "{$carrier->carrier_code} - {$carrier->carrier_name}" ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="inputEmail4">Inco term</label>
                                <select class="form-control form-select-multiple" name="inco-term" >
                                    <option value="" disabled="disabled" selected="selected" class="hidden"></option>
                                    <?php foreach ($incoterms as $incoterm): ?>
                                        <option value="<?= $incoterm->incoterm_code ?>"><?= "{$incoterm->incoterm_code} - {$incoterm->incoterm_desc}" ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputEmail4">Mode of shipment</label>
                                <select class="form-control form-select-multiple" name="mode-of-shipment" >
                                    <option value="" disabled="disabled" selected="selected" class="hidden"></option>
                                    <option value="AIR">AIR</option>
                                    <option value="SEA">SEA</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="inputEmail4">Gross weight</label>
                                <input type="text" class="form-control " id="gross_wt" name="gross-wt" placeholder="">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputEmail4">Chargeable weight</label>
                                <input type="text" class="form-control " id="chargeable_weight" name="chargeable-weight" placeholder="">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="inputEmail4">Uom</label>
                                <input type="text" class="form-control " id="uom" name="uom" placeholder="">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputEmail4">Forwarder</label>
                                <input type="text" class="form-control " id="forwarder" name="forwarder" placeholder="">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="inputEmail4">Supp invoice number</label>
                                <input type="text" class="form-control " id="supp_invoice_number" name="supp-invoice-number" placeholder="">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputEmail4">Invoice value</label>
                                <input type="text" class="form-control " id="invoice_value" name="invoice-value" placeholder="">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="inputEmail4">Invoice currency</label>
                                <input type="text" class="form-control " id="invoice_currency" name="invoice-currency" placeholder="">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputEmail4">Import entry no</label>
                                <input type="text" class="form-control " id="import_entry_no" name="import-entry-no" placeholder="">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="inputEmail4">Load type</label>
                                <select class="form-control form-select-multiple" name="load-type" >
                                    <option value="" disabled="disabled" selected="selected" class="hidden"></option>
                                    <?php foreach ($load_types as $load_type): ?>
                                        <option value="<?= $load_type->load_type_code ?>"><?= "{$load_type->load_type_code} - {$load_type->load_type_desc}" ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputEmail4">NTA ICC Number</label>
                                <input type="text" class="form-control " id="nta_icc_no" name="nta-icc-no" placeholder="">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="inputEmail4">PEZA Permit</label>
                                <input type="text" class="form-control " id="peza_permit" name="peza-permit" placeholder="">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputEmail4">Custom value</label>
                                <input type="number" class="form-control " id="custom_value" name="custom-value" placeholder="">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="inputEmail4">Custom Duties</label>
                                <input type="number" class="form-control " id="custom_duties" name="custom-duties" placeholder="">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputEmail4">VAT value</label>
                                <input type="number" class="form-control " id="vat_value" name="vat-value" placeholder="">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="inputEmail4">Date customs cleared</label>
                                <input autocomplete="off" type="text" class="form-control date" id="date_customs_cleared" name="date-customs-cleared" placeholder="">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputEmail4">No of containers</label>
                                <input type="text" class="form-control " id="no_of_containers" name="no-of-containers" placeholder="">
                            </div>
                        </div>
                        
                        
                        <div class="form-row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="inputEmail4">Estimate time of departure</label>
                                    <input autocomplete="off" type="text" class="form-control date" id="etd" name="etd" placeholder="">
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail4">Estimate time of arrival</label>
                                    <input autocomplete="off" type="text" class="form-control date" id="eta" name="eta" placeholder="">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="inputEmail4">Actual time of departure</label>
                                    <input autocomplete="off" type="text" class="form-control date" id="atd" name="atd" placeholder="">
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail4">Actual time of arrival</label>
                                    <input autocomplete="off" type="text" class="form-control date" id="ata" name="ata" placeholder="">
                                </div>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="inputEmail4">Country of origin</label>
                                    <select class="form-control form-select-multiple" name="origin-destination" >
                                        <option value="" disabled="disabled" selected="selected" class="hidden"></option>
                                        <?php foreach ($countries as $country): ?>
                                            <option value="<?= $country->country_code; ?>"><?= $country->country_code." - ".$country->country_name; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail4">Port of origin</label>
                                    <select class="form-control form-select-multiple disabled" name="port-of-loading" disabled="disabled">
                                        
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group ">
                                    <label for="inputEmail4">Country of destination</label>
                                    <select class="form-control form-select-multiple" name="final-destination" >
                                        <option value="" disabled="disabled" selected="selected" class="hidden"></option>
                                        <?php foreach ($countries as $country): ?>
                                            <option value="<?= $country->country_code; ?>"><?= $country->country_code." - ".$country->country_name; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail4">Port of destination</label>
                                    <select class="form-control form-select-multiple disabled" name="port-of-discharge" disabled="disabled">
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col">
                                <label for="inputEmail4">General description</label>
                                <textarea class="form-control" id="general_description" name="general-description" rows="5"></textarea>
                            </div>      
                        </div>  

                        <ul class="nav nav-tabs" id="transaction_tabs" role="tablist">
                            <li class="nav-item" role="presentation">
                                <a class="nav-link active" id="charges_tab" data-toggle="tab" href="#charges_content" role="tab" aria-controls="charges_content" aria-selected="false">Charges</a>
                            </li>
                        </ul>

                        <div class="tab-content" id="transaction_tab_content">
                            <div class="tab-pane fade show active" id="charges_content" role="tabpanel" aria-labelledby="charges_tab">
                                <?= $this->include('admin/transaction/charge_tbl') ?>
                            </div>
                        </div>

                        <?= $this->include('admin/transaction/transaction_history_create') ?>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<?= $this->include('admin/transaction/charge_mdl') ?>
<?= $this->include('admin/transaction/charge_mdl_edit') ?>


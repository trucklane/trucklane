<div class="btn-container my-3 clearfix">
    <button type="button" class="pull-right btn btn-primary btn-add-cont"><i class="fa fa-plus"></i></button>
</div>
<table class="table table-bordered table-hover" id="trxn_cont">
    <thead class="thead-light">
        <tr>
            <th>Container no.</th>
            <th>Seal no.</th>
            <th>Size</th>
            <th>Type</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <?php if (isset($container_list)): ?>
            <?php foreach ($container_list as $container):?>
                <tr>
                    <td>
                        <?= $container->container_number; ?>
                        <input type="hidden" name="cl-container-no[]" value="<?= $container->container_number; ?>">
                    </td>
                    <td>
                        <?= $container->seal_num; ?>
                        <input type="hidden" name="cl-seal-no[]" value="<?= $container->seal_num; ?>">
                    </td>
                    <td>
                        <?= $container->size; ?>
                        <input type="hidden" name="cl-size[]" value="<?= $container->size; ?>">
                    </td>
                    <td>
                        <?= $container->type; ?>
                        <input type="hidden" name="cl-type[]" value="<?= $container->type; ?>">
                    </td>
                    <td>
                        <button type="button" class="mr-1 btn btn-sm btn-warning btn-edit-cont-row"><i class="fa fa-pencil"></i> Edit</button>
                        <button type='button' class='btn btn-sm btn-danger btn-remove-row'><i class='fa fa-trash'></i> Delete</button>
                    </td>
                </tr>
            <?php endforeach; ?>
        <?php endif; ?>
    </tbody>
</table>






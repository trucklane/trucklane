<div class="modal fade" id="trans_chrg_modal" data-keyboard="false" data-backdrop="static"  aria-hidden="true" role="dialog" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated fadeIn">
            <div class="modal-header">
                <div class="col-md-9 "><h3></h3></div>
                <div class="col-md-3 "> <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> </div>
            </div>
            <div class="modal-body">
                <input type="hidden" class="disabled" name="charge_list" value='<?= json_encode($list_of_charges, JSON_HEX_QUOT | JSON_HEX_APOS) ?>'>
                <form name="temp-form-chrg">
                    <div>
                        <div class="form-row ">    
                            <div class="form-group col-md-12">
                                <label for="">Charge code</label>
                                <select class="modal-select form-control " name="temp_charge-code" >
                                    <option value="" disabled="disabled" selected="selected" class="hidden"></option>
                                    <?php if (isset($list_of_charges)):?>
                                        <?php foreach ($list_of_charges as $charge): ?>
                                            <option value="<?= $charge->charge_code; ?>"><?= $charge->charge_code." - ".$charge->charge_description; ?></option>
                                            <?php endforeach; ?>
                                    <?php endif ?>
                                </select>
                            </div>      
                        </div>
                        <div class="form-row" style="display:none;">    
                            <div class="form-group col-md-12">
                                <label for="">Charge description</label>
                                <textarea class="form-control " name="temp_charge-desc" rows="5"></textarea>
                            </div>      
                        </div>
                        <div class="form-row">    
                            <div class="form-group col-md-12">
                                <label for="">Charge amount</label>
                                <input type="number" class="form-control mb-2" name="temp_charge-amount" placeholder="">
                            </div>      
                        </div>
                        <div class="form-row">    
                            <div class="form-group col-md-12">
                                <label for="">Currency</label>
                                <input type="text" class="form-control mb-2" name="temp_charge-currency" placeholder="">
                            </div>      
                        </div>
                        <div class="form-row">    
                            <div class="form-group col-md-12">
                                <label for="">Php value</label>
                                <input type="text" class="form-control mb-2" name="temp_php-value" placeholder="">
                            </div>      
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn-temp-add-chrg">Add</button>
            </div>
        </div>
    </div>
</div>

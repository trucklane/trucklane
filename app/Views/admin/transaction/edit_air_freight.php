<?= $this->extend('admin/layouts/main') ?>
<?= $this->section('content') ?>

<div class="card">
    <div class="card-header bg-light">
        <h3>Edit House Bill Lading Entry - <?= strtoupper($transaction->service_type); ?>
            <div class="float-right">
                
                <button class="btn-edit-save-entry btn btn-warning">SAVE</button>
                <button class="btn-edit-post-entry ladda-button btn btn-success expand-left"><span class="label">POST</span> <span class="spinner"></span></button>
            </div>
        </h3>
    </div>
    <div class="card-body">
        <form name="edit-entry-form" accept-charset="utf-8">
            <input type="hidden" name="service-type" value="<?= strtoupper($transaction->service_type); ?>">
            <input type="hidden" name="transaction-id" value="<?= strtoupper($transaction->id); ?>">
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputEmail4">Shipper/Export</label>
                    <select class="select-shipper form-control form-select-multiple" name="shipper[]">
                        <?php foreach ($shippers as $shipper): ?>
                            <?php 
                                $selected = FALSE;
                                foreach ($trxn_shippers as $txn_shipper) {
                                   if ($txn_shipper->partner_code == $shipper->partner_code) {
                                        $selected  = TRUE;
                                        break;
                                   }
                                }
                            ?>
                            <option  <?= $selected ? "selected" : ""; ?> value="<?= $shipper->partner_code ?>"><?= "{$shipper->partner_code} - {$shipper->partner_name}" ?></option>
                        <?php endforeach; ?>
                    </select>
                    <input disabled="disabled" name="shippers_obj" type="hidden" value='<?= json_encode($shippers) ?>' />
                    <textarea disabled="disabled" class="disabled form-control mt-3" name="shipper-address" id="shipper_address" rows="4"></textarea>
                </div>
                <div class="form-group col-md-6">
                    <div>
                        <label for="inputEmail4">Master airway bill</label>
                        <select class="form-control selet-tags" name="master-bill-no">
                            <option value="" disabled="disabled" selected="selected" class="hidden"></option>
                            <?php foreach ($master_refs as $ref): ?>
                            <option <?= ($ref->master_ref == $transaction->master_ref) ? "selected" : "" ?> value="<?= $ref->master_ref ?>"><?= $ref->master_ref ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-check my-3">
                        <input <?= $transaction->direct == "1" ? "checked" : ""?> type="checkbox" class="form-check-input" name="direct" id="direct">
                        <label class="form-check-label" for="exampleCheck1">Direct</label>
                    </div>
                    <div>
                        <label for="inputPassword4">House airway bill</label>
                        <input value="<?= $transaction->house_ref; ?>" name="house-bill-no" type="text" class="form-control" id="house-bill-no" placeholder="House Bill No.">
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputEmail4">Consignee</label>
                    <select class="select-consignee form-control form-select-multiple" name="consignee[]" >
                        <?php foreach ($consignees as $consignee): ?>
                            <?php 
                                $c_selected = FALSE;
                                foreach ($trxn_consignees as $trxn_consignee) {
                                   if ($consignee->partner_code == $trxn_consignee->partner_code) {
                                        $c_selected  = TRUE;
                                        break;
                                   }
                                }
                            ?>
                            <option <?= $c_selected ? "selected" : ""; ?> value="<?= $consignee->partner_code ?>"><?= "{$consignee->partner_code} - {$consignee->partner_name}" ?></option>
                        <?php endforeach; ?>
                    </select>
                    <input disabled="disabled" name="consignee_obj" type="hidden" value='<?= json_encode($consignees) ?>' />
                    <textarea disabled="disabled" class="disabled form-control mt-3" name="consignee-address" id="consignee" rows="3"></textarea>
                </div>
                <div class="form-group col-md-6">
                    <label for="inputEmail4">To release cargo, please contact:</label>
                    <select class="select-agent form-control form-select-multiple" name="agent" >
                        <?php foreach ($agents as $agent): ?>
                            <option <?= $agent->partner_code == $transaction->agent_code ? "selected" : ""; ?> value="<?= $agent->partner_code ?>"><?= "{$agent->partner_code} - {$agent->partner_name}" ?></option>
                        <?php endforeach; ?>
                    </select>
                    <textarea disabled="disabled" class="disabled form-control mt-3" name="agent-address" id="agent_address" rows="3"></textarea>
                </div>
            </div>
            <input disabled="disabled" name="agent_obj" type="hidden" value='<?= json_encode($agents) ?>' />
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputEmail4">Notify party</label>
                    <select class="select-agent1-notify form-control form-select-multiple" name="agent1" >
                        <?php foreach ($agents as $agent): ?>
                            <option <?= $agent->partner_code == $transaction->notify_agent_code_1 ? "selected" : ""; ?> value="<?= $agent->partner_code ?>"><?= "{$agent->partner_code} - {$agent->partner_name}" ?></option>
                        <?php endforeach; ?>
                    </select>
                    <textarea disabled="disabled" class="disabled form-control mt-3" name="agent1-address" id="agent1_address" rows="3"></textarea>
                </div>
                <div class="form-group col-md-6">
                    <label for="inputEmail4">Also notify</label>
                    <select class="select-agent2-notify form-control form-select-multiple" name="agent2" >
                        
                        <?php foreach ($agents as $agent): ?>
                            <option <?= $agent->partner_code == $transaction->notify_agent_code_2 ? "selected" : ""; ?> value="<?= $agent->partner_code ?>"><?= "{$agent->partner_code} - {$agent->partner_name}" ?></option>
                        <?php endforeach; ?>
                    </select>
                    <textarea disabled="disabled" class="disabled form-control mt-3" name="agent2-address" id="agent2_address" rows="3"></textarea>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputEmail4">Transaction status</label>
                    <select class="form-control form-select-multiple" name="trans-status" >
                        <option value="" disabled="disabled" selected="selected" class="hidden"></option>
                        <?php foreach ($status as $st): ?>
                            <option <?= $st->status_code == $transaction->status ? "selected" : ""?> value="<?= $st->status_code ?>"><?= "{$st->status_name} - {$st->status_description}" ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-group col-md-6">
                    <label for="inputEmail4">Transaction status date</label>
                    <input type="text" class="form-control date" id="status_date" name="status-date" placeholder="" value="<?= date("m/d/Y", strtotime($transaction->status_datetime)); ?>">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputEmail4">Vessel/Voyage</label>
                    <input value="<?= $transaction->vessel_voyage; ?>" type="text" class="form-control " id="vessel_voyage" name="vessel-voyage" placeholder="">
                </div>
            </div>
            
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputEmail4">Estimate time of departure</label>
                    <input value="<?= date("m/d/Y", strtotime($transaction->etd)); ?>" type="text" class="form-control date" id="etd" name="etd" placeholder="">
                </div>
                <div class="form-group col-md-6">
                    <label for="inputEmail4">Estimate time of arrival</label>
                    <input value="<?= date("m/d/Y", strtotime($transaction->eta)); ?>" type="text" class="form-control date" id="eta" name="eta" placeholder="">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputEmail4">Actual time of departure</label>
                    <input value="<?= date("m/d/Y", strtotime($transaction->atd)); ?>" type="text" class="form-control date" id="atd" name="atd" placeholder="">
                </div>
                <div class="form-group col-md-6">
                    <label for="inputEmail4">Actual time of arrival</label>
                    <input value="<?= date("m/d/Y", strtotime($transaction->ata)); ?>" type="text" class="form-control date" id="ata" name="ata" placeholder="">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputEmail4">Place of receipt</label>
                    <select class="form-control form-select-multiple" name="origin-destination" >
                        <option value="" disabled="disabled" selected="selected" class="hidden"></option>
                        <?php foreach ($countries as $country): ?>
                            <option <?= $country->country_code == $transaction->origin_destination ? "selected" : ""?> value="<?= $country->country_code; ?>"><?= $country->country_code." - ".$country->country_name; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-group col-md-6">
                    <label for="inputEmail4">Final destination</label>
                    <select class="form-control form-select-multiple" name="final-destination" >
                        <option value="" disabled="disabled" selected="selected" class="hidden"></option>
                        <?php foreach ($countries as $country): ?>
                            <option <?= $country->country_code == $transaction->final_destination ? "selected" : ""?> value="<?= $country->country_code; ?>"><?= $country->country_code." - ".$country->country_name; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputEmail4">Port of discharge</label>
                    <select class="form-control form-select-multiple" name="port-of-discharge" >
                        <option value="" disabled="disabled" selected="selected" class="hidden"></option>
                        <?php foreach ($ports as $port): ?>
                            <option <?= $port->port_code == $transaction->discharge_port ? "selected" : ""?> value="<?= $port->port_code; ?>"><?= $port->port_code." - ".$port->port_name; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
               
                <div class="form-group col-md-6">
                    <label for="inputEmail4">Port of loading</label>
                    <select class="form-control form-select-multiple" name="port-of-loading" >
                        <option value="" disabled="disabled" selected="selected" class="hidden"></option>
                        <?php foreach ($ports as $port): ?>
                            <option <?= $port->port_code == $transaction->loading_port ? "selected" : ""?> value="<?= $port->port_code; ?>"><?= $port->port_code." - ".$port->port_name; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <!-- <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputEmail4">Freight payable at</label>
                    <input type="text" class="form-control mb-2" name="payable" placeholder="">
                </div>
                <div class="form-group col-md-6">
                    <label for="inputEmail4">Numbers of origina</label>
                    <input type="text" class="form-control mb-2" name="origina" placeholder="">
                </div>
            </div> -->
            <div class="attr-container">
                <?php if ($trxn_attribs): ?>
                    <?php foreach($trxn_attribs as $k => $ta): ?>
                        <div class="item-row">
                            <lable>#<span class="attr-num"><?= $k+1 ?></span></label>
                            <hr/>
                            <div class="form-row ">    
                                <div class="form-group col">
                                    <label for="inputEmail4">Container no.</label>
                                    <input value="<?= $ta->container_no; ?>" type="text" class="form-control mb-2" id="container-num" name="container-num[]" placeholder="">
                                </div>      
                                <div class="form-group col">
                                    <label for="inputEmail4">Seal no.</label>
                                    <input value="<?= $ta->seal_no; ?>" type="text" class="form-control mb-2" id="seal-num" name="seal-num[]" placeholder="">
                                </div>      
                                <div class="form-group col">
                                    <label for="inputEmail4">No. of pkgs</label>
                                    <input value="<?= $ta->no_of_pkgs; ?>" type="text" class="form-control mb-2" id="pkgs" name="pkgs[]" placeholder="">
                                </div>      
                                <div class="form-group col">
                                    <label for="inputEmail4">Gross weight</label>
                                    <input value="<?= $ta->gross_weight; ?>" type="text" class="form-control mb-2" id="weight" name="weight[]" placeholder="">
                                </div>      
                            </div>      
                            <div class="form-row">
                                <div class="form-group col">
                                    <label for="inputEmail4">CBM</label>
                                    <input value="<?= $ta->cbm; ?>" type="text" class="form-control mb-2" id="cbm" name="cbm[]" placeholder="">
                                </div>      
                                <div class="form-group col">
                                    <label for="inputEmail4">Size/Type</label>
                                    <input value="<?= $ta->size_type; ?>" type="text" class="form-control mb-2" id="size-type" name="size-type[]" placeholder="">
                                </div>      
                                <div class="form-group col">
                                    <label for="inputEmail4">FCL/LCL</label>
                                    <input value="<?= $ta->fcl_lcl; ?>" type="text" class="form-control mb-2" id="fcl-lcl" name="fcl-lcl[]" placeholder="">
                                </div>      
                            </div>
                            <div class="form-row">
                                <div class="form-group col">
                                    <label for="inputEmail4">Description</label>
                                    <textarea class="form-control" id="description" name="description[]" id="description" rows="5"><?= $ta->description; ?></textarea>
                                </div>      
                            </div>
                        </div>
                    <?php endforeach; ?>
                <?php else: ?>
                <div class="item-row">
                    <lable>#<span class="attr-num">1</span></label>
                    <hr/>
                    <div class="form-row ">    
                        <div class="form-group col">
                            <label for="inputEmail4">Container no.</label>
                            <input type="text" class="form-control mb-2" id="container-num" name="container-num[]" placeholder="">
                        </div>      
                        <div class="form-group col">
                            <label for="inputEmail4">Seal no.</label>
                            <input type="text" class="form-control mb-2" id="seal-num" name="seal-num[]" placeholder="">
                        </div>      
                        <div class="form-group col">
                            <label for="inputEmail4">No. of pkgs</label>
                            <input type="text" class="form-control mb-2" id="pkgs" name="pkgs[]" placeholder="">
                        </div>      
                        <div class="form-group col">
                            <label for="inputEmail4">Gross weight</label>
                            <input type="text" class="form-control mb-2" id="weight" name="weight[]" placeholder="">
                        </div>      
                    </div>      
                    <div class="form-row">
                        <div class="form-group col">
                            <label for="inputEmail4">CBM</label>
                            <input type="text" class="form-control mb-2" id="cbm" name="cbm[]" placeholder="">
                        </div>      
                        <div class="form-group col">
                            <label for="inputEmail4">Size/Type</label>
                            <input type="text" class="form-control mb-2" id="size-type" name="size-type[]" placeholder="">
                        </div>      
                        <div class="form-group col">
                            <label for="inputEmail4">FCL/LCL</label>
                            <input type="text" class="form-control mb-2" id="fcl-lcl" name="fcl-lcl[]" placeholder="">
                        </div>      
                    </div>
                    <div class="form-row">
                        <div class="form-group col">
                            <label for="inputEmail4">Description</label>
                            <textarea class="form-control" id="description" name="description[]" id="description" rows="5"></textarea>
                        </div>      
                    </div>
                </div>
                <?php endif; ?>
            </div>
            <div class="button-container">
                <button type="button" class="btn btn-primary btn-add-item">add item</button>
                <button type="button" class="btn btn-danger btn-remove-item">remove item</button>
            </div>
        </form>
    </div>
</div>




<?= $this->endSection() ?>
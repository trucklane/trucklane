<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-6">
    <h2><i class="fa fa-list"></i> Transactions</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">Home</a>
            </li>
            <li class="breadcrumb-item">
                <span>Transaction</span>
            </li>
            <li class="breadcrumb-item active">
                <strong>Entry - <?= $title; ?></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-6">
        <h2></h2>
        <div class="form-group pull-right">
            <button class="btn-reset-entry btn btn-info btn-sm mx-1" id="btn_reset_entry" >Reset</a>
            <button class="btn-save-entry btn btn-primary btn-sm mx-1" id="btn_save_entry" >Save</a>
            <button class="btn-post-entry btn btn-warning btn-sm mx-1" id="btn_post_entry">Post</button>
        </div>
    </div>
</div>
<br/>

<div class="wrapper wrapper-content animated white-bg ecommerce pb-5">
    <div class="row pb-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <form name="entry-form" accept-charset="utf-8">
                        <div class="card mb-3">
                            <div class="card-header">
                                Master Information
                            </div>
                            <div class="card-body">
                                <div class="form-row">
                                    <div class="form-group col-md-4">
                                        <div class="">
                                            <label for="inputEmail4">Master reference</label>
                                            <select class="form-control selet-tags" name="master-bill-no">
                                                <option value="" disabled="disabled" selected="selected" class="hidden"></option>
                                                <?php foreach ($master_refs as $ref): ?>
                                                <option value="<?= $ref->master_ref ?>"><?= $ref->master_ref ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-1">
                                        <label class="form-check-label" for="exampleCheck1">Direct</label>
                                        <div class="form-check mt-2">
                                            <input type="checkbox" class="form-check-input" name="direct" id="direct">
                                           
                                        </div>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <div class="">
                                            <label for="inputEmail4">Transaction date</label>
                                            <input autocomplete="off" type="text" class="form-control date" id="transaction_datetime" name="transaction-datetime" placeholder="" value="<?= date('m/d/Y')?>">
                                        </div>
                                    </div>
                                </div>
                              
                                <div class="form-row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="inputEmail4">Estimate date of departure</label>
                                            <input autocomplete="off" type="text" class="form-control date" id="etd" name="etd" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="inputEmail4">Estimate date of arrival</label>
                                            <input autocomplete="off" type="text" class="form-control date" id="eta" name="eta" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="inputEmail4">Actual date of departure</label>
                                            <input autocomplete="off" type="text" class="form-control date" id="atd" name="atd" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="inputEmail4">Actual date of arrival</label>
                                            <input autocomplete="off" type="text" class="form-control date" id="ata" name="ata" placeholder="">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="inputEmail4">Country of origin</label>
                                            <select class="form-control form-select-multiple" name="origin-destination" >
                                                <option value="" disabled="disabled" selected="selected" class="hidden"></option>
                                                <?php foreach ($countries as $country): ?>
                                                    <option value="<?= $country->country_code; ?>"><?= $country->country_code." - ".$country->country_name; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                       
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="inputEmail4">Port of origin</label>
                                            <select class="form-control form-select-multiple disabled" name="port-of-loading" disabled="disabled">
                                                
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group ">
                                            <label for="inputEmail4">Country of destination</label>
                                            <select class="form-control form-select-multiple" name="final-destination" >
                                                <option value="" disabled="disabled" selected="selected" class="hidden"></option>
                                                <?php foreach ($countries as $country): ?>
                                                    <option value="<?= $country->country_code; ?>"><?= $country->country_code." - ".$country->country_name; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="inputEmail4">Port of destination</label>
                                            <select class="form-control form-select-multiple disabled" name="port-of-discharge" disabled="disabled">
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="inputEmail4">Vessel</label>
                                            <input type="text" class="form-control " id="vessel_voyage" name="vessel-voyage" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="inputEmail4">Carriers</label>
                                            <select class="form-control form-select-multiple" name="carriers" >
                                                <option value="" disabled="disabled" selected="selected" class="hidden"></option>
                                                <?php foreach ($carriers as $carrier): ?>
                                                    <option value="<?= $carrier->carrier_code ?>"><?= "{$carrier->carrier_code} - {$carrier->carrier_name}" ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="inputEmail4">Gross weight</label>
                                            <input type="number" class="form-control " id="gross_wt" name="gross-wt" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="inputEmail4">Chargeable weight</label>
                                            <input type="number" class="form-control " id="chargeable_weight" name="chargeable-weight" placeholder="">
                                        </div>
                                    </div>
                                </div>
                               
                            </div>
                        </div>
                        

                        <div class="card my-3">
                            <div class="card-header">
                                Shipping Information
                            </div>   
                            <div class="card-body">
                                <div class="form-row">
                                    <div class="form-group col-md-4">
                                        <div class="">
                                            <label for="inputPassword4">House reference</label>
                                            <input name="house-bill-no" type="text" class="form-control" id="house-bill-no" placeholder="">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-4">
                                        <label for="inputEmail4">Consignee</label>
                                        <select class="select-consignee form-control form-select-multiple" name="consignee">
                                            <option value="" disabled="disabled" selected="selected" class="hidden"></option>
                                            <?php foreach ($consignees as $consignee): ?>
                                                <option value="<?= $consignee->partner_code ?>"><?= "{$consignee->partner_code} - {$consignee->partner_name}" ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                        <input disabled="disabled" name="consignee_obj" type="hidden" value='<?= json_encode($consignees) ?>' />
                                        <textarea disabled="disabled" class="disabled form-control mt-3" name="consignee-address" id="consignee" rows="3"></textarea>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="inputEmail4">Agent</label>
                                        <select class="select-agent form-control form-select-multiple" name="agent" >
                                                <option value="" disabled="disabled" selected="selected" class="hidden"></option>
                                            <?php foreach ($agents as $agent): ?>
                                                <option value="<?= $agent->partner_code ?>"><?= "{$agent->partner_code} - {$agent->partner_name}" ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                        <input disabled="disabled" name="agent_obj" type="hidden" value='<?= json_encode($agents) ?>' />
                                        <textarea disabled="disabled" class="disabled form-control mt-3" name="agent-address" id="agent_address" rows="3"></textarea>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="inputEmail4">Remarks</label>
                                        <textarea class="form-control" id="general_description" name="general-description" rows="5"></textarea>
                                    </div>      
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-4">
                                        <label for="inputEmail4">Notify party</label>
                                        <select class="select-agent-notify form-control form-select-multiple" name="agent_notify" >
                                            <option value="" disabled="disabled" selected="selected" class="hidden"></option>
                                            <?php foreach ($agents as $agent): ?>
                                                <option value="<?= $agent->partner_code ?>"><?= "{$agent->partner_code} - {$agent->partner_name}" ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                        <textarea disabled="disabled" class="disabled form-control mt-3" name="notify-address" id="notify_consignee" rows="3"></textarea>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="inputEmail4">Third party</label>
                                        <select class="select-agent-third form-control form-select-multiple" name="agent_third_party" >
                                            <option value="" disabled="disabled" selected="selected" class="hidden"></option>
                                            <?php foreach ($agents as $agent): ?>
                                                <option value="<?= $agent->partner_code ?>"><?= "{$agent->partner_code} - {$agent->partner_name}" ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                        <textarea disabled="disabled" class="disabled form-control mt-3" name="third-party-address" id="third_party_address" rows="3"></textarea>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-4">
                                        <label for="inputEmail4">Gross weight</label>
                                        <input type="number" class="form-control " id="gross_wt" name="gross-wt" placeholder="">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="inputEmail4">Chargeable weight</label>
                                        <input type="number" class="form-control " id="chargeable_weight" name="chargeable-weight" placeholder="">
                                    </div>
                                </div>
                            </div>
                        </div>



                        

                        <div class="form-row">
                            <div class="form-group col">
                                <label for="inputEmail4">General description</label>
                                <textarea class="form-control" id="general_description" name="general-description" rows="5"></textarea>
                            </div>      
                        </div>  

                        <ul class="nav nav-tabs" id="transaction_tabs" role="tablist">
                            <li class="nav-item" role="presentation">
                                <a class="nav-link active" id="material_tab" data-toggle="tab" href="#material_content" role="tab" aria-controls="material_content" aria-selected="true">PO Material</a>
                            </li>
                            <li class="nav-item" role="presentation">
                                <a class="nav-link" id="container_tab" data-toggle="tab" href="#container_content" role="tab" aria-controls="container_content" aria-selected="false">Container list</a>
                            </li>
                            <li class="nav-item" role="presentation">
                                <a class="nav-link" id="cargo_tab" data-toggle="tab" href="#cargo_content" role="tab" aria-controls="cargo_content" aria-selected="false">Loose cargo</a>
                            </li>
                            <li class="nav-item" role="presentation">
                                <a class="nav-link " id="charges_tab" data-toggle="tab" href="#charges_content" role="tab" aria-controls="charges_content" aria-selected="false">Charges</a>
                            </li>
                        </ul>

                        <div class="tab-content" id="transaction_tab_content">
                            <div class="tab-pane fade show active" id="material_content" role="tabpanel" aria-labelledby="material_tab">
                                <?= $this->include('admin/transaction/material_tbl') ?>
                            </div>
                            <div class="tab-pane fade " id="container_content" role="tabpanel" aria-labelledby="container_tab">
                                <?= $this->include('admin/transaction/container_tbl') ?>
                            </div>
                            <div class="tab-pane fade" id="cargo_content" role="tabpanel" aria-labelledby="cargo_tab">
                                <?= $this->include('admin/transaction/cargo_tbl') ?>
                            </div>
                            <div class="tab-pane fade " id="charges_content" role="tabpanel" aria-labelledby="charges_tab">
                                <?= $this->include('admin/transaction/charge_tbl') ?>
                            </div>
                        </div>

                        <?= $this->include('admin/transaction/transaction_history_create') ?>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<?= $this->include('admin/transaction/charge_mdl') ?>
<?= $this->include('admin/transaction/charge_mdl_edit') ?>
<?= $this->include('admin/transaction/material_mdl') ?>
<?= $this->include('admin/transaction/material_mdl_edit') ?>
<?= $this->include('admin/transaction/cargo_mdl') ?>
<?= $this->include('admin/transaction/cargo_mdl_edit') ?>
<?= $this->include('admin/transaction/container_mdl') ?>
<?= $this->include('admin/transaction/container_mdl_edit') ?>


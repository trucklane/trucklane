<div class="btn-container my-3 clearfix">
    <button type="button" class="pull-right btn btn-primary btn-add-chrg"><i class="fa fa-plus"></i></button>
</div>
<table class="table table-bordered table-hover" id="trxn_chrg">
    <thead class="thead-light">
        <tr>
            <th>Charge code</th>
            <th>Charge description</th>
            <th>Charge amount</th>
            <th>Currency</th>
            <th>Php value</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <?php if (isset($charges)): ?>
            <?php foreach ($charges as $charge):?>
                <tr>
                    <td>
                        <?= $charge->chg_code; ?>
                        <input type="hidden" name="charge-code[]" value="<?= $charge->chg_code; ?>">
                    </td>
                    <td>
                        <?= $charge->chg_desc; ?>
                        <input type="hidden" name="charge-desc[]" value="<?= $charge->chg_desc; ?>">
                    </td>
                    <td>
                        <?= $charge->chg_amount; ?>
                        <input type="hidden" name="charge-amount[]" value="<?= $charge->chg_amount; ?>">
                    </td>
                    <td>
                        <?= $charge->currency; ?>
                        <input type="hidden" name="charge-currency[]" value="<?= $charge->currency; ?>">
                    </td>
                    <td>
                        <?= $charge->php_value; ?>
                        <input type="hidden" name="php-value[]" value="<?= $charge->php_value; ?>">
                    </td>
                    <td>
                        <button type='button' class='btn btn-sm btn-warning btn-edit-charge-row'><i class='fa fa-pencil'></i> Edit</button>
                        <button type='button' class='btn btn-sm btn-danger btn-remove-row'><i class='fa fa-trash'></i> Delete</button>
                    </td>
                </tr>
            <?php endforeach; ?>
        <?php endif; ?>
    </tbody>
</table>

<div class="btn-container my-3 clearfix">
    <button type="button" class="pull-right btn btn-primary btn-add-mtrl"><i class="fa fa-plus"></i></button>
</div>
<table class="table table-bordered table-hover" id="trxn_mtrl">
    <thead class="thead-light">
        <tr>
            <th>PO no.</th>
            <th>Material id</th>
            <th>Item declaration</th>
            <th>Order qty</th>
            <th>Actual ship</th>
            <th>Order balance</th>
            <th>Unit of measure</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <?php if (isset($po_materials)): ?>
            <?php foreach ($po_materials as $material):?>
                <tr>
                    <td>
                        <?= $material->po_number; ?>
                        <input type="hidden" name="ml-po-num[]" value="<?= $material->po_number; ?>">
                    </td>
                    <td>
                        <?= $material->material_id; ?>
                        <input type="hidden" name="ml-material-id[]" value="<?= $material->material_id; ?>">
                    </td>
                    <td>
                        <?= $material->item_declaration; ?>
                        <input type="hidden" name="ml-item-declaration[]" value="<?= $material->item_declaration; ?>">
                    </td>
                    <td>
                        <?= $material->order_qty; ?>
                        <input type="hidden" name="ml-order-qty[]" value="<?= $material->order_qty; ?>">
                    </td>
                    <td>
                        <?= $material->actual_ship; ?>
                        <input type="hidden" name="ml-actual-ship[]" value="<?= $material->actual_ship; ?>">
                    </td>
                    <td>
                        <?= $material->order_balance; ?>
                        <input type="hidden" name="ml-order-balance[]" value="<?= $material->order_balance; ?>">
                    </td>
                    <td>
                        <?= $material->unit_of_measure; ?>
                        <input type="hidden" name="ml-unit-of-measure[]" value="<?= $material->unit_of_measure; ?>">
                    </td>
                    <td>
                        <button type='button' class='btn btn-sm btn-warning btn-edit-mtrl-row'><i class='fa fa-pencil'></i> Edit</button>
                        <button type='button' class='btn btn-sm btn-danger btn-remove-row'><i class='fa fa-trash'></i> Delete</button>
                    </td>
                </tr>
            <?php endforeach; ?>
        <?php endif; ?>
    </tbody>
</table>


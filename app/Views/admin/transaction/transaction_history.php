<div class="container mt-5 mb-5">
    <div class="row">
        <div class="col-md-12 ">
            <div class="mb-3">
                <form name="form-update-status">
                    <input type="hidden" name="transaction-id" value="<?= $transaction->id ?>" />
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputEmail4">Status</label>
                            <select class="form-control form-select-multiple" name="update-status">
                                <option value="" disabled="disabled" selected="selected" class="hidden"></option>
                                <?php foreach ($status as $st): ?>
                                <option value="<?= $st->status_code ?>">
                                    <?= "{$st->status_name} - {$st->status_description}" ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputEmail4">Status date & time</label>
                            <input autocomplete="off" type="text" class="form-control status-date"
                                id="update_status_date" name="update-status-date" placeholder="" value="<?= date('m/d/Y g:i a')?>">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col">
                            <textarea class="form-control" id="update_status_description"
                                name="update-status-description" rows="5"></textarea>
                        </div>
                    </div>
                    <input type="hidden" name="update-status-attached-file" />
                    <div class="button-container clearfix">
                        <button class="btn btn-primary btn-sm btn-submit-update-status">Update status</button>
                        <div class="dropzone hidden" id="form_upload" style="display: none;"></div>
                        <div class="upload-container pull-right">
                            <a class="upload-file-btn" href="javascript:void(0);"><i class="fa fa-paperclip"
                                    aria-hidden="true"></i> Attach a file</a>
                        </div>
                    </div>
            </div>
            </form>
            <div class="py-3">
                <h3>Transaction Status History</h3>
                <ul class="timeline mt-3">
                    <?php if ($history): ?>
                    <?php foreach($history as $item):  ?>
                    <li>
                        <a href="javascript:void(0);">
                            <strong><?= $item->status_name ?></strong> - <?= $item->status_description; ?>
                        </a>
                        <a href="javascript:void(0);"
                            class="float-right"><?= strtoupper(date("M d, Y g:i a", strtotime($item->created_at))); ?></a>
                        <p><?= $item->status_comment ?></p>
                        <?php if ($item->status_file): ?>
                        <div class="">
                            <ul class="list-unstyled">
                                <li><a href="<?= BASE."transaction/download/".$item->status_file; ?>"><i
                                            class="fa fa-paperclip" aria-hidden="true"></i>
                                        <?= $item->status_file;?></a></li>
                            </ul>
                        </div>
                        <?php endif; ?>
                        <hr class="mb-1" />
                        <p class="font-italic "><?= $item->first_name . " " .$item->last_name;?></p>
                    </li>
                    <?php endforeach; ?>
                    <?php endif; ?>
                </ul>
            </div>
        </div>
    </div>
</div>
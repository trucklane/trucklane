<div class="modal fade" id="trans_cargo_modal" data-keyboard="false" data-backdrop="static" tabindex="-1"  aria-hidden="true" role="dialog" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated fadeIn">
            <div class="modal-header">
                <div class="col-md-9 "><h3></h3></div>
                <div class="col-md-3 "> <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> </div>
            </div>
            <div class="modal-body">
                <form name="temp-form-cargo">
                    <div>
                        <div class="form-row ">    
                            <div class="form-group col-md-12">
                                <label for="inputEmail4">No. of pkgs</label>
                                <input type="number" class="form-control mb-2" name="temp_cargo-no-of-pkgs" placeholder="">
                            </div>      
                        </div>
                        <div class="form-row ">    
                            <div class="form-group col-md-12">
                                <label for="inputEmail4">Gross weight</label>
                                <input type="number" class="form-control mb-2" name="temp_cargo-gross-wt" placeholder="">
                            </div>      
                        </div>
                        <div class="form-row">    
                            <div class="form-group col-md-12">
                                <label for="inputEmail4">Chargeable weight</label>
                                <input type="number" class="form-control mb-2" name="temp_cargo-chargeable-wt" placeholder="">
                            </div>      
                        </div>
                        <div class="form-row">    
                            <div class="form-group col-md-12">
                                <label for="inputEmail4">Width</label>
                                <input type="number" class="form-control mb-2" name="temp_cargo-width" placeholder="">
                            </div>      
                        </div>
                        <div class="form-row">    
                            <div class="form-group col-md-12">
                                <label for="inputEmail4">Length</label>
                                <input type="number" class="form-control mb-2" name="temp_cargo-length" placeholder="">
                            </div>      
                        </div>
                        <div class="form-row">    
                            <div class="form-group col-md-12">
                                <label for="inputEmail4">Height</label>
                                <input type="number" class="form-control mb-2" name="temp_cargo-height" placeholder="">
                            </div>      
                        </div>
                        <div class="form-row">    
                            <div class="form-group col-md-12">
                                <label for="inputEmail4">Cbm</label>
                                <input type="number" class="form-control mb-2" name="temp_cargo-cbm" placeholder="">
                            </div>      
                        </div>
                        <div class="form-row">    
                            <div class="form-group col-md-12 select-parent">
                                <label for="inputEmail4">Truck type</label>
                                <select class="modal-select form-control " name="temp_cargo-truck-type" >
                                    <option value="" disabled="disabled" selected="selected" class="hidden"></option>
                                    <?php foreach ($vehicles as $vehicle): ?>
                                        <option value="<?= $vehicle->vehicle_code; ?>"><?= $vehicle->vehicle_code." - ".$vehicle->vehicle_desc; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>      
                        </div>
                        <div class="form-row">    
                            <div class="form-group col-md-12">
                                <label for="inputEmail4">No. of trucks</label>
                                <input type="number" class="form-control mb-2" name="temp_cargo-no-of-trucks" placeholder="">
                            </div>      
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn-temp-add-cargo">Add</button>
            </div>
        </div>
    </div>
</div>

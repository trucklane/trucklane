<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-6">
    <h2><i class="fa fa-list"></i> Transactions</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">Home</a>
            </li>
            <li class="breadcrumb-item">
                <span>Transaction</span>
            </li>
            <li class="breadcrumb-item active">
                <strong>Entry - <?= $title; ?></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-6">
        <h2></h2>
        <div class="form-group pull-right">
            <button class="btn-reset-entry btn btn-info btn-sm mx-1" id="btn_reset_entry" >Reset</a>
            <button class="btn-save-entry btn btn-primary btn-sm mx-1" id="btn_save_entry" >Save</a>
            <button class="btn-post-entry btn btn-warning btn-sm mx-1" id="btn_post_entry">Post</button>
        </div>
    </div>
</div>
<br/>

<div class="wrapper wrapper-content animated white-bg ecommerce pb-5">
    <div class="row pb-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <form name="entry-form" accept-charset="utf-8">
                        <input type="hidden" name="service-type" value="<?= strtoupper($service_type); ?>">
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="inputEmail4">Shipper/Export</label>
                                <select class="select-shipper form-control form-select-multiple" name="shipper[]" >
                                    <option value="" disabled="disabled" selected="selected" class="hidden"></option>
                                    <?php foreach ($shippers as $shipper): ?>
                                        <option value="<?= $shipper->partner_code ?>"><?= "{$shipper->partner_code} - {$shipper->partner_name}" ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <input disabled="disabled" name="shippers_obj" type="hidden" value='<?= json_encode($shippers) ?>' />
                                <textarea disabled="disabled" class="disabled form-control mt-3" name="shipper-address" id="shipper_address" rows="4"></textarea>
                            </div>
                            <div class="form-group col-md-6">
                                <div>
                                    <label for="inputEmail4">Master reference</label>
                                    <select class="form-control selet-tags" name="master-bill-no">
                                        <option value="" disabled="disabled" selected="selected" class="hidden"></option>
                                        <?php foreach ($master_refs as $ref): ?>
                                        <option value="<?= $ref->master_ref ?>"><?= $ref->master_ref ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="form-check my-3">
                                    <input type="checkbox" class="form-check-input" name="direct" id="direct">
                                    <label class="form-check-label" for="exampleCheck1">Direct</label>
                                </div>
                                <div>
                                    <label for="inputPassword4">House reference</label>
                                    <input name="house-bill-no" type="text" class="form-control" id="house-bill-no" placeholder="">
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="inputEmail4">Consignee</label>
                                <select class="select-consignee form-control form-select-multiple" name="consignee[]">
                                    <option value="" disabled="disabled" selected="selected" class="hidden"></option>
                                    <?php foreach ($consignees as $consignee): ?>
                                        <option value="<?= $consignee->partner_code ?>"><?= "{$consignee->partner_code} - {$consignee->partner_name}" ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <input disabled="disabled" name="consignee_obj" type="hidden" value='<?= json_encode($consignees) ?>' />
                                <textarea disabled="disabled" class="disabled form-control mt-3" name="consignee-address" id="consignee" rows="3"></textarea>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputEmail4">To release cargo, please contact:</label>
                                <select class="select-agent form-control form-select-multiple" name="agent" >
                                        <option value="" disabled="disabled" selected="selected" class="hidden"></option>
                                    <?php foreach ($agents as $agent): ?>
                                        <option value="<?= $agent->partner_code ?>"><?= "{$agent->partner_code} - {$agent->partner_name}" ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <textarea disabled="disabled" class="disabled form-control mt-3" name="agent-address" id="agent_address" rows="3"></textarea>
                            </div>
                        </div>
                        <input disabled="disabled" name="agent_obj" type="hidden" value='<?= json_encode($agents) ?>' />
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="inputEmail4">Notify party</label>
                                <select class="select-agent1-notify form-control form-select-multiple" name="agent1" >
                                    <option value="" disabled="disabled" selected="selected" class="hidden"></option>
                                    <?php foreach ($agents as $agent): ?>
                                        <option value="<?= $agent->partner_code ?>"><?= "{$agent->partner_code} - {$agent->partner_name}" ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <textarea disabled="disabled" class="disabled form-control mt-3" name="agent1-address" id="agent1_address" rows="3"></textarea>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputEmail4">Also notify</label>
                                <select class="select-agent2-notify form-control form-select-multiple" name="agent2" >
                                    <option value="" disabled="disabled" selected="selected" class="hidden"></option>
                                    <?php foreach ($agents as $agent): ?>
                                        <option value="<?= $agent->partner_code ?>"><?= "{$agent->partner_code} - {$agent->partner_name}" ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <textarea disabled="disabled" class="disabled form-control mt-3" name="agent2-address" id="agent2_address" rows="3"></textarea>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="inputEmail4">Transaction status</label>
                                <select class="form-control form-select-multiple" name="trans-status" >
                                    <option value="" disabled="disabled" selected="selected" class="hidden"></option>
                                    <?php foreach ($status as $st): ?>
                                        <option value="<?= $st->status_code ?>"><?= "{$st->status_name} - {$st->status_description}" ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputEmail4">Transaction status date</label>
                                <input autocomplete="off" type="text" class="form-control status-date" id="status_date" name="status-date" placeholder="">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="inputEmail4">Vessel/Voyage</label>
                                <input type="text" class="form-control " id="vessel_voyage" name="vessel-voyage" placeholder="">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputEmail4">Carriers</label>
                                <select class="form-control form-select-multiple" name="carriers" >
                                    <option value="" disabled="disabled" selected="selected" class="hidden"></option>
                                    <?php foreach ($carriers as $carrier): ?>
                                        <option value="<?= $carrier->carrier_code ?>"><?= "{$carrier->carrier_code} - {$carrier->carrier_name}" ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        
                        <div class="form-row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="inputEmail4">Estimate time of departure</label>
                                    <input autocomplete="off" type="text" class="form-control date" id="etd" name="etd" placeholder="">
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail4">Estimate time of arrival</label>
                                    <input autocomplete="off" type="text" class="form-control date" id="eta" name="eta" placeholder="">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="inputEmail4">Actual time of departure</label>
                                    <input autocomplete="off" type="text" class="form-control date" id="atd" name="atd" placeholder="">
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail4">Actual time of arrival</label>
                                    <input autocomplete="off" type="text" class="form-control date" id="ata" name="ata" placeholder="">
                                </div>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="inputEmail4">Country of origin</label>
                                    <select class="form-control form-select-multiple" name="origin-destination" >
                                        <option value="" disabled="disabled" selected="selected" class="hidden"></option>
                                        <?php foreach ($countries as $country): ?>
                                            <option value="<?= $country->country_code; ?>"><?= $country->country_code." - ".$country->country_name; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail4">Port of origin</label>
                                    <select class="form-control form-select-multiple disabled" name="port-of-loading" disabled="disabled">
                                        
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group ">
                                    <label for="inputEmail4">Country of destination</label>
                                    <select class="form-control form-select-multiple" name="final-destination" >
                                        <option value="" disabled="disabled" selected="selected" class="hidden"></option>
                                        <?php foreach ($countries as $country): ?>
                                            <option value="<?= $country->country_code; ?>"><?= $country->country_code." - ".$country->country_name; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail4">Port of destination</label>
                                    <select class="form-control form-select-multiple disabled" name="port-of-discharge" disabled="disabled">
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="attr-container mt-4">
                            <div class="btn-container my-3 clearfix">
                                <button type="button" class="pull-right btn btn-primary btn-add-attr"><i class="fa fa-plus"></i></button>
                            </div>
                            <table class="table table-bordered table-hover" id="transaction_attr">
                                <thead class="thead-light">
                                    <tr>
                                        <th>Container no.</th>
                                        <th>Seal no.</th>
                                        <th>No. of pkgs</th>
                                        <th>Gross wt.</th>
                                        <th>CBM</th>
                                        <th>Size/Type</th>
                                        <th>FCL/LCL</th>
                                        <th>Description</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody >

                                    
                                </tbody>
                            </table>
                        </div>
                        
                        
                    
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="trans_attr_modal" data-keyboard="false" data-backdrop="static" tabindex="-1"  aria-hidden="true" role="dialog" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated fadeIn">
            <div class="modal-header">
                <div class="col-md-9 "><h3></h3></div>
                <div class="col-md-3 "> <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> </div>
            </div>
            <div class="modal-body">
                <form name="temp-form-attr">
                    <div>
                        <div class="form-row ">    
                            <div class="form-group col-md-12">
                                <label for="inputEmail4">Container no.</label>
                                <input type="text" class="form-control mb-2" name="temp_container-num" placeholder="">
                            </div>      
                        </div>
                        <div class="form-row ">    
                            <div class="form-group col-md-12">
                                <label for="inputEmail4">Seal no.</label>
                                <input type="text" class="form-control mb-2" name="temp_seal-num" placeholder="">
                            </div>      
                        </div>
                        <div class="form-row">    
                            <div class="form-group col-md-12">
                                <label for="inputEmail4">No. of pkgs</label>
                                <input type="text" class="form-control mb-2" name="temp_pkgs" placeholder="">
                            </div>      
                        </div>
                        <div class="form-row">    
                            <div class="form-group col-md-12">
                                <label for="inputEmail4">Gross weight</label>
                                <input type="text" class="form-control mb-2" name="temp_weight" placeholder="">
                            </div>      
                        </div>
                        
                        <div class="form-row">
                            <div class="form-group col">
                                <label for="inputEmail4">CBM</label>
                                <input type="text" class="form-control mb-2" name="temp_cbm" placeholder="">
                            </div>   
                        </div>
                        <div class="form-row">
                            <div class="form-group col">
                                <label for="inputEmail4">Size/Type</label>
                                <input type="text" class="form-control mb-2" name="temp_size-type" placeholder="">
                            </div>      
                        </div>
                        <div class="form-row">
                            <div class="form-group col">
                                <label for="inputEmail4">FCL/LCL</label>
                                <input type="text" class="form-control mb-2" name="temp_fcl-lcl" placeholder="">
                            </div>     
                        </div>
                        <div class="form-row">
                            <div class="form-group col">
                                <label for="inputEmail4">Description</label>
                                <textarea class="form-control" id="description" name="temp_description" rows="5"></textarea>
                            </div>      
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn-temp-add">Add</button>
            </div>
        </div>
    </div>
</div>


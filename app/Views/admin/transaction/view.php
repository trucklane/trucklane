<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-6">
    <h2><i class="fa fa-list"></i> Transaction View - <?= $trxn_type; ?></h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">Home</a>
            </li>
            <li class="breadcrumb-item">
                <span>Transaction</span>
            </li>
            <li class="breadcrumb-item active">
                <strong>View</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-6">
        <h2></h2>
        
        <div class="form-group pull-right">

            <a href='<?= base_url("transaction/index") ?>' class="ml-1 btn btn-light btn-sm" >Back to list</a>
            <?php if ($transaction->status == "S") : ?>
            <a href='<?= base_url("transaction/{$transaction->id}/edit") ?>' class="ml-1 btn btn-warning btn-sm" >Edit</a>
            <?php endif; ?>
        </div>
    </div>
</div>
<br/>

<div class="wrapper wrapper-content animated white-bg ecommerce pb-5">
    <div class="row">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label for="staticEmail" class="col-sm-2 col-form-label">Shipper</label>
                                <div class="col-sm-10">
                                    <input disabled="disabled" type="text" readonly class="form-control-plaintext" id="static_shipper" value="<?= $shipper->partner_name; ?>">
                                    <textarea disabled="disabled" class="disabled form-control mt-3" name="shipper-address" id="shipper_address" rows="4"><?= $shipper_address; ?></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label for="staticEmail" class="col-sm-3 col-form-label">Master Reference</label>
                                <label class="col-sm-1 text-center col-form-label">:</label>
                                <div class="col-sm-8">
                                    <input disabled="disabled" type="text" readonly class="form-control-plaintext" id="static_master_ref" value="<?= $transaction->master_ref; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-check">
                                    <input disabled="disabled" type="checkbox" class="form-check-input" name="direct" id="direct" />
                                    <label class="form-check-label" for="exampleCheck1">Direct</label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="staticEmail" class="col-sm-3 col-form-label">House reference</label>
                                <label class="col-sm-1 text-center col-form-label">:</label>
                                <div class="col-sm-8">
                                    <input disabled="disabled" type="text" readonly class="form-control-plaintext" id="static_house_ref" value="<?= $transaction->house_ref; ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label for="staticEmail" class="col-sm-2 col-form-label">Consignee</label>
                                <div class="col-sm-10">
                                    <input disabled="disabled" type="text" readonly class="form-control-plaintext" id="static_consignee" value="<?= $consignee->partner_name; ?>">
                                    <textarea disabled="disabled" class="disabled form-control mt-3" name="consignee-address" id="consignee" rows="3"><?= $consignee_address; ?></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label for="staticEmail" class="col-sm-2 col-form-label">Agent</label>
                                <div class="col-sm-10">
                                    <input disabled="disabled" type="text" readonly class="form-control-plaintext" id="static_agent" value="<?= $agent_name; ?>">
                                    <textarea disabled="disabled" class="disabled form-control mt-3" name="agent1-address" id="agent1_address" rows="3"><?= $agent_address; ?></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label for="staticEmail" class="col-sm-2 col-form-label">Notify party</label>
                                <div class="col-sm-10">
                                    <input disabled="disabled" type="text" readonly class="form-control-plaintext" id="static_agent_1"value="">
                                    <?php //$agentAddress1 = "{$agent1->address1} {$agent1->address2} {$agent1->address3} {$agent1->zipcode} {$agent1->country_code}"?>
                                    <textarea disabled="disabled" class="disabled form-control mt-3" name="agent1-address" id="agent1_address" rows="3"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label for="staticEmail" class="col-sm-2 col-form-label">Also notify</label>
                                <div class="col-sm-10">
                                    <input disabled="disabled" type="text" readonly class="form-control-plaintext" id="static_agent_2" value="">
                                    <?php //$agent2Address = "{$agent2->address1} {$agent2->address2} {$agent2->address3} {$agent2->zipcode} {$agent2->country_code}"?>
                                    <textarea disabled="disabled" class="disabled form-control mt-3" name="agent2-address" id="agent2_address" rows="3"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br/>
                    <form name="entry-form" accept-charset="utf-8">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-3 col-form-label">Created by</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <?php if (isset($created_by->first_name) && isset($created_by->last_name)): ?>
                                        <input disabled="disabled" type="text" readonly class="form-control-plaintext" id="created_by" value="<?= $created_by->first_name . " " .$created_by->last_name ; ?>">
                                        <?php else: ?>
                                        <input disabled="disabled" type="text" readonly class="form-control-plaintext" id="created_by" value="">
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-3 col-form-label">Current status</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input disabled="disabled" type="text" readonly class="form-control-plaintext" id="current_status" value="<?= "{$current_status->status_name} - {$current_status->status_description}" ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-3 col-form-label">Vessel</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input disabled="disabled" type="text" readonly class="form-control-plaintext" id="vessel" value="<?= $transaction->vessel; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-3 col-form-label">Carrier</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <?php if (isset($carrier->carrier_code)) : ?>
                                        <input disabled="disabled" type="text" readonly class="form-control-plaintext" id="carrier" value="<?= $carrier->carrier_code. " - ".$carrier->carrier_name; ?>">
                                        <?php else: ?>
                                        <input disabled="disabled" type="text" readonly class="form-control-plaintext" id="carrier" value="">
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <?php if (strtolower($transaction->service_type) == "ia"): ?>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="inputEmail4" class="col-sm-3 col-form-label">Inco term</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input disabled="disabled" type="text" class="form-control-plaintext" readonly id="inco_term" name="inco-term" placeholder="" value="<?= $inco_term->incoterm_code ." - ". $inco_term->incoterm_desc; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-3 col-form-label">Mode of shipment</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control-plaintext " id="mode_of_shipment" name="mode-of-shipment" placeholder="" value="<?= $transaction->mode_of_shpt; ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="inputEmail4" class="col-sm-3 col-form-label">Gross weight</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input type="number" class="form-control-plaintext " id="gross_wt" name="gross-wt" placeholder="" value="<?= $transaction->gross_wt ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="inputEmail4" class="col-sm-3 col-form-label">Chargeable weight</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input type="number" class="form-control-plaintext " id="chargeable_weight" name="chargeable-weight" placeholder="" value="<?= $transaction->chargeable_wt ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="inputEmail4" class="col-sm-3 col-form-label">Uom</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control-plaintext " id="uom" name="uom" placeholder="" value="<?= $transaction->uom ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="inputEmail4" class="col-sm-3 col-form-label">Forwarder</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control-plaintext " id="forwarder" name="forwarder" placeholder="" value="<?= $transaction->forwarder ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="inputEmail4" class="col-sm-3 col-form-label">Supp invoice number</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control-plaintext " id="supp_invoice_number" name="supp-invoice-number" placeholder="" value="<?= $transaction->supp_invoice_number ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="inputEmail4" class="col-sm-3 col-form-label">Invoice value</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control-plaintext " id="invoice_value" name="invoice-value" placeholder="" value="<?= $transaction->invoice_value ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="inputEmail4" class="col-sm-3 col-form-label">Invoice currency</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control-plaintext " id="invoice_currency" name="invoice-currency" placeholder="" value="<?= $transaction->invoice_currency ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="inputEmail4" class="col-sm-3 col-form-label">Import entry no</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control-plaintext " id="import_entry_no" name="import-entry-no" placeholder="" value="<?= $transaction->import_entry_no; ?>">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <?php endif; ?>

                        <?php if (strtolower($transaction->service_type) == "ea"): ?>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="inputEmail4" class="col-sm-3 col-form-label">Inco term</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input disabled="disabled" type="text" class="form-control-plaintext" readonly id="inco_term" name="inco-term" placeholder="" value="<?= $inco_term->incoterm_code ." - ". $inco_term->incoterm_desc; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-3 col-form-label">Mode of shipment</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control-plaintext " id="mode_of_shipment" name="mode-of-shipment" placeholder="" value="<?= $transaction->mode_of_shpt; ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="inputEmail4" class="col-sm-3 col-form-label">Gross weight</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input type="number" class="form-control-plaintext " id="gross_wt" name="gross-wt" placeholder="" value="<?= $transaction->gross_wt ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="inputEmail4" class="col-sm-3 col-form-label">Chargeable weight</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input type="number" class="form-control-plaintext " id="chargeable_weight" name="chargeable-weight" placeholder="" value="<?= $transaction->chargeable_wt ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="inputEmail4" class="col-sm-3 col-form-label">Uom</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control-plaintext " id="uom" name="uom" placeholder="" value="<?= $transaction->uom ?>">
                                    </div>
                                </div>
                            </div>
                           
                        </div>
                        <?php endif; ?>

                        <?php if (strtolower($transaction->service_type) == "ib"): ?>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="inputEmail4" class="col-sm-3 col-form-label">Inco term</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input disabled="disabled" type="text" class="form-control-plaintext" readonly id="inco_term" name="inco-term" placeholder="" value="<?= $inco_term->incoterm_code ." - ". $inco_term->incoterm_desc; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-3 col-form-label">Mode of shipment</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control-plaintext " id="mode_of_shipment" name="mode-of-shipment" placeholder="" value="<?= $transaction->mode_of_shpt; ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="inputEmail4" class="col-sm-3 col-form-label">Gross weight</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input type="number" class="form-control-plaintext " id="gross_wt" name="gross-wt" placeholder="" value="<?= $transaction->gross_wt ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="inputEmail4" class="col-sm-3 col-form-label">Chargeable weight</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input type="number" class="form-control-plaintext " id="chargeable_weight" name="chargeable-weight" placeholder="" value="<?= $transaction->chargeable_wt ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="inputEmail4" class="col-sm-3 col-form-label">Uom</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control-plaintext " id="uom" name="uom" placeholder="" value="<?= $transaction->uom ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="inputEmail4" class="col-sm-3 col-form-label">Forwarder</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control-plaintext " id="forwarder" name="forwarder" placeholder="" value="<?= $transaction->forwarder ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="inputEmail4" class="col-sm-3 col-form-label">Supp invoice number</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control-plaintext " id="supp_invoice_number" name="supp-invoice-number" placeholder="" value="<?= $transaction->supp_invoice_number ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="inputEmail4" class="col-sm-3 col-form-label">Invoice value</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control-plaintext " id="invoice_value" name="invoice-value" placeholder="" value="<?= $transaction->invoice_value ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="inputEmail4" class="col-sm-3 col-form-label">Invoice currency</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control-plaintext " id="invoice_currency" name="invoice-currency" placeholder="" value="<?= $transaction->invoice_currency ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="inputEmail4" class="col-sm-3 col-form-label">Import entry no</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control-plaintext " id="import_entry_no" name="import-entry-no" placeholder="" value="<?= $transaction->import_entry_no; ?>">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="inputEmail4" class="col-sm-3 col-form-label">Load type</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control-plaintext " id="load_type" name="load-type" placeholder="" value="<?= $transaction->load_type; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="inputEmail4" class="col-sm-3 col-form-label">NTA ICC Number</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control-plaintext " id="nta_icc_no" name="nta-icc-no" placeholder="" value="<?= $transaction->nta_icc_number; ?>">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="inputEmail4" class="col-sm-3 col-form-label">PEZA Permit</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control-plaintext " id="peza_permit" name="peza-permit" placeholder="" <?= $transaction->peza_permit; ?>>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="inputEmail4" class="col-sm-3 col-form-label">Custom value</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input type="number" class="form-control-plaintext " id="custom_value" name="custom-value" placeholder="" <?= $transaction->custom_value; ?>>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="inputEmail4" class="col-sm-3 col-form-label">Custom Duties</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input type="number" class="form-control-plaintext " id="custom_duties" name="custom-duties" placeholder="" <?= $transaction->custom_duties; ?>>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="inputEmail4" class="col-sm-3 col-form-label">VAT value</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input type="number" class="form-control-plaintext " id="vat_value" name="vat-value" placeholder="" <?= $transaction->vat_value; ?>>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="inputEmail4" class="col-sm-3 col-form-label">Date customs cleared</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input autocomplete="off" type="text" class="form-control-plaintext date" id="date_customs_cleared" name="date-customs-cleared" placeholder="" <?= date("m/d/Y", strtotime($transaction->date_customs_cleared)); ?>> 
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="inputEmail4" class="col-sm-3 col-form-label">No of containers</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control-plaintext " id="no_of_containers" name="no-of-containers" placeholder="" <?= $transaction->no_of_containers; ?>>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="inputEmail4" class="col-sm-3 col-form-label">Warehouse receive date</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input autocomplete="off" type="text" class="form-control-plaintext date" id="warehouse_rcv_date" name="warehouse-rcv-date" placeholder="" <?= date("m/d/Y", strtotime($transaction->warehouse_rcv_date)); ?>>
                                    </div>
                                </div>
                            </div>
                           
                        </div>
                        
                     
                       
                        <?php endif; ?>
                        <?php if (strtolower($transaction->service_type) == "eb"): ?>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="inputEmail4" class="col-sm-3 col-form-label">Inco term</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input disabled="disabled" type="text" class="form-control-plaintext" readonly id="inco_term" name="inco-term" placeholder="" value="<?= $inco_term->incoterm_code ." - ". $inco_term->incoterm_desc; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-3 col-form-label">Mode of shipment</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control-plaintext " id="mode_of_shipment" name="mode-of-shipment" placeholder="" value="<?= $transaction->mode_of_shpt; ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="inputEmail4" class="col-sm-3 col-form-label">Gross weight</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input type="number" class="form-control-plaintext " id="gross_wt" name="gross-wt" placeholder="" value="<?= $transaction->gross_wt ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="inputEmail4" class="col-sm-3 col-form-label">Chargeable weight</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input type="number" class="form-control-plaintext " id="chargeable_weight" name="chargeable-weight" placeholder="" value="<?= $transaction->chargeable_wt ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="inputEmail4" class="col-sm-3 col-form-label">Uom</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control-plaintext " id="uom" name="uom" placeholder="" value="<?= $transaction->uom ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="inputEmail4" class="col-sm-3 col-form-label">Forwarder</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control-plaintext " id="forwarder" name="forwarder" placeholder="" value="<?= $transaction->forwarder ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="inputEmail4" class="col-sm-3 col-form-label">Supp invoice number</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control-plaintext " id="supp_invoice_number" name="supp-invoice-number" placeholder="" value="<?= $transaction->supp_invoice_number ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="inputEmail4" class="col-sm-3 col-form-label">Invoice value</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control-plaintext " id="invoice_value" name="invoice-value" placeholder="" value="<?= $transaction->invoice_value ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="inputEmail4" class="col-sm-3 col-form-label">Invoice currency</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control-plaintext " id="invoice_currency" name="invoice-currency" placeholder="" value="<?= $transaction->invoice_currency ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="inputEmail4" class="col-sm-3 col-form-label">Import entry no</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control-plaintext " id="import_entry_no" name="import-entry-no" placeholder="" value="<?= $transaction->import_entry_no; ?>">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="inputEmail4" class="col-sm-3 col-form-label">Load type</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control-plaintext " id="load_type" name="load-type" placeholder="" value="<?= $transaction->load_type; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="inputEmail4" class="col-sm-3 col-form-label">NTA ICC Number</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control-plaintext " id="nta_icc_no" name="nta-icc-no" placeholder="" value="<?= $transaction->nta_icc_number; ?>">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="inputEmail4" class="col-sm-3 col-form-label">PEZA Permit</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control-plaintext " id="peza_permit" name="peza-permit" placeholder="" <?= $transaction->peza_permit; ?>>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="inputEmail4" class="col-sm-3 col-form-label">Custom value</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input type="number" class="form-control-plaintext " id="custom_value" name="custom-value" placeholder="" <?= $transaction->custom_value; ?>>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="inputEmail4" class="col-sm-3 col-form-label">Custom Duties</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input type="number" class="form-control-plaintext " id="custom_duties" name="custom-duties" placeholder="" <?= $transaction->custom_duties; ?>>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="inputEmail4" class="col-sm-3 col-form-label">VAT value</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input type="number" class="form-control-plaintext " id="vat_value" name="vat-value" placeholder="" <?= $transaction->vat_value; ?>>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="inputEmail4" class="col-sm-3 col-form-label">Date customs cleared</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input autocomplete="off" type="text" class="form-control-plaintext date" id="date_customs_cleared" name="date-customs-cleared" placeholder="" <?= date("m/d/Y", strtotime($transaction->date_customs_cleared)); ?>> 
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="inputEmail4" class="col-sm-3 col-form-label">No of containers</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control-plaintext " id="no_of_containers" name="no-of-containers" placeholder="" <?= $transaction->no_of_containers; ?>>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php endif; ?>

                        <?php if (strtolower($transaction->service_type) == "es"): ?>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="inputEmail4" class="col-sm-3 col-form-label">Inco term</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input disabled="disabled" type="text" class="form-control-plaintext" readonly id="inco_term" name="inco-term" placeholder="" value="<?= $inco_term->incoterm_code ." - ". $inco_term->incoterm_desc; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-3 col-form-label">Mode of shipment</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control-plaintext " id="mode_of_shipment" name="mode-of-shipment" placeholder="" value="<?= $transaction->mode_of_shpt; ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="inputEmail4" class="col-sm-3 col-form-label">Gross weight</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input type="number" class="form-control-plaintext " id="gross_wt" name="gross-wt" placeholder="" value="<?= $transaction->gross_wt ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="inputEmail4" class="col-sm-3 col-form-label">Chargeable weight</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input type="number" class="form-control-plaintext " id="chargeable_weight" name="chargeable-weight" placeholder="" value="<?= $transaction->chargeable_wt ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="inputEmail4" class="col-sm-3 col-form-label">Uom</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control-plaintext " id="uom" name="uom" placeholder="" value="<?= $transaction->uom ?>">
                                    </div>
                                </div>
                            </div>
                           
                        </div>
                        <?php endif; ?>
                        
                        <?php if (strtolower($transaction->service_type) == "is"): ?>
                        
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="inputEmail4" class="col-sm-3 col-form-label">Inco term</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input disabled="disabled" type="text" class="form-control-plaintext" readonly id="inco_term" name="inco-term" placeholder="" value="<?= $inco_term->incoterm_code ." - ". $inco_term->incoterm_desc; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-3 col-form-label">Mode of shipment</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control-plaintext " id="mode_of_shipment" name="mode-of-shipment" placeholder="" value="<?= $transaction->mode_of_shpt; ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="inputEmail4" class="col-sm-3 col-form-label">Gross weight</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input type="number" class="form-control-plaintext " id="gross_wt" name="gross-wt" placeholder="" value="<?= $transaction->gross_wt ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="inputEmail4" class="col-sm-3 col-form-label">Chargeable weight</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input type="number" class="form-control-plaintext " id="chargeable_weight" name="chargeable-weight" placeholder="" value="<?= $transaction->chargeable_wt ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="inputEmail4" class="col-sm-3 col-form-label">Uom</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control-plaintext " id="uom" name="uom" placeholder="" value="<?= $transaction->uom ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="inputEmail4" class="col-sm-3 col-form-label">Forwarder</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control-plaintext " id="forwarder" name="forwarder" placeholder="" value="<?= $transaction->forwarder ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="inputEmail4" class="col-sm-3 col-form-label">Supp invoice number</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control-plaintext " id="supp_invoice_number" name="supp-invoice-number" placeholder="" value="<?= $transaction->supp_invoice_number ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="inputEmail4" class="col-sm-3 col-form-label">Invoice value</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control-plaintext " id="invoice_value" name="invoice-value" placeholder="" value="<?= $transaction->invoice_value ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="inputEmail4" class="col-sm-3 col-form-label">Invoice currency</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control-plaintext " id="invoice_currency" name="invoice-currency" placeholder="" value="<?= $transaction->invoice_currency ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="inputEmail4" class="col-sm-3 col-form-label">Import entry no</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control-plaintext " id="import_entry_no" name="import-entry-no" placeholder="" value="<?= $transaction->import_entry_no; ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php endif; ?>
                        <?php if (strtolower($transaction->service_type) == "ad"): ?>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="inputEmail4" class="col-sm-3 col-form-label">Inco term</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input disabled="disabled" type="text" class="form-control-plaintext" readonly id="inco_term" name="inco-term" placeholder="" value="<?= $inco_term->incoterm_code ." - ". $inco_term->incoterm_desc; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-3 col-form-label">Mode of shipment</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control-plaintext " id="mode_of_shipment" name="mode-of-shipment" placeholder="" value="<?= $transaction->mode_of_shpt; ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="inputEmail4" class="col-sm-3 col-form-label">Gross weight</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input type="number" class="form-control-plaintext " id="gross_wt" name="gross-wt" placeholder="" value="<?= $transaction->gross_wt ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="inputEmail4" class="col-sm-3 col-form-label">Chargeable weight</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input type="number" class="form-control-plaintext " id="chargeable_weight" name="chargeable-weight" placeholder="" value="<?= $transaction->chargeable_wt ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="inputEmail4" class="col-sm-3 col-form-label">Uom</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control-plaintext " id="uom" name="uom" placeholder="" value="<?= $transaction->uom ?>">
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        <?php endif; ?>
                        <?php if (strtolower($transaction->service_type) == "sd"): ?>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="inputEmail4" class="col-sm-3 col-form-label">Inco term</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input disabled="disabled" type="text" class="form-control-plaintext" readonly id="inco_term" name="inco-term" placeholder="" value="<?= $inco_term->incoterm_code ." - ". $inco_term->incoterm_desc; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-3 col-form-label">Mode of shipment</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control-plaintext " id="mode_of_shipment" name="mode-of-shipment" placeholder="" value="<?= $transaction->mode_of_shpt; ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="inputEmail4" class="col-sm-3 col-form-label">Gross weight</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input type="number" class="form-control-plaintext " id="gross_wt" name="gross-wt" placeholder="" value="<?= $transaction->gross_wt ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="inputEmail4" class="col-sm-3 col-form-label">Chargeable weight</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input type="number" class="form-control-plaintext " id="chargeable_weight" name="chargeable-weight" placeholder="" value="<?= $transaction->chargeable_wt ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="inputEmail4" class="col-sm-3 col-form-label">Uom</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control-plaintext " id="uom" name="uom" placeholder="" value="<?= $transaction->uom ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="inputEmail4" class="col-sm-3 col-form-label">Load type</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control-plaintext " id="load_type" name="load-type" placeholder="" value="<?= $transaction->load_type; ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="inputEmail4" class="col-sm-3 col-form-label">No of containers</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control-plaintext " id="no_of_containers" name="no-of-containers" placeholder="" <?= $transaction->no_of_containers; ?>>
                                    </div>
                                </div>
                            </div>
                            
                        </div>

                        <?php endif; ?>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-3 col-form-label">Estimate time of departure</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input disabled="disabled" type="text" readonly class="form-control-plaintext" id="etd" value="<?= date("F d, Y ", strtotime($transaction->etd)); ?>">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-3 col-form-label">Estimate time of arrival</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input disabled="disabled" type="text" readonly class="form-control-plaintext" id="eta" value="<?= date("F d, Y ", strtotime($transaction->eta)); ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-3 col-form-label">Actual time of departure</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input disabled="disabled" type="text" readonly class="form-control-plaintext" id="atd" value="<?= date("F d, Y ", strtotime($transaction->atd)); ?>">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-3 col-form-label">Actual time of arrival</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input disabled="disabled" type="text" readonly class="form-control-plaintext" id="ata" value="<?= date("F d, Y ", strtotime($transaction->ata)); ?>">
                                    </div>
                                </div>
                            </div>
                        </div>

                        
                        <?php if (isset($origin) && $origin):?>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-3 col-form-label">Country of origin</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input disabled="disabled" type="text" readonly class="form-control-plaintext" id="origin_destination" value="<?= $origin->country_code." - ".$origin->country_name; ?>">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-3 col-form-label">Port of origin</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input disabled="disabled" type="text" readonly class="form-control-plaintext" id="loading_port" value="<?= $loading->port_code." - ".$loading->port_name; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-3 col-form-label">Country of destination</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input disabled="disabled" type="text" readonly class="form-control-plaintext" id="final_destination" value="<?= $final->country_code." - ".$final->country_name; ?>">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-3 col-form-label">Port of destination</label>
                                    <label class="col-sm-1 text-center col-form-label">:</label>
                                    <div class="col-sm-8">
                                        <input disabled="disabled"type="text" readonly class="form-control-plaintext" id="discharge_port" value="<?= $discharge->port_code." - ".$discharge->port_name; ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php endif;?>
                        <div class="form-row">
                            <div class="form-group col">
                                <label for="inputEmail4">General description</label>
                                <textarea class="form-control" id="general_description" name="general-description" rows="5">
                                <?= $transaction->general_desc?>
                                </textarea>
                            </div>      
                        </div>
                    
                        <ul class="nav nav-tabs" id="transaction_tabs" role="tablist">
                            <?php if ($charges): ?>
                            <li class="nav-item" role="presentation">
                                <a class="nav-link active" id="charges_tab" data-toggle="tab" href="#charges_content" role="tab" aria-controls="charges_content" aria-selected="false">Charges</a>
                            </li>
                            <?php endif; ?>
                            <?php if ($po_materials): ?>
                            <li class="nav-item" role="presentation">
                                <a class="nav-link " id="material_tab" data-toggle="tab" href="#material_content" role="tab" aria-controls="material_content" aria-selected="true">PO Material</a>
                            </li>
                            <?php endif; ?>
                            <?php if ($container_list): ?>
                            <li class="nav-item" role="presentation">
                                <a class="nav-link" id="container_tab" data-toggle="tab" href="#container_content" role="tab" aria-controls="container_content" aria-selected="false">Container list</a>
                            </li>
                            <?php endif; ?>
                            <?php if ($loose_cargo): ?>
                            <li class="nav-item" role="presentation">
                                <a class="nav-link" id="cargo_tab" data-toggle="tab" href="#cargo_content" role="tab" aria-controls="cargo_content" aria-selected="false">Loose cargo</a>
                            </li>
                            <?php endif; ?>
                        </ul>

                        <div class="tab-content" id="transaction_tab_content">
                            <?php if ($po_materials): ?>
                            <div class="mt-3 tab-pane fade show active in" id="material_content" role="tabpanel" aria-labelledby="material_tab">
                                <table class="table table-bordered table-hover" id="trxn_mtrl">
                                    <thead class="thead-light">
                                        <tr>
                                            <th>PO no.</th>
                                            <th>Material id</th>
                                            <th>Item declaration</th>
                                            <th>Order qty</th>
                                            <th>Actual ship</th>
                                            <th>Order balance</th>
                                            <th>Unit of measure</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($po_materials as $material):?>
                                            <tr>
                                                <td><?= $material->po_number; ?></td>
                                                <td><?= $material->material_id; ?></td>
                                                <td><?= $material->item_declaration; ?></td>
                                                <td><?= $material->order_qty; ?></td>
                                                <td><?= $material->actual_ship; ?></td>
                                                <td><?= $material->order_balance; ?></td>
                                                <td><?= $material->unit_of_measure; ?></td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                            <?php endif; ?>
                            <?php if ($container_list): ?>
                            <div class="mt-3 tab-pane fade " id="container_content" role="tabpanel" aria-labelledby="container_tab">
                                <table class="table table-bordered table-hover" id="trxn_cont">
                                    <thead class="thead-light">
                                        <tr>
                                            <th>Container no.</th>
                                            <th>Seal no.</th>
                                            <th>Size</th>
                                            <th>Type</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($container_list as $container):?>
                                            <tr>
                                                <td><?= $container->container_number; ?></td>
                                                <td><?= $container->seal_num; ?></td>
                                                <td><?= $container->size; ?></td>
                                                <td><?= $container->type; ?></td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                            <?php endif; ?>
                            <?php if ($loose_cargo): ?>
                            <div class="mt-3 tab-pane fade" id="cargo_content" role="tabpanel" aria-labelledby="cargo_tab">
                                <table class="table table-bordered table-hover" id="trxn_cargo">
                                    <thead class="thead-light">
                                        <tr>
                                            <th>No. of pkgs</th>
                                            <th>Gross weight</th>
                                            <th>Chargeable weight</th>
                                            <th>Width</th>
                                            <th>Length</th>
                                            <th>Height</th>
                                            <th>Cbm</th>
                                            <th>Truck type</th>
                                            <th>No. of trucks</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($loose_cargo as $cargo):?>
                                            <tr>
                                                <td><?= $cargo->no_of_package; ?></td>
                                                <td><?= $cargo->gross_wt; ?></td>
                                                <td><?= $cargo->chargeable_wt; ?></td>
                                                <td><?= $cargo->width; ?></td>
                                                <td><?= $cargo->length; ?></td>
                                                <td><?= $cargo->height; ?></td>
                                                <td><?= $cargo->cbm; ?></td>
                                                <td><?= $cargo->truck_type; ?></td>
                                                <td><?= $cargo->no_of_trucks; ?></td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                            <?php endif; ?>
                            <?php if ($charges): ?>
                            <div class="mt-3 tab-pane fade show active" id="charges_content" role="tabpanel" aria-labelledby="charges_tab">
                                <table class="table table-bordered table-hover" id="trxn_chrg">
                                    <thead class="thead-light">
                                        <tr>
                                            <th>Charge code</th>
                                            <th>Charge description</th>
                                            <th>Charge amount</th>
                                            <th>Currency</th>
                                            <th>Php value</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($charges as $charge):?>
                                            <tr>
                                                <td><?= $charge->chg_code; ?></td>
                                                <td><?= $charge->chg_desc; ?></td>
                                                <td><?= $charge->chg_amount; ?></td>
                                                <td><?= $charge->currency; ?></td>
                                                <td><?= $charge->php_value; ?></td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                            <?php endif; ?>
                        </div>


                    </form>
                </div>
            </div>
        </div>
        
      
       <?= $this->include('admin/transaction/transaction_history') ?>
    </div>
</div>

<div class="btn-container my-3 clearfix">
    <button type="button" class="pull-right btn btn-primary btn-add-cargo"><i class="fa fa-plus"></i></button>
</div>
<table class="table table-bordered table-hover" id="trxn_cargo">
    <thead class="thead-light">
        <tr>
            <th>No. of pkgs</th>
            <th>Gross weight</th>
            <th>Chargeable weight</th>
            <th>Width</th>
            <th>Length</th>
            <th>Height</th>
            <th>Cbm</th>
            <th>Truck type</th>
            <th>No. of trucks</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <?php if (isset($loose_cargo)): ?>
        <?php foreach ($loose_cargo as $cargo):?>
        <tr>
            <td>
                <?= $cargo->no_of_package; ?>
                <input type="hidden" name="cargo-no-of-pkgs[]" value="<?= $cargo->no_of_package; ?>">
            </td>
            <td>
                <?= $cargo->gross_wt; ?>
                <input type="hidden" name="cargo-gross-wt[]" value="<?= $cargo->gross_wt; ?>">
            </td>
            <td>
                <?= $cargo->chargeable_wt; ?>
                <input type="hidden" name="cargo-chargeable-wt[]" value="<?= $cargo->chargeable_wt; ?>">
            </td>
            <td>
                <?= $cargo->width; ?>
                <input type="hidden" name="cargo-width[]" value="<?= $cargo->width; ?>">
            </td>
            <td>
                <?= $cargo->length; ?>
                <input type="hidden" name="cargo-length[]" value="<?= $cargo->length; ?>">
            </td>
            <td>
                <?= $cargo->height; ?>
                <input type="hidden" name="cargo-height[]" value="<?= $cargo->height; ?>">
            </td>
            <td>
                <?= $cargo->cbm; ?>
                <input type="hidden" name="cargo-cbm[]" value="<?= $cargo->cbm; ?>">
            </td>
            <td>
                <?= $cargo->truck_type; ?>
                <input type="hidden" name="cargo-truck-type[]" value="<?= $cargo->truck_type; ?>">
            </td>
            <td>
                <?= $cargo->no_of_trucks; ?>
                <input type="hidden" name="cargo-no-of-trucks[]" value="<?= $cargo->no_of_trucks; ?>">
            </td>
            <td>
                <button type='button' class='btn btn-sm btn-warning btn-edit-cargo-row'><i class='fa fa-pencil'></i> Edit</button>
                <button type='button' class='btn btn-sm btn-danger btn-remove-row'><i class='fa fa-trash'></i> Delete</button>
            </td>
        </tr>
        <?php endforeach; ?>
        <?php endif; ?>
    </tbody>
</table>

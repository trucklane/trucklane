<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><i class="fa fa-bars"></i> Create Main Menu</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="index.html">Home</a>
            </li>
            <li class="breadcrumb-item">
                <a>Setting</a>
            </li>
            <li class="breadcrumb-item active">
                <strong>Menu</strong>
            </li>
        </ol>

    </div>
    <div class="col-lg-2">

    </div>
</div>
<div class="wrapper wrapper-content  animated ">
    <div class="row">
        <div class="col-sm-12">
            <div class="ibox">
                <div class="ibox-content">
                    <form class="m-t" role="form" id="menu-form" name="menu-form" data-toggle="validator">
                    <fieldset class="form-horizontal">
                        <div class="form-group row d-none">
                            <label class="col-sm-2 control-label">ID:</label>
                            <div class="col-sm-10">
                                <input type="text"  readonly class="form-control" id="menu_id" name="menu_id" placeholder="Menu Id" required="" value="<?=$menu_id?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 control-label">Name:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" placeholder="Menu Name" name="menu_name" id="menu_name" required="" value="<?=$menu_name?>">
                            </div>
                        </div>
                     
                        <div class="form-group row">
                            <label class="col-sm-2 control-label ">Icon:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" placeholder="Icon" name="icon" id="icon" required="" value="<?=$icon?>">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-2 control-label">Is Enabled:</label>
                            <div class="col-sm-10">
                                <select class="form-control" name="is_enabled" name="is_enabled" required="">
                                    <option <?=($is_enabled == 1)?'selected':''?> value="1">Yes</option>
                                    <option <?=($is_enabled == 0)?'selected':''?> value="0">No</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 control-label">Sort Number:</label>
                            <div class="col-sm-10">
                                <input type="Number" id="sort_num"  name="sort_num" required class="form-control" placeholder="Sort Number" value="<?=$sort?>">
                            </div>
                        </div>

                        <div class="form-group row pull-right">
                            <div class="col-sm-12 ">
                                <button class="btn btn-primary btn-save" type="button"><i class="fa fa-save"></i> Save</button>
                                <button class="btn btn-default btn-menu-back" type="button"><i class="fa fa-mail-reply"></i> Back</button>
                                
                            </div>
                        </div>
                    </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

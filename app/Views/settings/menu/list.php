<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-6">
    <h2><i class="fa fa-list"></i> Menu List</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">Home</a>
            </li>
            <li class="breadcrumb-item">
                <span>Settings</span>
            </li>
            <li class="breadcrumb-item active">
                <strong>Menu List</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-6">
        <h2></h2>
        
        <div class="form-group pull-right">

        <a href="/settings/menu/create" class="btn btn-primary btn-sm" id="btn-add" ><i class="fa fa-plus"></i> Add Menu</a>
        <a href="/settings/submenu/create" class="btn btn-primary btn-sm" id="btn-add" ><i class="fa fa-plus"></i> Add Submenu</a>
        </div>
    </div>
</div>
<br/>
<div class="wrapper wrapper-content animated white-bg ecommerce">
    <div class="row">
        <div class="col-lg-12">
            <div class="table-responsive">
                <table id="menu" style="width: 100% !important; " class="table table-bordered table-hover" >
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>icon</th>
                            <th>Is Enabled</th>
                            <th>Sort</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody> </tbody>
                </table>  
            </div>        
        </div>
    </div>
</div>





<!-- <?#= $this->extend('admin/layouts/main') ?>
<?#= $this->section('content') ?>
<section class="content">
    <div class="row">
        <div class="col-12 mt-3">
            <div class="card">
                <?php #$session = \Config\Services::session(); ?>
                <?php #if(isset($session->success)) : ?>
                    <div class="alert alert-success text-center alert-dismissible fade mb-0" role="0">
                        <?php #echo $session->success;?>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                <?php #endif;?>
                <div class="card-header">
                    <h3 class="card-title">Menu List</h3>

                    <div class="card-tools">
                        <a href="/settings/menu_add" class="btn btn-primary btn-sm" id="btn-add" >Add Menu</a>
                    </div>
                </div>
                <div class="card-body table-responsive">
                    <table class="table table-bordered table-hover dataTable" id="menu-list">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>icon</th>
                                <th>Is Enabled</th>
                                <th>Sort</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<?#= $this->endSection() ?> -->


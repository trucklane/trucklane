<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-6">
    <h2><i class="fa fa-list"></i> Country List</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">Home</a>
            </li>
            <li class="breadcrumb-item">
                <span>Settings</span>
            </li>
            <li class="breadcrumb-item active">
                <strong>Country List</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-6">
        <h2></h2>
        <div class="form-group pull-right">
            <button type="button" class="btn btn-primary btn-sm" id="btn-add-country" ><i class="fa fa-plus"></i> Add Country</button>
        </div>
    </div>
</div>
<br/>
<div class="wrapper wrapper-content animated white-bg ecommerce">
    <div class="row">
        <div class="col-lg-12">
            <div class="table-responsive">
                <table id="country-list" style="width: 100% !important; " class="table table-bordered table-hover" >
                    <thead>
                        <tr>
                            <th>Country Code</th>
                            <th>Country Name</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody> </tbody>
                </table>  
            </div>        
        </div>
    </div>
</div>

<!-- modal -->

<div class="modal fade " id="country-form-modal" data-keyboard="false" data-backdrop="static" tabindex="-1"  aria-hidden="true" role="dialog" >
    <div class="modal-dialog">
        <div class="modal-content animated fadeIn">
            <div class="modal-header">
                <div class="col-lg-6 "><h3>Add/Edit Country</h3></div>
                <div class="col-lg-6 "> <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> </div>
            </div>
            <div class="modal-body">
                <form class="m-t" role="form" id="country-form" name="country-form" data-toggle="validator">
                    <div class="row">
                        <div class="col-lg-12 form-group">
                            <label for="inputName" class="control-label">Country Code</label>
                            <input type="text" class="form-control" id="country_code" name="country_code" placeholder="Country Code">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12 form-group">
                            <label for="inputName" class="control-label">Country Name</label>
                            <input type="text" class="form-control" id="country_name" name="country_name" placeholder="Country Name">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn-save-country">Submit</button>
            </div>
        </div>
    </div>
</div>

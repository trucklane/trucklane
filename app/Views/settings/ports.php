<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-6">
    <h2><i class="fa fa-list"></i> Port List</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">Home</a>
            </li>
            <li class="breadcrumb-item">
                <span>Settings</span>
            </li>
            <li class="breadcrumb-item active">
                <strong>Port List</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-6">
        <h2></h2>
        <div class="form-group pull-right">
            <button type="button" class="btn btn-primary btn-sm" id="btn-add-port" ><i class="fa fa-plus"></i> Add Port</button>
        </div>
    </div>
</div>
<br/>
<div class="wrapper wrapper-content animated white-bg ecommerce">
    <div class="row">
        <div class="col-lg-12">
            <div class="table-responsive">
                <table id="port-list" style="width: 100% !important; " class="table table-bordered table-hover" >
                    <thead>
                        <tr>
                            <th>Port Type</th>
                            <th>Port Code</th>
                            <th>Port Name</th>
                            <th>Country Code</th>
                            <th>Active</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php  
                        if ($ports) {
                            
                            foreach($ports as $port) {
                                echo "<tr>";
                                echo "<td>".strtoupper($port->port_type)."</td>";
                                echo "<td>{$port->port_code}</td>";
                                echo "<td>{$port->port_name}</td>";
                                echo "<td>{$port->port_country}</td>";
                                echo "<td>{$port->is_enabled}</td>";
                                echo "<td><button  data-id='{$port->port_id}' class='btn btn-primary btn-sm edit-port'><i class='fa fa-edit'></i> Edit</button> <button  data-id='{$port->port_id}'  class='btn btn-danger btn-sm del-port'><i class='fa fa-trash'></i> Delete</button></td>";
                                echo "</tr>";
                            }
                        }
                        ?>
                    </tbody>
                </table>  
            </div>        
        </div>
    </div>
</div>
<!-- modal -->

<div class="modal fade " id="port-form-modal" data-keyboard="false" data-backdrop="static" tabindex="-1"  aria-hidden="true" role="dialog" >
    <div class="modal-dialog">
        <div class="modal-content animated fadeIn">
            <div class="modal-header">
                <div class="col-lg-6 "><h3>Add/Edit Port</h3></div>
                <div class="col-lg-6 "> <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> </div>
            </div>
            <div class="modal-body">
                <form class="m-t" role="form" id="port-form" name="port-form" data-toggle="validator">
                    <div class="row">
                        <div class="col-lg-4 form-group">
                            <label for="inputName" class="control-label">Port Code</label>
                            <input type="text" class="form-control" id="port_code" name="port_code" placeholder="Port Code">
                            <input type="hidden" class="form-control" id="port_id" name="port_id" value="0" placeholder="Port id">
                        </div>

                        <div class="col-lg-8 form-group">
                            <label for="inputName" class="control-label">Port Name</label>
                            <input type="text" class="form-control" id="port_name" name="port_name" placeholder="Port Name">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-4 form-group">
                            <label for="inputName" class="control-label">Port Type</label>
                            <select name="port_type" id="port_type" class="form-control">
                                <option value=''>-- Select --</option>
                                <option value='air'>Airport</option>
                                <option value='sea'>Seaport</option>
                            </select>
                        </div>
                        <div class="col-lg-8 form-group">
                            <label for="inputName" class="control-label">Country</label>
                            <select id="port_country" name="port_country" class="form-control select2bs4">
                                <option value="">-- Select -- </option>
                                <?php 
                                    foreach($countries as $country) {
                                        $sel = (isset($port->port_country) && $port->port_country == $country->country_code) ? 'selected="selected"':'';

                                        echo "<option $sel value='{$country->country_code}'>{$country->country_name}</option>";
                                    }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-4 form-group">
                            <label for="inputName" class="control-label">Is Enabled</label>
                            <select name="is_enabled" id="is_enabled" class="form-control">
                                <option value=''>-- Select --</option>
                                <option value='1'>Enable</option>
                                <option value='0'>Disable</option>
                            </select>
                        </div>
                        <div class="col-lg-8 form-group">
                            <label for="inputName" class="control-label">IATA Area</label>
                            <input type="text" class="form-control" id="iata_area" name="iata_area" placeholder="IATA Area">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn-save-port">Submit</button>
            </div>
        </div>
    </div>
</div>

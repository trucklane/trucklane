<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-6">
    <h2><i class="fa fa-list"></i> Charges List</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">Home</a>
            </li>
            <li class="breadcrumb-item">
                <span>Settings</span>
            </li>
            <li class="breadcrumb-item active">
                <strong>Charges</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-6">
        <h2></h2>
        <div class="form-group pull-right">
            <button type="button" class="btn btn-primary btn-sm btn-add-charges" id="btn_add_charges" ><i class="fa fa-plus"></i> Add charge</button>
        </div>
    </div>
</div>

<div class="wrapper wrapper-content animated ">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <table class="table table-bordered table-hover dataTable" id="charges_list">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Charge code</th>
                                <th>Charge description</th>
                                <th>Report group</th>
                                <th>Is enabled</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if ($list): ?>
                                <?php foreach ($list as $charges): ?>
                                <tr>
                                    <td><?= $charges->id; ?></td>
                                    <td><?= $charges->charge_code; ?></td>
                                    <td><?= $charges->charge_description; ?></td>
                                    <td><?= $charges->report_group; ?> </td> 
                                    <td>
                                        <span class="badge badge-pill badge-<?= $charges->is_active == 1 ? 'success' : 'danger' ?>">
                                        <?= $charges->is_active == 1 ? 'True' : 'False' ?>
                                        </span>
                                    </td>
                                    <td>
                                        <button data-id="<?= $charges->id; ?>" class="btn-edit-charges btn btn-primary btn-sm"><i class="fa fa-edit"></i> Edit</button>
                                        <button data-id="<?= $charges->id; ?>" class="btn-del-charges btn btn-danger btn-sm"><i class="fa fa-trash"></i> Delete</button>
                                    </td>
                                </tr>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- modal -->

<div class="modal fade " id="charges-form-modal" data-keyboard="false" data-backdrop="static" tabindex="-1"  aria-hidden="true" role="dialog" >
    <div class="modal-dialog ">
        <div class="modal-content animated fadeIn">
            <div class="modal-header">
                <div class="col-lg-6 "><h3>Add charge</h3></div>
                <div class="col-lg-6 "> <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> </div>
            </div>
            <div class="modal-body">
                <form class="m-t" role="form" id="charges_form" name="charges-form" data-toggle="validator">
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="" class="control-label">Charge code</label>
                            <input type="text" class="form-control" id="charges_code" name="charges-code" placeholder="">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="" class="control-label">Charge Description</label>
                            <textarea class="form-control " name="charges-description" id="charges_description" rows="3"></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="" class="control-label">Report group</label>
                            <select class="form-control" name="charges-group">
                                <option value="PORT CHARGES">PORT CHARGES</option>
                                <option value="DELIVERY CHARGES">DELIVERY CHARGES</option>
                                <option value="BROKERAGE CHARGES">BROKERAGE CHARGES</option>
                                <option value="PICKUP CHARGES">PICKUP CHARGES</option>
                                <option value="ORIGIN CHARGES">ORIGIN CHARGES</option>
                                <option value="DESTINATION CHARGES">DESTINATION CHARGES</option>
                                <option value="SHIPPINGLINE CHARGES">SHIPPINGLINE CHARGES</option>
                                <option value="GOVERNMENT CHARGES">GOVERNMENT CHARGES</option>
                                <option value="CUSTOMWHSE CHARGES">CUSTOMWHSE CHARGES</option>
                                <option value="FORWARDING CHARGES">FORWARDING CHARGES</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-check">
                        <input type="checkbox" class="form-check-input" name="charges-enabled" id="charges_enabled">
                        <label class="form-check-label" for="">Is enabled</label>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn-save-charges">Submit</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade " id="edit-charges-form-modal" data-keyboard="false" data-backdrop="static" tabindex="-1"  aria-hidden="true" role="dialog" >
    <div class="modal-dialog ">
        <div class="modal-content animated fadeIn">
            <div class="modal-header">
                <div class="col-lg-6 "><h3>Edit charges</h3></div>
                <div class="col-lg-6 "> <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> </div>
            </div>
            <div class="modal-body">
                <form class="m-t" role="form" id="charges-form" name="edit-charges-form" data-toggle="validator">
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="" class="control-label">charges code</label>
                            <input type="text" class="form-control" id="edit_charges_code" name="edit-charges-code" placeholder="">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="" class="control-label">Charge Description</label>
                            <textarea class="form-control " name="edit-charges-description" id="edit_charges_description" rows="3"></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="" class="control-label">Report group</label>
                            <select class="form-control" name="edit-charges-group">
                                <option value="PORT CHARGES">PORT CHARGES</option>
                                <option value="DELIVERY CHARGES">DELIVERY CHARGES</option>
                                <option value="BROKERAGE CHARGES">BROKERAGE CHARGES</option>
                                <option value="PICKUP CHARGES">PICKUP CHARGES</option>
                                <option value="ORIGIN CHARGES">ORIGIN CHARGES</option>
                                <option value="DESTINATION CHARGES">DESTINATION CHARGES</option>
                                <option value="SHIPPINGLINE CHARGES">SHIPPINGLINE CHARGES</option>
                                <option value="GOVERNMENT CHARGES">GOVERNMENT CHARGES</option>
                                <option value="CUSTOMWHSE CHARGES">CUSTOMWHSE CHARGES</option>
                                <option value="FORWARDING CHARGES">FORWARDING CHARGES</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-check">
                        <input type="checkbox" class="form-check-input" name="edit-charges-enabled" id="edit_charges_enabled">
                        <label class="form-check-label" for="">Is enabled</label>
                    </div>
                    <input type="hidden" name="edit-charges-id" />
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn-save-edit-charges">Save</button>
            </div>
        </div>
    </div>
</div>
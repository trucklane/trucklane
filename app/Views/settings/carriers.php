<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-6">
    <h2><i class="fa fa-list"></i> Carrier List</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">Home</a>
            </li>
            <li class="breadcrumb-item">
                <span>Settings</span>
            </li>
            <li class="breadcrumb-item active">
                <strong>Carriers</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-6">
        <h2></h2>
        <div class="form-group pull-right">
            <button type="button" class="btn btn-primary btn-sm btn-add-carrier" id="btn_add_carrier" ><i class="fa fa-plus"></i> Add carrier</button>
        </div>
    </div>
</div>

<div class="wrapper wrapper-content animated ">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <table class="table table-bordered table-hover dataTable" id="carrier_list">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Carrier Code</th>
                                <th>Name</th>
                                <th>Type</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if ($list): ?>
                                <?php foreach ($list as $carrier): ?>
                                <tr>
                                    <td><?= $carrier->id; ?></td>
                                    <td><?= $carrier->carrier_code; ?></td>
                                    <td><?= $carrier->carrier_name; ?></td>
                                    <td><?= strtoupper($carrier->carrier_type); ?> </td> 
                                    <td>
                                        <button data-id="<?= $carrier->id; ?>" class="btn-edit-carrier btn btn-primary btn-sm"><i class="fa fa-edit"></i> Edit</button>
                                        <button data-id="<?= $carrier->id; ?>" class="btn-del-carrier btn btn-danger btn-sm"><i class="fa fa-trash"></i> Delete</button>
                                    </td>
                                </tr>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- modal -->

<div class="modal fade " id="carrier-form-modal" data-keyboard="false" data-backdrop="static" tabindex="-1"  aria-hidden="true" role="dialog" >
    <div class="modal-dialog ">
        <div class="modal-content animated fadeIn">
            <div class="modal-header">
                <div class="col-lg-6 "><h3>Add Carrier</h3></div>
                <div class="col-lg-6 "> <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> </div>
            </div>
            <div class="modal-body">
                <form class="m-t" role="form" id="carrier_form" name="carrier-form" data-toggle="validator">
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="" class="control-label">Carrier code</label>
                            <input type="text" class="form-control" id="carrier_code" name="carrier-code" placeholder="">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="" class="control-label">Carrier name</label>
                            <input type="text" class="form-control" id="carrier_name" name="carrier-name" placeholder="">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="" class="control-label">Carrier name</label>
                            <select class="form-control" name="carrier-type">
                                <option value="air">AIR</option>
                                <option value="sea">SEA</option>
                            </select>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn-save-carrier">Submit</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade " id="edit-carrier-form-modal" data-keyboard="false" data-backdrop="static" tabindex="-1"  aria-hidden="true" role="dialog" >
    <div class="modal-dialog ">
        <div class="modal-content animated fadeIn">
            <div class="modal-header">
                <div class="col-lg-6 "><h3>Edit Carrier</h3></div>
                <div class="col-lg-6 "> <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> </div>
            </div>
            <div class="modal-body">
                <form class="m-t" role="form" id="carrier-form" name="edit-carrier-form" data-toggle="validator">
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="" class="control-label">Carrier code</label>
                            <input type="text" class="form-control" id="edit_carrier_code" name="edit-carrier-code" placeholder="">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="" class="control-label">Carrier name</label>
                            <input type="text" class="form-control" id="edit_carrier_name" name="edit-carrier-name" placeholder="">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="" class="control-label">Carrier type</label>
                            <select class="form-control" name="edit-carrier-type">
                                <option value="air">AIR</option>
                                <option value="sea">SEA</option>
                            </select>
                        </div>
                    </div>
                    <input type="hidden" name="edit-carrier-id" />
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn-save-edit-carrier">Save</button>
            </div>
        </div>
    </div>
</div>
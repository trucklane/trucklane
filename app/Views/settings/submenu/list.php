<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-6">
        <h2><i class="fa fa-bars"></i> Menu Setting</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="index.html">Home</a>
            </li>
            <li class="breadcrumb-item">
                <a>Setting</a>
            </li>
            <li class="breadcrumb-item active">
                <strong>Manage Sub Menu</strong>
            </li>
        </ol>
        
    </div>
    <div class="col-lg-6">
        <h2></h2>
        <div class="form-group pull-right">
            <button class="btn btn-primary btn-add-main-menu" type="button">Add Main Menu</button>
            <button class="btn btn-primary btn-add-sub-menu" type="button">Add Sub Menu</button>
        </div>
    </div>
</div>

<div class="wrapper wrapper-content animated  ecommerce">

    <div class="row">
        <div class="col-lg-12">
            <div class="tabs-container">
                <div class="tab-content">
                    <div id="tab-1" class="tab-pane active">
                        <div class="panel-body">
                            
                            <div class="table-responsive">
                                <table id="submenu" style="width: 100% !important;"  class="table table-striped table-bordered table-hover dataTables-example" >
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Parent Menu</th>
                                            <th>Menu Name</th>
                                            <th>URL</th>
                                            <th>Icon</th>
                                            <th>Enabled</th>
                                            <th>Order</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody> </tbody>
                                </table>  
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
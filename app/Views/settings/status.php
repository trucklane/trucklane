<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-6">
    <h2><i class="fa fa-list"></i> Status List</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">Home</a>
            </li>
            <li class="breadcrumb-item">
                <span>Settings</span>
            </li>
            <li class="breadcrumb-item active">
                <strong>Status List</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-6">
        <h2></h2>
        <div class="form-group pull-right">
            <button type="button" class="btn btn-primary btn-sm btn-add-status" id="btn-add-status" ><i class="fa fa-plus"></i> Add status</button>
            
        </div>
    </div>
</div>

<div class="wrapper wrapper-content animated ">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <table class="table table-bordered table-hover dataTable" id="status-list">
                        <thead>
                            <tr>
                                <th>Status Code</th>
                                <th>Status Name</th>
                                <th>Status Description</th>
                                <th>Customer Visibility</th>
                                <th>Active</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if ($status_list): ?>
                                <?php foreach ($status_list as $status): ?>
                                <tr>
                                    <td><?= $status->status_code; ?></td>
                                    <td><?= $status->status_name; ?></td>
                                    <td><?= $status->status_description; ?></td>
                                    <td>
                                        <span class="badge badge-pill badge-<?= $status->status_visibility == 1 ? 'success' : 'danger' ?>">
                                        <?= $status->status_visibility == 1 ? 'True' : 'False' ?>
                                        </span>
                                    </td>
                                    <td>
                                        <span class="badge badge-pill badge-<?= $status->is_enabled == 1 ? 'success' : 'danger' ?>">
                                        <?= $status->is_enabled == 1 ? 'True' : 'False' ?>
                                        </span>
                                    </td> 
                                    <td>
                                        <button data-id="<?= $status->id; ?>" class="btn-edit-status btn btn-primary btn-sm"><i class="fa fa-edit"></i> Edit</button>
                                        <button data-id="<?= $status->id; ?>" class="btn-del-status btn btn-danger btn-sm"><i class="fa fa-trash"></i> Delete</button>
                                    </td>
                                </tr>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- modal -->

<div class="modal fade " id="status-form-modal" data-keyboard="false" data-backdrop="static" tabindex="-1"  aria-hidden="true" role="dialog" >
    <div class="modal-dialog ">
        <div class="modal-content animated fadeIn">
            <div class="modal-header">
                <div class="col-lg-6 "><h3>Add Status</h3></div>
                <div class="col-lg-6 "> <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> </div>
            </div>
            <div class="modal-body">
                <form class="m-t" role="form" id="status-form" name="status-form" data-toggle="validator">
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="inputName" class="control-label">Status Name</label>
                            <input type="text" class="form-control" id="status_name" name="status_name" placeholder="Status Name">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="inputName" class="control-label">Status Description</label>
                            <textarea class="form-control " name="status_desc" id="status_desc" rows="3"></textarea>
                        </div>
                    </div>
                    <div class="form-check">
                        <input type="checkbox" class="form-check-input" name="status_visibility" id="status_visibility">
                        <label class="form-check-label" for="exampleCheck1">Customer Visibility</label>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn-save-status">Submit</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade " id="edit-status-form-modal" data-keyboard="false" data-backdrop="static" tabindex="-1"  aria-hidden="true" role="dialog" >
    <div class="modal-dialog ">
        <div class="modal-content animated fadeIn">
            <div class="modal-header">
                <div class="col-lg-6 "><h3>Edit Status</h3></div>
                <div class="col-lg-6 "> <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> </div>
            </div>
            <div class="modal-body">
                <form class="m-t" role="form" id="status-form" name="edit-status-form" data-toggle="validator">
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="inputName" class="control-label">Status Name</label>
                            <input type="text" class="form-control" id="status_name" name="edit_status_name" placeholder="Status Name">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="inputName" class="control-label">Status Description</label>
                            <textarea class="form-control " name="edit_status_desc" id="status_desc" rows="3"></textarea>
                        </div>
                    </div>
                    <div class="form-check">
                        <input type="checkbox" class="form-check-input" name="edit_status_visibility" id="status_visibility">
                        <label class="form-check-label" for="exampleCheck1">Customer Visibility</label>
                    </div>
                    <div class="form-check">
                        <input type="checkbox" class="form-check-input" name="edit_status_enabled" id="edit_status_enabled">
                        <label class="form-check-label" for="exampleCheck1">Is enabled?</label>
                    </div>
                    <input type="hidden" name="edit_status_id" />
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn-save-edit-status">Save</button>
            </div>
        </div>
    </div>
</div>
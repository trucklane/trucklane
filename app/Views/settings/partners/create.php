<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-6">
    <h2><i class="fa fa-list"></i> Add/Edit Partners</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">Home</a>
            </li>
            <li class="breadcrumb-item">
                <span>Settings</span>
            </li>
            <li class="breadcrumb-item active">
                <strong>Add/Edit Partners</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-6">
        <h2></h2>
        <div class="form-group pull-right">
            
        </div>
    </div>
</div>
<div class="wrapper wrapper-content animated ">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <form action="/settings/partner_add" id="partner-form" class="needs-validation"  method="post" novalidate>
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-md-3 col-sm-6 ">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Partner Type</label>
                                    <select id="partner_type" name="partner_type" class="form-control select2">
                                        <option value="">-- Select -- </option>
                                        <option <?=(isset($partner->partner_type) && $partner->partner_type == 'A') ? 'selected="selected"':''?> value="A">Agent</option>
                                        <option <?=(isset($partner->partner_type) && $partner->partner_type == 'C' ) ? 'selected="selected"':''?> value="C">Consignee</option>
                                        <option <?=(isset($partner->partner_type) && $partner->partner_type == 'S' ) ? 'selected="selected"':''?> value="S">Shipper</option>
                                        <option <?=(isset($partner->partner_type) && $partner->partner_type == 'X' ) ? 'selected="selected"':''?> value="X">Customer</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-3 col-sm-6">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Partner Code</label>
                                    <input type="text" class="form-control" id="partner_code" name="partner_code"  value="<?= isset($partner) ? $partner->partner_code : set_value('partner_code') ?>" placeholder="Partner Code">
                                    <input type="hidden" class="form-control" id="partner_id" name="partner_id"  value="<?= isset($partner) ? $partner->id : set_value('partner_id') ?>" placeholder="Partner Id">
                                </div>
                            </div>

                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Partner Name</label>
                                    <input type="text" class="form-control" id="partner_name" name="partner_name"  value="<?= isset($partner) ? $partner->partner_name : set_value('partner_name') ?>" placeholder="Partner Name">
                                </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-6 col-sm-6 ">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Complete Address <small></small></label>
                                    <input type="text" class="form-control" id="address1" name="address1"  value="<?= isset($partner) ? $partner->address1 : set_value('address1') ?>" placeholder="Complete address">
                                </div>
                            </div>

                            <div class="col-md-6 col-sm-6">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Country</small></label>
                                    <select id="country_code" name="country_code" class="form-control select2">
                                        <option value="">-- Select -- </option>
                                        <?php 
                                            foreach($countries as $country) {
                                                $sel = (isset($partner->country_code) && $partner->country_code == $country->country_code) ? 'selected="selected"':'';

                                                echo "<option $sel value='{$country->country_code}'>{$country->country_name}</option>";
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>

                        </div>

                        <h4 class="mt-3">Contact Details</h4>
                        <hr/>

                        <div class="list_wrapper">  
                            <div class="row">
                                <div class="col-xs-6 col-sm-6 col-md-3">
                                    <div class="form-group">
                                        Contact Person
                                        <input name="contact_name[]" id="contact_name" type="text" placeholder="Contact Name" value="<?= isset($contact_list[0]) ? $contact_list[0]->contact_name : '' ?>" class="form-control"/>
                                    </div>
                                </div>

                                <div class="col-xs-6 col-sm-6 col-md-3">
                                    <div class="form-group">
                                        Email Address
                                        <input autocomplete="off" name="email_address[]" type="text" placeholder="Email Address"  value="<?= isset($contact_list[0]) ? $contact_list[0]->email_address : '' ?>" class="form-control"/>
                                    </div>
                                </div> 

                                <div class="col-xs-6 col-sm-6 col-md-3">
                                    <div class="form-group">
                                        Contact Number
                                        <input autocomplete="off" name="contact_no[]" type="text" placeholder="Contact No"  value="<?= isset($contact_list[0]) ? $contact_list[0]->contact_number : '' ?>" class="form-control"/>
                                    </div>
                                </div> 

                                <div class="col-xs-6 col-sm-6 col-md-2">
                                    <div class="form-group">
                                        Position
                                        <input autocomplete="off" name="position[]" type="text" placeholder="Position" value="<?= isset($contact_list[0]) ? $contact_list[0]->position : '' ?>"  class="form-control"/>
                                    </div>
                                </div> 
                                <div class="col-xs-1 col-sm-1  col-md-1">
                                    <br>
                                    <button class="btn btn-primary list_add_button" type="button">+</button>
                                </div>
                            </div>
                            
                            <?php if($contact_list) : ?>

                            <?php for($x=1; $x<count($contact_list); $x++) : ?>

                            <div class="row">
                                <div class="col-xs-6 col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <input name="contact_name[]" id="contact_name" type="text" value="<?=$contact_list[$x]->contact_name?>"  placeholder="Contact Name" class="form-control"/>
                                    </div>
                                </div>

                                <div class="col-xs-6 col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <input autocomplete="off" name="email_address[]" type="text" value="<?=$contact_list[$x]->email_address?>" placeholder="Email Address" class="form-control"/>
                                    </div>
                                </div> 

                                <div class="col-xs-6 col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <input autocomplete="off" name="contact_no[]" type="text" value="<?=$contact_list[$x]->contact_number?>" placeholder="Contact No" class="form-control"/>
                                    </div>
                                </div> 

                                <div class="col-xs-6 col-sm-6 col-md-2">
                                    <div class="form-group">
                                        <input autocomplete="off" name="position[]" type="text" value="<?=$contact_list[$x]->position?>" placeholder="Position" class="form-control"/>
                                    </div>
                                </div> 
                                <div class="col-xs-1 col-sm-1  col-md-1">
                                    <a href="javascript:void(0);" class="list_remove_button btn btn-danger">-</a>
                                </div>
                            </div>
                            <?php endfor;?>
                        </div>

                        <?php endif;?>

                        <button type="submit" class="btn btn-primary">Submit</button>
                        <a href="<?=BASE?>settings/partners" class="btn btn-default">Back</a>
                    </div>
                </form>
            </div>            
        </div>
    </div>
</div>
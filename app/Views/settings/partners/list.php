<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-6">
    <h2><i class="fa fa-list"></i> Partner List</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">Home</a>
            </li>
            <li class="breadcrumb-item">
                <span>Settings</span>
            </li>
            <li class="breadcrumb-item active">
                <strong>Partner List</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-6">
        <h2></h2>
        
        <div class="form-group pull-right">
            <a href="/settings/partner_add" class="btn btn-primary btn-sm" id="btn-add" ><i class="fa fa-plus"></i> Add Partners</a>
        </div>
    </div>
</div>
<br/>
<div class="wrapper wrapper-content animated white-bg ecommerce">
    <div class="row">
        <div class="col-lg-12">
            <div class="table-responsive">
                <table id="partner-list" style="width: 100% !important; " class="table table-bordered table-hover" >
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Partner Code</th>
                            <th>Partner Name</th>
                            <th>Partner Type</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody> </tbody>
                </table>  
            </div>        
        </div>
    </div>
</div>

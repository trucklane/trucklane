<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-6">
    <h2><i class="fa fa-list"></i> Unit of mearsure List</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">Home</a>
            </li>
            <li class="breadcrumb-item">
                <span>Settings</span>
            </li>
            <li class="breadcrumb-item active">
                <strong>Unit of measures</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-6">
        <h2></h2>
        <div class="form-group pull-right">
            <button type="button" class="btn btn-primary btn-sm btn-add-unit" id="btn_add_unit" ><i class="fa fa-plus"></i> Add unit of measure</button>
            
        </div>
    </div>
</div>

<div class="wrapper wrapper-content animated ">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <table class="table table-bordered table-hover dataTable" id="unit-list">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Code</th>
                                <th>Description</th>
                                <th>Active</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if ($list): ?>
                                <?php foreach ($list as $unit): ?>
                                <tr>
                                    <td><?= $unit->id; ?></td>
                                    <td><?= $unit->unit_code; ?></td>
                                    <td><?= $unit->unit_description; ?></td>
                                    <td>
                                        <span class="badge badge-pill badge-<?= $unit->is_active == 1 ? 'success' : 'danger' ?>">
                                        <?= $unit->is_active == 1 ? 'True' : 'False' ?>
                                        </span>
                                    </td> 
                                    <td>
                                        <button data-id="<?= $unit->id; ?>" class="btn-edit-unit btn btn-primary btn-sm"><i class="fa fa-edit"></i> Edit</button>
                                        <button data-id="<?= $unit->id; ?>" class="btn-del-unit btn btn-danger btn-sm"><i class="fa fa-trash"></i> Delete</button>
                                    </td>
                                </tr>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- modal -->

<div class="modal fade " id="unit-form-modal" data-keyboard="false" data-backdrop="static" tabindex="-1"  aria-hidden="true" role="dialog" >
    <div class="modal-dialog ">
        <div class="modal-content animated fadeIn">
            <div class="modal-header">
                <div class="col-lg-6 "><h3>Add Unit of measure</h3></div>
                <div class="col-lg-6 "> <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> </div>
            </div>
            <div class="modal-body">
                <form class="m-t" role="form" id="unit_form" name="unit-form" data-toggle="validator">
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="" class="control-label">Unit</label>
                            <input type="text" class="form-control" id="unit_code" name="unit-code" placeholder="">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="" class="control-label">Description</label>
                            <textarea class="form-control " name="unit-desc" id="unit_desc" rows="3"></textarea>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn-save-unit">Submit</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade " id="edit-unit-form-modal" data-keyboard="false" data-backdrop="static" tabindex="-1"  aria-hidden="true" role="dialog" >
    <div class="modal-dialog ">
        <div class="modal-content animated fadeIn">
            <div class="modal-header">
                <div class="col-lg-6 "><h3>Edit unit of measure</h3></div>
                <div class="col-lg-6 "> <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> </div>
            </div>
            <div class="modal-body">
                <form class="m-t" role="form" id="status-form" name="edit-unit-form" data-toggle="validator">
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="" class="control-label">Unit of measure code</label>
                            <input type="text" class="form-control" id="" name="edit-unit-code" placeholder="">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="" class="control-label">Unit of measure description</label>
                            <textarea class="form-control " name="edit-unit-desc" id="" rows="3"></textarea>
                        </div>
                    </div>
                    <div class="form-check">
                        <input type="checkbox" class="form-check-input" name="edit-unit-enabled" id="">
                        <label class="form-check-label" for="exampleCheck1">Is enabled?</label>
                    </div>
                    <input type="hidden" name="edit-unit-id" />
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn-save-edit-unit">Save</button>
            </div>
        </div>
    </div>
</div>
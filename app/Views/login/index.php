<?= $this->extend('admin/layouts/login') ?>
<?= $this->section('content') ?>
<div class="login-box">
    <!-- /.login-logo -->
    <div class="card">
        <div class="card-body login-card-body">
            <div class="login-logo">
                <img src="<?=BASE?>admin/assets/img/trucklane_logo.png" class="" alt="Company Logo">
            </div>
            <p class="login-box-msg">Sign in to start your session</p>

            <?php if (session()->get('success')): ?>
                <div class="alert alert-success" role="alert">
                    <?= session()->get('success') ?>
                </div>
            <?php endif; ?>

            <form action="<?=BASE?>login" method="post">
                <div class="input-group mb-3">
                    <input type="text" class="form-control" name="email" id="email" placeholder="Email/Username" value="<?= set_value('email') ?>">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-envelope"></span>
                        </div>
                    </div>
                </div>
                <div class="input-group mb-3">
                    <input type="password" class="form-control" name="password" id="password" placeholder="Password">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-lock"></span>
                        </div>
                    </div>
                </div>

                <?php if (isset($validation)): ?>
                    <div class="col-12">
                        <div class="alert alert-danger" role="alert">
                            <?= $validation->listErrors() ?>
                        </div>
                    </div>
                <?php endif; ?>

                <div class="row">
                    <div class="col-8">
                        <a href="#">I forgot my password</a>
                    </div>
                    <!-- /.col -->
                    <div class="col-4">
                        <button type="submit" class="btn btn-primary btn-block">Submit</button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>
        </div>
        <!-- /.login-card-body -->
    </div>
    <p class="text-center font-italic"><u><a href="<?=BASE?>tracking" >Tracking</a></u></p>
</div>
<!-- /.login-box -->
<?= $this->endSection() ?>

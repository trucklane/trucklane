<div class="wrapper wrapper-content animated ">
    <div class="row">

        <div class="col-lg-6">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>Top 5 Customers Per Service </h5>
                </div>
                <div class="ibox-content">
                    <div>
                        <canvas id="barChart" height="162"></canvas>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="col-lg-6 d-none">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>Area Chart Example <small>With custom colors.</small></h5>
                    
                </div>
                <div class="ibox-content" style="position: relative">
                    <div id="morris-bar1-chart"></div>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>Total Transaction Per Service</h5>
                </div>
                <div class="ibox-content">
                    <div id="morris-bar-chart"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>Line Chart Example </h5>
                    
                </div>
                <div class="ibox-content">
                    <div id="morris-line-chart"></div>
                </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>Transactions Per Customer </h5>

                </div>
                <div class="ibox-content">
                    <div>
                        <div id="pie"></div>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- <div class="col-lg-6">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>Donut Chart Example</h5>
                    
                </div>
                <div class="ibox-content">
                    <div id="morris-donut-chart" ></div>
                </div>
            </div>
        </div> -->
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>Simple one line Example </h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#" class="dropdown-item">Config option 1</a>
                            </li>
                            <li><a href="#" class="dropdown-item">Config option 2</a>
                            </li>
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div id="morris-one-line-chart"></div>
                </div>
            </div>
        </div>
    </div>
</div>